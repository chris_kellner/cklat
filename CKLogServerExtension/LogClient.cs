﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface;
using CKLogExtension.Messages;
using CKLib.Utilities.DataStructures;
using CKLib.Utilities.Connection;

namespace CKLogExtension.Server
{
    public class LogClient
    {
        #region Variables
        /// <summary>
        /// Reference to the owning plugin.
        /// </summary>
        private ServerLog4NetPlugin m_Plugin;

        /// <summary>
        /// Id of the client.
        /// </summary>
        private Guid m_ClientId;

        /// <summary>
        /// Collection of all messages received from the client.
        /// </summary>
        private SynchronizedObservableCollection<LogMessage> m_LogTable;

        /// <summary>
        /// The client's remote settings
        /// </summary>
        private ILoggerSettings m_ClientSettings;

        /// <summary>
        /// Information about the connection.
        /// </summary>
        private IConnectionInformation m_ConnectionInfo;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the LogClient class.
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="clientId"></param>
        public LogClient(ServerLog4NetPlugin plugin, Guid clientId)
        {
            m_Plugin = plugin;
            m_LogTable = new SynchronizedObservableCollection<LogMessage>(plugin.Dispatcher);
            m_ClientId = clientId;
            foreach (var share in plugin.Messenger.SharedObjectPool.GetObjectsSharedByOwner(clientId))
            {
                if (share is ILoggerSettings)
                {
                    m_ClientSettings = (ILoggerSettings)share;
                    break;
                }
            }

            if (m_Plugin.Messenger != null)
            {
                m_ConnectionInfo = m_Plugin.Messenger.GetConnectionInformation(m_ClientId);
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Adds a message to the set of messages from this client.
        /// </summary>
        /// <param name="message"></param>
        public void ReceiveMessage(LogMessage message)
        {
            m_LogTable.Add(message);
        }

        /// <summary>
        /// Performs log rolling operations described in the configuration.
        /// </summary>
        public void RollLog()
        {
            switch (LogConfiguration.Instance.LogRollMode)
            {
                case LogRollMode.Off:
                    return;
                case LogRollMode.MessageCount:
                    OnLogRollStarted();
                    int msgCount = LogConfiguration.Instance.MaximumLogMessages;
                    while (m_LogTable.Count > msgCount)
                    {
                        m_LogTable.RemoveAt(0);
                    }
                    OnLogRollCompleted();
                    break;
                case LogRollMode.MessageAge:
                    OnLogRollStarted();
                    int msgAgeMinutes = LogConfiguration.Instance.MaximumMessageAgeMinutes;
                    DateTime minTime = DateTime.Now.Subtract(TimeSpan.FromMinutes(msgAgeMinutes));
                    while (m_LogTable[0].TimeStamp < minTime)
                    {
                        m_LogTable.RemoveAt(0);
                    }
                    OnLogRollCompleted();
                    break;
            }
        }

        /// <summary>
        /// Raises the LogRollStarted event.
        /// </summary>
        private void OnLogRollStarted()
        {
            if (LogRollStarted != null)
            {
                LogRollStarted(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the LogRollCompleted event.
        /// </summary>
        private void OnLogRollCompleted()
        {
            if (LogRollCompleted != null)
            {
                LogRollCompleted(this, EventArgs.Empty);
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the id of the client.
        /// </summary>
        public Guid ClientId
        {
            get
            {
                return m_ClientId;
            }
        }

        public IConnectionInformation ConnectionInformation
        {
            get
            {
                return m_ConnectionInfo;
            }

            set
            {
                if (m_ConnectionInfo != null)
                {
                    m_ConnectionInfo.Update(value);
                }
                else
                {
                    m_ConnectionInfo = value;
                }
            }
        }

        public ILoggerSettings LogSettings
        {
            get
            {
                if (m_ClientSettings == null)
                {
                    foreach (var share in m_Plugin.Messenger.SharedObjectPool.GetObjectsSharedByOwner(m_ClientId))
                    {
                        if (share is ILoggerSettings)
                        {
                            m_ClientSettings = (ILoggerSettings)share;
                            break;
                        }
                    }
                }

                return m_ClientSettings;
            }
        }

        public IList<LogMessage> LogTable
        {
            get
            {
                return m_LogTable;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Called when log rolling has begun.
        /// </summary>
        public event EventHandler LogRollStarted;

        /// <summary>
        /// Called when log rolling has completed.
        /// </summary>
        public event EventHandler LogRollCompleted;
        #endregion
    }
}
