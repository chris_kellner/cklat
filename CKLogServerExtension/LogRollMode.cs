﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension
{
    public enum LogRollMode
    {
        /// <summary>
        /// Logg rolling is disabled.
        /// </summary>
        Off = 0,

        /// <summary>
        /// Only a certain number of messages will be saved in memory.
        /// </summary>
        MessageCount,

        /// <summary>
        /// Messages will only be saved in memory for so long.
        /// </summary>
        MessageAge
    }
}
