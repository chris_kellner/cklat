﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.DataStructures.Configuration;
using CKLib.Utilities.Patterns;
using System.ComponentModel;
using CKLogExtension.Server;

namespace CKLogExtension
{
    public class LogConfiguration : INotifyPropertyChanged, IDisposable
    {
        #region Constants
        public const string PART_ASSEMBLY_FILE = "%ASSEMBLYPATH%";
        public const string PART_ASSEMBLY = "%ASSEMBLYNAME%";
        public const string PART_VERSION = "%VERSION%";
        public const string PART_PUBLIC_KEY = "%KEY%";
        public const string PART_CULTURE = "%CULTURE%";
        public const string PART_TYPE = "%TYPE%";
        public const string PART_METHOD = "%METHOD%";
        public const string PART_METHOD_PARAMS = "%PARAMS%";

        /// <summary>
        /// The name of the setting group for this extension.
        /// </summary>
        public const string LOG_CONFIGURATION_GROUP_NAME = "CKLogExtension";

        /// <summary>
        /// The command line arguments used by redgate .Net reflector.
        /// </summary>
        public static readonly string DECOMPILER_REDGATE = string.Format("/select:\"code://{0}/{1}/{2}\" \"{3}\"", PART_ASSEMBLY, PART_TYPE, PART_METHOD, PART_ASSEMBLY_FILE);

        /// <summary>
        /// The command line arguments used by ILSpy.
        /// </summary>
        public static readonly string DECOMPILER_ILSPY = string.Format("\"{0}\" /navigateTo:M:{1}.{2}", PART_ASSEMBLY_FILE, PART_TYPE, PART_METHOD);

        /// <summary>
        /// The command line arguments used by ILdasm.
        /// </summary>
        public static readonly string DECOMPILER_ILDASM = string.Format("/item:\"{0}::{1}\" \"{2}\"", PART_TYPE, PART_METHOD, PART_ASSEMBLY_FILE);
        #endregion
        #region Variables
        /// <summary>
        /// The setting group for the logger settings.
        /// </summary>
        private SettingGroup m_SettingGroup;

        /// <summary>
        /// Help string for the decompiler command line interface.
        /// </summary>
        private string m_HelpString;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes static members of the LogConfiguration class.
        /// </summary>
        static LogConfiguration()
        {
            Singleton<LogConfiguration>.ValueFactory = () => new LogConfiguration();
        }

        /// <summary>
        /// Prevents extenal instantiation of the LogConfiguration class.
        /// </summary>
        private LogConfiguration()
        {
            m_SettingGroup = ConfigurationManager.Current[LOG_CONFIGURATION_GROUP_NAME, true];
            // Ensure the proper Name
            m_SettingGroup.ComponentName = "CKLogExtension";
            m_SettingGroup.PropertyChanged += m_SettingGroup_PropertyChanged;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Called when a setting changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_SettingGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of the LogConfiguration class.
        /// </summary>
        public static LogConfiguration Instance
        {
            get
            {
                return Singleton<LogConfiguration>.Instance;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use the row styles or not.
        /// </summary>
        public bool EnableRowStyles
        {
            get
            {
                bool val;
                if (m_SettingGroup.TryReadValue("EnableRowStyles", out val))
                {
                    return val;
                }

                return true;
            }

            set
            {
                m_SettingGroup.SetValue("EnableRowStyles", value);
            }
        }

        /// <summary>
        /// Gets or sets the style used for rolling the log.
        /// </summary>
        public LogRollMode LogRollMode
        {
            get
            {
                int value;
                if (m_SettingGroup.TryReadValue("LogRollMode", out value))
                {
                    return (LogRollMode)value;
                }

                return CKLogExtension.LogRollMode.Off;
            }

            set
            {
                m_SettingGroup.SetValue("LogRollMode", (int)value);
            }
        }

        /// <summary>
        /// Gets an array of log roll modes.
        /// </summary>
        public Array LogRollModes
        {
            get
            {
                return Enum.GetValues(typeof(LogRollMode));
            }
        }

        /// <summary>
        /// Gets or sets the number of messages to hold in memory before rolling old messages out.
        /// </summary>
        public int MaximumLogMessages
        {
            get
            {
                int rollCount;
                if (m_SettingGroup.TryReadValue("MaximumLogMessages", out rollCount))
                {
                    if (rollCount > 1)
                    {
                        return rollCount;
                    }

                    return 1;
                }

                return 500;
            }

            set
            {
                m_SettingGroup.SetValue("MaximumLogMessages", value);
            }
        }

        /// <summary>
        /// Gets or sets the number of minutes to hold messages in memory before rolling old messages out.
        /// </summary>
        public int MaximumMessageAgeMinutes
        {
            get
            {
                int rollCount;
                if (m_SettingGroup.TryReadValue("MaximumMessageAgeMinutes", out rollCount))
                {
                    if (rollCount > 1)
                    {
                        return rollCount;
                    }

                    return 1;
                }

                return 5;
            }

            set
            {
                m_SettingGroup.SetValue("MaximumMessageAgeMinutes", value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether inactive logs should be automatically removed.
        /// </summary>
        public bool AutomaticallyClearOldLogs
        {
            get
            {
                bool autoClear;
                if (m_SettingGroup.TryReadValue("AutomaticallyClearOldLogs", out autoClear))
                {
                    return autoClear;
                }

                return true;
            }

            set
            {
                m_SettingGroup.SetValue("AutomaticallyClearOldLogs", value);
            }
        }

        /// <summary>
        /// Gets the number of minutes to keep logs in the list once they go inactive (if auto clear is enabled).
        /// </summary>
        public int OldLogRemovalDelayMinutes
        {
            get
            {
                int delay;
                if (m_SettingGroup.TryReadValue("OldLogRemovalDelayMinutes", out delay))
                {
                    if (delay > 1)
                    {
                        return delay;
                    }

                    return 1;
                }

                return 10;
            }

            set
            {
                m_SettingGroup.SetValue("OldLogRemovalDelayMinutes", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the log should scroll when new messages are received.
        /// </summary>
        public bool AutoScrollOnNew
        {
            get
            {
                bool autoScroll;
                if (m_SettingGroup.TryReadValue("AutoScrollOnNew", out autoScroll))
                {
                    return autoScroll;
                }

                return true;
            }

            set
            {
                m_SettingGroup.SetValue("AutoScrollOnNew", value);
            }
        }

        /// <summary>
        /// Gets the log filters.
        /// </summary>
        public LogFilters DisplayFilters
        {
            get
            {
                LogFilters filters;
                if (m_SettingGroup.TryReadValue("DisplayFilters", out filters))
                {
                    return filters;
                }
                else
                {
                    filters = new LogFilters();
                    m_SettingGroup.SetValue("DisplayFilters", filters);
                    return filters;
                }
            }
        }

        /// <summary>
        /// Gets or sets the path to the decompiler.
        /// </summary>
        public string DecompilerPath
        {
            get
            {
                string path;
                if (m_SettingGroup.TryReadValue("DecompilerPath", out path))
                {
                    return path;
                }

                return string.Empty;
            }

            set
            {
                m_SettingGroup.SetValue("DecompilerPath", value);
            }
        }

        /// <summary>
        /// Gets or sets the command line arguments for the decompiler.
        /// </summary>
        public string DecompilerCommandLine
        {
            get
            {
                string path;
                if (m_SettingGroup.TryReadValue("DecompilerCommandLine", out path))
                {
                    return path;
                }

                return string.Empty;
            }

            set
            {
                m_SettingGroup.SetValue("DecompilerCommandLine", value);
            }
        }

        /// <summary>
        /// Gets a help string to tell the user what tokens are available for the decompiler.
        /// </summary>
        public string DecompilerHelpString
        {
            get
            {
                if (string.IsNullOrEmpty(m_HelpString))
                {
                    StringBuilder builder = new StringBuilder("Available Tokens are: " + Environment.NewLine);
                    builder.AppendLine(string.Format("{0} -> The Name of the Assembly", PART_ASSEMBLY));
                    builder.AppendLine(string.Format("{0} -> The Path to the Assembly", PART_ASSEMBLY_FILE));
                    builder.AppendLine(string.Format("{0} -> The Culture information of the Assembly", PART_CULTURE));
                    builder.AppendLine(string.Format("{0} -> The Version number of the Assembly", PART_VERSION));
                    builder.AppendLine(string.Format("{0} -> The Public Key Token of the Assembly", PART_PUBLIC_KEY));
                    builder.AppendLine(string.Format("{0} -> The Class name in the stack line", PART_TYPE));
                    builder.AppendLine(string.Format("{0} -> The Method name in the stack line", PART_METHOD));
                    builder.AppendLine(string.Format("{0} -> The class names of the parameters for the method concatenated by commas", PART_METHOD_PARAMS));
                    m_HelpString = builder.ToString();
                }

                return m_HelpString;
            }
        }
        #endregion
        #region INotifyPropertyChanged Members
        /// <summary>
        /// Called when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            m_SettingGroup.PropertyChanged -= m_SettingGroup_PropertyChanged;
            m_SettingGroup = null;
        }
    }
}
