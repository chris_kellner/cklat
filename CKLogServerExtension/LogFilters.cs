﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CKLogExtension.Messages;

namespace CKLogExtension.Server
{
    public class LogFilters : INotifyPropertyChanged
    {
        #region Variables
        private bool m_ShowEmergency = true;
        private bool m_ShowFatal = true;
        private bool m_ShowAlert = true;
        private bool m_ShowCritical = true;
        private bool m_ShowSevere = true;
        private bool m_ShowError = true;
        private bool m_ShowWarn = true;
        private bool m_ShowNotice = true;
        private bool m_ShowInfo = true;
        private bool m_ShowDebug = true;
        private bool m_ShowFine = true;
        private bool m_ShowTrace = true;
        private bool m_ShowFiner = true;
        private bool m_ShowVerbose = true;
        private bool m_ShowFinest = true;
        private string m_ShowString = "";
        private string m_HideString = "";
        private bool m_FilterEnabled = false;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new intance of the LogFilters class.
        /// </summary>
        public LogFilters()
        {
        }
        #endregion

        #region Properties
        public bool FilterEnabled
        {
            get
            {
                return m_FilterEnabled;
            }

            set
            {
                m_FilterEnabled = value;
                OnPropertyChanged("FilterEnabled");
            }
        }

        public string ShowString
        {
            get
            {
                return m_ShowString;
            }

            set
            {
                m_ShowString = value;
                OnPropertyChanged("ShowString");
            }
        }

        public string HideString
        {
            get
            {
                return m_HideString;
            }

            set
            {
                m_HideString = value;
                OnPropertyChanged("HideString");
            }
        }

        public bool ShowFatal
        {
            get
            {
                return m_ShowFatal;
            }

            set
            {
                m_ShowFatal = value;
                OnPropertyChanged("ShowFatal");
            }
        }

        public bool ShowError
        {
            get
            {
                return m_ShowError;
            }

            set
            {
                m_ShowError = value;
                OnPropertyChanged("ShowError");
            }
        }

        public bool ShowWarn
        {
            get
            {
                return m_ShowWarn;
            }

            set
            {
                m_ShowWarn = value;
                OnPropertyChanged("ShowWarn");
            }
        }

        public bool ShowInfo
        {
            get
            {
                return m_ShowInfo;
            }

            set
            {
                m_ShowInfo = value;
                OnPropertyChanged("ShowInfo");
            }
        }

        public bool ShowDebug
        {
            get
            {
                return m_ShowDebug;
            }

            set
            {
                m_ShowDebug = value;
                OnPropertyChanged("ShowDebug");
            }
        }

        public bool ShowTrace
        {
            get
            {
                return m_ShowTrace;
            }

            set
            {
                m_ShowTrace = value;
                OnPropertyChanged("ShowTrace");
            }
        }

        public bool ShowVerbose
        {
            get
            {
                return m_ShowVerbose;
            }

            set
            {
                m_ShowVerbose = value;
                OnPropertyChanged("ShowVerbose");
            }
        }
        /** OTHER PROPERTIES
        public bool ShowEmergency
        {
            get
            {
                return m_ShowEmergency;
            }

            set
            {
                m_ShowEmergency = value;
                OnPropertyChanged("ShowEmergency");
            }
        } 

        public bool ShowAlert
        {
            get
            {
                return m_ShowAlert;
            }

            set
            {
                m_ShowAlert = value;
                OnPropertyChanged("ShowAlert");
            }
        }

        public bool ShowCritical
        {
            get
            {
                return m_ShowCritical;
            }

            set
            {
                m_ShowCritical = value;
                OnPropertyChanged("ShowCritical");
            }
        }

        public bool ShowSevere
        {
            get
            {
                return m_ShowSevere;
            }

            set
            {
                m_ShowSevere = value;
                OnPropertyChanged("ShowSevere");
            }
        }

        public bool ShowNotice
        {
            get
            {
                return m_ShowNotice;
            }

            set
            {
                m_ShowNotice = value;
                OnPropertyChanged("ShowNotice");
            }
        }

        public bool ShowFine
        {
            get
            {
                return m_ShowFine;
            }

            set
            {
                m_ShowFine = value;
                OnPropertyChanged("ShowFine");
            }
        }

        public bool ShowFiner
        {
            get
            {
                return m_ShowFiner;
            }

            set
            {
                m_ShowFiner = value;
                OnPropertyChanged("ShowFiner");
            }
        }



        public bool ShowFinest
        {
            get
            {
                return m_ShowFinest;
            }

            set
            {
                m_ShowFinest = value;
                OnPropertyChanged("ShowFinest");
            }
        }*/
        #endregion

        /// <summary>
        /// Gets a filter which takes a log message and returns a value indicating whether the message passes the filter criteria.
        /// </summary>
        public Predicate<object> Filter
        {
            get
            {
                return (o) =>
                {
                    LogMessage msg = o as LogMessage;
                    if (FilterEnabled && msg != null)
                    {
                        bool levelOk = true;
                        switch (msg.Level)
                        {
                            case Interface.LoggingLevels.Emergency:
                            case Interface.LoggingLevels.Fatal:
                            case Interface.LoggingLevels.Alert:
                            case Interface.LoggingLevels.Critical:
                                levelOk = ShowFatal;
                                break;
                            case Interface.LoggingLevels.Severe:
                            case Interface.LoggingLevels.Error:
                                levelOk = ShowError;
                                break;
                            case Interface.LoggingLevels.Warn:
                                levelOk = ShowWarn;
                                break;
                            case Interface.LoggingLevels.Notice:
                            case Interface.LoggingLevels.Info:
                                levelOk = ShowInfo;
                                break;
                            case Interface.LoggingLevels.Fine:
                            case Interface.LoggingLevels.Debug:
                                levelOk = ShowDebug;
                                break;
                            case Interface.LoggingLevels.Finer:
                            case Interface.LoggingLevels.Trace:
                                levelOk = ShowTrace;
                                break;
                            case Interface.LoggingLevels.Finest:
                            case Interface.LoggingLevels.Verbose:
                                levelOk = ShowVerbose;
                                break;
                            default:
                                break;
                        }

                        if (!levelOk)
                        {
                            return false;
                        }

                        if (!string.IsNullOrEmpty(m_ShowString))
                        {
                            if (msg.Message.IndexOf(m_ShowString) < 0)
                            {
                                return false;
                            }
                        }

                        if (!string.IsNullOrEmpty(m_HideString))
                        {
                            if (msg.Message.IndexOf(m_HideString) > -1)
                            {
                                return false;
                            }
                        }
                    }

                    return true;
                };
            }
        }

        #region Methods
        /// <summary>
        /// Called when a property changes.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (!"Filter".Equals(propertyName))
            {
                OnPropertyChanged("Filter");
            }

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region INotifyPropertyChanged Members
        /// <summary>
        /// Called when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
