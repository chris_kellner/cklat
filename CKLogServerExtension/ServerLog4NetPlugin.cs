﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Connection.Extensions;
using CKLib.Utilities.Connection;
using CKLib.Utilities.Proxy;
using CKLogExtension.Messages;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Security;
using CKLib.Utilities.DataStructures;
using CKLogExtension.Server.Gui;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using CKLib.Utilities.DataStructures.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;

namespace CKLogExtension.Server
{
    [ServerSideExtension]
    public class ServerLog4NetPlugin : IExtension
    {
        private IMessenger m_Messenger;
        private IProxyFactory m_ProxyFactory;
        private SynchronizedObservableDictionary<Guid, LogClient> m_AllowedConnections;
        private LogViewer m_LogViewerGui;
        private DispatcherTimer m_RollTimer;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ServerLog4NetPlugin class.
        /// </summary>
        public ServerLog4NetPlugin()
        {
            m_LogViewerGui = new LogViewer(this);
            m_AllowedConnections = new SynchronizedObservableDictionary<Guid, LogClient>(m_LogViewerGui.Dispatcher);
            m_RollTimer = new DispatcherTimer(TimeSpan.FromSeconds(5), DispatcherPriority.Normal, OnTick, m_LogViewerGui.Dispatcher);
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets a reference to the IMessenger.
        /// </summary>
        public IMessenger Messenger
        {
            get
            {
                return m_Messenger;
            }
        }

        /// <summary>
        /// Gets a reference to the Proxy Factory.
        /// </summary>
        public IProxyFactory ProxyFactory
        {
            get
            {
                return m_ProxyFactory;
            }
        }

        /// <summary>
        /// Gets the dispatcher for the log viewer.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                if (m_LogViewerGui != null)
                {
                    return m_LogViewerGui.Dispatcher;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the collection of client logs.
        /// </summary>
        public IDictionary<Guid, LogClient> ClientLogs
        {
            get
            {
                return m_AllowedConnections;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Loads stored log data out of an xml file.
        /// </summary>
        /// <param name="filePath">The path to the file to load in.</param>
        public void LoadFromFile(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                if (File.Exists(filePath))
                {
                    using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        try
                        {
                            XmlSerializer cereal = new XmlSerializer(typeof(LogMessage[]));
                            var obj = cereal.Deserialize(fs) as LogMessage[];
                            if (obj != null)
                            {
                                // Read
                                foreach (var msg in obj)
                                {
                                    EnableConnectionAvailability(msg.SenderId);
                                    m_AllowedConnections[msg.SenderId].ReceiveMessage(msg);
                                }
                            }
                        }
                        catch (System.Xml.XmlException ex)
                        {
                            if (Messenger != null)
                            {
                                Messenger.SendLocalMessage(new StringMessage(ex.ToString()));
                            }
                            else
                            {
                                Debug.WriteLine(ex.ToString());
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves the current contents of this client's logs to the specified file.
        /// </summary>
        /// <param name="filePath">Path to the file to save the specified messages to</param>
        /// <param name="messages">The messages to be exported to a file.</param>
        public void SaveToFile(string filePath, IEnumerable<LogMessage> messages)
        {
            if (!string.IsNullOrEmpty(filePath) && messages != null)
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    XmlSerializer cereal = new XmlSerializer(typeof(LogMessage[]));
                    cereal.Serialize(fs, messages.ToArray());
                }
            }
        }

        /// <summary>
        /// Gets a log client by identifier.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public LogClient GetLogClient(Guid connectionId)
        {
            LogClient receiver = null;
            if (connectionId != null)
            {
                m_AllowedConnections.TryGetValue(connectionId, out receiver);
            }

            return receiver;
        }

        /// <summary>
        /// Receives a log message.
        /// </summary>
        /// <param name="pair"></param>
        private void OnMessage(MessagePair pair)
        {
            var theMessage = pair.MessageInformation.Message;
            if (theMessage is LogMessage)
            {
                var logMsg = (LogMessage)theMessage;

                LogClient receiver;
                if (m_AllowedConnections.TryGetValue(pair.Sender, out receiver))
                {
                    receiver.ReceiveMessage(logMsg);
                }
            }
        }

        /// <summary>
        /// Called every 5 seconds to clean out old entries.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTick(object sender, EventArgs e)
        {
            foreach (var client in m_AllowedConnections.Values)
            {
                client.RollLog();
            }
        }
        #endregion
        #region IServerExtension Members

        public void ShowGui()
        {
            m_LogViewerGui.Show();
        }

        #endregion

        #region IExtension Members

        public string ExtensionName
        {
            get { return "Log4NetPlugin"; }
        }

        public bool Initialize(IMessenger messenger, IProxyFactory proxyFactory)
        {
            ConfigurationManager.LoadSettingFile("CKLogExtension");
            m_Messenger = messenger;
            m_ProxyFactory = proxyFactory;
            m_RollTimer.Start();
            return true;
        }

        public void Activate()
        {
            if (m_Messenger != null)
            {
                m_Messenger.AddCallback(typeof(LogMessage), OnMessage, true);
            }
        }

        public void Deactivate()
        {
            if (m_Messenger != null)
            {
                m_Messenger.RemoveCallback(typeof(LogMessage), OnMessage);
            }

            if (m_LogViewerGui != null)
            {
                m_LogViewerGui.ForceClose();
            }
        }

        public Type[] GetCustomMessageTypes()
        {
            return new Type[]
            {
                typeof(LogMessage)
            };
        }

        #region IDisposable Members

        public void Dispose()
        {
            m_RollTimer.Stop();
            m_RollTimer.Tick -= OnTick;
        }

        #endregion


        public bool EnableConnectionAvailability(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(ServerLog4NetPlugin), m_Messenger, connectionId))
            {
                var client = m_AllowedConnections.GetOrAdd(connectionId, (k) => new LogClient(this, k));
                var info = m_Messenger.GetConnectionInformation(connectionId);
                if (info != null)
                {
                    client.ConnectionInformation = info;
                }

                return true;
            }

            return false;
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            m_AllowedConnections.Remove(connectionId);
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_AllowedConnections.Keys;
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            return m_AllowedConnections.ContainsKey(connectionId);
        }

        public System.Drawing.Image Icon
        {
            get { return null; }
        }
        #endregion
    }
}
