﻿
namespace CKLogExtension.Server.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CKLib.Utilities.Messages;
    #endregion
    /// <summary>
    /// This message is used to alert the GUI that it should roll logs where appropriate.
    /// </summary>
    public class RollLogMessage : IDataMessage
    {
        /// <summary>
        /// Initializes a new instance of the RollLogMessage.
        /// </summary>
        public RollLogMessage()
        {
        }
    }
}
