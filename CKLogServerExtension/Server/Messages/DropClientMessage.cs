﻿namespace CKLogExtension.Server.Messages
{
    #region Using List
    using System;
    using CKLib.Utilities.Messages;
    #endregion
    /// <summary>
    /// This message is sent locally to indicate that a client's data should be dropped.
    /// </summary>
    public class DropClientMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the DropClientMessage class.
        /// </summary>
        /// <param name="clientId">The id of the client to be dropped.</param>
        public DropClientMessage(Guid clientId)
        {
            ClientIdentifier = clientId;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the id of the clent to drop.
        /// </summary>
        public Guid ClientIdentifier
        {
            get;
            private set;
        }
        #endregion
    }
}
