﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Threading;
using System.Xml.Serialization;
using CKLib.Utilities.Connection;
using CKLib.Utilities.Connection.Extensions;
using CKLib.Utilities.DataStructures;
using CKLib.Utilities.DataStructures.Configuration;
using CKLib.Utilities.Diagnostics;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Security;
using CKLogExtension.Messages;
using CKLogExtension.Server.Gui;
using CKLogExtension.Server.Messages;

namespace CKLogExtension.Server
{
    [ServerSideExtension]
    public class ServerLogPlugin : IExtension
    {
        private IMessenger m_Messenger;
        private IProxyFactory m_ProxyFactory;
        private SynchronizedObservableDictionary<Guid, LogClient> m_AllowedConnections;
        private ICollection<LogClient> m_Observed;
        private LogViewer m_LogViewerGui;
        private DispatcherTimer m_RollTimer;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ServerLog4NetPlugin class.
        /// </summary>
        public ServerLogPlugin()
        {
            m_LogViewerGui = new LogViewer();
            m_AllowedConnections = new SynchronizedObservableDictionary<Guid, LogClient>(m_LogViewerGui.Dispatcher);
            m_Observed = m_AllowedConnections.GetValueNotifier();
            m_RollTimer = new DispatcherTimer(TimeSpan.FromSeconds(5), DispatcherPriority.Normal, OnTick, m_LogViewerGui.Dispatcher);
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets a reference to the IMessenger.
        /// </summary>
        public IMessenger Messenger
        {
            get
            {
                return m_Messenger;
            }
        }

        /// <summary>
        /// Gets a reference to the Proxy Factory.
        /// </summary>
        public IProxyFactory ProxyFactory
        {
            get
            {
                return m_ProxyFactory;
            }
        }

        /// <summary>
        /// Gets the dispatcher for the log viewer.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                if (m_LogViewerGui != null)
                {
                    return m_LogViewerGui.Dispatcher;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the collection of client logs.
        /// </summary>
        public ICollection<LogClient> ClientLogs
        {
            get
            {
                return m_Observed;
            }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Loads stored log data out of an xml file.
        /// </summary>
        /// <param name="filePath">The path to the file to load in.</param>
        public void LoadFromFile(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                if (File.Exists(filePath))
                {
                    using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        try
                        {
                            XmlSerializer cereal = new XmlSerializer(typeof(LogMessage[]));
                            var obj = cereal.Deserialize(fs) as LogMessage[];
                            if (obj != null)
                            {
                                // Read
                                foreach (var msg in obj)
                                {
                                    if (!IsConnectionAvailable(msg.SenderId))
                                    {
                                        NewClient(msg.SenderId, Path.GetFileNameWithoutExtension(filePath));
                                    }

                                    m_AllowedConnections[msg.SenderId].ReceiveMessage(msg);
                                }
                            }
                        }
                        catch (System.InvalidOperationException ex)
                        {
                            Error.WriteEntry("Failed to load plugin from file: " + filePath, ex);
                        }
                        catch (System.Xml.XmlException ex)
                        {
                            Error.WriteEntry("Failed to load plugin from file: " + filePath, ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves the current contents of this client's logs to the specified file.
        /// </summary>
        /// <param name="filePath">Path to the file to save the specified messages to</param>
        /// <param name="messages">The messages to be exported to a file.</param>
        public void SaveToFile(string filePath, IEnumerable<LogMessage> messages)
        {
            if (!string.IsNullOrEmpty(filePath) && messages != null)
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    XmlSerializer cereal = new XmlSerializer(typeof(LogMessage[]));
                    cereal.Serialize(fs, messages.ToArray());
                }
            }
        }

        /// <summary>
        /// Gets a log client by identifier.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public LogClient GetLogClient(Guid connectionId)
        {
            LogClient receiver = null;
            if (connectionId != null)
            {
                m_AllowedConnections.TryGetValue(connectionId, out receiver);
            }

            return receiver;
        }

        /// <summary>
        /// Launches the decompiler specified by configuration.
        /// </summary>
        /// <param name="info">The stack location to decompile.</param>
        public void StartDecompiler(LocationInfo info)
        {
            string path = LogConfiguration.Instance.DecompilerPath;
            var invalidChars = Path.GetInvalidPathChars();
            for (int i = 0; i < invalidChars.Length; i++)
            {
                path = path.Replace(invalidChars[i].ToString(), string.Empty);
            }

            if (File.Exists(path))
            {
                string command = LogConfiguration.Instance.DecompilerCommandLine;
                if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(command))
                {
                    if (info.AssemblyInfo != null)
                    {
                        command = command.Replace(LogConfiguration.PART_ASSEMBLY, info.AssemblyInfo.AssemblyName);
                        command = command.Replace(LogConfiguration.PART_ASSEMBLY_FILE, info.AssemblyInfo.AssemblyFileLocation);
                        command = command.Replace(LogConfiguration.PART_CULTURE, info.AssemblyInfo.CultureInfo);
                        command = command.Replace(LogConfiguration.PART_PUBLIC_KEY, info.AssemblyInfo.PublicKey);
                        command = command.Replace(LogConfiguration.PART_VERSION, info.AssemblyInfo.AssemblyVersion);
                    }

                    command = command.Replace(LogConfiguration.PART_METHOD_PARAMS, info.Parameters);
                    command = command.Replace(LogConfiguration.PART_METHOD, info.MethodName);
                    command = command.Replace(LogConfiguration.PART_TYPE, info.ClassName);
                    try
                    {
                        Process.Start(path, command);
                    }
                    catch (Exception ex)
                    {
                        Error.WriteEntry("Error while attempting to launch decompiler, full command was: " + command, ex);
                    }
                }
            }
            else
            {
                Error.WriteEntry("Invalid Decompiler path specified, unable to locate file at: " + path);
            }
        }

        /// <summary>
        /// Receives a log message.
        /// </summary>
        /// <param name="pair"></param>
        private void OnMessage(MessageEnvelope pair)
        {
            var theMessage = pair.Message;
            if (theMessage is LogMessage)
            {
                var logMsg = (LogMessage)theMessage;

                LogClient receiver;
                if (!m_AllowedConnections.TryGetValue(pair.Sender, out receiver))
                {
                    receiver = NewClient(pair.Sender);
                }

                if (receiver != null)
                {
                    receiver.ReceiveMessage(logMsg);
                }
            }
            else if (theMessage is CommandLineMessage)
            {
                List<string> args = new List<string>(((CommandLineMessage)theMessage).Args);
                if (args != null && args.Count > 0)
                {
                    int idx = args.FindIndex(s => ExtensionName.Equals(s, StringComparison.InvariantCultureIgnoreCase));
                    if (idx > -1)
                    {
                        if (idx + 2 < args.Count && args[idx + 1] == "/open")
                        {
                            string fileName = args[idx + 2];
                            if (!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
                            {
                                LoadFromFile(fileName);
                            }
                        }
                    }
                }
            }
            else if (theMessage is DropClientMessage)
            {
                LogClient oldClient;
                if (m_AllowedConnections.TryRemove(((DropClientMessage)theMessage).ClientIdentifier, out oldClient))
                {
                    oldClient.Dispose();
                }
            }
            else if (theMessage is RollLogMessage)
            {
                foreach (var client in m_AllowedConnections.Values)
                {
                    client.RollLog();
                }

                if (LogConfiguration.Instance.AutomaticallyClearOldLogs)
                {
                    DateTime removeTime = DateTime.UtcNow - TimeSpan.FromMinutes(LogConfiguration.Instance.OldLogRemovalDelayMinutes);
                    m_AllowedConnections.TryRemoveWhere(p =>
                        !p.Value.IsArchived &&
                        p.Value.ConnectionInformation.ConnectionStatus == ConnectionStatus.NotConnected &&
                        p.Value.LastReceiveTime < removeTime);
                }
            }
        }

        /// <summary>
        /// Called every 5 seconds to clean out old entries.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTick(object sender, EventArgs e)
        {
            Messenger.SendLocalMessage(new RollLogMessage());
        }
        #endregion
        #region IExtension Members

        public string ExtensionName
        {
            get { return "Log Viewer"; }
        }

        public bool Initialize(IMessenger messenger, IProxyFactory proxyFactory)
        {
            ConfigurationManager.LoadSettingFile("CKLogExtension");
            m_LogViewerGui.ServerPlugin = this;
            m_Messenger = messenger;
            m_ProxyFactory = proxyFactory;
            if (m_Messenger != null)
            {
                m_Messenger.AddCallback(typeof(LogMessage), OnMessage, true);
                m_Messenger.AddCallback(typeof(CommandLineMessage), OnMessage, true);
                m_Messenger.AddCallback(typeof(DropClientMessage), OnMessage, true);
                m_Messenger.AddCallback(typeof(RollLogMessage), OnMessage, true);
                m_RollTimer.Start();
                return true;
            }

            return false;
        }

        public Type[] GetCustomMessageTypes()
        {
            return new Type[]
            {
                typeof(LogMessage)
            };
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (m_Messenger != null)
            {
                m_Messenger.RemoveCallback(typeof(LogMessage), OnMessage);
                m_Messenger.RemoveCallback(typeof(CommandLineMessage), OnMessage);
                m_Messenger.RemoveCallback(typeof(DropClientMessage), OnMessage);
                m_Messenger.RemoveCallback(typeof(RollLogMessage), OnMessage);
            }

            m_RollTimer.Stop();
            m_RollTimer.Tick -= OnTick;
        }

        #endregion

        public bool EnableConnectionAvailability(Guid connectionId)
        {
            return NewClient(connectionId, null) != null;
        }

        /// <summary>
        /// Creates a new client.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private LogClient NewClient(Guid connectionId, string fileName = null)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(ServerLogPlugin), m_Messenger, connectionId))
            {
                var client = m_AllowedConnections.GetOrAdd(connectionId, (k) => new LogClient(this, k, fileName));
                client.RefreshConnectionInfo();
                m_LogViewerGui.RequestClient(client);

                return client;
            }

            return null;
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            // This extension is not concerned if a connection is broken.
            //m_AllowedConnections.Remove(connectionId);
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_AllowedConnections.Keys;
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            return m_AllowedConnections.ContainsKey(connectionId);
        }

        public Uri IconUri
        {
            get
            {
                return new Uri("/CKLogServerExtension;component/Resources/logger_Icon_Large.ico");
            }
        }

        public object UserInterface
        {
            get { return m_LogViewerGui; }
        }
        #endregion
    }
}
