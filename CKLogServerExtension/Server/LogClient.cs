﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface;
using CKLogExtension.Messages;
using CKLib.Utilities.DataStructures;
using CKLib.Utilities.Connection;
using CKLib.Utilities.Proxy.ObjectSharing;

namespace CKLogExtension.Server
{
    public class LogClient : IDisposable
    {
        #region Variables
        /// <summary>
        /// Reference to the owning plugin.
        /// </summary>
        private ServerLogPlugin m_Plugin;

        /// <summary>
        /// Id of the client.
        /// </summary>
        private Guid m_ClientId;

        /// <summary>
        /// Collection of all messages received from the client.
        /// </summary>
        private SynchronizedObservableCollection<LogMessage> m_LogTable;

        /// <summary>
        /// The client's remote settings
        /// </summary>
        private ILoggerSettings m_ClientSettings;

        /// <summary>
        /// Information about the connection.
        /// </summary>
        private IConnectionInformation m_ConnectionInfo;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the LogClient class.
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="clientId"></param>
        public LogClient(ServerLogPlugin plugin, Guid clientId, string fileName = null)
        {
            m_Plugin = plugin;
            m_LogTable = new SynchronizedObservableCollection<LogMessage>(plugin.Dispatcher);
            m_ClientId = clientId;
            if (!string.IsNullOrEmpty(fileName))
            {
                IsArchived = true;
                FileName = fileName;
            }

            foreach (var share in plugin.Messenger.SharedObjectPool.GetObjectsSharedByOwner(clientId))
            {
                if (share is ILoggerSettings)
                {
                    m_ClientSettings = (ILoggerSettings)share;
                    break;
                }
            }

            RefreshConnectionInfo();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets the latest connection information for this log client.
        /// </summary>
        public void RefreshConnectionInfo()
        {
            if (m_Plugin.Messenger != null)
            {
                var info = m_Plugin.Messenger.GetConnectionInformation(m_ClientId);
                if (info != null)
                {
                    m_ConnectionInfo = info;
                }

                if (m_ClientSettings == null)
                {
                    foreach (var share in m_Plugin.Messenger.SharedObjectPool.GetObjectsSharedByOwner(m_ClientId))
                    {
                        if (share is ILoggerSettings)
                        {
                            m_ClientSettings = (ILoggerSettings)share;
                            break;
                        }
                    }
                }
            }

            if (m_ConnectionInfo == null)
            {
                m_ConnectionInfo = new ConnectionInformation(m_ClientId);
            }
        }

        /// <summary>
        /// Alerts all views bound to the log table to be regenerated.
        /// </summary>
        public void RefreshView()
        {
            m_LogTable.RaiseResetEvent();
        }

        /// <summary>
        /// Adds a message to the set of messages from this client.
        /// </summary>
        /// <param name="message"></param>
        public void ReceiveMessage(LogMessage message)
        {
            m_LogTable.Add(message);
            LastReceiveTime = DateTime.UtcNow;
        }

        /// <summary>
        /// Performs log rolling operations described in the configuration.
        /// </summary>
        public void RollLog()
        {
            // Dont roll historical logs
            if (m_ConnectionInfo != null && m_ConnectionInfo.ConnectionStatus == ConnectionStatus.Connected)
            {
                switch (LogConfiguration.Instance.LogRollMode)
                {
                    case LogRollMode.Off:
                        return;
                    case LogRollMode.MessageCount:
                        OnLogRollStarted();
                        int msgCount = LogConfiguration.Instance.MaximumLogMessages;
                        while (m_LogTable.Count > msgCount)
                        {
                            m_LogTable.RemoveAt(0);
                        }

                        OnLogRollCompleted();
                        break;
                    case LogRollMode.MessageAge:
                        OnLogRollStarted();
                        int msgAgeMinutes = LogConfiguration.Instance.MaximumMessageAgeMinutes;
                        DateTime minTime = DateTime.Now.Subtract(TimeSpan.FromMinutes(msgAgeMinutes));
                        while (m_LogTable[0].TimeStamp < minTime)
                        {
                            m_LogTable.RemoveAt(0);
                        }

                        OnLogRollCompleted();
                        break;
                }
            }
        }

        /// <summary>
        /// Raises the LogRollStarted event.
        /// </summary>
        private void OnLogRollStarted()
        {
            if (LogRollStarted != null)
            {
                LogRollStarted(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the LogRollCompleted event.
        /// </summary>
        private void OnLogRollCompleted()
        {
            if (LogRollCompleted != null)
            {
                LogRollCompleted(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the client information string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string retVal = FileName;

            if (!this.IsArchived || string.IsNullOrEmpty(retVal))
            {
                if (this.ConnectionInformation != null)
                {
                    retVal = ConnectionInformation.ToString();
                }
            }

            if (string.IsNullOrEmpty(retVal))
            {
                retVal = base.ToString();
            }

            return retVal;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the id of the client.
        /// </summary>
        public Guid ClientId
        {
            get
            {
                return m_ClientId;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this client was created from a historical log.
        /// </summary>
        public bool IsArchived
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the filename of the historical log this client was created from.
        /// </summary>
        public string FileName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the time that the last message was received.
        /// </summary>
        public DateTime LastReceiveTime
        {
            get;
            private set;
        }

        public IConnectionInformation ConnectionInformation
        {
            get
            {
                return m_ConnectionInfo;
            }

            set
            {
                if (m_ConnectionInfo != null)
                {
                    m_ConnectionInfo.Update(value);
                }
                else
                {
                    m_ConnectionInfo = value;
                }
            }
        }

        public ILoggerSettings LogSettings
        {
            get
            {
                if (m_ClientSettings == null)
                {
                    foreach (var share in m_Plugin.Messenger.SharedObjectPool.GetObjectsSharedByOwner(m_ClientId))
                    {
                        if (share is ILoggerSettings)
                        {
                            m_ClientSettings = (ILoggerSettings)share;
                            break;
                        }
                    }
                }

                return m_ClientSettings;
            }
        }

        public IList<LogMessage> LogTable
        {
            get
            {
                return m_LogTable;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Called when log rolling has begun.
        /// </summary>
        public event EventHandler LogRollStarted;

        /// <summary>
        /// Called when log rolling has completed.
        /// </summary>
        public event EventHandler LogRollCompleted;
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            LogRollCompleted = null;
            LogRollStarted = null;
            m_ClientSettings = null;
            m_ConnectionInfo = null;
            m_LogTable.Clear();
            m_Plugin = null;
        }
        #endregion
    }
}
