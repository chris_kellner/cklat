﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace CKLogExtension.Server.Gui
{
    public static class DefaultLogCommands
    {
        public static readonly RoutedUICommand ForwardDebugStream = new RoutedUICommand("Forward Debug Stream", "ForwardDebugStream", typeof(LogViewer));
        public static readonly RoutedUICommand ForwardTraceStream = new RoutedUICommand("Forward Trace Stream", "ForwardTraceStream", typeof(LogViewer));
        public static readonly RoutedUICommand ForwardConsoleStream = new RoutedUICommand("Forward Console Stream", "ForwardConsoleStream", typeof(LogViewer));
        public static readonly RoutedUICommand ForwardErrorStream = new RoutedUICommand("Forward Error Stream", "ForwardErrorStream", typeof(LogViewer));
    }
}
