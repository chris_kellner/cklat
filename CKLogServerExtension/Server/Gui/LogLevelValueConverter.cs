﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CKLogExtension.Interface;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace CKLogExtension.Server.Gui
{
    public class LogLevelValueConverter : IValueConverter
    {

        public BitmapImage FatalImage { get; set; }
        public BitmapImage ErrorImage { get; set; }
        public BitmapImage WarningImage { get; set; }
        public BitmapImage InfoImage { get; set; }
        public BitmapImage DebugImage { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is LoggingLevels))
            {
                return null;
            }

            LoggingLevels level = (LoggingLevels)value;
            switch (level)
            {
                case LoggingLevels.Emergency:
                case LoggingLevels.Fatal:
                case LoggingLevels.Alert:
                case LoggingLevels.Critical:
                    return FatalImage;
                case LoggingLevels.Severe:
                case LoggingLevels.Error:
                    return ErrorImage;
                case LoggingLevels.Warn:
                case LoggingLevels.Notice:
                    return WarningImage;
                case LoggingLevels.Info:
                    return InfoImage;
                case LoggingLevels.Debug:
                case LoggingLevels.Fine:
                case LoggingLevels.Trace:
                case LoggingLevels.Finer:
                case LoggingLevels.Verbose:
                case LoggingLevels.Finest:
                default:
                    return DebugImage;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
