﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CKLib.Utilities.DataStructures.Configuration;

namespace CKLogExtension.Server.Gui
{
    /// <summary>
    /// Interaction logic for LoggerSettings.xaml
    /// </summary>
    public partial class LoggerSettings : Window
    {
        public LoggerSettings()
        {
            InitializeComponent();
            DataContext = LogConfiguration.Instance;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            LogConfiguration.Instance.DisplayFilters.PropertyChanged += DisplayFilters_PropertyChanged;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            LogConfiguration.Instance.DisplayFilters.PropertyChanged -= DisplayFilters_PropertyChanged;
        }

        void DisplayFilters_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            ForceRefresh = true;
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationManager.Save(LogConfiguration.LOG_CONFIGURATION_GROUP_NAME);
            Close();
        }

        public bool ForceRefresh { get; set; }

        private void rb_redgate_Checked(object sender, RoutedEventArgs e)
        {
            LogConfiguration.Instance.DecompilerCommandLine = LogConfiguration.DECOMPILER_REDGATE;
        }

        private void rb_ilspy_Checked(object sender, RoutedEventArgs e)
        {
            LogConfiguration.Instance.DecompilerCommandLine = LogConfiguration.DECOMPILER_ILSPY;
        }

        private void rb_ildasm_Checked(object sender, RoutedEventArgs e)
        {
            LogConfiguration.Instance.DecompilerCommandLine = LogConfiguration.DECOMPILER_ILDASM;
        }
    }
}
