﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CKLogExtension.Interface;

namespace CKLogExtension.Server.Gui
{
    public class MultiLevelBindingConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool[] levels = ConvertFromObjs<bool>(values).ToArray();
            return levels.All(b => b);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                bool bVal = (bool)value;
                return Repeat(bVal, targetTypes.Length);
            }

            return Repeat(null, targetTypes.Length);
        }

        public static object[] Repeat(object tVal, int length)
        {
            object[] arr = new object[length];
            for (int i = 0; i < length; i++)
            {
                arr[i] = tVal;
            }

            return arr;
        }

        private static IEnumerable<T> ConvertFromObjs<T>(object[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                var cVal = values[i];
                if (cVal is T)
                {
                    yield return (T)cVal;
                }
                else
                {
                    yield return default(T);
                }
            }
        }
    }
}
