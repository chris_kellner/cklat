﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CKLogExtension.Server.Gui
{
    /// <summary>
    /// Interaction logic for WindowHost.xaml
    /// </summary>
    public partial class WindowHost : Window
    {

        private bool m_CanClose = false;

        #region Dependency Properties
        // Using a DependencyProperty as the backing store for ServerPlugin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ServerPluginProperty =
            DependencyProperty.Register("ServerPlugin", typeof(ServerLogPlugin), typeof(WindowHost), new UIPropertyMetadata(null));
        #endregion

        public WindowHost()
        {
            DataContext = this;
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the server plugin.
        /// </summary>
        public ServerLogPlugin ServerPlugin
        {
            get
            {
                return (ServerLogPlugin)GetValue(ServerPluginProperty);
            }
            set
            {
                SetValue(ServerPluginProperty, value);
            }
        }

        /// <summary>
        /// Forces the gui to close due to shutdown.
        /// </summary>
        public void ForceClose()
        {
            m_CanClose = true;
            Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = !m_CanClose;
            logViewer.SelectedClient = null;
            Hide();
        }
    }
}
