﻿

namespace CKLogExtension.Server.Gui
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using CKLib.Utilities.DataStructures.Linq;
    using CKLogExtension.Interface;
    using CKLogExtension.Messages;
    using CKLogExtension.Server.Messages;
    using CKLib.Utilities.DataStructures;
    #endregion
    /// <summary>
    /// Interaction logic for LogViewer.xaml
    /// </summary>
    public partial class LogViewer : UserControl
    {
        #region Variables
        /// <summary>
        /// The set of currently displayed clients.
        /// </summary>
        private LogClient[] m_DisplayedClients;

        /// <summary>
        /// Indicates that the selection is changing.
        /// </summary>
        private bool m_ClientChanging;
        #endregion
        #region Dependency Properties
        // Using a DependencyProperty as the backing store for ServerPlugin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ServerPluginProperty =
            DependencyProperty.Register("ServerPlugin", typeof(ServerLogPlugin), typeof(LogViewer), new UIPropertyMetadata(null, ServerPluginChanged));

        private static void ServerPluginChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            LogViewer view = source as LogViewer;
            if (view != null)
            {
                if (e.NewValue != null)
                {
                    view.list_Clients.ItemsSource = ((ServerLogPlugin)e.NewValue).ClientLogs;
                    view.dgv_Display.Items.Filter = LogConfiguration.Instance.DisplayFilters.Filter;
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the LogViewer class.
        /// </summary>
        public LogViewer()
        {
            InitializeComponent();
            tool_Filters.DataContext = LogConfiguration.Instance;
            tool_FilterStrings.DataContext = LogConfiguration.Instance;
            cmb_forceLevel.ItemsSource = Enum.GetValues(typeof(LoggingLevels));
        }

        public LogViewer(ServerLogPlugin plugin)
            : this()
        {
            ServerPlugin = plugin;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the server plugin.
        /// </summary>
        public ServerLogPlugin ServerPlugin
        {
            get 
            {
                return (ServerLogPlugin)GetValue(ServerPluginProperty);
            }
            set
            {
                SetValue(ServerPluginProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the currently displayed log client.
        /// </summary>
        public LogClient SelectedClient
        {
            get
            {
                if (m_DisplayedClients != null)
                {
                    return m_DisplayedClients.FirstOrDefault();
                }

                return null;
            }

            set
            {
                if (value != null)
                {
                    SelectedClients = new LogClient[] { value };
                }
                else
                {
                    SelectedClients = new LogClient[0];
                }
            }
        }

        public LogClient[] SelectedClients
        {
            get
            {
                return m_DisplayedClients;
            }

            set
            {
                m_ClientChanging = true;
                OnClientsChanging(m_DisplayedClients);
                m_DisplayedClients = value;
                OnClientsChanged(value);
                m_ClientChanging = false;
            }
        }

        /// <summary>
        /// Gets the selected LogClients.
        /// </summary>
        public LogClient[] ListboxSelection
        {
            get
            {
                var selected = list_Clients.SelectedItems;
                if (selected != null && selected.Count > 0)
                {
                    LogClient[] selection = selected.Cast<LogClient>().ToArray();
                    return selection;
                }

                return new LogClient[0];
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Posts a request to change the active client.
        /// </summary>
        /// <param name="client">The client to change to.</param>
        public void RequestClient(LogClient client)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new Action<LogClient>(RequestClient), client);
            }
            else
            {
                SelectedClient = client;
            }
        }

        /// <summary>
        /// Refreshes ther current view.
        /// </summary>
        private void RefreshFilters()
        {
            if (SelectedClients != null && SelectedClients.Length > 0)
            {
                if (SelectedClients.Length > 1)
                {
                    SelectedClients = SelectedClients;
                }
                else
                {
                    SelectedClient.RefreshView();
                }
            }
        }

        /// <summary>
        /// Occurs when the currently displayed clients are changing.
        /// </summary>
        /// <param name="c"></param>
        private void OnClientsChanging(LogClient[] c)
        {
            if (c != null && c.Length > 0)
            {
                var tmp = CollectionViewSource.GetDefaultView(dgv_Display.ItemsSource);
                tmp.CollectionChanged -= tmp_CollectionChanged;
            }
        }

        /// <summary>
        /// Called when the currently bound collection adds an entry.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmp_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (LogConfiguration.Instance.AutoScrollOnNew)
            {
                if(e.Action == NotifyCollectionChangedAction.Add){
                    var items = e.NewItems;
                    if (items != null && items.Count > 0)
                    {
                        dgv_Display.ScrollIntoView(items[items.Count-1]);
                    }
                }
            }
        }

        /// <summary>
        /// Occurs when the currently displayed client is changed.
        /// </summary>
        /// <param name="c"></param>
        private void OnClientsChanged(LogClient[] c)
        {
            if (c != null && c.Length > 0)
            {
                if (c.Length == 1 && c[0].LogSettings != null)
                {
                    dgv_Display.ItemsSource = c[0].LogTable;
                }
                else
                {
                    dgv_Display.ItemsSource = new CollectionObserver<LogMessage>(Dispatcher, c.Select(l => l.LogTable as INotifyCollectionChanged));
                }

                hdr_ProcessId.Visibility = c.Length > 1 ? Visibility.Visible : Visibility.Collapsed;
                hdr_UserName.Visibility = c.Length > 1 ? Visibility.Visible : Visibility.Collapsed;

                var tmp = CollectionViewSource.GetDefaultView(dgv_Display.ItemsSource);
                tmp.CollectionChanged += tmp_CollectionChanged;
            }
            else
            {
                dgv_Display.ItemsSource = null;
            }
        }

        /// <summary>
        /// Updates the client context menu.
        /// </summary>
        /// <param name="c"></param>
        private void UpdateMenuForClients(LogClient[] c)
        {
            if (c != null && c.Length > 0)
            {
                if (c.Length == 1 && c[0].LogSettings != null)
                {
                    menu_Forward.IsEnabled =
                        menu_logFirstChanceExceptions.IsEnabled =
                        menu_collectFullTrace.IsEnabled =
                        menu_ForceLogLevel.IsEnabled = true;
                    cmb_forceLevel.Text = c[0].LogSettings.LogLevel.ToString();
                    menu_collectFullTrace.IsChecked = c[0].LogSettings.CollectFullTrace;
                    menu_logFirstChanceExceptions.IsChecked = c[0].LogSettings.LogFirstChanceExceptions;
                    menu_Debug.IsChecked = c[0].LogSettings.LogDebugStream;
                    menu_Trace.IsChecked = c[0].LogSettings.LogTraceStream;
                    menu_Console.IsChecked = c[0].LogSettings.LogConsoleStream;
                    menu_Error.IsChecked = c[0].LogSettings.LogErrorStream;
                }
                else
                {
                    menu_Forward.IsEnabled =
                        menu_logFirstChanceExceptions.IsEnabled =
                        menu_collectFullTrace.IsEnabled =
                        menu_ForceLogLevel.IsEnabled = false;
                }
            }
        }
        #endregion
        #region Event Handlers
        #region Remote Settings
        private void cmb_LogLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (e.AddedItems != null && e.AddedItems.Count > 0)
                {
                    if (selection != null && selection.LogSettings != null && e.AddedItems[0] is LoggingLevels)
                    {
                        selection.LogSettings.LogLevel = (LoggingLevels)e.AddedItems[0];
                        selection.LogSettings.OverrideLogLevel = (LoggingLevels)e.AddedItems[0] != LoggingLevels.Invalid;
                    }
                }
            }
        }

        private void cb_logFirstChanceExceptions_Checked(object sender, RoutedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (selection != null && selection.LogSettings != null)
                {
                    selection.LogSettings.LogFirstChanceExceptions = true;
                }
            }
        }

        private void cb_logFirstChanceExceptions_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (selection != null && selection.LogSettings != null)
                {
                    selection.LogSettings.LogFirstChanceExceptions = false;
                }
            }
        }

        private void cb_CollectTrace_Checked(object sender, RoutedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (selection != null && selection.LogSettings != null)
                {
                    selection.LogSettings.CollectFullTrace = true;
                }
            }
        }

        private void cb_CollectTrace_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (selection != null && selection.LogSettings != null)
                {
                    selection.LogSettings.CollectFullTrace = false;
                }
            }
        }

        private void btn_forceScan_Click(object sender, RoutedEventArgs e)
        {
            if (!m_ClientChanging && ListboxSelection != null && ListboxSelection.Length == 1)
            {
                var selection = ListboxSelection[0];
                if (selection != null && selection.LogSettings != null)
                {
                    selection.LogSettings.ForceScan();
                }
            }
        }
        #endregion
        private void btn_applySelection_Click(object sender, RoutedEventArgs e)
        {
            LogClient[] selection = ListboxSelection;
            if (selection.Length > 0)
            {
                SelectedClients = selection;
            }
        }

        private void list_Clients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LogClient[] selection = ListboxSelection;
            if (selection.Length > 0)
            {
                SelectedClients = selection;
            }
        }

        private void btn_cleanUp_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("This will erase all data for the client, are you sure you wish to continue?", "Confirm delete.", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                LogClient[] selection = ListboxSelection;
                if (selection.Length > 0)
                {
                    for (int i = 0; i < selection.Length; i++)
                    {
                        ServerPlugin.Messenger.SendLocalMessage(new DropClientMessage(selection[i].ClientId));
                    }
                }

                SelectedClient = null;
            }
        }

        private void btn_Settings_Click(object sender, RoutedEventArgs e)
        {
            LoggerSettings setting = new LoggerSettings();
            setting.ShowDialog();
            if (setting.ForceRefresh)
            {
                RefreshFilters();
            }
        }

        /// <summary>
        /// Displays information about this application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Yet Implemented.");
        }

        /// <summary>
        /// Occurs when the clients context menu is opened.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void context_Clients_Opened(object sender, RoutedEventArgs e)
        {
            UpdateMenuForClients(ListboxSelection);
        }

        /// <summary>
        /// Called when the import button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_Import_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;
            dialog.DefaultExt = ".cklog";
            dialog.Filter = "CK Log Extension Archive Files (.cklog)|*.cklog|All Files (*.*)|*.*";
            dialog.Multiselect = true;

            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string[] fileNames = dialog.FileNames;
                if (fileNames != null)
                {
                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        ServerPlugin.LoadFromFile(fileNames[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Called when the export button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_Export_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.DefaultExt = ".cklog";
            dialog.Filter = "CK Log Extension Archive Files (.cklog)|*.cklog|All Files (*.*)|*.*";
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string fileName = dialog.FileName;
                if (!string.IsNullOrEmpty(fileName))
                {
                    if (SelectedClients != null && SelectedClients.Length > 0)
                    {
                        var messages = SelectedClients.SelectMany(client => client.LogTable.ToArray());
                        ServerPlugin.SaveToFile(fileName, messages);
                    }
                }
            }
        }

        /// <summary>
        /// Called when the user clicks a filter button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFilters();
        }

        private void TextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            RefreshFilters();
        }

        private void dgv_Display_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = dgv_Display.SelectedItem as LogMessage;
            if (item != null)
            {
                DetailsView details = new DetailsView(ServerPlugin, item);
                details.Show();
            }
        }
        #endregion

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            RoutedUICommand command = e.Command as RoutedUICommand;
            var selection = ListboxSelection;
            if (command != null && selection != null && selection.Length == 1)
            {
                var selectedClient = selection[0];
                switch (command.Name)
                {
                    case "ForwardDebugStream":
                        selectedClient.LogSettings.LogDebugStream = !selectedClient.LogSettings.LogDebugStream;
                        break;
                    case "ForwardTraceStream":
                        selectedClient.LogSettings.LogTraceStream = !selectedClient.LogSettings.LogTraceStream;
                        break;
                    case "ForwardConsoleStream":
                        selectedClient.LogSettings.LogConsoleStream = !selectedClient.LogSettings.LogConsoleStream;
                        break;
                    case "ForwardErrorStream":
                        selectedClient.LogSettings.LogErrorStream = !selectedClient.LogSettings.LogErrorStream;
                        break;
                }
            }
        }
    }
}
