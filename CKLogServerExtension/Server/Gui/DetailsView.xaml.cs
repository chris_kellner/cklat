﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CKLogExtension.Messages;

namespace CKLogExtension.Server.Gui
{
    /// <summary>
    /// Interaction logic for DetailsView.xaml
    /// </summary>
    public partial class DetailsView : Window
    {
        #region Variables
        /// <summary>
        /// The message that this form is displaying.
        /// </summary>
        private LogMessage m_Message;

        /// <summary>
        /// The plugin that owns this form.
        /// </summary>
        private ServerLogPlugin m_Plugin;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the DetailsView class.
        /// </summary>
        public DetailsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the DetailsView class.
        /// </summary>
        /// <param name="message">The message to view.</param>
        public DetailsView(ServerLogPlugin plugin,LogMessage message)
            : this()
        {
            SelectedMessage = message;
            m_Plugin = plugin;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the log message to display.
        /// </summary>
        public LogMessage SelectedMessage
        {
            get
            {
                return m_Message;
            }

            set
            {
                m_Message = value;
                DataContext = m_Message;
                if (m_Message != null)
                {
                    if (m_Message.FullTrace != null)
                    {
                        list_StackTrace.Content = m_Message.FullTrace;
                    }

                    root_Node.ItemsSource = new RemoteException[] { m_Message.ExceptionObject };
                }
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when the Decompile... menu item is clicked on a stack line.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void decompile_Click(object sender, RoutedEventArgs e)
        {
            MenuItem m = sender as MenuItem;
            if (m != null)
            {
                ContextMenu menu = m.CommandParameter as ContextMenu;
                if (menu != null)
                {
                    ContentPresenter content = menu.PlacementTarget as ContentPresenter;
                    if (content != null)
                    {
                        LocationInfo info = content.Content as LocationInfo;
                        if (info != null)
                        {
                            if (m_Plugin != null)
                            {
                                m_Plugin.StartDecompiler(info);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
