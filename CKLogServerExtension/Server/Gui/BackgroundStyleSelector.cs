﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using CKLogExtension.Messages;

namespace CKLogExtension.Server.Gui
{
    public class BackgroundStyleSelector : StyleSelector
    {
        public Style DefaultStyle { get; set; }
        public Style FatalStyle { get; set; }
        public Style ErrorStyle { get; set; }
        public Style WarningStyle { get; set; }
        public Style InfoStyle { get; set; }
        public Style DebugStyle { get; set; }


        public override System.Windows.Style SelectStyle(object item, System.Windows.DependencyObject container)
        {
            LogMessage row = item as LogMessage;
            if (row == null)
            {
                return base.SelectStyle(item, container);
            }
            else
            {
                if (LogConfiguration.Instance.EnableRowStyles)
                {
                    switch (row.Level)
                    {
                        case Interface.LoggingLevels.Fatal:
                        case Interface.LoggingLevels.Emergency:
                        case Interface.LoggingLevels.Alert:
                        case Interface.LoggingLevels.Critical:
                        case Interface.LoggingLevels.Severe:
                            return FatalStyle;
                        case Interface.LoggingLevels.Error:
                            return ErrorStyle;
                        case Interface.LoggingLevels.Warn:
                        case Interface.LoggingLevels.Notice:
                            return WarningStyle;
                        case Interface.LoggingLevels.Info:
                            return InfoStyle;
                        default:
                            return DebugStyle;
                    }
                }
                else
                {
                    return DefaultStyle;
                }
            }
        }
    }
}
