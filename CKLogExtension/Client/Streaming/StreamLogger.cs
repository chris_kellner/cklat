﻿

namespace CKLogExtension.Client
{
    #region Using List
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using CKLib.Utilities.Patterns;
    using CKLogExtension.Client.Interface;
    using CKLogExtension.Client.Streaming;
    using CKLogExtension.Interface;
    using CKLogExtension.Messages;
    #endregion
    public class StreamLogger : IDisposable
    {
        #region Inner classes
        public enum StreamType
        {
            Console = 0,
            Error = 1,
            Debug = 2,
            Trace = 3,
            Fatal = 4
        }

        private class StreamListener : TraceListener
        {
            #region Variables
            /// <summary>
            /// The type of stream being forwarded.
            /// </summary>
            private readonly StreamType m_forwardType;

            /// <summary>
            /// The action to take when a message is written.
            /// </summary>
            private readonly Action<string, bool> m_onStreamWritten;

            /// <summary>
            /// A buffer to use when building messages.
            /// </summary>
            private StringBuilder m_Buffer = new StringBuilder(100);
            #endregion
            #region Constructors
            /// <summary>
            /// Initializes a new instance of the StreamForwarder class.
            /// </summary>
            /// <param name="forwardType">The type of stream to forward.</param>
            /// <param name="onStreamWritten">The action to take when a message is written.</param>
            public StreamListener(StreamType streamType, Action<string, bool> onStreamWritten)
            {
                m_forwardType = streamType;
                m_onStreamWritten = onStreamWritten;
            }
            #endregion
            #region Properties
            /// <summary>
            /// Returns true;
            /// </summary>
            public override bool IsThreadSafe
            {
                get
                {
                    return true;
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is active.
            /// </summary>
            public bool Active
            {
                get
                {
                    switch (m_forwardType)
                    {
                        case StreamType.Debug:
                            return Debug.Listeners.Contains(this);
                        case StreamType.Trace:
                            return Trace.Listeners.Contains(this);
                        default:
                            return false;
                    }
                }

                set
                {
                    switch (m_forwardType)
                    {
                        case StreamType.Debug:
                            if (value)
                            {
                                Debug.Listeners.Add(this);
                            }
                            else
                            {
                                Debug.Listeners.Remove(this);
                            }

                            break;
                        case StreamType.Trace:
                            if (value)
                            {
                                Trace.Listeners.Add(this);
                            }
                            else
                            {
                                Trace.Listeners.Remove(this);
                            }

                            break;
                    }
                }
            }
            #endregion
            #region Methods
            /// <summary>
            /// Appends a value to the buffer, and returns the segment upon completion.
            /// </summary>
            /// <param name="value">The value to append.</param>
            /// <returns>A complete write.</returns>
            public string AppendBuffer(string value)
            {
                lock (m_Buffer)
                {
                    m_Buffer.Append(value);
                    if (value.Contains(Environment.NewLine))
                    {
                        string ret = m_Buffer.ToString();
                        m_Buffer.Remove(0, m_Buffer.Length);
                        return ret;
                    }
                }

                return string.Empty;
            }

            /// <summary>
            /// Called when a message is written to the stream.
            /// </summary>
            /// <param name="message">The message to write.</param>
            public override void Write(string message)
            {
                m_onStreamWritten(message, false);
            }

            /// <summary>
            /// Called when a message is written to the stream.
            /// </summary>
            /// <param name="message">The message to write.</param>
            public override void WriteLine(string message)
            {
                m_onStreamWritten(message + Environment.NewLine, true);
            }
            #endregion
        }

        /// <summary>
        /// This class is used to wrap the base Error or Console streams.
        /// </summary>
        private class StreamForwarder : TextWriter
        {
            #region Variables
            /// <summary>
            /// The underlying text writer.
            /// </summary>
            private TextWriter m_BaseWriter;

            /// <summary>
            /// The type of stream being forwarded.
            /// </summary>
            private readonly StreamType m_forwardType;

            /// <summary>
            /// The action to take when a message is written.
            /// </summary>
            private readonly Action<string, bool> m_onStreamWritten;

            /// <summary>
            /// A buffer to use when building messages.
            /// </summary>
            private StringBuilder m_Buffer = new StringBuilder(100);
            #endregion
            #region Constructors
            /// <summary>
            /// Initializes a new instance of the StreamForwarder class.
            /// </summary>
            /// <param name="forwardType">The type of stream to forward.</param>
            /// <param name="onStreamWritten">The action to take when a message is written.</param>
            public StreamForwarder(StreamType forwardType, Action<string, bool> onStreamWritten)
            {
                m_forwardType = forwardType;
                switch (m_forwardType)
                {
                    case StreamType.Console:
                        m_BaseWriter = Console.Out;
                        break;
                    case StreamType.Error:
                        m_BaseWriter = Console.Error;
                        break;
                }

                m_onStreamWritten = onStreamWritten;
            }
            #endregion
            #region Properties
            /// <summary>
            /// Gets the encoding used in the stream.
            /// </summary>
            public override Encoding Encoding
            {
                get { return m_BaseWriter.Encoding; }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this stream should be forwarded or left alone.
            /// </summary>
            public bool Active
            {
                get
                {
                    switch (m_forwardType)
                    {
                        case StreamType.Console:
                            return this == Console.Out;
                        case StreamType.Error:
                            return this == Console.Error;
                    }

                    return false;
                }

                set
                {
                    switch (m_forwardType)
                    {
                        case StreamType.Console:
                            if (!value)
                            {
                                Console.SetOut(m_BaseWriter);
                            }
                            else
                            {
                                Console.SetOut(this);
                            }

                            break;
                        case StreamType.Error:
                            if (!value)
                            {
                                Console.SetError(m_BaseWriter);
                            }
                            else
                            {
                                Console.SetError(this);
                            }

                            break;
                    }
                }
            }
            #endregion
            #region Methods
            /// <summary>
            /// Appends a value to the buffer, and returns the segment upon completion.
            /// </summary>
            /// <param name="value">The value to append.</param>
            /// <returns>A complete write.</returns>
            public string AppendBuffer(string value)
            {
                lock (m_Buffer)
                {
                    m_Buffer.Append(value);
                    if (value.Contains(NewLine))
                    {
                        string ret = m_Buffer.ToString();
                        m_Buffer.Remove(0, m_Buffer.Length);
                        return ret;
                    }
                }

                return string.Empty;
            }

            #region Write
            public override void Write(bool value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(char value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(char[] buffer)
            {
                m_BaseWriter.Write(buffer);
                m_onStreamWritten(new string(buffer), false);
            }

            public override void Write(char[] buffer, int index, int count)
            {
                m_BaseWriter.Write(buffer, index, count);
                char[] b = new char[count];
                Array.Copy(buffer, index, b, 0, count);
                m_onStreamWritten(new string(b), false);
            }

            public override void Write(decimal value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(double value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(float value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(int value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(long value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(object value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value != null ? value.ToString() : string.Empty, false);
            }

            public override void Write(string format, object arg0)
            {
                m_BaseWriter.Write(format, arg0);
                m_onStreamWritten(string.Format(format, arg0), false);
            }

            public override void Write(string format, object arg0, object arg1)
            {
                m_BaseWriter.Write(format, arg0, arg1);
                m_onStreamWritten(string.Format(format, arg0, arg1), false);
            }

            public override void Write(string format, object arg0, object arg1, object arg2)
            {
                m_BaseWriter.Write(format, arg0, arg1, arg2);
                m_onStreamWritten(string.Format(format, arg0, arg1, arg2), false);
            }

            public override void Write(string format, params object[] arg)
            {
                m_BaseWriter.Write(format, arg);
                m_onStreamWritten(string.Format(format, arg), false);
            }

            public override void Write(string value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value, false);
            }

            public override void Write(uint value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }

            public override void Write(ulong value)
            {
                m_BaseWriter.Write(value);
                m_onStreamWritten(value.ToString(), false);
            }
            #endregion
            #region WriteLine
            public override void WriteLine()
            {
                m_BaseWriter.WriteLine();
                m_onStreamWritten(NewLine, true);
            }

            public override void WriteLine(char value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(bool value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(char[] buffer)
            {
                m_BaseWriter.WriteLine(buffer);
                m_onStreamWritten(new string(buffer), true);
            }

            public override void WriteLine(decimal value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(double value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(float value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(int value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(long value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(object value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(string format, object arg0)
            {
                m_BaseWriter.WriteLine(format, arg0);
                m_onStreamWritten(string.Format(format, arg0), true);
            }

            public override void WriteLine(string format, object arg0, object arg1)
            {
                m_BaseWriter.WriteLine(format, arg0, arg1);
                m_onStreamWritten(string.Format(format, arg0, arg1), true);
            }

            public override void WriteLine(string format, object arg0, object arg1, object arg2)
            {
                m_BaseWriter.WriteLine(format, arg0, arg1, arg2);
                m_onStreamWritten(string.Format(format, arg0, arg1, arg2), true);
            }

            public override void WriteLine(string format, params object[] arg)
            {
                m_BaseWriter.WriteLine(format, arg);
                m_onStreamWritten(string.Format(format, arg), true);
            }

            public override void WriteLine(char[] buffer, int index, int count)
            {
                m_BaseWriter.WriteLine(buffer, index, count);
                char[] b = new char[count];
                Array.Copy(buffer, index, b, 0, count);
                m_onStreamWritten(new string(b), true);
            }

            public override void WriteLine(uint value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(ulong value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value.ToString(), true);
            }

            public override void WriteLine(string value)
            {
                m_BaseWriter.WriteLine(value);
                m_onStreamWritten(value, true);
            }
            #endregion
            #endregion
        }
        #endregion
        #region Variables
        /// <summary>
        /// Used for forwarding writes to the error stream.
        /// </summary>
        private StreamForwarder m_ErrorForwarder;

        /// <summary>
        /// Used for forwarding writes to the console stream.
        /// </summary>
        private StreamForwarder m_ConsoleForwarder;

        /// <summary>
        /// Used for listening to writes to the debug stream.
        /// </summary>
        private StreamListener m_DebugListener;

        /// <summary>
        /// Used for listening to writes to the trace stream.
        /// </summary>
        private StreamListener m_TraceListener;

        /// <summary>
        /// The owning client plugin.
        /// </summary>
        private ClientLogPlugin m_ClientPlugin;

        /// <summary>
        /// Indicates whether or not first chance exceptions are being logged.
        /// </summary>
        private bool m_FirstChanceEnabled;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the StreamLogger class.
        /// </summary>
        private StreamLogger()
        {
            m_ErrorForwarder = new StreamForwarder(StreamType.Error, OnError);
            m_ConsoleForwarder = new StreamForwarder(StreamType.Console, OnConsole);
            m_DebugListener = new StreamListener(StreamType.Debug, OnDebug);
            m_TraceListener = new StreamListener(StreamType.Trace, OnTrace);
        }

        /// <summary>
        /// Initializes static members of the StreamLogger class.
        /// </summary>
        static StreamLogger()
        {
            Singleton<StreamLogger>.ValueFactory = () => new StreamLogger();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Initializes this instance of the StreamLogger class.
        /// </summary>
        public bool Initialize(ClientLogPlugin plugin)
        {
            m_ClientPlugin = plugin;
            return true;
        }

        /// <summary>
        /// Called when a message is written to the serror stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="newLine"></param>
        private void OnError(string value, bool newLine)
        {
            string realValue = m_ErrorForwarder.AppendBuffer(value + (newLine ? m_ErrorForwarder.NewLine : string.Empty));
            if (!string.IsNullOrEmpty(realValue))
            {
                OnLogged(new StreamLoggingEvent(
                    CKLogExtension.Interface.LoggingLevels.Error,
                    "[Error]", realValue, null));
            }
        }

        /// <summary>
        /// Called when a message is written to the console stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="newLine"></param>
        private void OnConsole(string value, bool newLine)
        {
            string realValue = m_ConsoleForwarder.AppendBuffer(value + (newLine ? m_ConsoleForwarder.NewLine : string.Empty));
            if (!string.IsNullOrEmpty(realValue))
            {
                OnLogged(new StreamLoggingEvent(
                    CKLogExtension.Interface.LoggingLevels.Info,
                    "[Console]", realValue, null));
            }
        }

        /// <summary>
        /// Called when a message is written to the debug stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="newLine"></param>
        private void OnDebug(string value, bool newLine)
        {
            string realValue = m_DebugListener.AppendBuffer(value + (newLine ? Environment.NewLine : string.Empty));
            if (!string.IsNullOrEmpty(realValue))
            {
                OnLogged(new StreamLoggingEvent(
                    CKLogExtension.Interface.LoggingLevels.Debug,
                    "[Debug]", realValue, null));
            }
        }

        /// <summary>
        /// Called when a message is written to the trace stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="newLine"></param>
        private void OnTrace(string value, bool newLine)
        {
            string realValue = m_TraceListener.AppendBuffer(value + (newLine ? Environment.NewLine : string.Empty));
            if (!string.IsNullOrEmpty(realValue))
            {
                OnLogged(new StreamLoggingEvent(
                    CKLogExtension.Interface.LoggingLevels.Trace,
                    "[Trace]", realValue, null));
            }
        }

        /// <summary>
        /// Called when a message has been logged.
        /// </summary>
        /// <param name="logEvt"></param>
        private void OnLogged(IBasicLoggingEvent logEvt)
        {
            LogMessage msg = new LogMessage(logEvt);
            if (m_ClientPlugin.LoggerSettings.CollectFullTrace)
            {
                msg.FullTrace = new RemoteStackTrace(new StackTrace(2, true));
            }

            m_ClientPlugin.Messenger.SendNetworkMessage(msg);
        }

        /// <summary>
        /// Registers for first chance exceptions when called from .Net 4.0+ code.
        /// </summary>
        /// <param name="register">True to add a handler, false to remove the handler.</param>
        private void RegisterForFirstChanceExceptions(bool register)
        {
            m_FirstChanceEnabled = false;
            IReportFirstChanceExceptions m_appDomainProxy = null;
            if (m_appDomainProxy == null)
            {
                m_appDomainProxy = m_ClientPlugin.ProxyFactory.CreateProxy<IReportFirstChanceExceptions>(AppDomain.CurrentDomain, false);
            }

            if (m_appDomainProxy != null)
            {
                m_appDomainProxy.FirstChanceException -= m_appDomainProxy_FirstChanceException;
                if (register)
                {
                    m_FirstChanceEnabled = true;
                    m_appDomainProxy.FirstChanceException += m_appDomainProxy_FirstChanceException;
                }
            }
        }

        /// <summary>
        /// Called when a first chance exception is thrown from managed code.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_appDomainProxy_FirstChanceException(object sender, EventArgs e)
        {
            IHaveAnException args = m_ClientPlugin.ProxyFactory.CreateProxy<IHaveAnException>(e, false);
            if (args != null)
            {
                var exception = args.Exception;
                if (exception != null)
                {
                    OnLogged(new StreamLoggingEvent(LoggingLevels.Error, "[First Chance Exception]", exception.Message, exception));
                }
            }
        }

        /// <summary>
        /// Releases of resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            this[StreamType.Console] = false;
            this[StreamType.Debug] = false;
            this[StreamType.Error] = false;
            this[StreamType.Fatal] = false;
            this[StreamType.Trace] = false;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        public static StreamLogger Instance
        {
            get
            {
                return Singleton<StreamLogger>.Instance;
            }
        }

        /// <summary>
        /// Gets or sets the logging activation of the basic stream types.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool this[StreamType stream]{
            get
            {
                switch (stream)
                {
                    case StreamType.Debug:
                        return m_DebugListener.Active;
                    case StreamType.Trace:
                        return m_TraceListener.Active;
                    case StreamType.Console:
                        return m_ConsoleForwarder.Active;
                    case StreamType.Error:
                        return m_ErrorForwarder.Active;
                    case StreamType.Fatal:
                        return m_FirstChanceEnabled;
                    default:
                        return false;
                }
            }

            set
            {
                switch (stream)
                {
                    case StreamType.Debug:
                        m_DebugListener.Active = value;
                        break;
                    case StreamType.Trace:
                        m_TraceListener.Active = value;
                        break;
                    case StreamType.Console:
                        m_ConsoleForwarder.Active = value;
                        break;
                    case StreamType.Error:
                        m_ErrorForwarder.Active = value;
                        break;
                    case StreamType.Fatal:
                        RegisterForFirstChanceExceptions(value);
                        break;
                }
            }
        }
        #endregion
    }
}
