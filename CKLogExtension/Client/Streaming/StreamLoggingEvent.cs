﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface.log4net;
using CKLogExtension.Client.Interface;
using CKLogExtension.Interface;

namespace CKLogExtension.Client.Streaming
{
    public class StreamLoggingEvent : IBasicLoggingEvent
    {
        public StreamLoggingEvent(LoggingLevels level, string loggerName, string message, Exception exception)
        {
            TimeStamp = DateTime.UtcNow;
            ExceptionObject = exception;
            Level = level;
            LoggerName = loggerName;
            RenderedMessage = message;
            Domain = AppDomain.CurrentDomain.FriendlyName;
        }

        public Exception ExceptionObject
        {
            get;
            private set;
        }

        public LoggingLevels Level
        {
            get;
            private set;
        }

        public string LoggerName
        {
            get;
            private set;
        }

        public string Domain
        {
            get;
            private set;
        }

        public string RenderedMessage
        {
            get;
            private set;
        }

        public DateTime TimeStamp
        {
            get;
            private set;
        }
    }
}
