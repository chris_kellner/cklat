﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface.log4net;
using CKLib.Utilities.Connection;
using CKLib.Utilities.Proxy;
using System.Diagnostics;
using CKLogExtension.Messages;

namespace CKLogExtension.Client
{
    public class SenderAppender
    {
        private IMessenger m_Messenger;
        private Log4NetPlugin m_plugin;

        public SenderAppender(IMessenger messenger)
        {
            m_Messenger = messenger;
            m_plugin = Log4NetPlugin.Instance;
        }
        #region IAppender Members

        public void Close()
        {
            m_Messenger = null;
            m_plugin = null;
        }

        public void DoAppend(object loggingEvent)
        {
            if (m_Messenger != null && m_plugin != null)
            {
                var logEvt = ProxyFactory.Instance.CreateProxy<ILoggingEvent>(loggingEvent, false);
                if (logEvt != null)
                {
                    var msg = new LogMessage(logEvt);
                    if (m_plugin.CollectFullTrace)
                    {
                        msg.FullTrace = new RemoteStackTrace(new StackTrace(7, true));
                    }

                    m_Messenger.SendNetworkMessage(msg);
                }
            }
        }

        public string Name
        {
            get;
            set;
        }

        #endregion

        public object MakeProxy(IProxyFactory factory)
        {
            Type iAppenderType = FactoryHelpers.FindType("log4net.Appender.IAppender");
            if (iAppenderType != null)
            {
                return factory.CreateProxy(this, iAppenderType, false);
            }

            return null;
        }
    }
}
