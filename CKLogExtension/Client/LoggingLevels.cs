﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface
{
    public enum LoggingLevels : int
    {
        Invalid = 0,
        Off = 2147483647,
        Emergency = 120000,
        Fatal = 110000,
        Alert = 100000,
        Critical = 90000,
        Severe = 80000,
        Error = 70000,
        Warn = 60000,
        Notice = 50000,
        Info = 40000,
        Debug = 30000,
        Fine = 30001,
        Trace = 20000,
        Finer = 20001,
        Verbose = 10000,
        Finest = 10001,
        All = -2147483648
    }
}
