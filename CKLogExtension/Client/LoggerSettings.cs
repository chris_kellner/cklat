﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface;
using System.ComponentModel;

namespace CKLogExtension.Client
{
    public class LoggerSettings : INotifyPropertyChanged, ILoggerSettings
    {
        private bool m_OverrideLogLevel;
        private LoggingLevels m_LogLevel;
        private bool m_CollectFullTrace;
        private bool m_LogFirstChanceExceptions;
        private bool m_LogDebugStream;
        private bool m_LogTraceStream;
        private bool m_LogConsoleStream;
        private bool m_LogErrorStream;
        private ClientLogPlugin m_Plugin;
        #region Constructors
        /// <summary>
        /// Initializes a new instnace of the LoggerSettings class.
        /// </summary>
        /// <param name="plugin"></param>
        public LoggerSettings(ClientLogPlugin plugin)
        {
            m_Plugin = plugin;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the forced logging level.
        /// </summary>
        public LoggingLevels LogLevel
        {
            get
            {
                return m_LogLevel;
            }

            set
            {
                m_LogLevel = value;
                OnPropertyChanged("LogLevel");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to override the default log level.
        /// </summary>
        public bool OverrideLogLevel
        {
            get
            {
                return m_OverrideLogLevel;
            }

            set
            {
                m_OverrideLogLevel = value;
                OnPropertyChanged("OverrideLogLevel");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to collect full stack traces with each log entry.
        /// </summary>
        public bool CollectFullTrace
        {
            get
            {
                return m_CollectFullTrace;
            }

            set
            {
                m_CollectFullTrace = value;
                OnPropertyChanged("CollectFullTrace");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to forward the standard debug stream.
        /// </summary>
        public bool LogDebugStream
        {
            get
            {
                return m_LogDebugStream;
            }

            set
            {
                m_LogDebugStream = value;
                OnPropertyChanged("LogDebugStream");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to forward the standard trace stream.
        /// </summary>
        public bool LogTraceStream
        {
            get
            {
                return m_LogTraceStream;
            }

            set
            {
                m_LogTraceStream = value;
                OnPropertyChanged("LogTraceStream");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to forward the standard console stream.
        /// </summary>
        public bool LogConsoleStream
        {
            get
            {
                return m_LogConsoleStream;
            }

            set
            {
                m_LogConsoleStream = value;
                OnPropertyChanged("LogConsoleStream");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to forward the standard error stream.
        /// </summary>
        public bool LogErrorStream
        {
            get
            {
                return m_LogErrorStream;
            }

            set
            {
                m_LogErrorStream = value;
                OnPropertyChanged("LogErrorStream");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to log first chance exceptions.
        /// </summary>
        public bool LogFirstChanceExceptions
        {
            get
            {
                return m_LogFirstChanceExceptions;
            }

            set
            {
                m_LogFirstChanceExceptions = value;
                OnPropertyChanged("LogFirstChanceExceptions");
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Forces the client to rescan for new loggers.
        /// </summary>
        public void ForceScan()
        {
            m_Plugin.ScanForLoggers();
        }

        /// <summary>
        /// Called when a property changes.
        /// </summary>
        /// <param name="propertyName">The name of the changed property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
