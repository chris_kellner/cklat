﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface
{
    public interface ILoggerSettings
    {
        LoggingLevels LogLevel { get; set; }
        bool OverrideLogLevel { get; set; }
        bool CollectFullTrace { get; set; }
        bool LogFirstChanceExceptions { get; set; }
        bool LogDebugStream { get; set; }
        bool LogTraceStream { get; set; }
        bool LogConsoleStream { get; set; }
        bool LogErrorStream { get; set; }
        void ForceScan();
    }
}
