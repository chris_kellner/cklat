﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Client.Interface
{
    public interface IReportFirstChanceExceptions
    {
        /// <summary>
        /// Occurs when an exception is raised from managed code before the CLR has a chance to search for exception handlers.
        /// </summary>
        event EventHandler<EventArgs> FirstChanceException;
    }
}
