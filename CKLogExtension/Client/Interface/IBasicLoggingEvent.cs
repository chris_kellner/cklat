﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface;

namespace CKLogExtension.Client.Interface
{
    public interface IBasicLoggingEvent
    {
        Exception ExceptionObject { get; }
        LoggingLevels Level { get; }
        string LoggerName { get; }
        string Domain { get; }
        string RenderedMessage { get; }
        DateTime TimeStamp { get; }
    }
}
