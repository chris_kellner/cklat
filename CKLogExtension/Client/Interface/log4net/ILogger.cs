﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    /// <summary>
    /// This interface wraps the Logger class from log4net and is not intended to be passed back to log4net.
    /// </summary>
    public interface ILogger
    {
        string Name { get; }
        object Repository { get; }

        void AddAppender(object appender);
        object GetAppender(string name);
        bool IsEnabledFor(object level);
        //void Log(ILoggingEvent logEvent);
        object Level { get; set; }
        void Log(Type callerStackBoundaryDeclaringType, object level, object message, Exception exception);
    }
}
