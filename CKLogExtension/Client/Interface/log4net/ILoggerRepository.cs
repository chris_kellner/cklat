﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface ILoggerRepository
    {
        bool Configured { get; set; }
        string Name { get; set; }
        IPluginMap PluginMap { get; }

        object Threshold { get; set; }
        event EventHandler<EventArgs> LoggerCreatedEvent;

        ILogger Exists(string name);
        Array GetAppenders();
        Array GetCurrentLoggers();
        ILogger GetLogger(string name);
        ILogger Root { get; }
        void Log(ILoggingEvent logEvent);
    }
}
