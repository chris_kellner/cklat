﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;

namespace CKLogExtension.Interface.log4net
{
    public interface ILoggingEvent
    {
        string Domain { get; }
        Exception ExceptionObject { get; }
        string Identity { get; }
        object Level { get; }
        ILocationInfo LocationInformation { get; }
        string LoggerName { get; }
        object MessageObject { get; }
        string RenderedMessage { get; }
        ILoggerRepository Repository { get; }
        DateTime TimeStamp { get; }
        string UserName { get; }

        string GetExceptionString();
        ILoggingEventData GetLoggingEventData();
        object LookupProperty(string key);
    }
}
