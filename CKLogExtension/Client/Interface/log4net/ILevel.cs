﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface ILevel
    {
        string DisplayName { get; }
        string Name { get; }
        int Value { get; }
        int CompareTo(object otherLevel);
    }
}
