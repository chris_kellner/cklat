﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface ILoggingEventData
    {
        string Domain { get; }
        string ExceptionString { get; }
        string Identity { get; }
        //Level Level;
        ILocationInfo LocationInfo { get; }
        string LoggerName { get; }
        string Message { get; }
        string ThreadName { get; }
        DateTime TimeStamp { get; }
        string UserName { get; }
    }
}
