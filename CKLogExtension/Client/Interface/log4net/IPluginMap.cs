﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CKLogExtension.Interface.log4net
{
    public interface IPluginMap
    {
        void Add(object plugin);
        void Remove(object plugin);
        IList AllPlugins { get; }
    }
}
