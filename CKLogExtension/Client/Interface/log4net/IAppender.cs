﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface IAppender
    {
        string Name { get; set; }

        void Close();
        void DoAppend(ILoggingEvent loggingEvent);
    }
}
