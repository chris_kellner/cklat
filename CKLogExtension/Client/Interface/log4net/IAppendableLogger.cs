﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface IAppendableLogger : ILogger
    {
        void AddAppender(object appender);
    }
}
