﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Interface.log4net
{
    public interface ILocationInfo
    {
        string ClassName { get; }
        string FileName { get; }
        string FullInfo { get; }
        string LineNumber { get; }
        string MethodName { get; }
    }
}
