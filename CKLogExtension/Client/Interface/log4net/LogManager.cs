﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy;
using System.Reflection;

namespace CKLogExtension.Interface.log4net
{
    public class LogManager
    {
        private static Type m_LogManagerType;
        private static MethodInfo m_GetLoggerMethod;
        private static MethodInfo m_GetCurrentLoggersMethod;
        private static MethodInfo m_GetAllRepositoriesMethod;
        private static bool m_IsValid;
        private static IProxyFactory m_ProxyFactory;

        public static bool IsValid
        {
            get
            {
                return EnsureState();
            }
        }

        public static IProxyFactory ProxyFactory
        {
            get
            {
                return m_ProxyFactory;
            }
            set
            {
                m_ProxyFactory = value;
            }
        }

        static LogManager(){
            EnsureState();
        }

        public static bool EnsureState()
        {
            if (!m_IsValid)
            {
                m_LogManagerType = FactoryHelpers.FindType("log4net.LogManager");
                if (m_LogManagerType != null)
                {
                    m_IsValid = true;
                    m_GetLoggerMethod = m_LogManagerType.GetMethod("GetLogger", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string), typeof(string) }, null);
                    m_GetCurrentLoggersMethod = m_LogManagerType.GetMethod("GetCurrentLoggers", BindingFlags.Public | BindingFlags.Static, null, Type.EmptyTypes, null);
                    m_GetAllRepositoriesMethod = m_LogManagerType.GetMethod("GetAllRepositories", BindingFlags.Public | BindingFlags.Static, null, Type.EmptyTypes, null);
                }
            }

            return m_IsValid;
        }

        public static ILog GetLogger(string repoName, string loggerName)
        {
            if (m_GetLoggerMethod != null && m_ProxyFactory != null)
            {
                object iLogObj = m_GetLoggerMethod.Invoke(null, new object[] { repoName, loggerName });
                if (iLogObj != null)
                {
                    return m_ProxyFactory.CreateProxy<ILog>(iLogObj, false);
                }
            }

            return null;
        }

        public static ILog[] GetCurrentLoggers()
        {
            if (m_GetCurrentLoggersMethod != null && m_ProxyFactory != null)
            {
                var iLogObjs = m_GetCurrentLoggersMethod.Invoke(null, new object[] { }) as Array;
                if (iLogObjs != null && iLogObjs.Length > 0)
                {
                    ILog[] returnLogs = new ILog[iLogObjs.Length];
                    for (int i = 0; i < returnLogs.Length; i++)
                    {
                        returnLogs[i] = m_ProxyFactory.CreateProxy<ILog>(iLogObjs.GetValue(i), false);
                    }
                    return returnLogs;
                }
            }

            return new ILog[0];
        }

        public static ILoggerRepository[] GetAllRepositories()
        {
            if (m_GetAllRepositoriesMethod != null && m_ProxyFactory != null)
            {
                var iLogObjs = m_GetAllRepositoriesMethod.Invoke(null, new object[] { }) as Array;
                if (iLogObjs != null && iLogObjs.Length > 0)
                {
                    ILoggerRepository[] returnLogs = new ILoggerRepository[iLogObjs.Length];
                    for (int i = 0; i < returnLogs.Length; i++)
                    {
                        returnLogs[i] = m_ProxyFactory.CreateProxy<ILoggerRepository>(iLogObjs.GetValue(i), false);
                    }
                    return returnLogs;
                }
            }

            return new ILoggerRepository[0];
        }
    }
}
