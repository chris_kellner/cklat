﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy;

namespace CKLogExtension.Interface.log4net
{
    public static class Level
    {
        #region Variables
        private static Type m_LevelType;
        private static object m_Off;
        private static object m_Emergency;
        private static object m_Fatal;
        private static object m_Alert;
        private static object m_Critical;
        private static object m_Severe;
        private static object m_Error;
        private static object m_Warn;
        private static object m_Notice;
        private static object m_Info;
        private static object m_Debug;
        private static object m_Fine;
        private static object m_Trace;
        private static object m_Finer;
        private static object m_Verbose;
        private static object m_Finest;
        private static object m_All;
        #endregion
        #region Constructors
        static Level()
        {
            m_LevelType = FactoryHelpers.FindType("log4net.Core.Level");
            if (m_LevelType != null)
            {
                var fieldInfo = m_LevelType.GetField("Off", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Off = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Emergency", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Emergency = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Fatal", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Fatal = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Alert", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Alert = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Critical", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Critical = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Severe", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Severe = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Error", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Error = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Warn", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Warn = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Notice", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Notice = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Info", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Info = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Debug", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Debug = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Fine", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Fine = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Trace", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Trace = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Finer", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Finer = fieldInfo.GetValue(null);
                }
                fieldInfo = m_LevelType.GetField("Verbose", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Verbose = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("Finest", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_Finest = fieldInfo.GetValue(null);
                }

                fieldInfo = m_LevelType.GetField("All", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (fieldInfo != null)
                {
                    m_All = fieldInfo.GetValue(null);
                }
            }
        }
        #endregion
        #region Properties
        public static object Off { get { return m_Off; } }
        public static object Emergency { get { return m_Emergency; } }
        public static object Fatal { get { return m_Fatal; } }
        public static object Alert { get { return m_Alert; } }
        public static object Critical { get { return m_Critical; } }
        public static object Severe { get { return m_Severe; } }
        public static object Error { get { return m_Error; } }
        public static object Warn { get { return m_Warn; } }
        public static object Notice { get { return m_Notice; } }
        public static object Info { get { return m_Info; } }
        public static object Debug { get { return m_Debug; } }
        public static object Fine { get { return m_Fine; } }
        public static object Trace { get { return m_Trace; } }
        public static object Finer { get { return m_Finer; }}
        public static object Verbose { get { return m_Verbose; } }
        public static object Finest { get { return m_Finest; } }
        public static object All { get { return m_All; } }
        #endregion
        #region Methods
        /// <summary>
        /// Converts a log4net Level object into the LoggingLevels enum.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static LoggingLevels LevelToLoggingLevels(object level)
        {
            if (m_LevelType != null && m_LevelType.IsInstanceOfType(level))
            {
                ILevel lvl = ProxyFactory.Instance.CreateProxy<ILevel>(level, false);
                if (lvl != null)
                {
                    if (Enum.IsDefined(typeof(LoggingLevels), lvl.Value))
                    {
                        if (lvl.Value < (int)LoggingLevels.Info)
                        {
                            switch (lvl.Name)
                            {
                                case "FINE":
                                    return LoggingLevels.Fine;
                                case "FINER":
                                    return LoggingLevels.Finer;
                                case "FINEST":
                                    return LoggingLevels.Finest;
                            }
                        }

                        return (LoggingLevels)lvl.Value;
                    }
                }
            }

            return LoggingLevels.Off;
        }

        /// <summary>
        /// Provides a Level object for use by log4net.
        /// </summary>
        /// <param name="lvl"></param>
        /// <returns></returns>
        public static object LoggingLevelsToLevel(LoggingLevels lvl)
        {
            switch (lvl)
            {
                case LoggingLevels.All:
                    return All;
                case LoggingLevels.Finest:
                    return Finest;
                case LoggingLevels.Verbose:
                    return Verbose;
                case LoggingLevels.Finer:
                    return Finer;
                case LoggingLevels.Trace:
                    return Trace;
                case LoggingLevels.Fine:
                    return Fine;
                case LoggingLevels.Debug:
                    return Debug;
                case LoggingLevels.Info:
                    return Info;
                case LoggingLevels.Notice:
                    return Notice;
                case LoggingLevels.Warn:
                    return Warn;
                case LoggingLevels.Error:
                    return Error;
                case LoggingLevels.Severe:
                    return Severe;
                case LoggingLevels.Critical:
                    return Critical;
                case LoggingLevels.Alert:
                    return Alert;
                case LoggingLevels.Fatal:
                    return Fatal;
                case LoggingLevels.Emergency:
                    return Emergency;
                default:
                    return Off;
            }
        }
        #endregion
    }
}
