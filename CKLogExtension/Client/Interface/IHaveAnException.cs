﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLogExtension.Client.Interface
{
    public interface IHaveAnException
    {
        /// <summary>
        /// Gets the exception object.
        /// </summary>
        Exception Exception { get; }
    }
}
