﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Connection.Extensions;
using CKLib.Utilities.Connection;
using CKLib.Utilities.Proxy;
using CKLogExtension.Messages;
using CKLib.Utilities.Patterns;
using CKLogExtension.Interface.log4net;
using CKLib.Utilities.Security;
using CKLogExtension.Interface;
using CKLogExtension.Client.Interface;

namespace CKLogExtension.Client
{
    [ClientSideExtension]
    public class ClientLogPlugin : IExtension
    {
        #region Variables
        /// <summary>
        /// Reference to the IMessenger owning this extension.
        /// </summary>
        private IMessenger m_Messenger;

        /// <summary>
        /// Reference to an IProxyFactory used for making type proxies.
        /// </summary>
        private IProxyFactory m_ProxyFactory;

        /// <summary>
        /// A set of connections which are allowed to communicate with this extension.
        /// </summary>
        private SynchronizedCollection<Guid> m_AllowedConnections;

        /// <summary>
        /// The settings which are used to customize and remotely control this extension.
        /// </summary>
        private LoggerSettings m_LoggerSettings;
        #endregion
        /// <summary>
        /// Initializes a new instance of the ClientLog4NetPlugin class.
        /// </summary>
        public ClientLogPlugin()
        {
            m_AllowedConnections = new SynchronizedCollection<Guid>();
            m_LoggerSettings = new LoggerSettings(this);
            m_LoggerSettings.PropertyChanged += m_LoggerSettings_PropertyChanged;
        }

        /// <summary>
        /// Called when the logger settings have been adjusted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_LoggerSettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Log4NetPlugin.Instance.UpdateSettings(m_LoggerSettings);
            switch (e.PropertyName)
            {
                case "LogDebugStream":
                    StreamLogger.Instance[StreamLogger.StreamType.Debug] = m_LoggerSettings.LogDebugStream;
                    break;
                case "LogTraceStream":
                    StreamLogger.Instance[StreamLogger.StreamType.Trace] = m_LoggerSettings.LogTraceStream;
                    break;
                case "LogConsoleStream":
                    StreamLogger.Instance[StreamLogger.StreamType.Console] = m_LoggerSettings.LogConsoleStream;
                    break;
                case "LogErrorStream":
                    StreamLogger.Instance[StreamLogger.StreamType.Error] = m_LoggerSettings.LogErrorStream;
                    break;
                case "LogFirstChanceExceptions":
                    StreamLogger.Instance[StreamLogger.StreamType.Fatal] = m_LoggerSettings.LogFirstChanceExceptions;
                    break;
            }
        }

        #region Properties
        /// <summary>
        /// Gets the IMessenger that owns this extension.
        /// </summary>
        public IMessenger Messenger
        {
            get
            {
                return m_Messenger;
            }
        }

        /// <summary>
        /// Gets an IProxyFactory used for making type proxies.
        /// </summary>
        public IProxyFactory ProxyFactory
        {
            get
            {
                return m_ProxyFactory;
            }
        }

        /// <summary>
        /// Gets the settings for the logger.
        /// </summary>
        public LoggerSettings LoggerSettings
        {
            get
            {
                return m_LoggerSettings;
            }
        }
        #endregion

        #region IExtension Members

        public string ExtensionName
        {
            get { return "Log Viewer"; }
        }

        public bool Initialize(IMessenger messenger, IProxyFactory proxyFactory)
        {
            m_Messenger = messenger;
            m_ProxyFactory = proxyFactory;
            bool valid = false;
            new Log4NetPlugin(this);

            if (StreamLogger.Instance.Initialize(this))
            {
                valid = true;
            }

            LogManager.ProxyFactory = m_ProxyFactory;
            valid |= ScanForLoggers();
            m_Messenger.SharedObjectPool.ShareObject<ILoggerSettings>(m_LoggerSettings);

            return valid;
        }

        public bool ScanForLoggers()
        {
            bool valid = false;
            if (LogManager.IsValid)
            {
                Log4NetPlugin.Instance.Scan();
                valid = true;
            }

            return valid;
        }

        public Type[] GetCustomMessageTypes()
        {
            return new Type[]
            {
                typeof(LogMessage)
            };
        }

        public bool EnableConnectionAvailability(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(ClientLogPlugin), m_Messenger, connectionId))
            {
                if (!m_AllowedConnections.Contains(connectionId))
                {
                    m_AllowedConnections.Add(connectionId);
                }

                return true;
            }

            return false;
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            m_AllowedConnections.Remove(connectionId);
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_AllowedConnections;
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            return m_AllowedConnections.Contains(connectionId);
        }

        public object UserInterface
        {
            get { return null; }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
