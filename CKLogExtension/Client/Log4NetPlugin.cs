﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLogExtension.Interface.log4net;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Patterns;
using CKLib.Utilities.Messages;
using CKLogExtension.Interface;
using CKLib.Utilities.Diagnostics;

namespace CKLogExtension.Client
{
    public class Log4NetPlugin
    {
        private ClientLogPlugin m_Client;
        private string m_Name;
        private bool m_CollectFullTrace;
        /// <summary>
        /// Reference to a root log4net logger which can be used for logging.
        /// </summary>
        private ILogger m_RootLogger;

        #region PluginSkeleton Members
        public Log4NetPlugin(ClientLogPlugin plugin)
        {
            m_Client = plugin;
            m_Name = "Log4NetPlugin" + Guid.NewGuid();

            Singleton<Log4NetPlugin>.ValueFactory = () => this;
        }

        public static Log4NetPlugin Instance
        {
            get
            {
                if (Singleton<Log4NetPlugin>.IsValueAssigned || Singleton<Log4NetPlugin>.ValueFactory != null)
                {
                    return Singleton<Log4NetPlugin>.Instance;
                }

                return null;
            }
        }

        public bool CollectFullTrace
        {
            get
            {
                return m_CollectFullTrace;
            }

            set
            {
                m_CollectFullTrace = value;
            }
        }

        public void UpdateSettings(LoggerSettings settings)
        {
            m_CollectFullTrace = settings.CollectFullTrace;

            foreach (var lgr in m_Loggers)
            {
                if (lgr.Value != null)
                {
                    ILogger proxy = null;
                    if (lgr.Value.Logger is ILogger)
                    {
                        proxy = lgr.Value.Logger as ILogger;
                    }
                    else
                    {
                        proxy = m_Client.ProxyFactory.CreateProxy<ILogger>(lgr.Value.Logger, true);
                    }

                    if (proxy != null)
                    {
                        UpdateLoggingLevel(settings, proxy);
                    }
                }
            }
        }

        private Dictionary<string, ILog> m_Loggers = new Dictionary<string, ILog>();

        public object MakeProxy(IProxyFactory factory)
        {
            Type iPluginType = FactoryHelpers.FindType("log4net.Plugin.IPlugin");
            if (iPluginType != null)
            {
                return factory.CreateProxy(this, iPluginType, true);
            }

            return null;
        }

        public void Scan()
        {
            var repositories = LogManager.GetAllRepositories();
            for (int i = 0; i < repositories.Length; i++)
            {
                var repo = repositories[i];
                if (repo != null)
                {
                    if (!repo.PluginMap.AllPlugins.Contains(Proxy))
                    {
                        repo.PluginMap.Add(Proxy);
                    }
                }
            }

            var loggers = LogManager.GetCurrentLoggers();
            for (int i = 0; i < loggers.Length; i++)
            {
                var lgr = loggers[i].Logger;
                if (lgr != null)
                {
                    AddAppender(lgr);
                }
            }
        }

        public void Log(Type callerStacKBoundaryDeclaringType, LoggingLevels level, object message, Exception ex)
        {
            if (m_RootLogger == null)
            {
                var repositories = LogManager.GetAllRepositories();
                for (int i = 0; i < repositories.Length; i++)
                {
                    var repo = repositories[i];
                    if (repo != null)
                    {
                        var root = repo.Root;
                        if (root != null)
                        {
                            m_RootLogger = root;
                            break;
                        }
                    }
                }
            }

            if (m_RootLogger != null)
            {
                m_RootLogger.Log(callerStacKBoundaryDeclaringType, Level.LoggingLevelsToLevel(level), message, ex);
            }
        }

        private void UpdateLoggingLevel(LoggerSettings settings, ILogger logger)
        {
            object level = Level.LoggingLevelsToLevel(settings.LogLevel);
            if (level != null)
            {
                if (settings.OverrideLogLevel && logger != null)
                {
                    var repositories = LogManager.GetAllRepositories();
                    for (int i = 0; i < repositories.Length; i++)
                    {
                        var repo = repositories[i];
                        if (repo != null)
                        {
                            repo.Threshold = level;
                            repo.Root.Level = level;
                        }
                    }

                    logger.Level = Level.LoggingLevelsToLevel(settings.LogLevel);
                    if (!logger.IsEnabledFor(level))
                    {
                        Error.WriteEntry("Failed to set log level for logger: " + logger.Name);
                    }
                }
            }
        }

        public void AddAppender(object loggr)
        {
            ILogger lgr = null;
            if (loggr is ILogger)
            {
                lgr = loggr as ILogger;
            }
            else{
                lgr = m_Client.ProxyFactory.CreateProxy<CKLogExtension.Interface.log4net.ILogger>(loggr, true);
            }

            if (lgr != null)
            {
                UpdateLoggingLevel(m_Client.LoggerSettings, lgr);

                var existingAppender = lgr.GetAppender("Sender");
                if (!m_Loggers.ContainsKey(lgr.Name) && existingAppender == null)
                {
                    SenderAppender appender = new SenderAppender(m_Client.Messenger);
                    appender.Name = "Sender";

                    lgr.AddAppender(appender.MakeProxy(m_Client.ProxyFactory));

                    ILoggerRepository repo = m_Client.ProxyFactory.CreateProxy<ILoggerRepository>(lgr.Repository, false);
                    if (repo != null)
                    {
                        m_Loggers.Add(lgr.Name, LogManager.GetLogger(repo.Name, lgr.Name));
                    }
                }
            }
        }

        public ILog GetLogger(string name)
        {
            ILog log;
            m_Loggers.TryGetValue(name, out log);
            return log;
        }

        public void Attach(object repository)
        {
            ILoggerRepository repo = m_Client.ProxyFactory.CreateProxy<ILoggerRepository>(repository, false);
            if (repo != null)
            {
                // Attach to current
                var loggers = repo.GetCurrentLoggers();
                for (int i = 0; i < loggers.Length; i++)
                {
                    AddAppender(repo.Root);
                    AddAppender(loggers.GetValue(i));
                }

                // Attach future
                repo.LoggerCreatedEvent +=new EventHandler<EventArgs>(repo_LoggerCreatedEvent);
            }
        }

        void repo_LoggerCreatedEvent(object sender, EventArgs e)
        {
            ILoggerCreatedEventArgs logEvtArgs = m_Client.ProxyFactory.CreateProxy<ILoggerCreatedEventArgs>(e, false);
            if (logEvtArgs != null)
            {
                AddAppender(logEvtArgs.Logger);
            }
        }

        void repository_ConfigurationChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region IPlugin Members

        private object m_Proxy;
        private object Proxy
        {
            get
            {
                if (m_Proxy == null)
                {
                    m_Proxy = MakeProxy(m_Client.ProxyFactory);
                }

                return m_Proxy;
            }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public void Shutdown()
        {
            m_RootLogger = null;
            m_Proxy = null;
            m_Loggers.Clear();
            //throw new NotImplementedException();
        }

        #endregion
    }
}
