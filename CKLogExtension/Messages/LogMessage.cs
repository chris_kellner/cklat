﻿

namespace CKLogExtension.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Runtime.Serialization;
    using System.Text;
    using CKLib.Utilities.Messages;
    using CKLogExtension.Client.Interface;
    using CKLogExtension.Interface;
    using CKLogExtension.Interface.log4net;
    #endregion
    #region Helper Classes
    [DataContract]
    public class AssemblyInfo
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AssemblyInfo class.
        /// </summary>
        public AssemblyInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the AssemblyInfo class.
        /// </summary>
        /// <param name="asm">The assembly to acuire the information from.</param>
        public AssemblyInfo(Assembly asm)
        {
            if (asm != null)
            {
                IsGlobal = asm.GlobalAssemblyCache;
                if (!IsGlobal)
                {
                    IsDynamic = asm is AssemblyBuilder;
                }

                if (!IsDynamic)
                {
                    AssemblyFileLocation = asm.Location;
                    var asmName = System.Reflection.AssemblyName.GetAssemblyName(AssemblyFileLocation);
                    AssemblyName = asmName.Name;
                    AssemblyVersion = asmName.Version.ToString();
                    var key = asmName.GetPublicKeyToken();
                    PublicKey = key != null ? GetHexString(key) : "null";
                    CultureInfo = asmName.CultureInfo.ToString();
                }
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a hex string from the input bytes.
        /// </summary>
        /// <param name="bytes">A byte array to get the hex from.</param>
        /// <returns>A hexadecimal string representing the specified bytes.</returns>
        private string GetHexString(byte[] bytes)
        {
            char[] hexValues = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
            char[] returnChars = new char[bytes.Length * 2];
            for (int i = 0, j = 0; i < bytes.Length; i++)
            {
                returnChars[j++] = hexValues[bytes[i] / 0x10];
                returnChars[j++] = hexValues[bytes[i] % 0x10];
            }

            return new string(returnChars);
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the assembly is loaded from the GAC.
        /// </summary>
        [DataMember]
        public bool IsGlobal
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the assembly is a dynamic assembly.
        /// </summary>
        [DataMember]
        public bool IsDynamic
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the assembly name.
        /// </summary>
        [DataMember]
        public string AssemblyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the assembly version.
        /// </summary>
        [DataMember]
        public string AssemblyVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the public key of the assembly.
        /// </summary>
        [DataMember]
        public string PublicKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the culture info of the assembly.
        /// </summary>
        [DataMember]
        public string CultureInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the file location where the assembly was loaded from.
        /// </summary>
        [DataMember]
        public string AssemblyFileLocation
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class LocationInfo : ILocationInfo
    {
        #region Variables
        private string m_ClassName;
        private string m_FileName;
        private string m_FullInfo;
        private string m_LineNumber;
        private string m_MethodName;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new intance of the LocationInfo class.
        /// </summary>
        public LocationInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LocationInfo class from the input copy.
        /// </summary>
        /// <param name="copy">The ILocationInfo object to copy the data out of.</param>
        public LocationInfo(ILocationInfo copy)
        {
            m_ClassName = copy.ClassName;
            m_FileName = copy.FileName;
            m_FullInfo = copy.FullInfo;
            m_LineNumber = copy.LineNumber;
            m_MethodName = copy.MethodName;
        }

        /// <summary>
        /// Initializes a new instance of the LocationInfo class.
        /// </summary>
        /// <param name="frame">The stack frame to copy the location information from.</param>
        public LocationInfo(StackFrame frame)
        {
            if (frame != null)
            {
                var method = frame.GetMethod();
                if (method != null)
                {
                    string methodName = method.Name;
                    m_MethodName = methodName;
                    Type declaringType = method.DeclaringType;
                    if (declaringType != null)
                    {
                        m_ClassName = declaringType.FullName;
                        AssemblyInfo info = new AssemblyInfo(declaringType.Assembly);
                        if (!info.IsDynamic)
                        {
                            AssemblyInfo = info;
                        }
                    }

                    var parameters = CKLib.Utilities.Proxy.FactoryHelpers.GetMethodParameterTypes(method);
                    if (parameters != null)
                    {
                        Parameters = string.Join(",", parameters.Select(p => p.FullName).ToArray());
                    }
                }

                m_FileName = frame.GetFileName();
                m_LineNumber = frame.GetFileLineNumber().ToString();
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Retrieves the location information in a human readable string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(m_FullInfo))
            {
                m_FullInfo = string.Format(
                "at {0} : {1}, in file {2} at line {3}.",
                m_ClassName,
                m_MethodName,
                m_FileName,
                m_LineNumber);
            }

            return m_FullInfo;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the name of the class declaring the method for this frame.
        /// </summary>
        [DataMember]
        public string ClassName
        {
            get
            {
                return m_ClassName;
            }

            set
            {
                m_ClassName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the file that owns the class that declares the method for this frame.
        /// </summary>
        [DataMember]
        public string FileName
        {
            get
            {
                return m_FileName;
            }

            set
            {
                m_FileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the formatted information.
        /// </summary>
        [DataMember]
        public string FullInfo
        {
            get
            {
                return m_FullInfo;
            }

            set
            {
                m_FullInfo = value;
            }
        }

        /// <summary>
        /// Gets or sets the line number in the file that owns this class that declares the method for this frame.
        /// </summary>
        [DataMember]
        public string LineNumber
        {
            get
            {
                return m_LineNumber;
            }

            set
            {
                m_LineNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the method for this frame.
        /// </summary>
        [DataMember]
        public string MethodName
        {
            get
            {
                return m_MethodName;
            }

            set
            {
                m_MethodName = value;
            }
        }

        /// <summary>
        /// Gets or sets the method parameters.
        /// </summary>
        [DataMember]
        public string Parameters
        {
            get;
            set;
        }

        [DataMember]
        public AssemblyInfo AssemblyInfo
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class RemoteException
    {
        #region Variables
        /// <summary>
        /// A string representation of the original exception.
        /// </summary>
        [DataMember]
        private string m_ToString;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RemoteException class.
        /// </summary>
        public RemoteException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the RemoteException class.
        /// </summary>
        /// <param name="ex">The exception to take the information out of.</param>
        public RemoteException(Exception ex)
        {
            if (ex != null)
            {
                if (ex.InnerException != null)
                {
                    InnerException = new RemoteException(ex.InnerException);
                }

                var type = ex.GetType();
                if (type != null)
                {
                    TypeName = type.Name;
                }

                Message = ex.Message;
                Source = ex.Source;
                HelpLink = ex.HelpLink;
                m_ToString = ex.ToString();

                if (ex.StackTrace != null)
                {
                    var stack = new System.Diagnostics.StackTrace(ex);
                    StackTrace = new RemoteStackTrace(stack);
                }
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a string representation of this remote exception
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (m_ToString != null)
            {
                return m_ToString;
            }

            return Message;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the type name of the exception.
        /// </summary>
        [DataMember]
        public string TypeName
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the stack trace for the exception.
        /// </summary>
        [DataMember]
        public RemoteStackTrace StackTrace { get; set; }
        /// <summary>
        /// Gets or sets the inner exception.
        /// </summary>
        [DataMember]
        public RemoteException InnerException { get; set; }
        /// <summary>
        /// Gets or sets the message associated with the exception.
        /// </summary>
        [DataMember]
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the source of the exception.
        /// </summary>
        [DataMember]
        public string Source { get; set; }
        /// <summary>
        /// Gets or sets the help link for the exception.
        /// </summary>
        [DataMember]
        public string HelpLink { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public IEnumerable<RemoteException> InnerExceptions
        {
            get
            {
                yield return InnerException;
            }
        }
        #endregion
    }

    [DataContract]
    public class RemoteStackTrace
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RemoteStackTrace class.
        /// </summary>
        public RemoteStackTrace()
        {
        }

        /// <summary>
        /// Initializes a new instance of the RemoteStackTrace class.
        /// </summary>
        /// <param name="stackTrace">The stack trace to copy the information from.</param>
        public RemoteStackTrace(StackTrace stackTrace)
        {
            if (stackTrace != null)
            {
                var frames = stackTrace.GetFrames();
                StackLines = new LocationInfo[frames.Length];

                for (int i = 0; i < StackLines.Length; i++)
                {
                    StackLines[i] = new LocationInfo(frames[i]);
                }
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the lines in the stack trace.
        /// </summary>
        [DataMember]
        public LocationInfo[] StackLines
        {
            get;
            set;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a string representation of the remote stack trace.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (StackLines != null && StackLines.Length > 0)
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < StackLines.Length; i++)
                {
                    builder.AppendLine(StackLines[i].ToString());
                }

                return builder.ToString();
            }

            return string.Empty;
        }
        #endregion
    }
    #endregion

    [DataContract]
    public class LogMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the LogMessage class.
        /// </summary>
        public LogMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LogMessage class.
        /// </summary>
        /// <param name="logEvt">The Log4Net logging event that occured.</param>
        public LogMessage(ILoggingEvent logEvt)
        {
            Message = logEvt.RenderedMessage;
            LocationInformation = new LocationInfo(logEvt.LocationInformation);
            TimeStamp = logEvt.TimeStamp;
            LoggerName = logEvt.LoggerName;
            Domain = logEvt.Domain;
            UserName = logEvt.UserName;
            Level = Interface.log4net.Level.LevelToLoggingLevels(logEvt.Level);
            ProcessId = System.Diagnostics.Process.GetCurrentProcess().Id;

            if (logEvt.ExceptionObject != null)
            {
                ExceptionObject = new RemoteException(logEvt.ExceptionObject);
            }
        }

        /// <summary>
        /// Initializes a new instance of the LogMessage class.
        /// </summary>
        /// <param name="logEvt">The basic logging event that occured.</param>
        public LogMessage(IBasicLoggingEvent logEvt)
        {
            Message = logEvt.RenderedMessage;
            TimeStamp = logEvt.TimeStamp;
            LoggerName = logEvt.LoggerName;
            Level = logEvt.Level;
            ProcessId = System.Diagnostics.Process.GetCurrentProcess().Id;
            Domain = logEvt.Domain;
            UserName = Environment.UserName;

            if (logEvt.ExceptionObject != null)
            {
                ExceptionObject = new RemoteException(logEvt.ExceptionObject);
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the process id that logged this message.
        /// </summary>
        [DataMember]
        public int ProcessId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the logging level that this message was logged at.
        /// </summary>
        [DataMember]
        public LoggingLevels Level
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the repository that logged this messagee.
        /// </summary>
        [DataMember]
        public string Repository
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Domain
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the username of the logged in user that logged this message.
        /// </summary>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the logger that logged this message.
        /// </summary>
        [DataMember]
        public string LoggerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the time at which this message was logged.
        /// </summary>
        [DataMember]
        public DateTime TimeStamp
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the location information of where this message was logged.
        /// </summary>
        [DataMember]
        public LocationInfo LocationInformation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [DataMember]
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the exception object.
        /// </summary>
        [DataMember]
        public RemoteException ExceptionObject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the full trace information.
        /// </summary>
        [DataMember]
        public RemoteStackTrace FullTrace
        {
            get;
            set;
        }
        #endregion
    }
}
