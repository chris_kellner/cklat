﻿using CKLib.Utilities.Proxy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CKLib.ProxyTesting.TestCases
{
    public interface ITestRefProperties
    {
        IString Data0 { get; set; }
    }

    public interface IString
    {
        IString ToUpper();
        string ToLower();
    }

    public class TestRefProperties
    {
        public string Data0 { get; set; }
        public string GetString()
        {
            return Data0.ToUpper();
        }

        public static void RunTests()
        {
            TestRefProperties subject = new TestRefProperties();
            subject.Data0 = "thisi s a test of the reference properties";
            ITestRefProperties proxy = ProxyFactory.Instance.CreateProxy<ITestRefProperties>(subject, true);
            Debug.Assert(proxy != null, "Failed to create proxy");
            Debug.Assert(proxy.Data0.ToString() == subject.Data0.ToString());

            IString replacement = proxy.Data0;
            replacement = replacement.ToUpper();
            string otherRelacer = replacement.ToLower();
            Debug.Assert(replacement.ToString() == subject.Data0.ToUpper().ToString());
            Debug.Assert(otherRelacer == subject.Data0.ToLower().ToString());

            proxy.Data0 = replacement;
            Debug.Assert(proxy.Data0.ToString() == subject.Data0.ToString());
        }
    }
}
