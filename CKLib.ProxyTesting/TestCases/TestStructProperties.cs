﻿using CKLib.Utilities.Proxy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CKLib.ProxyTesting.TestCases
{
    public interface ITestStructProperties
    {
        string GetTime();
        ITimeSpan Data0 { get; set; }
        object Data1 { get; set; }
    }

    public interface ITimeSpan
    {
        ITimeSpan Add(ITimeSpan other);
    }

    public class TestStructProperties
    {
        public TimeSpan Data0 { get; set; }
        public DateTime Data1 { get; set; }
        public string GetTime()
        {
            return (Data1 + Data0).ToString();
        }

        #region Tests
        public static void RunTests()
        {
            TestStructProperties subject = new TestStructProperties();
            subject.Data0 = TimeSpan.FromSeconds(1);
            subject.Data1 = DateTime.Now;
            string targetTime = subject.GetTime();

            // Test proxy
            ITestStructProperties proxy = ProxyFactory.Instance.CreateProxy<ITestStructProperties>(subject, true);
            Debug.Assert(proxy != null, "Failed to create proxy.");
            Debug.Assert(proxy.ToString() == subject.ToString(), "Invalid proxy created.");

            // Test timespan GET / SET
            ITimeSpan spanProxy = proxy.Data0;
            Debug.Assert(spanProxy.ToString() == subject.Data0.ToString(),
                "Data corruption detected: Structure proxy failed.");
            Debug.WriteLine(spanProxy);
            spanProxy = spanProxy.Add(ProxyFactory.Instance.CreateProxy<ITimeSpan>(TimeSpan.FromSeconds(2), true));
            Debug.WriteLine(spanProxy);
            Debug.Assert(spanProxy.ToString() == subject.Data0.Add(TimeSpan.FromSeconds(2)).ToString(),
                "Data corruption detected: Structure proxy failed.");
            proxy.Data0 = spanProxy;
            Debug.Assert(proxy.Data0.ToString() == subject.Data0.ToString(),
                "Failed to set timespan: Structure proxy failed.");
            Console.WriteLine(proxy.GetTime());

            // Test datetime -> object GET / SET
            object dateProxy = proxy.Data1;
            Debug.Assert(dateProxy != null);
            if (dateProxy != null)
            {
                Debug.Assert(dateProxy.ToString() == subject.Data1.ToString());
                proxy.Data1 = dateProxy;
                Debug.Assert(dateProxy.ToString() == subject.Data1.ToString(),
                    "Failed to set date time: Structure proxy failed.");
            }

            // Sanity check
            Debug.Assert(subject.GetTime() == proxy.GetTime());
        }
        #endregion
    }
}
