﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy.ObjectSharing;
using System.Collections;

namespace CKLib.ProxyTesting
{
    public delegate void SimpleTestWithString(string s);

    public interface IRecursiveTreeNode
    {
        string Tag { get; }
        IRectangle Value { get; set; }
        object MyRect { get; set; }
        IList Children { get; }
        IKey Key { get; set; }
        event Action<string> MyEvent;

        IRecursiveTreeNode GetChild();

        void SetTag(IRecursiveTreeNode onNode, string tag);
    }

    public interface IKey
    {
        string Value { get; set; }
    }

    public struct Key
    {
        public static readonly Key Empty = new Key(string.Empty);

        public Key(string value)
        {
            m_Value = value;
        }

        public static implicit operator Key(string value)
        {
            return new Key(value);
        }

        private string m_Value;
        public string Value
        {
            get
            {
                return m_Value;
            }

            set
            {
                m_Value = value;
            }
        }

        public override string ToString()
        {
            return Value;
        }
    }

    public class RecursiveTreeNode
    {
        public RecursiveTreeNode()
        {
            Tag = "Test";
            Key = Tag;
            Children = new List<RecursiveTreeNode>();
            Value = new Rectangle();
        }

        public event SimpleTestWithString MyEvent;

        public void RaiseMyEvent(string s)
        {
            if (MyEvent != null)
            {
                MyEvent(s);
            }
        }

        public RecursiveTreeNode GetChild()
        {
            return new RecursiveTreeNode();
        }

        public void SetTag(RecursiveTreeNode onNode, string tag)
        {
            onNode.Tag = tag;
        }

        public List<RecursiveTreeNode> Children
        {
            get;
            set;
        }

        public string Tag
        {
            get;
            set;
        }

        public Key Key
        {
            get;
            set;
        }

        public Rectangle Value
        {
            get;
            set;
        }

        public Rectangle MyRect
        {
            get
            {
                return Value;
            }

            set
            {
                Value = value;
            }
        }

        public override string ToString()
        {
            return Tag + " : " + Value;
        }
    }
}
