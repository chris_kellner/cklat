﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Proxy.ObjectSharing;
using CKLib.Utilities.Diagnostics;
using CKLib.ProxyTesting.TestCases;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Serialization;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace CKLib.ProxyTesting
{
    static class Program
    {
        static Program()
        {
            Console.SetError(Console.Out);
        }

        public static T Reserialize<T>(this T obj)
        {
            byte[] data = null;
            var serial = new BinarySerializer();
            using (var mem = new MemoryStream())
            {
                serial.Serialize(mem, obj);
                data = mem.ToArray();
            }

            T newMsg;
            using (var mem = new MemoryStream(data))
            {
                newMsg = serial.Deserialize<T>(mem);
            }

            return newMsg;
        }

        public static T ReserializeXml<T>(this T obj)
        {
            byte[] data = null;
            var serial = new System.Xml.Serialization.XmlSerializer(typeof(T));
            using (var mem = new MemoryStream())
            {
                serial.Serialize(mem, obj);
                data = mem.ToArray();
            }

            T newMsg;
            using (var mem = new MemoryStream(data))
            {
                newMsg = (T)serial.Deserialize(mem);
            }

            return newMsg;
        }


        public static T ReserializeSys<T>(this T obj)
        {
            byte[] data = null;
            var serial = new BinaryFormatter();
            using (var mem = new MemoryStream())
            {
                serial.Serialize(mem, obj);
                data = mem.ToArray();
            }

            T newMsg;
            using (var mem = new MemoryStream(data))
            {
                newMsg = (T)serial.Deserialize(mem);
            }

            return newMsg;
        }

        static void TestSerializer(int iterations)
        {
            //var msg = new MessageEnvelope(Guid.NewGuid(), new CommandLineMessage()
            //{
            //    SenderId = Guid.NewGuid(),
            //    Args = new string[]{
            //        "One", "Two", "Three"
            //    }
            //});
            //var dict = new CommandLineMessage()
            //{
            //    SenderId = Guid.NewGuid(),
            //    Args = new string[]{
            //        "One", "Two", "Three"
            //    }
            //};
            //var dict = new Dictionary<string, object>()
            //{
            //    { "hi", 12 },
            //    { "bye", "This is neato!" }
            //};
            var dict = new ArrayList(){
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  };

            var newMsg = dict.Reserialize();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                newMsg = dict.Reserialize();
            }
            sw.Stop();
            Standard.WriteEntry("CK-BIN - " + sw.Elapsed);
        }

        static void Main(string[] args)
        {
            TestSerializer(10000);
            Console.ReadKey();
            return;
            TestRefProperties.RunTests();
            TestStructProperties.RunTests();

            RecursiveTreeNode node = new RecursiveTreeNode();
            node.Children.Add(new RecursiveTreeNode());
            IRecursiveTreeNode iNode = ProxyFactory.Instance.CreateProxy<IRecursiveTreeNode>(node, true);
            iNode.MyEvent += iNode_MyEvent;
            var rect = iNode.Value;
            rect.X = 3;
            var child = iNode.GetChild();
            iNode.Value = rect;
            iNode.SetTag(child, "Tag has been set");
            node.RaiseMyEvent("My Event Has Risen!");
            Standard.WriteEntry(iNode);
            Standard.WriteEntry(child);
            Standard.WriteEntry(child.Tag);
            var obj = child as IProxyObject;
            var wrap = obj.GetWrappedObject();

            IKey mKey = child.Key;
            mKey.Value = "New Value Assigned";
            child.Key = mKey;
            string location = null;
            FactoryHelpers.ExportCommunicationModule(ref location);
            Standard.WriteEntry(child.Key);
            //
            Console.ReadLine();
        }

        static void iNode_MyEvent(string obj)
        {
            Console.WriteLine("From My Event: " + obj);
        }
    }
}
