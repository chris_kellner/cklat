﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy.ObjectSharing;

namespace CKLib.ProxyTesting
{
    public class Rectangle
    {
        public int X
        {
            get;
            set;
        }

        public int Y
        {
            get { return 12; }
        }

        public int Width
        {
            get { return int.MaxValue; }
        }

        public int Height
        {
            get { return int.MinValue; }
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2}, {3})", X, Y, Width, Height);
        }
    }
}
