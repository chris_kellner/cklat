﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.ObjectViewer
{
    #region Using List
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Reflection;
    using System.Threading;
    using System.Windows;
    using System.Windows.Threading;
    #endregion

    /// <summary>
    /// Interaction logic for ObjectViewerWindow.xaml
    /// </summary>
    public partial class ObjectViewerWindow : Window
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ObjectViewerWindow class.
        /// </summary>
        public ObjectViewerWindow()
        {
            InitializeComponent();
            var maxSize = SystemParameters.WorkArea;
            MaxHeight = maxSize.Height;
            MaxWidth = maxSize.Width;
            SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
        }
        #endregion

        #region Methods
        ///// <summary>
        ///// Creates a new object viewer window with the specified root.
        ///// </summary>
        ///// <param name="root">The root object to initialize the window with.</param>
        //public static ObjectViewerWindow Show(object root)
        //{
        //    ObjectViewerWindow window = new ObjectViewerWindow();
        //    window.AddRoot(root);
        //    window.Show();
        //    return window;
        //}

        ///// <summary>
        ///// Creates a new object viewer window with the specified root.
        ///// </summary>
        ///// <param name="root">The root object to initialize the window with.</param>
        ///// <param name="searchFlags">The BindingFlags to use when recursing through the children.</param>
        //public static ObjectViewerWindow Show(object root, BindingFlags searchFlags)
        //{
        //    ObjectViewerWindow window = new ObjectViewerWindow();
        //    window.AddRoot(root, searchFlags);
        //    window.Show();
        //    return window;
        //}

        /// <summary>
        /// Creates a new object viewer window with the specified root.
        /// </summary>
        /// <param name="root">The root object to initialize the window with.</param>
        /// <param name="searchFlags">The BindingFlags to use when recursing through the children.</param>
        /// <param name="searchAttributes">The attributes that the properties on the object must match to be included.</param>
        /// <returns>An object viewer window constructed using the specified parameters.</returns>
        public static ObjectViewerWindow Show(object root,
            BindingFlags searchFlags = BindingFlags.DeclaredOnly,
            Attribute[] searchAttributes = null,
            bool allowInvocation = true)
        {
            if (ValidateThread())
            {
                ObjectViewerWindow window = new ObjectViewerWindow();
                window.AddRoot(root, searchFlags, searchAttributes);
                window.Show();
                return window;
            }
            else if (allowInvocation)
            {
                var app = Application.Current;
                if (app != null)
                {
                    var disp = app.Dispatcher;
                    if (disp != null)
                    {
                        return disp.Invoke(
                            new Func<object, BindingFlags, Attribute[], bool, ObjectViewerWindow>(Show),
                            root,
                            searchFlags,
                            searchAttributes,
                            false) as ObjectViewerWindow;
                    }
                }
            }

            return null;
        }

        private static bool ValidateThread()
        {
            return System.Threading.Thread.CurrentThread.GetApartmentState() == System.Threading.ApartmentState.STA;
        }

        /// <summary>
        /// Adds an object to the editor.
        /// </summary>
        /// <param name="config">The item to add.</param>
        public virtual void AddRoot(object obj)
        {
            if (obj != null)
            {
                edit_Main.AddRoot(obj);
            }
        }

        /// <summary>
        /// Adds an object to the editor.
        /// </summary>
        /// <param name="obj">The item to add.</param>
        /// <param name="searchFlags">The BindingFlags to use when recursing through children.</param>
        public virtual void AddRoot(object obj, BindingFlags searchFlags)
        {
            if (obj != null)
            {
                edit_Main.AddRoot(obj, searchFlags);
            }
        }

        /// <summary>
        /// Adds the specified object as a root in the object viewer.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        /// <param name="searchFlags">The BindingFlags to use when recursing the hierarchy.</param>
        /// <param name="searchAttributes">The attributes that the properties on the object must match to be included.</param>
        public virtual void AddRoot(object obj, BindingFlags searchFlags, Attribute[] searchAttributes)
        {
            if (obj != null)
            {
                edit_Main.AddRoot(obj, searchFlags, searchAttributes);
            }
        }

        /// <summary>
        /// Removes an object from the editor.
        /// </summary>
        /// <param name="config">The item to remove.</param>
        public virtual void RemoveRoot(object obj)
        {
            if (obj != null)
            {
                edit_Main.RemoveRoot(obj);
            }
        }
        #endregion
    }
}
