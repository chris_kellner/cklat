﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.ObjectViewer
{
    #region Using List
    using CKLib.Controls.TreeGridView;
using CKLib.Utilities.DataStructures;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
    #endregion

    /// <summary>
    /// Interaction logic for ObjectViewer.xaml
    /// </summary>
    public partial class ObjectViewer : UserControl
    {
        #region Dependency Properties
        // Using a DependencyProperty as the backing store for DisplayedObject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayedObjectProperty =
            DependencyProperty.Register("DisplayedObject", typeof(object), typeof(ObjectViewer), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for DefaultSearchFlags.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DefaultSearchFlagsProperty =
            DependencyProperty.Register("DefaultSearchFlags", typeof(BindingFlags), typeof(ObjectViewer), new PropertyMetadata(BindingFlags.DeclaredOnly));

        // Using a DependencyProperty as the backing store for DefaultSearchAttributes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DefaultSearchAttributesProperty =
            DependencyProperty.Register("DefaultSearchAttributes", typeof(Attribute[]), typeof(ObjectViewer), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for CacheChildNodes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CacheChildNodesProperty =
            DependencyProperty.Register("CacheChildNodes", typeof(bool), typeof(ObjectViewer), new PropertyMetadata(false));
 
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ObjectViewer control.
        /// </summary>
        public ObjectViewer()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the Data view of the TreeGridView.
        /// </summary>
        public TreeGrid TreeGrid
        {
            get
            {
                return grid_Main.TreeGrid;
            }
        }

        /// <summary>
        /// Gets or sets the BindingFlags to apply by default when roots are added without specifying.
        /// </summary>
        public BindingFlags DefaultSearchFlags
        {
            get { return (BindingFlags)GetValue(DefaultSearchFlagsProperty); }
            set { SetValue(DefaultSearchFlagsProperty, value); }
        }

        /// <summary>
        /// The attributes to apply by default when searching for child nodes.
        /// </summary>
        public Attribute[] DefaultSearchAttributes
        {
            get { return (Attribute[])GetValue(DefaultSearchAttributesProperty); }
            set { SetValue(DefaultSearchAttributesProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Displayed object in this viewer.
        /// </summary>
        /// <remarks>Additional objects can be added after the fact, but this one is data bound.</remarks>
        public object DisplayedObject
        {
            get { return (object)GetValue(DisplayedObjectProperty); }
            set { SetValue(DisplayedObjectProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to cache the generated child nodes for faster access.
        /// </summary>
        public bool CacheChildNodes
        {
            get { return (bool)GetValue(CacheChildNodesProperty); }
            set { SetValue(CacheChildNodesProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this viewer is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return grid_Main.IsReadOnly;
            }

            set
            {
                grid_Main.IsReadOnly = value;
            }
        }
        #endregion

        /// <summary>
        /// Raised when a property is being decided upon.
        /// </summary>
        public event EventHandler<PropertyFilterEventArgs> FilterProperty;

        #region Methods
        /// <summary>
        /// Called when a dependency property changes.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == DisplayedObjectProperty)
            {
                RemoveRoot(e.OldValue);
                AddRoot(e.NewValue);
            }
        }

        /// <summary>
        /// Called by the reflection nodes when determining whether or not to accept a property.
        /// </summary>
        /// <param name="descriptor">The descriptor being filtered.</param>
        /// <returns>True to accept the property, false to reject it.</returns>
        protected virtual bool OnFilter(PropertyDescriptor descriptor)
        {
            var handler = FilterProperty;
            if (handler != null)
            {
                PropertyFilterEventArgs e = new PropertyFilterEventArgs(descriptor);
                handler(this, e);
                return !e.RejectProperty;
            }

            return true;
        }

        /// <summary>
        /// Adds the specified object as a root in the object viewer.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        public void AddRoot(object obj)
        {
            AddRoot(obj, DefaultSearchFlags);
        }

        /// <summary>
        /// Adds the specified object as a root in the object viewer.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        /// <param name="searchFlags">The BindingFlags to use when recursing the hierarchy.</param>
        public void AddRoot(object obj, BindingFlags searchFlags)
        {
            AddRoot(obj, searchFlags, DefaultSearchAttributes);
        }

        /// <summary>
        /// Adds the specified object as a root in the object viewer.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        /// <param name="searchFlags">The BindingFlags to use when recursing the hierarchy.</param>
        /// <param name="searchAttributes">The attributes that the properties on the object must match to be included.</param>
        public void AddRoot(object obj, BindingFlags searchFlags, Attribute[] searchAttributes)
        {
            if (obj != null)
            {
                var grid = grid_Main.TreeGrid;
                var root = ReflectionNode.Create(obj,
                    searchFlags, searchAttributes);
                root.Filter = OnFilter;
                root.CacheChildNodes = CacheChildNodes;
                int row = grid.AddRoot(root, r => ((ReflectionNode)r).Children);
                grid.SetRowExpandedState(row, true);
            }
        }

        /// <summary>
        /// Removes the specified object from the viewer.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        public void RemoveRoot(object obj)
        {
            if (obj != null)
            {
                var grid = grid_Main.TreeGrid;
                var row = grid.Roots.FirstOrDefault(r =>
                {
                    var node = r.SelectedObject as ReflectionNode;
                    if (node != null)
                    {
                        return node.GetCachedValue() == obj;
                    }

                    return false;
                });

                if (row != null)
                {
                    var node = row.SelectedObject as ReflectionNode;
                    if (node != null)
                    {
                        grid.RemoveRoot(row.SelectedObject);
                        node.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}
