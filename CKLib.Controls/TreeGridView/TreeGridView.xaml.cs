﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.TreeGridView
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;
    #endregion

    /// <summary>
    /// Interaction logic for TreeGridView.xaml
    /// </summary>
    public partial class TreeGridView : UserControl
    {

        #region Dependency Properties
        public TreeGrid TreeGrid
        {
            get { return (TreeGrid)GetValue(TreeGridProperty); }
            set { SetValue(TreeGridProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TreeGrid.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TreeGridProperty =
            DependencyProperty.Register("TreeGrid", typeof(TreeGrid), typeof(TreeGridView), new UIPropertyMetadata(null));

        #endregion

        #region Variables
        /// <summary>
        /// Binds the header to the appropriate data member.
        /// </summary>
        private Binding m_HeaderBinding;

        /// <summary>
        /// Binds the image to the appropriate data member.
        /// </summary>
        private Binding m_ImagePathBinding;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TreeGridView class.
        /// </summary>
        public TreeGridView()
        {
            m_HeaderBinding = new Binding();
            m_ImagePathBinding = new Binding();
            m_ImagePathBinding.Mode = BindingMode.OneWay;
            m_ImagePathBinding.FallbackValue = null;
            InitializeComponent();
            TreeGrid = new Controls.TreeGridView.TreeGrid();
            //RefreshColumns();
            //HookTreeGrid(TreeGrid);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether or not this grid view is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return Grid_Main.IsReadOnly;
            }

            set
            {
                Grid_Main.IsReadOnly = value;
            }
        }

        /// <summary>
        /// Gets the set of rows that are currently selected.
        /// </summary>
        public IList SelectedRows
        {
            get
            {
                return Grid_Main.SelectedItems;
            }
        }

        /// <summary>
        /// Gets the row that is currently selected.
        /// </summary>
        public TreeGridRow SelectedRow
        {
            get
            {
                return Grid_Main.SelectedItem as TreeGridRow;
            }
        }

        /// <summary>
        /// Gets or sets the set of column definitions for this tree grid view.
        /// </summary>
        public ObservableCollection<TreeGridColumn> Columns
        {
            get
            {
                var grid = TreeGrid;
                if (grid != null)
                {
                    return grid.Columns;
                }

                return null;
            }

            set
            {
                var grid = TreeGrid;
                if (grid != null)
                {
                    grid.Columns = value;
                }
            }
        }

        /// <summary>
        /// Gets the column definition used to define the header column.
        /// </summary>
        public TreeGridColumn HeaderColumn
        {
            get
            {
                if (TreeGrid != null)
                {
                    return TreeGrid.HeaderColumn;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the display name of the header column.
        /// </summary>
        public string HeaderColumnName
        {
            get
            {
                var col = HeaderColumn;
                if (col != null)
                {
                    return col.Name;
                }

                return string.Empty;
            }

            set
            {
                var col = HeaderColumn;
                if (col != null)
                {
                    col.Name = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the property to bind the header cell value to.
        /// </summary>
        public string HeaderColumnDisplayMember
        {
            get
            {
                var col = HeaderColumn;
                if (col != null)
                {
                    return col.DataBoundPropertyName;
                }

                return string.Empty;
            }

            set
            {
                var col = HeaderColumn;
                if (col != null)
                {
                    col.DataBoundPropertyName = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the property to bind to for images.
        /// </summary>
        public string ImageMemberName
        {
            get
            {
                return m_ImagePathBinding.Path.Path;
            }

            set
            {
                m_ImagePathBinding.Path = new PropertyPath(value);
            }
        }

        /// <summary>
        /// Gets or sets a conversion object to obtain the images from the property.
        /// </summary>
        public IValueConverter ImageMemberConverter
        {
            get
            {
                return m_ImagePathBinding.Converter;
            }

            set
            {
                m_ImagePathBinding.Converter = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to include images in the header.
        /// </summary>
        public bool ShowHeaderImage
        {
            get;
            set;
        }

        /// <summary>
        /// Provides protected access to the internal DataGrid.
        /// </summary>
        protected DataGrid InternalGrid
        {
            get
            {
                return Grid_Main;
            }
        }
        #endregion

        #region Methods
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == TreeGridProperty)
            {
                var oldGrid = e.OldValue as TreeGrid;
                if (oldGrid != null)
                {
                    UnhookTreeGrid(oldGrid);
                }

                var grid = e.NewValue as TreeGrid;
                if (grid != null)
                {
                    HookTreeGrid(grid);
                }

                RefreshColumns();
            }
        }

        private void UnhookTreeGrid(TreeGrid grid)
        {
            if (grid != null)
            {
                grid.Columns.CollectionChanged -= Columns_CollectionChanged;
            }
        }

        private void HookTreeGrid(TreeGrid grid)
        {
            if (grid != null)
            {
                grid.Columns.CollectionChanged -= Columns_CollectionChanged;
                grid.Columns.CollectionChanged += Columns_CollectionChanged;
            }
        }

        private void Columns_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Grid_Main.Columns.Insert(e.NewStartingIndex + 1, BuildColumn((TreeGridColumn)e.NewItems[0]));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // +1 for header column.
                    Grid_Main.Columns.RemoveAt(e.OldStartingIndex + 1);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    Grid_Main.Columns[e.NewStartingIndex + 1] = BuildColumn((TreeGridColumn)e.NewItems[0]);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    RefreshColumns();
                    break;
            }
        }

        private DataGridColumn BuildColumn(TreeGridColumn col)
        {
            DataGridColumn newCol = Activator.CreateInstance(col.ColumnType) as DataGridColumn;
            newCol.Header = col.Name;

            DataGridBoundColumn boundCol = newCol as DataGridBoundColumn;
            if (boundCol != null)
            {
                if (!string.IsNullOrEmpty(col.DataBoundPropertyName))
                {
                    boundCol.Binding = new Binding("SelectedObject." + col.DataBoundPropertyName);
                }
            }

            DataGridTemplateColumn templateCol = newCol as DataGridTemplateColumn;
            if (templateCol != null)
            {
                templateCol.CellTemplate = col.CellTemplate;
                templateCol.CellTemplateSelector = col.CellTemplateSelector;
                templateCol.CellEditingTemplate = col.EditTemplate ?? col.CellTemplate;
                templateCol.CellEditingTemplateSelector = col.EditingTemplateSelector ?? col.CellTemplateSelector;
            }

            col.PropertyChanged += (s, e) =>
            {
                switch (e.PropertyName)
                {
                    case "Name":
                        newCol.Header = col.Name;
                        break;
                    case "DataBoundPropertyName":
                        if (boundCol != null)
                        {
                            boundCol.Binding = new Binding("SelectedObject." + col.DataBoundPropertyName);
                        }

                        break;
                    case "CellTemplate":
                        if (templateCol != null)
                        {
                            templateCol.CellTemplate = col.CellTemplate;
                        }

                        break;
                    case "CellTemplateSelector":
                        if (templateCol != null)
                        {
                            templateCol.CellTemplateSelector = col.CellTemplateSelector;
                        }

                        break;

                    case "EditingTemplateSelector":
                        if (templateCol != null)
                        {
                            templateCol.CellEditingTemplateSelector = col.EditingTemplateSelector;
                        }

                        break;

                    case "EditTemplate":
                        if (templateCol != null)
                        {
                            templateCol.CellEditingTemplate = col.EditTemplate;
                        }

                        break;
                }
            };

            return newCol;
        }

        private void RefreshColumns()
        {
            if (Grid_Main.Columns.Count > 1)
            {
                Grid_Main.Columns.Clear();
            }

            var grid = TreeGrid;
            if (grid != null)
            {
                var template = (DataTemplate)Grid_Main.Resources["PART_HEADER_TEMPLATE"];
                var header = HeaderColumn;
                DataGridColumn newCol = new DataGridTemplateColumn()
                {
                    Header = header.Name,
                    CellTemplate = template,
                };

                m_HeaderBinding.Path = new PropertyPath("SelectedObject." + grid.HeaderColumn.DataBoundPropertyName);

                header.PropertyChanged += (s, e) =>
                {
                    switch (e.PropertyName)
                    {
                        case "Name":
                            newCol.Header = header.Name;
                            break;
                        case "DataBoundPropertyName":
                            m_HeaderBinding.Path.Path = "SelectedObject." + grid.HeaderColumn.DataBoundPropertyName;
                            break;
                    }
                };

                Grid_Main.Columns.Add(newCol);

                foreach (var col in TreeGrid.Columns)
                {
                    newCol = BuildColumn(col);
                    Grid_Main.Columns.Add(newCol);
                }
            }
        }

        private void Grid_Main_Initialized(object sender, EventArgs e)
        {
            Grid_Main.DataContext = this;
        }

        private void Col_Header_Checkbox_Checked_1(object sender, RoutedEventArgs e)
        {
            var src = e.Source as CheckBox;
            if (src != null)
            {
                var row = src.DataContext as TreeGridRow;
                if (row != null)
                {
                    row.IsExpanded = true;
                }
            }
        }

        private void Col_Header_Checkbox_Unchecked_1(object sender, RoutedEventArgs e)
        {
            var src = e.Source as CheckBox;
            if (src != null)
            {
                var row = src.DataContext as TreeGridRow;
                if (row != null)
                {
                    row.IsExpanded = false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TChild"></typeparam>
        /// <param name="parent"></param>
        /// <returns></returns>
        private static TChild GetChildElement<TChild>(DependencyObject parent, string childName) where TChild : FrameworkElement
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (child is TChild)
                {
                    string name = child.GetValue(Control.NameProperty) as string;
                    if (string.IsNullOrEmpty(childName) || name == childName)
                    {
                        return (TChild)child;
                    }
                }
                else
                {
                    var next = GetChildElement<TChild>(child, childName);
                    if (next != null)
                    {
                        return next;
                    }
                }
            }

            return null;
        }

        private void PART_Header_Text_Initialized(object sender, EventArgs e)
        {
            TextBlock block = sender as TextBlock;
            if (block != null)
            {
                block.SetBinding(TextBlock.TextProperty, m_HeaderBinding);
            }
        }

        private void PART_HEADER_IMAGE_Initialized(object sender, EventArgs e)
        {
            Image img = sender as Image;
            if (img != null && ShowHeaderImage)
            {
                img.SetBinding(Image.SourceProperty, m_ImagePathBinding);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Raised when the current selection in the grid changes.
        /// </summary>
        public virtual event SelectionChangedEventHandler SelectionChanged
        {
            add
            {
                Grid_Main.SelectionChanged += value;
            }

            remove
            {
                Grid_Main.SelectionChanged -= value;
            }
        }
        #endregion
    }
}
