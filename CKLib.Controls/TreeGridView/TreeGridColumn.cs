﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.TreeGridView
{
    #region Using List
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    #endregion

    /// <summary>
    /// This class is used to describe how to make the appropriate DataGridColumn to represent the data.
    /// </summary>
    public class TreeGridColumn : INotifyPropertyChanged
    {
        #region Variables
        /// <summary>
        /// The display name of the grid.
        /// </summary>
        private string m_Name;

        /// <summary>
        /// The path to the member that this column should bind to.
        /// </summary>
        private string m_DataBoundPropertyName;
        
        /// <summary>
        /// A cell template to apply to all entries in this column.
        /// </summary>
        private DataTemplate m_CellTemplate;

        /// <summary>
        /// A cell template to apply to all entries in this column.
        /// </summary>
        private DataTemplate m_EditTemplate;

        /// <summary>
        /// An object used to select the proper cell template for each entry in this column.
        /// </summary>
        private DataTemplateSelector m_CellTemplateSelector;

        /// <summary>
        /// The DataGridColumn type to create.
        /// </summary>
        private Type m_ColumnType;

        /// <summary>
        /// An object used to select the proper cell template for editing each entry in this column.
        /// </summary>
        private DataTemplateSelector m_EditingTemplateSelector;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the display name for this column.
        /// </summary>
        public string Name
        {
            get
            {
                return m_Name;
            }

            set
            {
                m_Name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets the data member to show in this column.
        /// </summary>
        /// <remarks>Use of this property may disable cell templates and selectors.</remarks>
        public string DataBoundPropertyName
        {
            get
            {
                return m_DataBoundPropertyName;
            }

            set
            {
                m_DataBoundPropertyName = value;
                OnPropertyChanged("DataBoundPropertyName");
            }
        }

        /// <summary>
        /// The type of DataGridColumn to create for this definition.
        /// </summary>
        public Type ColumnType
        {
            get
            {
                if (m_ColumnType != null)
                {
                    return m_ColumnType;
                }
                else if (string.IsNullOrEmpty(DataBoundPropertyName))
                {
                    return typeof(DataGridTemplateColumn);
                }
                else
                {
                    return typeof(DataGridTextColumn);
                }
            }

            set
            {
                if (m_ColumnType != value)
                {
                    if (typeof(DataGridColumn).IsAssignableFrom(value))
                    {
                        m_ColumnType = value;
                        OnPropertyChanged("ColumnType");
                    }
                    else if(value != null)
                    {
                        throw new InvalidOperationException(value.Name + " does not derive from System.Windows.Controls.DataGridColumn.");
                    }
                }
            }
        }

        /// <summary>
        /// The template to use for all cells in this column.
        /// </summary>
        /// <remarks>Only used for DataGridTemplateColumn derivations.</remarks>
        public DataTemplate CellTemplate
        {
            get
            {
                return m_CellTemplate;
            }

            set
            {
                if (m_CellTemplate != value)
                {
                    m_CellTemplate = value;
                    OnPropertyChanged("CellTemplate");
                }
            }
        }

        /// <summary>
        /// The template to use for editing all cells in this column.
        /// </summary>
        /// <remarks>Only used for DataGridTemplateColumn derivations.</remarks>
        public DataTemplate EditTemplate
        {
            get
            {
                return m_EditTemplate;
            }

            set
            {
                if (m_EditTemplate != value)
                {
                    m_EditTemplate = value;
                    OnPropertyChanged("EditTemplate");
                }
            }
        }

        /// <summary>
        /// The object used to select the template to use for cells in this column.
        /// </summary>
        /// <remarks>Only used for DataGridTemplateColumn derivations.</remarks>
        public DataTemplateSelector CellTemplateSelector
        {
            get
            {
                return m_CellTemplateSelector;
            }

            set
            {
                m_CellTemplateSelector = value;
                OnPropertyChanged("CellTemplateSelector");
            }
        }

        /// <summary>
        /// The object used to select the template to use for editing the values in the cells in this column.
        /// </summary>
        /// <remarks>Only used for DataGridTemplateColumn derivations.</remarks>
        public DataTemplateSelector EditingTemplateSelector
        {
            get
            {
                return m_EditingTemplateSelector;
            }

            set
            {
                m_EditingTemplateSelector = value;
                OnPropertyChanged("EditingTemplateSelector");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when the value of a property changes.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Raised when the value of a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
