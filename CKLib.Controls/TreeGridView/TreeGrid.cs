﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.TreeGridView
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    #endregion

    public class TreeGrid
    {
        #region Variables
        /// <summary>
        /// The bindable rows collection.
        /// </summary>
        private ObservableCollection<TreeGridRow> m_Rows;

        /// <summary>
        /// Definition for the header column that is required to operate this grid.
        /// </summary>
        private TreeGridColumn m_HeaderColumn;

        /// <summary>
        /// The bindable columns collection.
        /// </summary>
        private ObservableCollection<TreeGridColumn> m_Columns;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TreeGrid class.
        /// </summary>
        public TreeGrid()
        {
            MaxLevel = Int32.MaxValue;
            m_Rows = new ObservableCollection<TreeGridRow>();
            m_Columns = new ObservableCollection<TreeGridColumn>();
            m_HeaderColumn = new TreeGridColumn()
            {
                Name = "Name"
            };
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the maximum depth that the grid nodes are allowed to recurse.
        /// </summary>
        public int MaxLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the set of rows which exist at the root (0) level.
        /// </summary>
        public IEnumerable<TreeGridRow> Roots
        {
            get
            {
                return m_Rows.Where(r => r.Level == 0).ToArray();
            }
        }

        /// <summary>
        /// Gets the set of rows that are currently "visible".
        /// </summary>
        public IEnumerable<TreeGridRow> Rows
        {
            get
            {
                return m_Rows;
            }
        }

        /// <summary>
        /// Gets the column definitions for this grid.
        /// </summary>
        public ObservableCollection<TreeGridColumn> Columns
        {
            get
            {
                return m_Columns;
            }

            set
            {
                m_Columns = value;
            }
        }

        /// <summary>
        /// Gets the header column definition.
        /// </summary>
        public TreeGridColumn HeaderColumn
        {
            get
            {
                return m_HeaderColumn;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a level-0 object to this tree-grid.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="childAccessor"></param>
        /// <returns></returns>
        public int AddRoot(object obj, Func<object, IEnumerable<object>> childAccessor)
        {
            return AddRow(null, obj, childAccessor);
        }

        /// <summary>
        /// Removes a level-0 object from this tree-grid.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RemoveRoot(object obj)
        {
            var row = m_Rows.FirstOrDefault(r => r.Level == 0 && r.SelectedObject == obj);
            row.IsExpanded = false;
            return m_Rows.Remove(row);
        }

        /// <summary>
        /// Sets the expanded state of the row at the specified index.
        /// </summary>
        /// <param name="rowIndex">The index of the row to expand or collapse.</param>
        /// <param name="expanded">The new expanded state.</param>
        public void SetRowExpandedState(int rowIndex, bool expanded)
        {
            if (rowIndex < m_Rows.Count && rowIndex >= 0)
            {
                var row = m_Rows[rowIndex];
                if (row.HasChildren)
                {
                    row.IsExpanded = expanded;
                }
            }
        }

        /// <summary>
        /// Called internally to add a new tree grid row to the specified parent.
        /// </summary>
        /// <param name="parent">The row to serve as the parent for the new row.</param>
        /// <param name="obj">The object to populate the new row with.</param>
        /// <param name="childAccessor">A method used to retrieve the children of the specified object.</param>
        /// <returns>The index of the new row.</returns>
        protected int AddRow(TreeGridRow parent, object obj, Func<object, IEnumerable<object>> childAccessor)
        {
            TreeGridRow row = new TreeGridRow(parent)
            {
                TreeGrid = this,
                SelectedObject = obj,
                ChildAccessor = childAccessor
            };

            m_Rows.Add(row);
            HookRow(row);
            return m_Rows.IndexOf(row);
        }
        
        /// <summary>
        /// Called internally to remove a row at the specified index.
        /// </summary>
        /// <param name="index">The index of the row to be removed.</param>
        /// <returns>The row that was removed.</returns>
        protected TreeGridRow RemoveRow(int index)
        {
            var row = m_Rows[index];
            UnhookRow(row);
            m_Rows.RemoveAt(index);
            return row;
        }

        /// <summary>
        /// Gets the index of the specified row.
        /// </summary>
        /// <param name="row">The row to get the index of.</param>
        /// <returns>The index of the specified row.</returns>
        public int IndexOfRow(TreeGridRow row)
        {
            return m_Rows.IndexOf(row);
        }

        /// <summary>
        /// Called by a row when its expanded property is about to change.
        /// </summary>
        /// <param name="sender">The row whose property is changing.</param>
        /// <param name="e">The expanding event args.</param>
        private void row_ExpandedChanging(object sender, TreeGridRowEventArgs e)
        {
            int idx = m_Rows.IndexOf(e.Row);
            if (idx > -1)
            {
                foreach (var child in e.Row.Children)
                {
                    child.IsExpanded = false;
                    m_Rows.RemoveAt(idx + 1);
                }
            }
        }

        /// <summary>
        /// Hooks the row for expansion events.
        /// </summary>
        /// <param name="row">The row to hook.</param>
        internal void HookRow(TreeGridRow row)
        {
            if (row != null)
            {
                row.ExpandedChanging -= row_ExpandedChanging;
                row.ExpandedChanged -= row_ExpandedChanged;
                row.ExpandedChanging += row_ExpandedChanging;
                row.ExpandedChanged += row_ExpandedChanged;
            }
        }

        /// <summary>
        /// Unhooks the row for expansion events.
        /// </summary>
        /// <param name="row">The row to unhook.</param>
        internal void UnhookRow(TreeGridRow row)
        {
            if (row != null)
            {
                row.ExpandedChanging -= row_ExpandedChanging;
                row.ExpandedChanged -= row_ExpandedChanged;
            }
        }

        /// <summary>
        /// Called when a given row has finished expanding.
        /// </summary>
        /// <param name="sender">The row that expanded.</param>
        /// <param name="e">The expanding event args.</param>
        private void row_ExpandedChanged(object sender, TreeGridRowEventArgs e)
        {
            int idx = m_Rows.IndexOf(e.Row);
            if (idx > -1)
            {
                foreach (var child in e.Row.Children)
                {
                    child.IsExpanded = false;
                    m_Rows.Insert(++idx, child);
                }
            }
        }
        #endregion
    }
}
