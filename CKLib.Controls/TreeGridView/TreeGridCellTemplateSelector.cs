﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Controls.TreeGridView
{
    #region Using List
    using CKLib.Utilities.DataStructures;
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    #endregion

    public class TreeGridCellTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextboxTemplate
        {
            get;
            set;
        }

        public DataTemplate CheckboxTemplate
        {
            get;
            set;
        }

        public DataTemplate ComboboxTemplate
        {
            get;
            set;
        }

        public DataTemplate Default
        {
            get;
            set;
        }

        public ResourceDictionary Resources
        {
            get;
            set;
        }

        public string DataMember
        {
            get;
            set;
        }

        public TreeGridCellTemplateSelector()
        {
            Resources = new ResourceDictionary();
        }

        public override System.Windows.DataTemplate SelectTemplate(object obj, System.Windows.DependencyObject container)
        {
            bool hasConverter = false;
            bool isReadOnly = false;
            Type objType = null;
            if (obj != null && !string.IsNullOrEmpty(DataMember))
            {
                var node = ReflectionNode.Create(obj, DataMember);
                if (node != null && node.Descriptor != null)
                {
                    isReadOnly = node.IsReadOnly;
                    objType = node.Descriptor.PropertyType;
                    if (!isReadOnly)
                    {
                        var converter = node.Descriptor.Converter;
                        hasConverter = converter.CanConvertFrom(typeof(string));
                    }
                }
                else
                {
                    objType = obj.GetType();
                }
            }

            if (objType == null)
            {
                return null;
            }

            DataTemplate template = null;
            if (!isReadOnly)
            {
                if (Resources != null)
                {
                    template = Resources[obj] as DataTemplate;
                    if (template == null && objType != typeof(object))
                    {
                        DataTemplateKey key = new DataTemplateKey(objType);
                        template = Resources[key] as DataTemplate;
                    }
                }

                if (template == null)
                {
                    if (typeof(bool).IsAssignableFrom(objType))
                    {
                        template = CheckboxTemplate;
                    }
                    else if (typeof(Enum).IsAssignableFrom(objType))
                    {
                        template = ComboboxTemplate;
                    }
                    else if (hasConverter)
                    {
                        template = TextboxTemplate;
                    }
                }
            }

            if (template == null)
            {
                template = Default;
            }

            return template;
        }
    }
}
