﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Scripting.Hosting;
using System.Threading;

namespace CKPythonExtension.Client
{
    public class PythonTask
    {
        public PythonTask(string pythonString)
        {
            PythonString = pythonString;
        }
        public string PythonString { get; set; }
        public object Result { get; internal set; }
        public Thread ExecutionThread { get; internal set; }
        public bool IsComplete { get; internal set; }
        public Exception ExceptionObject { get; internal set; }
        public ScriptScope TaskScope { get; internal set; }
        public Guid Requestor { get; set; }
        public void AbortTask()
        {
            PythonHost.Instance.Break(ExecutionThread);
        }
        public bool IsRunning
        {
            get
            {
                return PythonHost.Instance.GetRunningState(ExecutionThread);
            }
        }
    }
}
