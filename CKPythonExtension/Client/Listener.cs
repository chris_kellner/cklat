﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;

namespace CKPythonExtension.Client
{
    public class TraceEventArgs : EventArgs
    {
        public string Message { get; set; }
    }

    public class Listener
    {
        #region Internal classes

        delegate int HookProc(int code, IntPtr wParam, IntPtr lParam);

        private enum HookType : int
        {
            WH_JOURNALRECORD = 0,
            WH_JOURNALPLAYBACK = 1,
            WH_KEYBOARD = 2,
            WH_GETMESSAGE = 3,
            WH_CALLWNDPROC = 4,
            WH_CBT = 5,
            WH_SYSMSGFILTER = 6,
            WH_MOUSE = 7,
            WH_HARDWARE = 8,
            WH_DEBUG = 9,
            WH_SHELL = 10,
            WH_FOREGROUNDIDLE = 11,
            WH_CALLWNDPROCRET = 12,
            WH_KEYBOARD_LL = 13,
            WH_MOUSE_LL = 14
        }

        private class UnmanagedImports
        {
            [DllImport("user32.dll")]
            public static extern IntPtr SetWindowsHookEx(HookType code, HookProc func, IntPtr hInstance, int threadID);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool UnhookWindowsHookEx(IntPtr hhk);

            [DllImport("user32.dll")]
            public static extern int CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        }
        #endregion

        #region Variables
        private HookProc myCallbackDelegate = null;
        IntPtr m_pHook;
        #endregion

        public void Init()
        {
            // initialize our delegate
            this.myCallbackDelegate = new HookProc(this.MyCallbackFunction);
            m_keyQueue = new Queue<KeyEventArgs>();
            bool good;
            //m_waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset, null, out good);
            m_running = true;
            // setup a keyboard hook
            var id = Thread.CurrentThread.ManagedThreadId;
            var id2 = AppDomain.GetCurrentThreadId();
            m_pHook = UnmanagedImports.SetWindowsHookEx(HookType.WH_KEYBOARD, this.myCallbackDelegate, IntPtr.Zero, 0);
        }

        private Queue<KeyEventArgs> m_keyQueue;
        private EventWaitHandle m_waitHandle;
        private bool m_running;
        public void Run()
        {
            //while (m_running)
            //{
                while (m_keyQueue.Count > 0)
                {
                    var evt = m_keyQueue.Dequeue();
                    if (KeyEvent != null)
                    {
                        KeyEvent(this, evt);
                    }
                }
                //m_waitHandle.WaitOne();
            //}
        }

        public event KeyEventHandler KeyEvent;

        public void Release()
        {
            if(m_pHook != null && m_pHook != IntPtr.Zero){
                UnmanagedImports.UnhookWindowsHookEx(m_pHook);
            }
            m_running = false;
            //if (m_waitHandle != null && !m_waitHandle.SafeWaitHandle.IsClosed)
            //{
            //    m_waitHandle.Set();
            //    m_waitHandle.Dispose();
            //}
        }

        private int MyCallbackFunction(int code, IntPtr wParam, IntPtr lParam)
        {
            if (code < 0)
            {
                //you need to call CallNextHookEx without further processing
                //and return the value returned by CallNextHookEx
                return UnmanagedImports.CallNextHookEx(IntPtr.Zero, code, wParam, lParam);
            }
            // we can convert the 2nd parameter (the key code) to a System.Windows.Forms.Keys enum constant
            Keys keyPressed = (Keys)wParam.ToInt32();
            Console.WriteLine(keyPressed);
            Run();
            //m_waitHandle.Set();
            //return the value returned by CallNextHookEx
            return UnmanagedImports.CallNextHookEx(IntPtr.Zero, code, wParam, lParam);
        }
    }
}
