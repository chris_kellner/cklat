﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.DataStructures;

namespace CKPythonExtension.Client
{
    public class PythonAssistant
    {
        #region Variables
        /// <summary>
        /// Used to store all objects between scripts.
        /// </summary>
        private SynchronizedDictionary<string, object> m_Dictionary;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PythonAssistant class.
        /// </summary>
        public PythonAssistant()
        {
            m_Dictionary = new SynchronizedDictionary<string, object>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Stores an object for later consumption.
        /// </summary>
        /// <param name="key">The key with which to store and access the object.</param>
        /// <param name="value">The object to store.</param>
        public void Store(string key, object value)
        {
            m_Dictionary[key] = value;
        }

        /// <summary>
        /// Retrieves a previously stored object with the specified key.
        /// </summary>
        /// <param name="key">The key to retrieve the object for.</param>
        /// <returns>The stored object or null.</returns>
        public object Retrieve(string key)
        {
            object o;
            if (m_Dictionary.TryGetValue(key, out o))
            {
                return o;
            }

            return null;
        }

        /// <summary>
        /// Deletes the entry named by the specified key. If the object is IDisposable the dispose
        /// method is called after removal.
        /// </summary>
        /// <param name="key">The key to delete the object of.</param>
        public void Delete(string key)
        {
            object o;
            if (m_Dictionary.TryRemove(key, out o))
            {
                if (o is IDisposable)
                {
                    try
                    {
                        ((IDisposable)o).Dispose();
                    }
                    catch (ObjectDisposedException)
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Empties the store of all stored objects.
        /// Objects which are IDisposable are Disposed after removal.
        /// </summary>
        public void Clear()
        {
            foreach (var key in GetKeys())
            {
                Delete(key);
            }
        }

        /// <summary>
        /// Gets the set of all keys which have been used to store objects.
        /// </summary>
        /// <returns>An enumeration of keys.</returns>
        public IEnumerable<String> GetKeys()
        {
            return m_Dictionary.Keys;
        }
        #endregion
    }
}
