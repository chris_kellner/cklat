﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using IronPython.Hosting;
using IronPython.Runtime.Exceptions;
using Microsoft.Scripting.Hosting;
using CKLib.Utilities.Patterns;
using CKLib.Utilities.DataStructures;

namespace CKPythonExtension.Client
{
    public class PythonHost : IDisposable
    {
        #region Internal Classes
        private class PythonStream : Stream
        {
            private event EventHandler<TraceEventArgs> m_Write;

            public override void Write(byte[] buffer, int offset, int count)
            {
                string s = Encoding.ASCII.GetString(buffer, offset, count);
                if (m_Write != null)
                {
                    m_Write(this, new TraceEventArgs() { Message = s });
                }
            }

            public event EventHandler<TraceEventArgs> Written
            {
                add
                {
                    m_Write += value;
                }

                remove
                {
                    m_Write -= value;
                }
            }

            public override bool CanRead
            {
                get { return false; }
            }

            public override bool CanSeek
            {
                get { return false; }
            }

            public override bool CanWrite
            {
                get { return true; }
            }

            public override void Flush()
            {
                
            }

            public override long Length
            {
                get { throw new NotImplementedException(); }
            }

            public override long Position
            {
                get
                {
                    throw new NotImplementedException();
                }
                set
                {
                    throw new NotImplementedException();
                }
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                throw new NotImplementedException();
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotImplementedException();
            }

            public override void SetLength(long value)
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region Variables
        /// <summary>
        /// Performs most all of the python dirty work.
        /// </summary>
        private ScriptEngine m_scriptEngine;

        /// <summary>
        /// A collection of threads and their respective running states.
        /// </summary>
        private SynchronizedDictionary<Thread, bool> m_runningFlags;

        /// <summary>
        /// Occurs when output is posted to the python stream.
        /// </summary>
        private event EventHandler<TraceEventArgs> m_pythonOutput;
        
        /// <summary>
        /// Occurs when an error occurs in the python engine.
        /// </summary>
        private event EventHandler<TraceEventArgs> m_pythonError;

        /// <summary>
        /// Listens on the windows message pump for keyboard messages.
        /// </summary>
        private Listener m_keyboardListener;

        /// <summary>
        /// Class used for persisting object between scripts.
        /// </summary>
        private PythonAssistant m_Assistant;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PythonHost class.
        /// </summary>
        private PythonHost()
        {
            m_scriptEngine = Python.CreateEngine();
            m_scriptEngine.SetTrace(OnTracebackFrame);
            m_runningFlags = new SynchronizedDictionary<Thread, bool>();
            m_keyboardListener = new Listener();

            var outputStream = new PythonStream();
            outputStream.Written += OnOutput;
            m_scriptEngine.Runtime.IO.SetOutput(outputStream, Encoding.ASCII);

            var errorStream = new PythonStream();
            errorStream.Written += OnError;
            m_scriptEngine.Runtime.IO.SetErrorOutput(errorStream, Encoding.ASCII);

            m_Assistant = new PythonAssistant();
            InitializeListener();
        }

        static PythonHost()
        {
            Singleton<PythonHost>.ValueFactory = () => new PythonHost();
        }
        #endregion

        #region Methods
        public PythonTask AsyncExecute(string pythonString, Guid requestor, Action<PythonTask> onComplete = null)
        {
            PythonTask task = new PythonTask(pythonString);
            task.Requestor = requestor;
            ThreadPool.QueueUserWorkItem(o =>
            {
                var x = o as PythonTask;
                if (x != null)
                {
                    InterruptableExecute(x);
                    if (onComplete != null)
                    {
                        onComplete(x);
                    }
                }
            }, task);
            return task;
        }

        public PythonTask InterruptableExecute(PythonTask x, IDictionary<string, object> scopeVariables = null)
        {
            x.ExecutionThread = Thread.CurrentThread;
            SetRunningState(true);
            try
            {
                ScriptScope scope = scopeVariables != null ? m_scriptEngine.CreateScope(scopeVariables) : m_scriptEngine.CreateScope();
                scope.SetVariable("Assistant", m_Assistant);
                x.TaskScope = scope;
                ScriptSource src = m_scriptEngine.CreateScriptSourceFromString(x.PythonString);
                x.Result = src.Execute(scope);
            }
            catch (ThreadAbortException e)
            {
                if (e.ExceptionState is Microsoft.Scripting.KeyboardInterruptException)
                {
                    Thread.ResetAbort();
                }
            }
            catch (Exception ex)
            {
                x.ExceptionObject = ex;
            }
            finally
            {
                SetRunningState(false);
                x.IsComplete = true;
            }
            return x;
        }

        public PythonTask InterruptableExecute(string pythonString, Guid requestor, IDictionary<string, object> scopeVariables = null)
        {
            PythonTask x = new PythonTask(pythonString);
            x.Requestor = requestor;
            return InterruptableExecute(x, scopeVariables);
        }

        public void Break(Thread scriptThread = null)
        {
            if (GetRunningState(scriptThread))
            {
                scriptThread.Abort(new Microsoft.Scripting.KeyboardInterruptException(""));
            }
        }

        public IEnumerable<Thread> GetThreadsExecutingScripts()
        {
            return from t in m_runningFlags
                   where t.Value
                   select t.Key;
        }

        private Thread m_keyboardThread;
        private void InitializeListener()
        {
            m_keyboardThread = new Thread(() =>
            {
                m_keyboardListener.Init();
                m_keyboardListener.Run();
            });
            m_keyboardThread.Start();
        }

        private void SetRunningState(bool state, Thread thread = null)
        {
            if (thread == null)
            {
                thread = Thread.CurrentThread;
            }

            m_runningFlags[thread] = state;
        }

        internal bool GetRunningState(Thread thread = null)
        {
            if (thread == null)
            {
                thread = Thread.CurrentThread;
            }
            bool running;
            if (m_runningFlags.TryGetValue(thread, out running))
            {
                return running;
            }
            return false;
        }

        private void OnOutput(object sender, TraceEventArgs e)
        {
            if (m_pythonOutput != null)
            {
                m_pythonOutput(this, e);
            }
        }

        private void OnError(object sender, TraceEventArgs e)
        {
            if (m_pythonError != null)
            {
                m_pythonError(this, e);
            }
        }

        private TracebackDelegate OnTracebackFrame(TraceBackFrame frame, string result, object payload)
        {
            return OnTracebackFrame;
        }
        #endregion

        #region Properties
        public static PythonHost Instance
        {
            get
            {
                return Singleton<PythonHost>.Instance;
            }
        }

        public ScriptEngine ScriptEngine
        {
            get
            {
                return m_scriptEngine;
            }
        }
        #endregion

        #region Events
        public event EventHandler<TraceEventArgs> PythonOutput
        {
            add
            {
                m_pythonOutput += value;
            }

            remove
            {
                m_pythonOutput -= value;
            }
        }

        public event EventHandler<TraceEventArgs> PythonError
        {
            add
            {
                m_pythonError += value;
            }

            remove
            {
                m_pythonError -= value;
            }
        }
        #endregion

        protected virtual void Dispose(bool disposing)
        {
            m_keyboardListener.Release();
            if (disposing)
            {

            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~PythonHost()
        {
        }
    }
}
