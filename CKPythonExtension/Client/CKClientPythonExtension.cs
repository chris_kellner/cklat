﻿
namespace CKPythonExtension.Client
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.Connection.Extensions;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Security;
    using CKPythonExtension.Messages;
    #endregion
    [LocalOnly]
    [ClientSideExtension(ExtensionName = "Remote Python Scripter")]
    public class CKClientPythonExtension : IExtension
    {
        #region Variables
        /// <summary>
        /// Reference to the IMessenger used by this extension.
        /// </summary>
        private IMessenger m_Messenger;

        /// <summary>
        /// Reference to the IProxyFactory used by this extension.
        /// </summary>
        private IProxyFactory m_ProxyFactory;

        /// <summary>
        /// Collection of connection ids which have access to this extension.
        /// </summary>
        private SynchronizedObservableCollection<Guid> m_AllowedConnections;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CKClientPythonExtension class.
        /// </summary>
        public CKClientPythonExtension()
        {
            m_AllowedConnections = new SynchronizedObservableCollection<Guid>(null);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when a message is received.
        /// </summary>
        /// <param name="pair">Message information.</param>
        private void OnMessageReceived(MessageEnvelope pair)
        {
            if (pair.Message.GetType() == typeof(RunScriptRequestMessage))
            {
                var msg = (RunScriptRequestMessage)pair.Message;
                if (!string.IsNullOrEmpty(msg.ScriptString))
                {
                    PythonTask taskToExecute = PythonHost.Instance.AsyncExecute(msg.ScriptString, pair.Sender, (t) =>
                    {
                        if (m_Messenger != null)
                        {
                            string result = (t.ExceptionObject != null ? t.ExceptionObject.Message : (t.Result != null ? t.Result.ToString() : ""));
                            m_Messenger.SendNetworkMessage(
                                t.Requestor,
                                new RunScriptResponseMessage() {
                                    ScriptResult = result,
                                    RequestId = msg.RequestId
                                });
                        }
                    });
                }
            }
        }

        /// <summary>
        /// Called when output is placed in the Python std::out stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_PythonOutput(object sender, TraceEventArgs e)
        {
            if (m_Messenger != null)
            {
                m_Messenger.SendNetworkMessage(new PythonOutputMessage(false, e.Message));
            }
        }

        /// <summary>
        /// Called when the output is placed in the python std::error stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_PythonError(object sender, TraceEventArgs e)
        {
            if (m_Messenger != null)
            {
                m_Messenger.SendNetworkMessage(new PythonOutputMessage(true, e.Message));
            }
        }
        #endregion

        #region IExtension Members

        public string ExtensionName
        {
            get { return "Remote Python Scripter"; }
        }

        public bool Initialize(CKLib.Utilities.Connection.IMessenger messenger, CKLib.Utilities.Proxy.IProxyFactory proxyFactory)
        {
            m_Messenger = messenger;
            m_ProxyFactory = proxyFactory;
            if (m_Messenger != null)
            {
                m_Messenger.AddCallback(typeof(RunScriptRequestMessage), OnMessageReceived);
                PythonHost.Instance.PythonError += new EventHandler<TraceEventArgs>(Instance_PythonError);
                PythonHost.Instance.PythonOutput += new EventHandler<TraceEventArgs>(Instance_PythonOutput);
                return true;
            }

            return false;
        }

        public Type[] GetCustomMessageTypes()
        {
            return new Type[] {
                typeof(PythonOutputMessage),
                typeof(RunScriptRequestMessage),
                typeof(RunScriptResponseMessage)
            };
        }

        public bool EnableConnectionAvailability(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(CKClientPythonExtension), m_Messenger, connectionId))
            {
                if (!m_AllowedConnections.Contains(connectionId))
                {
                    m_AllowedConnections.Add(connectionId);
                }

                return true;
            }

            return false;
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            m_AllowedConnections.Remove(connectionId);
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_AllowedConnections.ToArray();
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(CKClientPythonExtension), m_Messenger, connectionId))
            {
                return m_AllowedConnections.Contains(connectionId);
            }

            return false;
        }

        public object UserInterface
        {
            get { return null; }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (m_Messenger != null)
            {
                m_Messenger.AddCallback(typeof(RunScriptRequestMessage), OnMessageReceived);
            }
        }

        #endregion
    }
}
