﻿

namespace CKPythonExtension.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Security;
    #endregion

    [LocalOnly]
    [DataContract]
    public class RunScriptRequestMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RunScriptRequestMessage class.
        /// </summary>
        public RunScriptRequestMessage()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// A string representing the script to run.
        /// </summary>
        [DataMember]
        public string ScriptString
        {
            get;
            set;
        }

        /// <summary>
        /// Used to tag the request by the requestor.
        /// </summary>
        [DataMember]
        public Guid RequestId
        {
            get;
            set;
        }
        #endregion
    }
}
