﻿

namespace CKPythonExtension.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CKLib.Utilities.Messages;
    using System.Runtime.Serialization;
    using CKLib.Utilities.Security;
    #endregion
    [LocalOnly]
    [DataContract]
    public class RunScriptResponseMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the RunScriptResponseMessage class.
        /// </summary>
        public RunScriptResponseMessage()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// A string representing the result of the script that was run.
        /// </summary>
        [DataMember]
        public string ScriptResult
        {
            get;
            set;
        }

        /// <summary>
        /// Used to tag the request by the requestor.
        /// </summary>
        [DataMember]
        public Guid RequestId
        {
            get;
            set;
        }
        #endregion
    }
}
