﻿

namespace CKPythonExtension.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CKLib.Utilities.Security;
    using System.Runtime.Serialization;
    using CKLib.Utilities.Messages;
    #endregion
    [LocalOnly]
    [DataContract]
    public class PythonOutputMessage : IDataMessage
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PythonOutputMessage class.
        /// </summary>
        public PythonOutputMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the PythonOutputMessage class.
        /// </summary>
        /// <param name="error">Indicates whether the message should be treated as an error.</param>
        /// <param name="message">The message being output/</param>
        public PythonOutputMessage(bool error, string message)
        {
            IsError = error;
            PythonMessage = message;
        }
        #endregion

        #region Properties
        /// <summary>
        /// A string representing the output of a script.
        /// </summary>
        [DataMember]
        public string PythonMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PythonMessage represents an error.
        /// </summary>
        [DataMember]
        public bool IsError
        {
            get;
            set;
        }
        #endregion
    }
}
