// CppBootstrap.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include <string>
//#include "objbase.h"
//#include "MSCorEE.h"
#import "C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.tlb" raw_interfaces_only
using namespace mscorlib;

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

/*
#ifdef __cplusplus
extern "C" {
#endif
void WINAPI Bootstrap() {
	HRESULT hr;
	//ICLRRuntimeHost pClrHost = NULL;
	ICLRMetaHost *pMetaHost = NULL;
	hr = CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost, (LPVOID*)&pMetaHost);
	//Dot net 4 New method for loading the CLR
    if(FAILED(hr)){
	    MessageBox(NULL, L"CLRCreateInstance Done.", NULL, NULL);
    }
	ICLRRuntimeInfo * lpRuntimeInfo = NULL;
	hr = pMetaHost->GetRuntime(
		L"v4.0.30319",
		IID_ICLRRuntimeInfo,
		(LPVOID*)&lpRuntimeInfo);
	if(FAILED(hr)){
		MessageBox(NULL, L"Failed to get Runtime Host.", NULL, NULL);
	}
	ICLRRuntimeHost * lpRuntimeHost = NULL;
    hr = lpRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost, IID_ICLRRuntimeHost, (LPVOID *)&lpRuntimeHost);
	if(FAILED(hr)){
		MessageBox(NULL, L"Failed to get Runtime Interface.", NULL, NULL);
	}
	hr = lpRuntimeHost->Start();
	if(FAILED(hr)){
		MessageBox(NULL, L"Failed to start Runtime.", NULL, NULL);
	}

	LPTSTR strDLLPath1 = new TCHAR[MAX_PATH];
	::GetModuleFileName((HINSTANCE)&__ImageBase, strDLLPath1, MAX_PATH);
	std::wstring foo = L"";
	foo.append(strDLLPath1);

	int i = foo.find(
		L"CppBootstrap_4_0.dll"
		);
	foo = foo.substr(0, i);
	foo.append(L"StandaloneLogger.dll");
	 DWORD dwRet = 0;

	hr = lpRuntimeHost->ExecuteInDefaultAppDomain(
        foo.c_str(), L"SBIRS.Logger.SBIRSLogger", L"Run", L"", &dwRet);
}
#ifdef __cplusplus
}
#endif
*/

#ifdef __cplusplus
extern "C" {
#endif
void WINAPI Bootstrap2() {
	HRESULT hr;
	//ICLRRuntimeHost pClrHost = NULL;
	ICLRMetaHost *pMetaHost = NULL;
	hr = CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost, (LPVOID*)&pMetaHost);
	//Dot net 4 New method for loading the CLR
    if(FAILED(hr)){
	    MessageBox(NULL, L"CLRCreateInstance Failed.", NULL, NULL);
    }

	ICLRRuntimeInfo * lpRuntimeInfo = NULL;
    ICLRRuntimeHost * lpRuntimeHost = NULL;
    // Declare enumeration variables
    IEnumUnknown* enumerator = NULL;
    IUnknown* unk = NULL;
    hr = pMetaHost->EnumerateLoadedRuntimes(::GetCurrentProcess(), &enumerator);
    if(FAILED(hr)){
        MessageBox(NULL, L"Failed to get Runtime Enumerator.", NULL, NULL);
    }
    else{
        while(enumerator->Next(1, &unk, NULL) == S_OK){
            hr = unk->QueryInterface(IID_ICLRRuntimeInfo, (LPVOID*)&lpRuntimeInfo);
            if(FAILED(hr)){
                unk->Release();
                unk = NULL;
                continue;
            }
            else{
                hr = lpRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost, IID_ICLRRuntimeHost, (LPVOID *)&lpRuntimeHost);
                if(FAILED(hr)){
                    MessageBox(NULL, L"Failed to get Runtime Interface.", NULL, NULL);
                }
                hr = lpRuntimeHost->Start();
                if(FAILED(hr)){
                    MessageBox(NULL, L"Failed to start Runtime.", NULL, NULL);
                }
                if(SUCCEEDED(hr) && lpRuntimeHost != NULL){
                    hr = StartInDefaultAppDomain(lpRuntimeHost, L"CKClient.dll", L"CKDevClient.InjectionPoint", L"Run", L"");
                    if(!FAILED(hr)){
                        break;
                    }
                }
                if(FAILED(hr)){
                    MessageBox(NULL, L"Failed to locate the Client dll.", NULL, NULL);
                }
            }
        }

        if(lpRuntimeInfo == NULL){
            MessageBox(NULL, L"Failed to get Runtime Host.", NULL, NULL);
        }
        else{
           /* lpRuntimeInfo->Release();
            lpRuntimeInfo = NULL;*/
        }

        if(unk != NULL){
            unk->Release();
            unk = NULL;
        }

        if(enumerator != NULL){
            enumerator->Release();
            enumerator = NULL;
        }
    }

}
#ifdef __cplusplus
}
#endif


HRESULT StartInDefaultAppDomain(ICLRRuntimeHost* lpRuntimeHost, const TCHAR* managedDllName, const TCHAR* fullTypeName, const TCHAR* methodName, const TCHAR* parameter){
    LPTSTR strDLLPath1 = new TCHAR[MAX_PATH];
	::GetModuleFileName((HINSTANCE)&__ImageBase, strDLLPath1, MAX_PATH);
	std::wstring foo = L"";
	foo.append(strDLLPath1);

    int i = foo.find(
		L"DevClientBootstrapper"
		);

	foo = foo.substr(0, i);
	foo.append(managedDllName);
	 DWORD dwRet = 0;

	HRESULT hr = lpRuntimeHost->ExecuteInDefaultAppDomain(
        foo.c_str(), fullTypeName, methodName, parameter, &dwRet);

    return hr;
}
