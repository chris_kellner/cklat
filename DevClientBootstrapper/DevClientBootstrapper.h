
#pragma once
#ifdef __cplusplus
extern "C" {
#endif
void WINAPI Bootstrap();
void WINAPI Bootstrap2();
HRESULT StartInDefaultAppDomain(ICLRRuntimeHost* host, const TCHAR* managedDllName, const TCHAR* fullTypeName, const TCHAR* methodName, const TCHAR* parameter);
#ifdef __cplusplus
}
#endif
