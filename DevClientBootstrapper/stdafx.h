// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include "MSCorEE.h"
#include "metahost.h"
#pragma comment(lib, "mscoree.lib")
#include "DevClientBootstrapper.h"
#include <string>
/*C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\*/
//#pragma comment(lib, "mscorlib")

// TODO: reference additional headers your program requires here
