﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Connection.Client;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Patterns;
using CKDevClient.Interface;
using System.Reflection;
using System.IO;
using CKLib.Utilities.DataStructures.Configuration;
using CKLib.Utilities.Diagnostics;

namespace CKDevClient
{
    public class InjectionPoint
    {
        private ClientSettings settings = ClientSettings.Instance;

        public InjectionPoint()
        {
        }

        static InjectionPoint()
        {
            ConfigurationManager.ProductName = "CKDevHost";
            ConfigurationManager.ProductVersion = new Version(0, 3, 0, 0);
            ConfigurationManager.Reload(false);
        }

        static void ConfigureStreams()
        {
            Error.OnEntry += Error_OnEntry;
            Warning.OnEntry += Error_OnEntry;
            Standard.OnEntry += Error_OnEntry;
        }

        static void Error_OnEntry(object sender, OutputEventArgs e)
        {
            Client.Instance.SendNetworkMessage(new StringMessage(e.ToString()));
        }

        public static Int32 Run(String parameter)
        {
            if (!Client.Instance.ConnectionOk)
            {
                var group = ConfigurationManager.Current["CKDevClient"];
                if (group != null)
                {
                    string currentPath = Assembly.GetExecutingAssembly().Location;
                    currentPath = Path.GetDirectoryName(currentPath);
                    currentPath = Path.Combine(currentPath, @"Plugins\");
                    string[] plugins;
                    if (group.TryReadValues("Extensions", out plugins))
                    {
                        var pluginPaths = plugins.Select(s => Path.Combine(currentPath, s)).ToArray();
                        Client.Instance.ExtensionManager.Initialize(
                            Client.Instance,
                            pluginPaths);
                    }
                }

                Client.Instance.AddCallback(typeof(StringMessage), Instance.ProcessMessage);
                Client.Instance.ReconnectionMode = ClientReconnectionMode.RetryOnce;
                Client.Instance.ConnectionOpened += Instance.Instance_ConnectionOpened;
                Client.Instance.ConnectToLocalhost();
            }

            return 0;
        }

        private void Instance_ConnectionOpened(object sender, ClientConnectionEventArgs e)
        {
            Client.Instance.SharedObjectPool.ShareObject<IClientSettings>(settings);
        }

        public static InjectionPoint Instance
        {
            get
            {
                return Singleton<InjectionPoint>.Instance;
            }
        }

        private void ProcessMessage(MessageEnvelope pair)
        {
            var msg = pair.Message as StringMessage;
            if (msg != null)
            {
                Console.WriteLine(msg.Message);
                Client.Instance.SendNetworkMessage(pair.Message);
            }
        }
    }
}
