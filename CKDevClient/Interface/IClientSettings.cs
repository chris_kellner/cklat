﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Security;

namespace CKDevClient.Interface
{
    public interface IProcessInformation
    {
        int Id { get; }
        string MainWindowTitle { get; }
        string ProcessName { get; }
    }

    public interface IClientSettings
    {
        IProcessInformation ProcessInformation
        {
            get;
        }

        [LocalOnly]
        void ConnectToServer(string address, string serverName);

        [LocalOnly]
        void DumpDynamicTypeModule();

        string[] GetAvailableExtensions(Guid serverId);
    }
}
