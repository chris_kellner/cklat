﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKDevClient.Interface;
using CKLib.Utilities.Patterns;
using System.Diagnostics;
using CKLib.Utilities.Connection.Client;
using CKLib.Utilities.Security;
using CKLib.Utilities.Proxy;

namespace CKDevClient
{
    public class ProcessInformation : IProcessInformation
    {
        public ProcessInformation()
        {
        }

        public ProcessInformation(Process p)
        {
            ProcessName = p.ProcessName;
            MainWindowTitle = p.MainWindowTitle;
            Id = p.Id;
        }

        #region IProcessInformation Members

        public string ProcessName
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string MainWindowTitle
        {
            get;
            set;
        }

        #endregion
    }

    public class ClientSettings
    {
        public ClientSettings()
        {
            ProcessInformation = new ProcessInformation(Process.GetCurrentProcess());
        }

        [LocalOnly]
        public void ConnectToServer(string address, string serverName)
        {
            Client.Instance.ConnectToServer(address, serverName, false, null, null);
        }

        [LocalOnly]
        public void DumpDynamicTypeModule()
        {
            //FactoryHelpers.ExportCommunicationModule();
        }

        public ProcessInformation ProcessInformation
        {
            get;
            private set;
        }

        public string[] GetAvailableExtensions(Guid serverId)
        {
            return Client.Instance.ExtensionManager.GetExtensionsForConnection(serverId).Select(e=>e.ExtensionName).ToArray();
        }

        public static ClientSettings Instance
        {
            get
            {
                return Singleton<ClientSettings>.Instance;
            }
        }
    }
}
