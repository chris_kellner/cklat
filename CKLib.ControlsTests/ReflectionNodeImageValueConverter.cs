﻿using CKLib.Controls.TreeGridView;
using CKLib.Utilities.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CKLib.ControlsTests
{
    public class ReflectionNodeImageValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TreeGridRow row = value as TreeGridRow;
            if (row != null)
            {
                string path = parameter as string;
                if (!string.IsNullOrEmpty(path))
                {

                }

                var node = row.SelectedObject as ReflectionNode;
                if (node != null)
                {
                    if (node.IsReadOnly)
                    {
                        return @"C:\Projects\Git\CKLAT\CKLib.ControlsTests\R.png";
                    }
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
