﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CKLib.ProxyTesting;
using CKLib.Utilities.DataStructures;

namespace CKLib.ControlsTests
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            var node1 = ReflectionNode.Create(this);

            var grid = tree_Grid.TreeGrid;
            //grid.MaxLevel = 3;
            grid.AddRoot(node1, (n) => ((ReflectionNode)n).Children);
            grid.AddRoot(this, (d) => GetChildren((DependencyObject)d));
            tree_Grid.TreeGrid = grid;
        }

        public static IEnumerable<DependencyObject> GetChildren(DependencyObject obj)
        {
            int count = VisualTreeHelper.GetChildrenCount(obj);
            for (int i = 0; i < count; i++)
            {
                yield return VisualTreeHelper.GetChild(obj, i);
            }
        }
    }
}
