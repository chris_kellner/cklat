﻿using CKLib.Utilities.Interception.Behaviors;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Interception
{
    [TestFixture(Category="Interception")]
    public class PropertyChangedInterceptorFactoryTests
    {
        public interface TestInterface
        {
            string Name { get; set; }
            int Value { get; set; }
            string Write();
        }

        public class TestClass : TestInterface
        {

            public string Name
            {
                get;
                set;
            }

            public int Value
            {
                get;
                set;
            }

            public string Write()
            {
                return Name;
            }
        }

        [Test]
        public void TestCreate()
        {
            TestClass cl = new TestClass();
            PropertyChangedInterceptorFactory factory = new PropertyChangedInterceptorFactory();
            TestInterface iFace = factory.CreateInterceptor<TestInterface>(cl);
            Assert.IsNotNull(iFace);
        }

        [Test]
        public void TestNotify()
        {
            PropertyChangedEventHandler h = null;
            h = (PropertyChangedEventHandler)Delegate.Combine(h, (PropertyChangedEventHandler)((s, e) => { }));
            Assert.IsNotNull(h);
            TestClass cl = new TestClass();
            PropertyChangedInterceptorFactory factory = new PropertyChangedInterceptorFactory();
            TestInterface iFace = factory.CreateInterceptor<TestInterface>(cl);
            Assert.IsNotNull(iFace);
            ((INotifyPropertyChanged)iFace).PropertyChanged += (s, e) =>
            {
                Assert.AreEqual("Test", iFace.Name);
                cl.Value = 12;
            };

            iFace.Name = "Test";
            Assert.AreEqual(12, cl.Value);
        }
    }
}
