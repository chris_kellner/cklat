﻿using CKLib.Utilities.Diagnostics;
using CKLib.Utilities.Interception;
using CKLib.Utilities.Interception.Behaviors;
using CKLib.Utilities.Proxy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Tests.CKLib.Utilities.Interception
{
    [TestFixture]
    public class ProxyInterceptorFactoryTests
    {
        public interface TestInterface4
        {
            bool IsAlive { get; set; }
        }

        public interface TestInterface1
        {
            string Name { get; set; }
        }

        public interface TestInterface2
        {
            string Name { get; set; }
            int Value { get; set; }
        }

        public interface TestInterface3
        {
            string Name { get; set; }
            int Value { get; set; }
            string Write();
        }

        public interface TestProxyInterface
        {
            TestInterface3 Child { get; set; }
        }

        public class TestClass : TestInterface4
        {
            public string Name
            {
                get;
                set;
            }

            public int Value
            {
                get;
                set;
            }

            public string Write()
            {
                return Name;
            }

            public TestClass Child
            {
                get;
                set;
            }

            public bool IsAlive
            {
                get;
                set;
            }
        }

        [Test]
        public void TestCreate1()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface1 iFace = factory.CreateInterceptor<TestInterface1>(cl);
            Assert.IsNotNull(iFace);
        }

        [Test]
        public void TestInterceptor()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            IInterceptor iFace = (IInterceptor)factory.CreateInterceptor<TestInterface1>(cl);
            try
            {
                Assert.IsNotNull(iFace);
                Assert.IsNotNull(iFace.GetInner());
                Assert.IsNotNull(iFace.GetRoot());
                Assert.IsNotNull(iFace.CreatorFactory);
            }
            catch (InvalidProgramException ex)
            {
                string location = null;
                FactoryHelpers.ExportCommunicationModule(ref location);
                if (location != null)
                {
                    System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(location));
                }
                throw;
            }
        }

        [Test]
        public void TestInterceptor_Stacked()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory proxy = new ProxyInterceptorFactory();
            PropertyChangedInterceptorFactory propChanged = new PropertyChangedInterceptorFactory();
            TraceInterceptorFactory trace = new TraceInterceptorFactory();
            TestInterface1 iFace = proxy.CreateInterceptor<TestInterface1>(cl);
            iFace = propChanged.CreateInterceptor<TestInterface1>(iFace);
            iFace = trace.CreateInterceptor<TestInterface1>(iFace);

            Assert.IsInstanceOf<IInterceptor>(iFace);
            Assert.IsInstanceOf<TestInterface1>(iFace);
            Assert.IsInstanceOf<INotifyPropertyChanged>(iFace);
            Assert.IsInstanceOf<ITraceable>(iFace);
            Assert.IsInstanceOf<TestInterface4>(iFace);
            Assert.IsInstanceOf<TestClass>(((IInterceptor)iFace).GetRoot());
            try
            {
                ((INotifyPropertyChanged)iFace).PropertyChanged += (s, e) => { Thread.Sleep(1000); };
                ((ITraceable)iFace).TraceEvent += (s, e) =>
                {
                    if (e.Event == TraceEvent.Exit)
                    {
                        Assert.LessOrEqual(1000, e.Elapsed.TotalMilliseconds);
                    }
                };

                ((TestInterface4)iFace).IsAlive = true;
            }
            catch (BadImageFormatException m)
            {
                string path = null;
                FactoryHelpers.ExportCommunicationModule(ref path);
                Process.Start(Path.GetDirectoryName(path));
                throw;
            }
        }

        [Test]
        public void TestCreate_Getter_Conversion()
        {
            TestClass cl = new TestClass();
            cl.Child = new TestClass()
            {
                Name = "Child-A",
                Value = 2,
                IsAlive = true
            };
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestProxyInterface iFace = factory.CreateInterceptor<TestProxyInterface>(cl);
            Assert.IsNotNull(iFace);
            Assert.IsNotNull(iFace.Child);
        }

        [Test]
        public void TestCreate_Setter_Conversion()
        {
            TestClass cl = new TestClass();
            cl.Child = new TestClass()
            {
                Name = "Child-A",
                Value = 2,
                IsAlive = true
            };
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestProxyInterface iFace = factory.CreateInterceptor<TestProxyInterface>(cl);
            Assert.IsNotNull(iFace);
            Assert.IsNotNull(iFace.Child);

            var child = iFace.Child;
            child.Name = "Child-B";
            iFace.Child = child;
            Assert.AreEqual("Child-B", cl.Child.Name);
        }

        [Test]
        public void TestCreate2()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface2 iFace = factory.CreateInterceptor<TestInterface2>(cl);
            Assert.IsNotNull(iFace);
        }

        [Test]
        public void TestCreate3()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface3 iFace = factory.CreateInterceptor<TestInterface3>(cl);
            Assert.IsNotNull(iFace);
        }

        [Test]
        public void TestCreate_WithBaseInterfaces()
        {
            TestClass cl = new TestClass();
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface3 iFace = factory.CreateInterceptor<TestInterface3>(cl);
            Assert.IsNotNull(iFace);
            Assert.IsInstanceOf<TestInterface4>(iFace);
        }

        [Test]
        public void TestRefType()
        {
            TestClass cl = new TestClass()
            {
                Name = "Test"
            };
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface3 iFace = factory.CreateInterceptor<TestInterface3>(cl);
            Assert.IsNotNull(iFace);
            Assert.AreEqual("Test", iFace.Name);
            iFace.Name = "Test2";
            Assert.AreEqual("Test2", cl.Name);
        }

        [Test]
        public void TestValueType()
        {
            TestClass cl = new TestClass()
            {
                Value = 12
            };
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface3 iFace = factory.CreateInterceptor<TestInterface3>(cl);
            Assert.IsNotNull(iFace);
            Assert.AreEqual(12, iFace.Value);
            iFace.Value = 13;
            Assert.AreEqual(13, cl.Value);
        }

        [Test]
        public void TestMethod()
        {
            TestClass cl = new TestClass()
            {
                Name = "Test"
            };
            ProxyInterceptorFactory factory = new ProxyInterceptorFactory();
            TestInterface3 iFace = factory.CreateInterceptor<TestInterface3>(cl);
            Assert.IsNotNull(iFace);
            Assert.AreEqual("Test", iFace.Write());
            iFace.Name = "Test2";
            Assert.AreEqual("Test2", cl.Write());
        }
    }
}
