﻿using CKLib.Utilities.Diagnostics;
using CKLib.Utilities.Interception.Behaviors;
using CKLib.Utilities.Proxy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Interception
{
    [TestFixture]
    public class TraceInterceptorFactoryTests
    {
        public interface TestInterface
        {
            string Name { get; set; }
            int Value { get; set; }
            string Write();
        }

        public class TestClass : TestInterface
        {
            public string Name
            {
                get;
                set;
            }

            public int Value
            {
                get;
                set;
            }

            public string Write()
            {
                return Name;
            }
        }

        [Test]
        public void TestTrace_Create()
        {
            TraceInterceptorFactory factory = new TraceInterceptorFactory();
            TestClass cl = new TestClass();
            TestInterface iFace = factory.CreateInterceptor<TestInterface>(cl);
            Assert.IsInstanceOf<ITraceable>(iFace);
        }

        [Test]
        public void TestTrace_Exit()
        {
            TraceInterceptorFactory factory = new TraceInterceptorFactory();
            TestClass cl = new TestClass(){
                Name = "Yes"
            };

            TestInterface iFace = factory.CreateInterceptor<TestInterface>(cl);
            ((ITraceable)iFace).TraceEvent += (s, e) =>
            {
                if (e.Event == TraceEvent.Exit)
                {
                    Assert.AreEqual("Yes", e.Data);
                }
            };

            iFace.Write();
        }

        [Test]
        public void TestTrace_Enter()
        {
            TraceInterceptorFactory factory = new TraceInterceptorFactory();
            TestClass cl = new TestClass()
            {
                Name = "Yes"
            };

            TestInterface iFace = factory.CreateInterceptor<TestInterface>(cl);
            ((ITraceable)iFace).TraceEvent += (s, e) =>
            {
                if (e.Event == TraceEvent.Enter)
                {
                    Assert.AreEqual("No", ((object[])e.Data)[0]);
                }
            };

            iFace.Name = "No";
        }
    }
}
