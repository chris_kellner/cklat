﻿using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Evaluator.Runtime;
using CKLib.Utilities.Language.Parse;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Language
{
    [TestFixture]
    public class ScriptMethodExtensionTests
    {
        static MathGrammar grammar = new MathGrammar();

        [Test]
        public void TestFunc_1_From_Func()
        {
            grammar.AotCompile();
            ScriptMethod m = grammar.Evaluate<ScriptMethod>("c => c * 5");
            ScriptFunction f = m.CreateDelegate();
            Assert.AreEqual(15, (int)f(3));

            var f1 = f.CastDelegate<Func<int, int>>();

            Assert.AreEqual(15, f1(3));
        }

        [Test]
        public void TestFunc_1_From_Method()
        {
            grammar.AotCompile();
            ScriptMethod m = grammar.Evaluate<ScriptMethod>("(c) => c * 5");
            var f1 = m.MakeDelegate<Func<int, int>>(null);

            Assert.AreEqual(15, f1(3));
        }

        [Test]
        [TestCase("(c,d)=>c*d", 3,5,15)]
        [TestCase("(int c, int d)=>c*d", 3, 5, 15)]
        [TestCase("(int c, int d)=>2 * c + d", 5, 5, 15)]
        [TestCase("(int c, int d)=> c*d", 3, 5, 15)]
        [TestCase("(c, d)=> { return c*d; }", 3, 5, 15)]
        public void TestFunc_int_2(string function, int c, int d, int expected)
        {
            grammar.AotCompile();
            ScriptMethod m = grammar.Evaluate<ScriptMethod>(function);
            var f1 = m.MakeDelegate<Func<int, int, int>>(null);

            Assert.AreEqual(expected, f1(c, d));
        }

        [Test]
        public void TestAction_1()
        {
            grammar.AotCompile();
            var ctx = new ScriptContext();
            ctx.References.ImportType(typeof(Console));
            ScriptMethod m = grammar.Evaluate<ScriptMethod>(ctx, "c => { Console.WriteLine(c); }");
            var f1 = m.MakeDelegate<Action<int>>();
            for (int i = 0; i < 5; i++)
            {
                f1(i);
            }
        }
    }
}
