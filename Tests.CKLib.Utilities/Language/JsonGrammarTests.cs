﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Evaluator.Runtime;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Translation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Language
{
    [TestFixture]
    public class JsonGrammarTests
    {
        #region TestClasses
        private class SimpleObject
        {
            public string Name { get; set; }
            public int Value { get; set; }

            public override string ToString()
            {
                return base.ToString() + " Name: " + Name + " Value: " + Value;
            }
        }
        #endregion

        private JsonGrammar m_Grammar;

        [TestFixtureSetUp]
        public void Setup()
        {
            m_Grammar = new JsonGrammar();
            m_Grammar.AotCompile();
        }

        [Test]
        [TestCase(1009, typeof(int), "1009")]
        [TestCase("1009", typeof(string), "\"1009\"")]
        [TestCase(1.15, typeof(double), "1.15")]
        [TestCase(ConsoleColor.Black, typeof(ConsoleColor), "\"Black\"")]
        [TestCase(new byte[] { 1, 2, 3 }, typeof(byte[]), "[1, 2, 3]")]
        public void TestSimpleTypes(object value, Type tResult, string json)
        {
            var rslt = m_Grammar.Evaluate(tResult, json);
            Assert.AreEqual(value, rslt);
        }

        [Test]
        public void TestSimpleObject_WithType()
        {
            var rslt = m_Grammar
                .Evaluate<SimpleObject>("{ \"Name\":\"Chris\", \"Value\":12 }");
            Assert.AreEqual("Chris", rslt.Name);
            Assert.AreEqual(12, rslt.Value);
        }

        [Test]
        public void TestSimpleObject_AsObject()
        {
            var rslt = m_Grammar
                .Evaluate<object>("{ \"Name\":\"Chris\", \"Value\":12 }");
            var obj = rslt as IDictionary<string, object>;
            Assert.NotNull(obj);
            Assert.AreEqual("Chris", obj["Name"]);
            Assert.AreEqual(12, obj["Value"]);
        }

        [Test]
        public void TestSimpleObject_AsMeta()
        {
            var rslt = m_Grammar
                .Evaluate<IMetadataAccessible>("{ \"Name\":\"Chris\", \"Value\":12 }");
            Assert.AreEqual("Chris", rslt.GetMember<string>("Name"));
            Assert.AreEqual(12, rslt.GetMember<int>("Value"));
        }

        [Test]
        public void DumpInStages()
        {
            using (var fs = new FileStream("json.grm", FileMode.Create, FileAccess.Write))
            {
                using (FlatGrammarWriter writer = new FlatGrammarWriter(fs))
                {
                    writer.WriteGrammar(m_Grammar);
                }
            }

            string json = "{ \"Name\":\"Chris\", \"Value\":12 }";
            var tokens = m_Grammar.CreateLexer().LexTokens(json);

            var parseTree = m_Grammar.Parse(json);
            using (var sr = new StreamWriter("jsonSample.txt"))
            {
                sr.WriteLine("Raw:");
                sr.WriteLine(json);
                sr.WriteLine();
                sr.WriteLine("Tokens:");
                foreach (var token in tokens)
                {
                    sr.WriteLine(token);
                }

                sr.WriteLine();
                sr.WriteLine("Parse Tree:");
                sr.WriteLine(parseTree.ToString(true));

                XmlLanguageWriter xml = new XmlLanguageWriter(m_Grammar);
                xml.Write(parseTree);
                sr.WriteLine();
                sr.WriteLine("Xml Tree:");
                sr.WriteLine(xml.PrettyPrint());

                var obj = m_Grammar.CreateEvaluator(new ScriptContext()).Evaluate<SimpleObject>(parseTree);
                sr.WriteLine();
                sr.WriteLine("Created:");
                sr.WriteLine(obj);
            }
        }
    }
}
