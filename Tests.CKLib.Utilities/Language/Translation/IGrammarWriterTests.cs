﻿using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Translation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Language.Translation
{
    [TestFixture]
    public class IGrammarWriterTests
    {
        [Test]
        public void TestSaveMathGrammar()
        {
            var g = new MathGrammar();
            g.AotCompile();

            using (var fs = new FileStream("temp.grm", FileMode.Create, FileAccess.Write))
            {
                using (var writer = new FlatGrammarWriter(fs))
                {
                    writer.WriteGrammar(g);
                }
            }

            Process.Start("temp.grm");
        }
    }
}
