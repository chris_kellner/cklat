﻿using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Translation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Tests.CKLib.Utilities.Language.Translation
{
    [TestFixture]
    public class FlatGrammarReaderTests
    {
        private FlatGrammarReader grammarReader;

        [TestFixtureSetUp]
        public void SetUp()
        {
            grammarReader = new FlatGrammarReader();
        }

        [Test]
        [TestCase(@"/(?s)\/\*(.*?)\*\//", 0)]
        [TestCase(@"/(?m)\/\/.*$/", 0)]
        [TestCase(@"    .Block_Comment -> /(?s)\/\*(.*?)\*\//", 22)]
        public void TestRegex(string regex, int idx)
        {
            Grammar r = grammarReader.Grammar;
            Terminal term = (Terminal)r.FindGrammarElement("regex");
            Token t = term.ParseToken(new ScriptSource()
            {
                Input = regex,
                CurrentPosition = idx
            });

            Assert.IsNotNull(t);
        }

        [Test]
        public void TestFlatGrammar()
        {
            using (var fs = new FileStream("temp.grm", FileMode.Open))
            {
                var grammar = grammarReader.Grammar;
                using (var sr = new StreamReader(fs))
                {
                    string script = sr.ReadToEnd();
                    var lexer = grammar.CreateLexer();
                    var tokens = lexer.LexTokens(script).ToList();
                    foreach (var token in tokens)
                    {
                        Console.WriteLine(token);
                    }
                    Console.WriteLine("Lex time: " + lexer.LastLexTime);

                    var parser = grammar.CreateParser();
                    var parsed = parser.Parse(tokens);
                    foreach (var piece in parsed)
                    {
                        Console.WriteLine(piece);
                    }
                    Console.WriteLine("Parse time: " + parser.LastParseTime);

                    foreach (var failure in parser.LastErrors.Where(r => r.PatternIndex > 0))
                    {
                        Console.WriteLine(failure);
                    }

                    CompilationUnit unit = new CompilationUnit(grammar);
                    var root = unit.MakeRoot(parsed);

                    var eval = grammar.CreateEvaluator(new ScriptContext());
                    Grammar g = eval.Evaluate<Grammar>(root);
                    Assert.NotNull(g);
                    Assert.AreEqual("CMath", g.Name);
                }
            }
        }

        [Test]
        public void TestReadGrammar()
        {
            using (var fs = new FileStream("temp.grm", FileMode.Open))
            {
                var g = grammarReader.ReadGrammar(fs);
                Assert.NotNull(g);
                Assert.AreEqual("CMath", g.Name);
            }
        }

        [Test]
        [TestCase("absngkn%%>")]
        [TestCase(@"
{
  ""QuoteChar"": ""\"""",
  ""CanAssign"": False,
  ""HasValue"": False
}")]
        [TestCase(" this is neato! ")]
        [TestCase(@"{
block here
}")]
        public void TestCodeBlock(string code)
        {
            var block = StringUtils.ToCodeBlock(code, "<%Json", "%>", '%');

            var token = CTokens.CodeBlock(null, "json", "<%Json", "%>", '%');
            var tok = token.ParseToken(new ScriptSource()
            {
                 Input = block
            });

            //Assert.AreEqual("(?ns)<%Json(.(?!%>)|%%>)*%>", token.Pattern);
            Assert.IsNotNull(tok);
            string back = StringUtils.FromCodeBlock(tok.Value, "<%Json", "%>", '%');
            Assert.AreEqual(code, back);
        }
    }
}
