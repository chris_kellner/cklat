﻿using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Translation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Language
{
    [TestFixture]
    public class MathGrammarTests
    {
        private Grammar grammar;
        private CompilationUnit compilationUnit;

        [TestFixtureSetUp]
        public void SetUp()
        {
            grammar = new MathGrammar();
            grammar.EnableGrammarValidation = true;
            grammar.AotCompile();
            compilationUnit = new CompilationUnit(grammar);
        }

        [Test]
        public void TestForLoop()
        {
            string input = @"
{
    for(int i=0;i<5;i=i + 1){
        Console.WriteLine(i);
    }
    for(int i=0;i<5;i++){
        Console.WriteLine(i);
    }
    for(int i=5;i>= 0;--i){
        Console.WriteLine(i);
    }
    // Skip this one until we can break
    int i = 0;
    for(;;){
        Console.WriteLine(i++);
        if(i > 3)
        {
            Console.WriteLine(""breaking"");
            break;
        }
    }

    int j = 10;
    // prints 9 ... 1
    while(--j > 0){
        Console.WriteLine(j);
    }

    // prints 0 ... 10
    do{
        Console.WriteLine(j);
    }while(j++ < 10);
}
";
            ScriptContext ctx = new ScriptContext();
            ctx.SetGlobal("x", 100);
            ctx.References.ImportNamespace(typeof(int).Assembly, "System");
            Evaluate(ctx, input);

            Console.WriteLine(input);
            foreach (var var in ctx.Globals)
            {
                Console.WriteLine(var.Key + ": " + var.Value);
            }
        }

        [Test]
        public void TestList()
        {
            string input = @"
List<string> strings = new List<string>();
strings.Add(""1"");
strings.Add(""2"");
return strings;
";
            ScriptContext ctx = new ScriptContext();
            ctx.References.ImportNamespace(typeof(int).Assembly, "System.Collections.Generic");
            List<string> strings = (List<string>)Evaluate(ctx, input);

            Assert.NotNull(strings);
            Assert.Contains("1", strings);
            Assert.Contains("2", strings);

            string first = (string)Evaluate(ctx, "return strings[0];");
            Assert.AreEqual("1", first);
        }

        [Test]
        public void TestBreak()
        {
            string input = @"
    int i = 0;
    for(;;){
        Console.WriteLine(i++);
        if(i > 3)
        {
            Console.WriteLine(""breaking"");
            break;
        }
    }
";
            ScriptContext ctx = new ScriptContext();
            ctx.SetGlobal("x", 100);
            ctx.References.ImportNamespace(typeof(int).Assembly, "System");
            Evaluate(ctx, input);

            Console.WriteLine(input);
            foreach (var var in ctx.Globals)
            {
                Console.WriteLine(var.Key + ": " + var.Value);
            }
        }

        [Test]
        public void TestLambda()
        {
            string input = @"var y = c => 5 * c; 
                            for(int i=0;i<50;i++){
                                Console.WriteLine(y(3));
                            }";
            ScriptContext ctx = new ScriptContext();
            ctx.SetGlobal("x", 100);
            ctx.References.ImportNamespace(typeof(int).Assembly, "System");
            Evaluate(ctx, input);

            Console.WriteLine(input);
            foreach (var var in ctx.Globals)
            {
                Console.WriteLine(var.Key + ": " + var.Value);
            }
        }

        private object Evaluate(ScriptContext ctx, string input)
        {
            //var grammar = new MathGrammar();
            var lex = grammar.CreateLexer();
            List<AElement> tokens = lex.LexTokens(input).ToList();
            Console.WriteLine("Lex time: " + lex.LastLexTime.TotalMilliseconds + "ms");
            try
            {
                foreach (Token t in tokens)
                {
                    Console.WriteLine(t);
                }
            }
            catch (LexerException ex)
            {
                Console.WriteLine(ex.Message);
            }

            var parser = grammar.CreateParser();
            var parsed = parser.Parse(tokens);
            Console.WriteLine("Parse time: " + parser.LastParseTime.TotalMilliseconds + "ms");
            var evaluator = grammar.CreateEvaluator(ctx);
            XmlLanguageWriter writer = new XmlLanguageWriter(grammar);
            var sw = new System.Diagnostics.Stopwatch();
            EvaluationResult retVal = null;
            var root = compilationUnit.MakeRoot(parsed);
            
            Console.WriteLine(root.ToString(true));
            sw.Start();
            retVal = evaluator.Evaluate(root);
            sw.Stop();
            Console.WriteLine(retVal.Value);
            
            Console.WriteLine("Eval time: " + sw.Elapsed.TotalMilliseconds + "ms");
            //writer.Save("out.xml");
            //System.Diagnostics.Process.Start("out.xml");
            return retVal.Value;
        }

        [Test]
        public void TestCurry()
        {
            var func = Compile(@"
{
    double y = 500 * x - 12;
    Console.BackgroundColor = ConsoleColor.Blue;
    Console.WriteLine(y);
    return y;
}
");

            for (int i = 0; i < 10; i++)
            {
                func(i);
            }

            Assert.AreEqual(ConsoleColor.Blue, Console.BackgroundColor);
        }

        Func<double, double> Compile(string input)
        {
            var ctx = new ScriptContext();
            ctx.References.ImportNamespace(typeof(int).Assembly, "System");
            ctx.DeclareGlobal("x", typeof(double));

            var del = grammar.CreateDelegate<double>(ctx, input);

            return x =>
            {
                ctx.SetGlobal("x", x);
                return del();
            };
        }

        [Test]
        [TestCase("() =>")]
        [TestCase("(c) =>")]
        [TestCase("c =>")]
        [TestCase("(c,d) =>")]
        [TestCase("(int c) =>")]
        [TestCase("(int c, int d) =>")]
        public void TestLambdaParameters_void(string text)
        {
            var elements = grammar.Parse(text);
            Assert.IsTrue(elements.Nodes[0].IsType("method_parameters"), "Was: " + elements.Nodes[0].ToString(true));
            Assert.IsTrue(elements.Nodes[1].IsType("Op_Lambda"), "Was: " + elements.Nodes[0].ToString(true));
        }

        [Test]
        [TestCase("1+1", 2)]
        [TestCase("int i=1;return i++;", 1)]
        [TestCase("int i=1;return ++i;", 2)]
        [TestCase("int i=2;return i--;", 2)]
        [TestCase("int i=2;return --i;", 1)]
        [TestCase("12.0 * 3", 36.0)]
        [TestCase("12.0f * 2.0", 24d)]
        [TestCase("12 - 10", 2)]
        [TestCase("12 - -10", 22)]
        [TestCase("12 - 10 * 2", -8)]
        [TestCase("12 - 10 /2", 7)]
        [TestCase("12 / 10", 1)]
        [TestCase("12 / 10f", 1.2f)]
        [TestCase("12f / 10", 1.2f)]
        [TestCase("12f + 10 - 4 / 2.0", 20d)]
        [TestCase("12f + (10 - 4) / 2.0", 15d)]
        [TestCase("(12f + 10 - 4) / 2.0", 9d)]
        public void TestMathExpressions(string input, object expectedResult)
        {
            var rslt = grammar.Evaluate<object>(input);
            Assert.AreEqual(expectedResult, rslt);
        }

        [Test]
        [TestCase("\"Hi \" + \"there\"", "Hi there")]
        public void TestStringExpressions(string input, object expectedResult)
        {
            var rslt = grammar.Evaluate<object>(input);
            Assert.AreEqual(expectedResult, rslt);
        }

        [Test]
        [TestCase("false", false)]
        [TestCase("true", true)]
        [TestCase("127b", (byte)127)]
        [TestCase("1", 1)]
        [TestCase("-4", -4)]
        [TestCase("123f", 123f)]
        [TestCase("1.23f", 1.23f)]
        [TestCase("-123f", -123f)]
        [TestCase("12e3f", 12e3f)]
        [TestCase("-12e-3f", -12e-3f)]
        [TestCase("12e+3f", 12e+3f)]
        [TestCase("6.3123", 6.3123d)]
        [TestCase("6d", 6d)]
        [TestCase("6e10d", 6e10)]
        [TestCase("6e10", 6e10)]
        [TestCase("-6e10d", -6e10)]
        [TestCase("-6e-2d", -6e-2)]
        [TestCase("-6e+2", -6e+2)]
        [TestCase("\"Hi there\"", "Hi there")]
        public void TestLiterals(string input, object expectedResult)
        {
            var rslt = grammar.Evaluate<object>(input);
            Assert.AreEqual(expectedResult, rslt);
        }

        [Test]
        [TestCase(1, "One")]
        [TestCase(2, "Two")]
        [TestCase(3, "Four")]
        [TestCase(4, "Four")]
        [TestCase(5, "Five")]
        public void TestSwitch(object target, object value)
        {
            string script = @"switch(target){
case 1:
    return ""One"";
case 2:
    return ""Two"";
case 3:
case 4:
    return ""Four"";
default:
    return ""Five"";
}";
            var ctx = new ScriptContext();
            ctx.SetGlobal("target", target);
            var rslt = grammar.Evaluate<object>(ctx, script);
            Assert.AreEqual(value, rslt);
        }

        [Test]
        public void RunValidation()
        {
            var results = grammar.ValidateGrammar();
            bool fail = false;
            foreach (var rslt in results)
            {
                Console.WriteLine(rslt);
                fail |= rslt.Status == global::CKLib.Utilities.Language.Validation.ValidationStatus.Fail;
            }

            Assert.IsFalse(fail);
        }
    }
}