﻿using CKLib.Utilities.Proxy;
using CKLib.Utilities.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities
{
    public static class TestHelpers
    {
        public static T Reserialize<T>(this T obj)
        {
            byte[] data = null;
            var serial = new BinarySerializer();
            using (var mem = new MemoryStream())
            {
                serial.Serialize(mem, obj);
                data = mem.ToArray();
            }

            T newMsg;
            using (var mem = new MemoryStream(data))
            {
                newMsg = serial.Deserialize<T>(mem);
            }

            return newMsg;
        }

        public static void DumpModuleOnFailure(this Action a)
        {
            try
            {
                a();
            }
            catch
            {
                string location = null;
                FactoryHelpers.ExportCommunicationModule(ref location);
                if (!string.IsNullOrEmpty(location))
                {
                    System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(location));
                }
                throw;
            }
        }
    }
}
