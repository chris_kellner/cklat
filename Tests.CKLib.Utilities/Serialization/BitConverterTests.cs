﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Serialization;
using System.IO;

namespace Tests.CKLib.Utilities.Serialization
{
    [TestFixture]
    public class BitConverterTests
    {
        [Test]
        [TestCase(12, true)]
        [TestCase(294994, false)]
        [TestCase(2080637010, false)]
        public void TestSevenBitEncode(int value, bool bigEndian)
        {
            BitConverter conv = new BitConverter(bigEndian);
            byte[] data;
            using (MemoryStream str = new MemoryStream())
            {
                conv.SevenBitEncode(str, value);
                data = str.ToArray();
            }

            int newVal;
            using (MemoryStream str = new MemoryStream(data))
            {
                newVal = conv.SevenBitDecodeInt32(str);
            }

            Assert.AreEqual(value, newVal);
        }
    }
}
