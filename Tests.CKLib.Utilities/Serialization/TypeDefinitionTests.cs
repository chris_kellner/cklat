﻿using CKLib.Utilities.Messages;
using CKLib.Utilities.Serialization;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;


namespace Tests.CKLib.Utilities.Serialization
{
    [TestFixture]
    public class TypeDefinitionTests
    {
        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", "Foo")]
        public void Test_SetFields(Type type, string field, object value)
        {
            var obj = Activator.CreateInstance(type);
            var def = TypeDefinition.Create(type);
            def.Complete(type);
            def[field].SetValue(obj, value);
            Assert.AreEqual(value, def[field].GetValue(obj));
        }

        [Test]
        public void Test_DictionaryAdder()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            var adder = dict.GetType().CreateAdder();
            adder(dict, new KeyValuePair<string, object>("Test", 12));
            Assert.IsTrue(dict.ContainsKey("Test"));
            Assert.AreEqual(12, dict["Test"]);
        }

        [Test]
        public void Test_StructFieldAccessors()
        {
            var flags = BindingFlags.Instance | BindingFlags.NonPublic;
            object inst = new KeyValuePair<string, object>("Test", "Real Test");
            var getter = typeof(KeyValuePair<string, object>).GetField("key", flags).CreateGetter();
            var setter = typeof(KeyValuePair<string, object>).GetField("key", flags).CreateSetter();
            setter(ref inst, "new");
            Assert.AreEqual("new", getter(inst));
        }

        public struct Cool
        {
            public string m_Field;
        }

        [Test]
        public void Test_ClassFieldAccessors()
        {
            var flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            object inst = new Cool();


            var getter = typeof(Cool).GetField("m_Field", flags).CreateGetter();
            //var setter = CreateSetMethod(typeof(Cool).GetField("m_Field", flags));
            var setter = typeof(Cool).GetField("m_Field", flags).CreateSetter();
            setter(ref inst, "new");
            Assert.AreEqual("new", getter(inst));
        }
    }
}
