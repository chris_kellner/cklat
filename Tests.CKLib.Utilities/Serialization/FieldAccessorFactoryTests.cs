﻿using CKLib.Utilities.Messages;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Serialization;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Tests.CKLib.Utilities.Serialization
{
    [TestFixture]
    public class FieldAccessorFactoryTests
    {
        public enum TestEnum
        {
            One,
            Two,
            Three
        }

        public class TestClass
        {
            public int Value0 { get; set; }
            public string Value1 { get; set; }
            public TestEnum Value2 { get; set; }
        }

        [Test]
        [TestCase(typeof(IFieldAccessor), "FieldName")]
        [TestCase(typeof(APICallRequestMessage), "FunctionName")]
        public void TestFieldName(Type type, string propertyName)
        {
            var info = type.GetProperty(propertyName);
            var accessor = FieldAccessorFactory.CreateAccessor(info);
            Assert.AreEqual(info.Name, accessor.FieldName);
        }

        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName")]
        public void TestFieldType(Type type, string propertyName)
        {
            var info = type.GetProperty(propertyName);
            var accessor = FieldAccessorFactory.CreateAccessor(info);
            Assert.AreEqual(info.PropertyType, accessor.FieldType);
        }


        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", "Test")]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", "")]
        [TestCase(typeof(TestClass), "Value0", 1)]
        [TestCase(typeof(TestClass), "Value1", "")]
        [TestCase(typeof(TestClass), "Value2", TestEnum.Three)]
        public void TestShouldSerializeValue_True(Type type, string propertyName, object sampleValue)
        {
            var info = type.GetProperty(propertyName);
            var accessor = FieldAccessorFactory.CreateAccessor(info);
            Assert.IsTrue(accessor.ShouldSerializeValue(sampleValue));
        }

        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", null)]
        [TestCase(typeof(TestClass), "Value0", default(int))]
        [TestCase(typeof(TestClass), "Value1", default(string))]
        [TestCase(typeof(TestClass), "Value2", default(TestEnum))]
        [TestCase(typeof(StringMessage), "SenderId", null)]
        public void TestShouldSerializeValue_False(Type type, string propertyName, object sampleValue)
        {
            var info = type.GetProperty(propertyName);
            var accessor = FieldAccessorFactory.CreateAccessor(info);
            Assert.IsFalse(accessor.ShouldSerializeValue(sampleValue));
        }

        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", "Test")]
        [TestCase(typeof(TestClass), "Value0", 12)]
        [TestCase(typeof(TestClass), "Value1", "Hello")]
        [TestCase(typeof(TestClass), "Value2", TestEnum.Three)]
        public void TestSetter_Getter(Type type, string propertyName, object value)
        {
            var info = type.GetProperty(propertyName);
            var accessor = FieldAccessorFactory.CreateAccessor(info);
            var obj = Activator.CreateInstance(type);
            accessor.SetValue(obj, value);
            Assert.AreEqual(value, accessor.GetValue(obj));
        }

        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", "test")]
        public void Test_CreateSetter(Type type, string propertyName, object value)
        {
            var info = type.GetProperty(propertyName);
            var setter = info.CreateSetter();
            var getter = info.CreateGetter();
            var obj = Activator.CreateInstance(type);
            setter(ref obj, value);
            Assert.AreEqual(value, getter(obj));
        }

        /*
        [Test]
        [TestCase(typeof(APICallRequestMessage), "FunctionName", typeof(string))]
        public void Test_TypeDefinition(Type type, string propertyName, Type fieldType)
        {
            var info = type.GetProperty(propertyName);
            IFieldAccessor field = null;
            try
            {
                field = FieldAccessorFactory.CreateAccessor(info);

            }
            catch
            {
                string location = null;
                FactoryHelpers.ExportCommunicationModule(ref location);
                System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(location));
            }

            Assert.AreEqual(TypeRegistry.Instance.GetTypeDefinition(fieldType, false),
                field.TypeDefinition);
        }*/
    }
}
