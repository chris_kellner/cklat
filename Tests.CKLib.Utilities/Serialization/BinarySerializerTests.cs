﻿using CKLib.Utilities.Messages;
using CKLib.Utilities.Serialization;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Serialization
{
    [TestFixture]
    public class BinarySerializerTests
    {
        [Test]
        public void TestReferenceType()
        {
            StringMessage msg = new StringMessage("This is a secret message");
            var newMsg = msg.Reserialize();

            Assert.IsNotNull(newMsg);
            Assert.AreEqual(msg.Message, newMsg.Message);
        }

        [Test]
        public void TestReferenceType2()
        {
            StringMessage msg = new StringMessage("This is a secret message");
            msg.SenderId = Guid.NewGuid();
            var newMsg = msg.Reserialize();

            Assert.IsNotNull(newMsg);
            Assert.AreEqual(msg.Message, newMsg.Message);
            Assert.AreEqual(msg.SenderId, msg.SenderId);
        }

        [Test]
        public void TestStringArray()
        {
            CommandLineMessage msg = new CommandLineMessage();
            msg.Args = new string[]
            {
                "One", "Two", "Three"
            };
            msg.SenderId = Guid.NewGuid();

            var newMsg = msg.Reserialize();
            Assert.IsNotNull(newMsg.Args);
            Assert.IsNotEmpty(newMsg.Args);
        }

        [Test]
        public void TestInheritance()
        {
            var msg = new MessageEnvelope(Guid.NewGuid(), new CommandLineMessage()
            {
                SenderId = Guid.NewGuid(),
                Args = new string[]{
                    "One", "Two", "Three"
                }
            });

            var newMsg = msg.Reserialize();
            Assert.IsNotNull(newMsg);
            Assert.IsNotNull(newMsg.Message);
            Assert.IsInstanceOf<CommandLineMessage>(newMsg.Message);
        }

        [Test]
        [TestCase(1000)]
        public void TestTiming(int iterations)
        {
            var msg = new MessageEnvelope(Guid.NewGuid(), new CommandLineMessage()
            {
                SenderId = Guid.NewGuid(),
                Args = new string[]{
                    "One", "Two", "Three"
                }
            });

            var newMsg = msg.Reserialize();
            for (int i = 0; i < iterations; i++)
            {
                newMsg = msg.Reserialize();
            }
        }

        [Test]
        public void TestArrayOfThings()
        {
            IDataMessage[] msgs = {
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  };
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Length, newMsgs.Length);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs[0]);
            Assert.IsInstanceOf<StringMessage>(newMsgs[1]);
        }

        [Test]
        public void TestArrayListOfThings()
        {
            var msgs = new ArrayList(){
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  };
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Count, newMsgs.Count);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs[0]);
            Assert.IsInstanceOf<StringMessage>(newMsgs[1]);
        }

        [Test]
        public void TestDictionaryOfThings()
        {
            var msgs = new Dictionary<string, IDataMessage>(){
                                      { "CommandLineMessage", new CommandLineMessage() { Args = new string[] { "a", "test"}}},
                                      { "StringMessage" , new StringMessage() { Message = "Hello"}}
                                  };
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Count, newMsgs.Count);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs["CommandLineMessage"]);
            Assert.IsInstanceOf<StringMessage>(newMsgs["StringMessage"]);
        }

        [Test]
        public void TestQueueOfThings()
        {
            var msgs = new Queue(new IDataMessage[]{
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  });
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Count, newMsgs.Count);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs.Dequeue());
            Assert.IsInstanceOf<StringMessage>(newMsgs.Dequeue());
        }

        [Test]
        public void TestGenericQueueOfThings()
        {
            var msgs = new Queue<IDataMessage>(new List<IDataMessage>(){
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  });
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Count, newMsgs.Count);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs.Dequeue());
            Assert.IsInstanceOf<StringMessage>(newMsgs.Dequeue());
        }

        [Test]
        public void TestListOfThings()
        {
            var msgs = new List<IDataMessage>(){
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  };
            var newMsgs = msgs.Reserialize();
            Assert.AreEqual(msgs.Count, newMsgs.Count);
            Assert.IsInstanceOf<CommandLineMessage>(newMsgs[0]);
            Assert.IsInstanceOf<StringMessage>(newMsgs[1]);
        }

        [Test]
        public void TestException()
        {
            var msgs = new System.Xml.XmlException("This is a false exception",
                new InvalidOperationException("Here is an inner exception"), 12, 5);
            try
            {
                throw msgs;
            }
            catch (System.Xml.XmlException ex)
            {
                msgs = ex;
            }

            var newMsgs = msgs.Reserialize();
            Assert.IsNotNull(newMsgs);
            Assert.AreEqual(msgs.Message, newMsgs.Message);
            Assert.AreEqual(msgs.LineNumber, newMsgs.LineNumber);
            Assert.IsInstanceOf<InvalidOperationException>(newMsgs.InnerException);
        }


        [Test]
        public void TestDeserializeRejection()
        {
            var msgs = new List<IDataMessage>(){
                                      new CommandLineMessage() { Args = new string[] { "a", "test"}},
                                      new StringMessage() { Message = "Hello"}
                                  };
            byte[] data = null;
            var serial = new BinarySerializer();
            using (var mem = new MemoryStream())
            {
                serial.Serialize(mem, msgs);
                data = mem.ToArray();
            }

            IDataMessage newMsg;
            using (var mem = new MemoryStream(data))
            {
                newMsg = serial.Deserialize<IDataMessage>(mem);
            }

            Assert.IsNull(newMsg);
        }

        public class XmlStringTest
        {
            [RequestedFormat("XML")]
            public StringMessage Value { get; set; }
        }

        public class XmlEnvelopeTest
        {
            [RequestedFormat("XML")]
            public MessageEnvelope Value { get; set; }
        }

        public class TestXmlArray
        {
            [RequestedFormat("XML")]
            public List<string> Items { get; set; }
        }

        [Test]
        public void TestRequestFormat_Works()
        {
            var test = new XmlStringTest()
            {
                Value = new StringMessage("This is a test")
            };

            var newMsg = test.Reserialize();
            Assert.AreEqual(newMsg.Value.Message, test.Value.Message);
        }

        [Test]
        public void TestRequestFormat_Array()
        {
            var test = new TestXmlArray()
            {
                Items = new List<string>()
                {
                    "Lol", "2113", "Testing"
                }
            };

            var newTest = test.Reserialize();

        }

        [Test]
        [ExpectedException(ExpectedException=typeof(InvalidOperationException))]
        public void TestRequestFormat_Fail()
        {
            var msg = new XmlEnvelopeTest()
            {
                Value = new MessageEnvelope(Guid.NewGuid(), new CommandLineMessage()
                {
                    SenderId = Guid.NewGuid(),
                    Args = new string[]{
                    "One", "Two", "Three"
                }
                })
            };

            var newMsg = msg.Reserialize();
        }

        [Test]
        [TestCase("Text")]
        [TestCase(12)]
        public void TestSimpleTypes(object obj)
        {
            var newObj = obj.Reserialize();
            Assert.AreEqual(obj, newObj);
        }
    }
}
