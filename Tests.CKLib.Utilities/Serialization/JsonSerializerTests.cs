﻿using CKLib.Utilities.Serialization.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tests.CKLib.Utilities.Serialization
{
    [TestFixture]
    public class JsonSerializerTests
    {
        [Test]
        [TestCase(1009, "1009")]
        [TestCase("1009", "\"1009\"")]
        [TestCase(1.15, "1.15")]
        [TestCase(ConsoleColor.Black, "\"Black\"")]
        [TestCase(new byte[] {1,2,3}, "[1, 2, 3]")]
        public void TestCanSerializePrimitive(object value, string expected)
        {
            StringWriter writer = new StringWriter();
            using (JsonWriter json = new JsonWriter(writer))
            {
                json.WriteValue(value);
            }
            Assert.AreEqual(expected, writer.ToString());
        }
    }
}
