﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using CKLib.Utilities.Diagnostics;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Threading;
    #endregion

    [Obsolete("Use CKLib.Utilities.DataStructures.ReflectionNode instead.", false)]
    public class PropertyPair
    {
        #region Constructors
        public PropertyPair()
        {
        }

        internal PropertyPair(object obj, PropertyWrapper property)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (property.Info == null)
            {
                throw new ArgumentNullException("property");
            }

            Owner = obj;
            Property = property;
        }
        #endregion
        #region Properties
        private object Owner
        {
            get;
            set;
        }

        private PropertyWrapper Property
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return Property.Info.Name;
            }
        }

        public object Value
        {
            get
            {
                if (Property.Info.CanRead)
                {
                    try
                    {
                        return Property.Info.GetValue(Owner, null);
                    }
                    catch (Exception e)
                    {
                        // Any exception could potentially be thrown here, since this will largely be used for the PropertyGrid control,
                        //  just log them and move on.
                        Debug.WriteEntry(string.Format("Exception while getting value for property {0}.", Name), e);
                        return e;
                    }
                }

                return null;
            }

            set
            {
                if (Property.Info.CanWrite && PropertyType.IsInstanceOfType(value))
                {
                    try
                    {
                        Property.Info.SetValue(Owner, value, null);
                    }
                    catch (Exception e)
                    {
                        // Any exception could potentially be thrown here, since this will largely be used for the PropertyGrid control,
                        //  just log them and move on.
                        Debug.WriteEntry(string.Format("Exception while setting value for property {0}.", Name), e);
                    }
                }
            }
        }

        public PropertyEnumerator Enumerator
        {
            get
            {
                return new PropertyEnumerator(Value);
            }
        }

        public Type PropertyType
        {
            get
            {
                return Property.Info.PropertyType;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return Property.Info.CanWrite;
            }
        }

        public Array EnumerationOptions
        {
            get
            {
                if (PropertyType.IsEnum)
                {
                    return Enum.GetValues(PropertyType);
                }

                return null;
            }
        }
        #endregion
        #region Methods
        public override bool Equals(object obj)
        {
            return Equals((PropertyPair)obj);
        }

        public virtual bool Equals(PropertyPair other)
        {
            return this == other;
        }

        public static bool operator ==(PropertyPair first, PropertyPair second)
        {
            if (null != (object)first)
            {
                if (null == (object)second)
                {
                    return false;
                }

                return first.Name == second.Name && first.Property.Equals(second.Property);
            }
            else return null == (object)second;
        }

        public static bool operator !=(PropertyPair first, PropertyPair second)
        {
            return !(first == second);
        }

        public override int GetHashCode()
        {
            if (Name != null)
            {
                return Name.GetHashCode();
            }

            return 0;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Name, Value);
        }
        #endregion
    }

    [Obsolete("Use CKLib.Utilities.DataStructures.ReflectionNode instead.", false)]
    internal struct PropertyWrapper
    {
        private PropertyInfo m_Info;
        private int m_Version;
        public PropertyWrapper(PropertyInfo info)
        {
            m_Info = info;
            m_Version = 0;
        }

        public PropertyWrapper(PropertyWrapper old)
        {
            m_Info = old.m_Info;
            if (old.m_Version < Int32.MaxValue)
            {
                m_Version = old.m_Version + 1;
            }
            else
            {
                m_Version = 0;
            }
        }

        public static implicit operator PropertyWrapper(PropertyInfo info)
        {
            return new PropertyWrapper(info);
        }

        public static implicit operator PropertyInfo(PropertyWrapper wrapper)
        {
            return wrapper.Info;
        }

        public PropertyInfo Info
        {
            get
            {
                return m_Info;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is PropertyWrapper)
            {
                return Equals((PropertyWrapper)obj);
            }

            return base.Equals(obj);
        }

        public bool Equals(PropertyWrapper other)
        {
            return m_Info == other.m_Info && m_Version == other.m_Version;
        }
    }

    [Obsolete("Use CKLib.Utilities.DataStructures.ReflectionNode instead.", false)]
    public class PropertyEnumerator : IDisposable, IEnumerable<PropertyPair>, IList<PropertyPair>, IList, INotifyCollectionChanged
    {
        #region Variables
        /// <summary>
        /// The object whose properties should be enumerated.
        /// </summary>
        private object m_Object;

        /// <summary>
        /// A collection of properties.
        /// </summary>
        private SynchronizedObservableDictionary<string, PropertyWrapper> m_Bag;
        #endregion

        /// <summary>
        /// Initializes a new instance of the PropertyEnumerator class.
        /// </summary>
        /// <param name="obj">The object to enumerate the properties of.</param>
        public PropertyEnumerator(object obj)
        {
            m_Bag = new SynchronizedObservableDictionary<string, PropertyWrapper>(Dispatcher.CurrentDispatcher);
            m_Bag.CollectionChanged += m_Bag_CollectionChanged;
            PropertyObject = obj;
        }

        /// <summary>
        /// Occurs when one of the properties on the object has changed.
        /// </summary>
        /// <param name="sender">The property dictionary.</param>
        /// <param name="e">Arguments that describe which property changed.</param>
        void m_Bag_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                Func<object, PropertyPair> itemConverter = (i) => new PropertyPair(m_Object, ((KeyValuePair<string, PropertyWrapper>)i).Value);
                NotifyCollectionChangedEventArgs e2 = null;
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemConverter(e.NewItems[0]), e.NewStartingIndex);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, itemConverter(e.OldItems[0]), e.OldStartingIndex);
                        break;
                    case NotifyCollectionChangedAction.Move:
                        e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, itemConverter(e.NewItems[0]), e.NewStartingIndex, e.OldStartingIndex);
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, itemConverter(e.NewItems[0]), itemConverter(e.OldItems[0]), e.OldStartingIndex);
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        e2 = e;
                        break;
                }

                if (CollectionChanged != null)
                {
                    CollectionChanged(this, e2);
                }
            }
        }

        void changed_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyWrapper old;
            if (m_Bag.TryGetValue(e.PropertyName, out old))
            {
                // New version
                m_Bag[e.PropertyName] = new PropertyWrapper(old);
            }
        }

        /// <summary>
        /// Gets the set of properties to be displayed.
        /// </summary>
        public IEnumerable<PropertyPair> Properties
        {
            get
            {
                foreach (var prop in m_Bag)
                {
                    yield return new PropertyPair(m_Object, prop.Value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the dispatcher to route update notifications to.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                return m_Bag.Dispatcher;
            }

            set
            {
                m_Bag.Dispatcher = Dispatcher;
            }
        }

        /// <summary>
        /// Gets or sets the object used to initialize this instance.
        /// </summary>
        public object PropertyObject
        {
            get
            {
                return m_Object;
            }
            set
            {
                // Release old reference
                var changed = m_Object as INotifyPropertyChanged;
                if (changed != null)
                {
                    changed.PropertyChanged -= new PropertyChangedEventHandler(changed_PropertyChanged);
                }

                m_Object = value;
                FillBag();
                // Get new reference
                changed = m_Object as INotifyPropertyChanged;
                if (changed != null)
                {
                    changed.PropertyChanged -= new PropertyChangedEventHandler(changed_PropertyChanged);
                    changed.PropertyChanged += new PropertyChangedEventHandler(changed_PropertyChanged);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the object is a plain-old-data (POD) type.
        /// </summary>
        public bool IsPlainOldData
        {
            get
            {
                if (m_Object != null)
                {
                    TypeCode code = Type.GetTypeCode(m_Object.GetType());
                    return code != TypeCode.Object;
                }

                return true;
            }
        }

        /// <summary>
        /// Fills the property bag with all of the public properties found on the object.
        /// </summary>
        private void FillBag()
        {
            m_Bag.Clear();
            if (m_Object != null && !IsPlainOldData)
            {
                PropertyInfo[] properties = m_Object.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
                for (int i = 0; i < properties.Length; i++)
                {
                    if (properties[i].CanRead && !properties[i].IsSpecialName && !typeof(PropertyEnumerator).IsAssignableFrom(properties[i].PropertyType))
                    {
                        if (FilterProperty(properties[i]))
                        {
                            m_Bag[properties[i].Name] = properties[i];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method is called for each property found on the target object, a return value of false
        /// will prevent that property from being displayed.
        /// </summary>
        /// <param name="info">The property to filter.</param>
        /// <returns>This function should return <c>true</c> to allow the property to be displayed, and 
        /// <c>false</c> to filter it out.</returns>
        protected virtual bool FilterProperty(PropertyInfo info)
        {
            if (info != null)
            {
                var attrs = info.GetCustomAttributes(typeof(BrowsableAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    BrowsableAttribute atr = attrs[0] as BrowsableAttribute;
                    return atr.Browsable;
                }
            }

            return true;
        }

        public IEnumerator<PropertyPair> GetEnumerator()
        {
            return Properties.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #region IDisposable Members
        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            CollectionChanged = null;

            var changed = m_Object as INotifyPropertyChanged;
            if (changed != null)
            {
                changed.PropertyChanged -= new PropertyChangedEventHandler(changed_PropertyChanged);
            }

            if (m_Bag != null)
            {
                m_Bag.CollectionChanged -= m_Bag_CollectionChanged;
                m_Bag.Clear();
                m_Bag = null;
            }

            m_Object = null;
        }
        #endregion
        #region IList<PropertyPair> Members
        public int IndexOf(PropertyPair item)
        {
            return m_Bag.IndexOf(item.Name);
        }

        void IList<PropertyPair>.Insert(int index, PropertyPair item)
        {
            throw new NotImplementedException();
        }

        void IList<PropertyPair>.RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public PropertyPair this[int index]
        {
            get
            {
                var val = m_Bag[index];
                return new PropertyPair(m_Object, val.Value);
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
        #region ICollection<PropertyPair> Members
        void ICollection<PropertyPair>.Add(PropertyPair item)
        {
            throw new NotImplementedException();
        }

        void ICollection<PropertyPair>.Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(PropertyPair item)
        {
            return m_Bag.ContainsKey(item.Name);
        }

        public void CopyTo(PropertyPair[] array, int arrayIndex)
        {
            var temp = m_Bag.Select(p => new PropertyPair(m_Object, p.Value)).ToArray();
            for (int i = 0; i < temp.Length; i++)
            {
                array[i + arrayIndex] = temp[i];
            }
        }

        public int Count
        {
            get { return m_Bag.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        bool ICollection<PropertyPair>.Remove(PropertyPair item)
        {
            throw new NotSupportedException();
        }
        #endregion
        #region IList Members
        int IList.Add(object value)
        {
            throw new NotSupportedException();
        }

        void IList.Clear()
        {
            throw new NotSupportedException();
        }

        bool IList.Contains(object value)
        {
            return Contains((PropertyPair)value);
        }

        int IList.IndexOf(object value)
        {
            return IndexOf((PropertyPair)value);
        }

        void IList.Insert(int index, object value)
        {
            throw new NotSupportedException();
        }

        bool IList.IsFixedSize
        {
            get { return true; }
        }

        bool IList.IsReadOnly
        {
            get { return true; }
        }

        void IList.Remove(object value)
        {
            throw new NotSupportedException();
        }

        void IList.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (PropertyPair)value;
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            var arr = this.ToArray();
            Array.Copy(arr, 0, array, index, arr.Length);
        }

        int ICollection.Count
        {
            get { return Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return false; }
        }

        object ICollection.SyncRoot
        {
            get { return null; }
        }
        #endregion
    }
}
