﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using CKLib.Utilities.Diagnostics;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    #endregion

    /// <summary>
    /// This class is used to traverse the runtime meta-data for a given object.
    /// CustomTypeDescriptors are used when searching for properties so things like dependency properties will show up
    /// on DependencyObjects.
    /// Although this class does not appear to have a <c>Value</c> property, the nodes returned by the <c>Create</c> method
    /// will infact have a type-safe property with the appropriate accessors named <c>Value</c> for use with Data-Binding.
    /// </summary>
    [System.Security.Permissions.ReflectionPermission(System.Security.Permissions.SecurityAction.Assert)]
    public class ReflectionNode : IDisposable, IEnumerable<ReflectionNode>, INotifyPropertyChanged
    {
        #region Constants
        /// <summary>
        /// The maximum number of iterations to expand a collection to.
        /// </summary>
        private const int MAX_ENUMERATION_EXPANSION = 10000;

        /// <summary>
        /// The maxmim amount of time to spend expanding an enumeration.
        /// </summary>
        private static readonly TimeSpan MAX_ENUMERATION_TIME = TimeSpan.FromSeconds(2);
        #endregion

        #region Inner Classes
        protected class EnumerableNode : ReflectionNode
        {
            #region Constructors
            /// <summary>
            /// Initializes a new instance of the Enumerable node class.
            /// </summary>
            /// <param name="root">The root for the enumerable.</param>
            private EnumerableNode(IEnumerable root)
                : base(root)
            {
            }

            /// <summary>
            /// Initializes a new instance of the Enumerable node class.
            /// </summary>
            /// <param name="parent">The parent node.</param>
            /// <param name="childProperty">The child property to create the node for.</param>
            private EnumerableNode(ReflectionNode parent, PropertyDescriptor childProperty)
                : base(parent, childProperty)
            {
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets a string equal to "Expanding this node will enumerate the collection."
            /// </summary>
            public string Value
            {
                get
                {
                    return "Expanding this node will enumerate the collection.";
                }
            }

            /// <summary>
            /// Gets a string equal to "{Enumeration Node}"
            /// </summary>
            public override string DisplayName
            {
                get
                {
                    return "{Enumeration Node}";
                }
            }

            /// <summary>
            /// Gets a string equal to "Expanding this node will enumerate the collection."
            /// </summary>
            public override string Description
            {
                get
                {
                    return "Expanding this node will enumerate the collection.";
                }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Creates a new instance of the Enumerable node class.
            /// </summary>
            /// <param name="parent">The parent node to the node being built.</param>
            /// <param name="childProperty">The property on the parent to build a node for.</param>
            public static ReflectionNode BuildEnumerable(ReflectionNode parent, PropertyDescriptor childProperty)
            {
                if (typeof(IEnumerable).IsAssignableFrom(childProperty.PropertyType) &&
                    !typeof(string).IsAssignableFrom(childProperty.PropertyType))
                {
                    return new EnumerableNode(parent, childProperty);
                }

                return null;
            }

            /// <summary>
            /// Creates a new instance of the Enumerable node class.
            /// </summary>
            /// <param name="root">The IEnumerable to create.</param>
            /// <param name="initializationFlags">The flags to pass on through inheritance.</param>
            /// <param name="searchAttributes">The attributes to search with through inheritance.</param>
            /// <returns>A new instance of the EnumerableNode, or null or root is null.</returns>
            public static ReflectionNode BuildEnumerable(object root, BindingFlags initializationFlags, Attribute[] searchAttributes)
            {
                if (root != null)
                {
                    if (root is IEnumerable && !(root is string))
                    {
                        return new EnumerableNode((IEnumerable)root)
                        {
                            m_InitializationFlags = initializationFlags,
                            m_SearchAttributes = searchAttributes
                        };
                    }
                }

                return null;
            }

            /// <summary>
            /// Fills the child nodes with the items in the enumeration.
            /// </summary>
            /// <param name="appendableCollection">A collection which can be filled with child nodes.</param>
            protected override void FillChildren(ICollection<ReflectionNode> appendableCollection)
            {
                var val = InternalValue as IEnumerable;
                if (val != null)
                {
                    var enumerator = val.GetEnumerator();
                    int i = 0;
                    DateTime startTime = DateTime.Now;
                    while (enumerator.MoveNext())
                    {
                        var item = enumerator.Current;
                        string name = string.Format("[{0}]", i);
                        var node = ReflectionNode.Create(item, m_InitializationFlags, m_SearchAttributes);
                        node.InheritFrom(this);
                        node.Name = name;
                        appendableCollection.Add(node);

                        if (++i > MAX_ENUMERATION_EXPANSION)
                        {
                            node = ReflectionNode.Create("Enumeration Contained Too Many Items.");
                            node.Name = "[END ENUMERATION]";
                            appendableCollection.Add(node);
                            break;
                        }

                        if (DateTime.Now - MAX_ENUMERATION_TIME > startTime)
                        {
                            node = ReflectionNode.Create("Enumeration Timed Out.");
                            node.Name = "[END ENUMERATION]";
                            appendableCollection.Add(node);
                            break;
                        }
                    }
                }
            }
            #endregion
        }

        protected class ReflectionSubNode<TValue> : ReflectionNode
        {
            #region Inner Classes
            protected class ReadOnlySubNode : ReflectionSubNode<TValue>
            {
                #region Constructors
                /// <summary>
                /// Initializes a new instance of the ReflectionNode class.
                /// </summary>
                /// <param name="root">The object to initialize the node with.</param>
                public ReadOnlySubNode(TValue root)
                    : base(root)
                {
                }

                /// <summary>
                /// Initializes a new instance of the ReflectionNode class.
                /// </summary>
                /// <param name="parent">The parent of this node.</param>
                /// <param name="childProperty">The property to initialize this node with.</param>
                public ReadOnlySubNode(ReflectionNode parent, PropertyDescriptor childProperty)
                    : base(parent, childProperty)
                {
                }
                #endregion

                #region Properties
                /// <summary>
                /// Gets the current value of this node.
                /// </summary>
                public virtual TValue Value
                {
                    get
                    {
                        if (InternalValue != null)
                        {
                            return (TValue)InternalValue;
                        }

                        return default(TValue);
                    }
                }
                #endregion
            }

            protected class WritableSubNode : ReflectionSubNode<TValue>
            {
                #region Constructors
                /// <summary>
                /// Initializes a new instance of the ReflectionNode class.
                /// </summary>
                /// <param name="root">The object to initialize the node with.</param>
                public WritableSubNode(TValue root)
                    : base(root)
                {
                }

                /// <summary>
                /// Initializes a new instance of the ReflectionNode class.
                /// </summary>
                /// <param name="parent">The parent of this node.</param>
                /// <param name="childProperty">The property to initialize this node with.</param>
                public WritableSubNode(ReflectionNode parent, PropertyDescriptor childProperty)
                    : base(parent, childProperty)
                {
                }
                #endregion

                #region Properties
                /// <summary>
                /// Gets the current value of this node.
                /// </summary>
                public virtual TValue Value
                {
                    get
                    {
                        if (InternalValue != null)
                        {
                            return (TValue)InternalValue;
                        }

                        return default(TValue);
                    }

                    set
                    {
                        InternalValue = value;
                    }
                }
                #endregion
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the ReflectionNode class.
            /// </summary>
            /// <param name="root">The object to initialize the node with.</param>
            public ReflectionSubNode(TValue root)
                : base(root)
            {
            }

            /// <summary>
            /// Initializes a new instance of the ReflectionNode class.
            /// </summary>
            /// <param name="parent">The parent of this node.</param>
            /// <param name="childProperty">The property to initialize this node with.</param>
            protected ReflectionSubNode(ReflectionNode parent, PropertyDescriptor childProperty)
                : base(parent, childProperty)
            {
            }
            #endregion

            #region Methods
            public static ReflectionNode Build(object root)
            {
                if (root is TValue)
                {
                    return new ReadOnlySubNode((TValue)root);
                }

                return null;
            }

            public static ReflectionNode Build(ReflectionNode parent, PropertyDescriptor childProperty)
            {
                if (childProperty.IsReadOnly)
                {
                    return new ReadOnlySubNode(parent, childProperty);
                }
                else
                {
                    return new WritableSubNode(parent, childProperty);
                }
            }
            #endregion
        }

        private abstract class PrivateDescriptor : PropertyDescriptor
        {
            #region Inner Classes
            private class PrivateFieldDescriptor : PrivateDescriptor
            {
                private FieldInfo m_Info;

                public PrivateFieldDescriptor(string name, Attribute[] attributes)
                    : base(name, attributes)
                {
                }

                public override MemberInfo Member
                {
                    get
                    {
                        return m_Info;
                    }
                    protected set
                    {
                        m_Info = value as FieldInfo;
                    }
                }

                public override object GetValue(object component)
                {
                    return m_Info.GetValue(component);
                }

                public override void SetValue(object component, object value)
                {
                    m_Info.SetValue(component, value);
                }
            }

            private class PrivatePropertyDescriptor : PrivateDescriptor
            {
                private PropertyInfo m_Info;

                public PrivatePropertyDescriptor(string name, Attribute[] attributes)
                    : base(name, attributes)
                {
                }

                public override MemberInfo Member
                {
                    get
                    {
                        return m_Info;
                    }
                    protected set
                    {
                        m_Info = value as PropertyInfo;
                        if (m_Info == null)
                        {
                            throw new ArgumentNullException("value");
                        }
                    }
                }

                public override object GetValue(object component)
                {
                    return m_Info.GetValue(component, null);
                }

                public override void SetValue(object component, object value)
                {
                    m_Info.SetValue(component, value, null);
                }
            }
            #endregion

            #region Variables
            /// <summary>
            /// Indicates whether the property is read only.
            /// </summary>
            private bool m_IsReadOnly;

            /// <summary>
            /// Describes the member type.
            /// </summary>
            private Type m_PropertyType;
            #endregion

            #region Constructors
            private PrivateDescriptor(string name, Attribute[] attributes)
                : base(name, attributes)
            {
            }
            #endregion

            #region Properties
            public virtual MemberInfo Member
            {
                get;
                protected set;
            }

            public override Type ComponentType
            {
                get { return Member.ReflectedType; }
            }

            public override bool IsReadOnly
            {
                get { return m_IsReadOnly; }
            }

            public override Type PropertyType
            {
                get { return m_PropertyType; }
            }
            #endregion

            #region Methods
            public static PropertyDescriptor FromProperty(PropertyInfo info)
            {
                var atrs = info.GetCustomAttributes(true).Cast<Attribute>().ToArray();
                return new PrivatePropertyDescriptor(info.Name, atrs)
                {
                    Member = info,
                    m_IsReadOnly = !info.CanWrite,
                    m_PropertyType = info.PropertyType
                };
            }

            public static PropertyDescriptor FromField(FieldInfo info)
            {
                var atrs = info.GetCustomAttributes(true).Cast<Attribute>().ToArray();
                return new PrivateFieldDescriptor(info.Name, atrs)
                {
                    Member = info,
                    m_IsReadOnly = info.IsInitOnly,
                    m_PropertyType = info.FieldType
                };
            }

            public override bool CanResetValue(object component)
            {
                var atr = TypeDescriptor.GetAttributes(component)[typeof(DefaultValueAttribute)] as DefaultValueAttribute;
                return atr != null;
            }

            public override void ResetValue(object component)
            {
                var atr = TypeDescriptor.GetAttributes(component)[typeof(DefaultValueAttribute)] as DefaultValueAttribute;
                if (atr != null)
                {
                    SetValue(component, atr.Value);
                }
            }

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }
            #endregion
        }

        private class PrivateTypeDescriptor : ICustomTypeDescriptor
        {
            public PrivateTypeDescriptor(Type type, BindingFlags searchFlags)
            {
                DescribedType = type;
                SearchFlags = searchFlags;
            }

            public Type DescribedType
            {
                get;
                private set;
            }

            public BindingFlags SearchFlags
            {
                get;
                private set;
            }

            public AttributeCollection GetAttributes()
            {
                return TypeDescriptor.GetAttributes(DescribedType);
            }

            public string GetClassName()
            {
                return DescribedType.Name;
            }

            public string GetComponentName()
            {
                return DescribedType.Name;
            }

            public TypeConverter GetConverter()
            {
                var atrs = GetAttributes()[typeof(TypeConverterAttribute)] as TypeConverterAttribute;
                if (atrs != null)
                {
                    Type t = Type.GetType(atrs.ConverterTypeName);
                    if (t != null)
                    {
                        return Activator.CreateInstance(t) as TypeConverter;
                    }
                }

                return null;
            }

            public EventDescriptor GetDefaultEvent()
            {
                var atr = GetAttributes()[typeof(DefaultEventAttribute)] as DefaultEventAttribute;
                if (atr != null)
                {
                    return GetEvents()[atr.Name];
                }

                return null;
            }

            public PropertyDescriptor GetDefaultProperty()
            {
                var atr = GetAttributes()[typeof(DefaultPropertyAttribute)] as DefaultPropertyAttribute;
                if (atr != null)
                {
                    return GetProperties()[atr.Name];
                }

                return null;
            }

            public object GetEditor(Type editorBaseType)
            {
                return TypeDescriptor.GetEditor(DescribedType, editorBaseType);
            }

            public EventDescriptorCollection GetEvents(Attribute[] attributes)
            {
                return TypeDescriptor.GetEvents(DescribedType, attributes);
            }

            public EventDescriptorCollection GetEvents()
            {
                return TypeDescriptor.GetEvents(DescribedType);
            }

            public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
            {
                var props = TypeDescriptor.GetProperties(DescribedType, attributes);
                if ((SearchFlags & BindingFlags.NonPublic) == BindingFlags.NonPublic)
                {
                    PropertyDescriptor[] tmp = new PropertyDescriptor[props.Count];
                    props.CopyTo(tmp, 0);
                    PropertyDescriptorCollection col = new PropertyDescriptorCollection(tmp, false);
                    var privateProps = from prop in DescribedType.GetProperties(SearchFlags | BindingFlags.Instance)
                                       where prop.CanRead
                                       where prop.GetIndexParameters().Length == 0
                                       let p = PrivateDescriptor.FromProperty(prop)
                                       where attributes.All(a => p.Attributes[a.GetType()] != null && p.Attributes[a.GetType()].Match(a))
                                       select p;
                    foreach (var prop in privateProps)
                    {
                        col.Add(prop);
                    }

                    if ((SearchFlags & BindingFlags.GetField) == BindingFlags.GetField)
                    {
                        var fields = from field in DescribedType.GetFields(SearchFlags | BindingFlags.Instance)
                                     let p = PrivateDescriptor.FromField(field)
                                     where attributes.All(a => p.Attributes[a.GetType()] != null && p.Attributes[a.GetType()].Match(a))
                                     select p;
                        foreach (var field in fields)
                        {
                            col.Add(field);
                        }
                    }

                    props = col;
                }

                return props;
            }

            public PropertyDescriptorCollection GetProperties()
            {
                var props = TypeDescriptor.GetProperties(DescribedType);
                if ((SearchFlags & BindingFlags.NonPublic) == BindingFlags.NonPublic)
                {
                    PropertyDescriptor[] tmp = new PropertyDescriptor[props.Count];
                    props.CopyTo(tmp, 0);
                    PropertyDescriptorCollection col = new PropertyDescriptorCollection(tmp, false);
                    var privateProps = from prop in DescribedType.GetProperties(SearchFlags | BindingFlags.Instance)
                                       where prop.CanRead
                                       where prop.GetIndexParameters().Length == 0
                                       select PrivateDescriptor.FromProperty(prop);
                    foreach (var prop in privateProps)
                    {
                        col.Add(prop);
                    }

                    if ((SearchFlags & BindingFlags.GetField) == BindingFlags.GetField)
                    {
                        var fields = from field in DescribedType.GetFields(SearchFlags | BindingFlags.Instance)
                                     select PrivateDescriptor.FromField(field);
                        foreach (var field in fields)
                        {
                            col.Add(field);
                        }
                    }

                    props = col;
                }

                return props;
            }

            public object GetPropertyOwner(PropertyDescriptor pd)
            {
                return null;
            }
        }
        #endregion

        #region Delegates
        private delegate ReflectionNode RootConstructor(object root);

        private delegate ReflectionNode NodeConstructor(ReflectionNode parent, PropertyDescriptor prop);
        #endregion

        #region Variables
        /// <summary>
        /// The value this node was initialized with.
        /// </summary>
        private object m_Value;

        /// <summary>
        /// The function to use as a filter for child properties.
        /// </summary>
        private Predicate<PropertyDescriptor> m_Filter;

        /// <summary>
        /// A collection of child nodes for this node.
        /// </summary>
        private List<ReflectionNode> m_Children;

        /// <summary>
        /// The descriptor used to initialize this instance.
        /// </summary>
        private PropertyDescriptor m_Property;

        /// <summary>
        /// The binding flags used to initialize child nodes.
        /// </summary>
        private BindingFlags m_InitializationFlags;

        /// <summary>
        /// The attributes required to retrieve children.
        /// </summary>
        private Attribute[] m_SearchAttributes;

        /// <summary>
        /// Indicates whether child nodes should be cached.
        /// </summary>
        private bool m_CacheChildren;

        /// <summary>
        /// An internal cache of constructors used for generation of type safe reflection node children.
        /// </summary>
        private static Dictionary<Type, NodeConstructor> m_CachedConstructors =
            new Dictionary<Type, NodeConstructor>();

        /// <summary>
        /// An internal cache of constructors used for generation of type safe reflection node roots.
        /// </summary>
        private static Dictionary<Type, RootConstructor> m_RootConstructors =
            new Dictionary<Type, RootConstructor>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ReflectionNode class.
        /// </summary>
        /// <param name="root">The object to initialize the node with.</param>
        protected ReflectionNode(object root)
        {
            Name = "[root]";
            m_Value = root;
        }

        /// <summary>
        /// Initializes a new instance of the ReflectionNode class.
        /// </summary>
        /// <param name="parent">The parent of this node.</param>
        /// <param name="childProperty">The property to initialize this node with.</param>
        protected ReflectionNode(ReflectionNode parent, PropertyDescriptor childProperty)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (childProperty == null)
            {
                throw new ArgumentNullException("childProperty");
            }

            Name = childProperty.Name;
            m_Property = childProperty;
            InheritFrom(parent);
            Update();
            HookEvents();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value used to initialize this node.
        /// </summary>
        protected object InternalValue
        {
            get
            {
                return m_Value;
            }

            set
            {
                SetValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the function to use as a filter for child properties.
        /// </summary>
        public Predicate<PropertyDescriptor> Filter
        {
            get
            {
                return m_Filter;
            }

            set
            {
                m_Filter = value;
            }
        }

        /// <summary>
        /// Gets the name for this node.
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the name to display in an editor or property grid.
        /// </summary>
        public virtual string DisplayName
        {
            get
            {
                if (Descriptor != null)
                {
                    return Descriptor.DisplayName;
                }

                return Name;
            }
        }

        /// <summary>
        /// Gets a description of the property from which this node is derived.
        /// </summary>
        public virtual string Description
        {
            get
            {
                if (Descriptor != null)
                {
                    return Descriptor.Description;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not child nodes should be cached.
        /// </summary>
        public bool CacheChildNodes
        {
            get
            {
                return m_CacheChildren;
            }

            set
            {
                m_CacheChildren = value;
            }
        }

        /// <summary>
        /// Gets the parent for this node.
        /// </summary>
        public ReflectionNode Parent
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the value of this node is readonly.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                if (m_Property != null)
                {
                    return m_Property.IsReadOnly;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets the root reflection node for this node.
        /// </summary>
        public ReflectionNode Root
        {
            get
            {
                ReflectionNode current = this;
                while (current.Parent != null)
                {
                    current = current.Parent;
                }

                return current;
            }
        }

        /// <summary>
        /// Gets the property used to initialize this node.
        /// </summary>
        public PropertyDescriptor Descriptor
        {
            get
            {
                return m_Property;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this node represents a plain-old-data type.
        /// </summary>
        public bool IsPlainOldData
        {
            get
            {
                if (m_Value != null)
                {
                    var code = Type.GetTypeCode(m_Value.GetType());
                    return code != TypeCode.Object && code != TypeCode.DBNull;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets all properties for this node that should appear as child nodes.
        /// </summary>
        public IEnumerable<PropertyDescriptor> Properties
        {
            get
            {
                if (m_Value != null)
                {
                    if (!IsPlainOldData)
                    {
                        PropertyDescriptorCollection properties = GetProperties();
                        foreach (var prop in properties.Cast<PropertyDescriptor>())
                        {
                            if (FilterProperty(prop))
                            {
                                yield return prop;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the set of child reflection nodes for this node.
        /// </summary>
        public IEnumerable<ReflectionNode> Children
        {
            get
            {
                return EnsureChildren();
            }
        }

        /// <summary>
        /// Gets the node with a property whose name matches the specified name.
        /// </summary>
        /// <param name="propertyName">The name of the property to get the node for.</param>
        /// <returns>A reflection node whose name matches the input name.</returns>
        public ReflectionNode this[string propertyName]
        {
            get
            {
                return GetChild(propertyName);
            }
        }

        /// <summary>
        /// If the value of this node is an enumeration, gets the valid enumeration values for this type.
        /// </summary>
        public ICollection EnumerationValues
        {
            get
            {
                if (Descriptor != null)
                {
                    Type t = Descriptor.PropertyType;
                    if (typeof(Enum).IsAssignableFrom(t))
                    {
                        return Enum.GetValues(t);
                    }

                    var converter = Descriptor.Converter;
                    if (converter != null)
                    {
                        if (converter.GetStandardValuesSupported())
                        {
                            return converter.GetStandardValues();
                        }
                    }
                }

                return new object[0];
            }
        }
        #endregion

        #region Methods
        #region Static Creation Methods
        /// <summary>
        /// Creates a type-safe reflection node child from the specified parent using the specified PropertyDescriptor.
        /// </summary>
        /// <param name="parent">The node that will serve as the parent.</param>
        /// <param name="info">The PropertyDescriptor that describes the child to create.</param>
        /// <returns>A Type-Safe reflection node.</returns>
        protected static ReflectionNode Create(ReflectionNode parent, PropertyDescriptor info)
        {
            NodeConstructor constructor;
            var tNode = info.PropertyType;
            if (!m_CachedConstructors.TryGetValue(tNode, out constructor))
            {
                lock (m_CachedConstructors)
                {
                    if (!m_CachedConstructors.TryGetValue(tNode, out constructor))
                    {
                        var genType = typeof(ReflectionSubNode<>).MakeGenericType(tNode);
                        constructor = Delegate.CreateDelegate(typeof(NodeConstructor), genType, "Build") as NodeConstructor;
                        m_CachedConstructors[genType] = constructor;
                    }
                }
            }

            return constructor(parent, info);
        }

        /// <summary>
        /// Creates a reflection node from the specified root.
        /// </summary>
        /// <param name="root">The root object from which all children will be derived.</param>
        /// <returns>The reflection node with the specified child construction parameters.</returns>
        public static ReflectionNode Create(object root)
        {
            return Create(root, BindingFlags.Public);
        }

        /// <summary>
        /// Creates a reflection node from the specified root.
        /// </summary>
        /// <param name="root">The root object from which all children will be derived.</param>
        /// <param name="flags">The binding flags to use when producing children.</param>
        /// <returns>The reflection node with the specified child construction parameters.</returns>
        public static ReflectionNode Create(object root, BindingFlags flags)
        {
            return Create(root, flags, null);
        }

        /// <summary>
        /// Creates a reflection node from the specified root.
        /// </summary>
        /// <param name="root">The root object from which all children will be derived.</param>
        /// <param name="flags">The binding flags to use when producing children.</param>
        /// <param name="customAttributes">The attributes required for a member to be considered as a child.</param>
        /// <returns>The reflection node with the specified child construction parameters.</returns>
        public static ReflectionNode Create(object root, BindingFlags flags, Attribute[] customAttributes)
        {
            if (root != null)
            {
                RootConstructor constructor;
                var tNode = root.GetType();
                if (!m_RootConstructors.TryGetValue(tNode, out constructor))
                {
                    lock (m_RootConstructors)
                    {
                        if (!m_RootConstructors.TryGetValue(tNode, out constructor))
                        {
                            var genType = typeof(ReflectionSubNode<>).MakeGenericType(tNode);
                            constructor = Delegate.CreateDelegate(typeof(RootConstructor), genType, "Build") as RootConstructor;
                            m_RootConstructors[genType] = constructor;
                        }
                    }
                }

                var val = constructor(root);
                val.m_InitializationFlags = flags;
                val.m_SearchAttributes = customAttributes;
                return val;
            }

            return null;
        }

        /// <summary>
        /// Creates a reflection node from the specified root, following the specified child path.
        /// </summary>
        /// <param name="root">The root object from which all children will be derived.</param>
        /// <param name="childPath">The '.' delimted path to the desired child.</param>
        /// <returns>The reflection node at the end of the specified path.</returns>
        public static ReflectionNode Create(object root, string childPath)
        {
            return Create(root, childPath, BindingFlags.Public);
        }

        /// <summary>
        /// Creates a reflection node from the specified root, following the specified child path.
        /// </summary>
        /// <param name="root">The root object from which all children will be derived.</param>
        /// <param name="childPath">The '.' delimted path to the desired child.</param>
        /// <param name="flags">The binding flags to use when producing children.</param>
        /// <returns>The reflection node at the end of the specified path.</returns>
        public static ReflectionNode Create(object root, string childPath, BindingFlags flags)
        {
            if (root != null)
            {
                ReflectionNode node = Create(root, flags);
                return node[childPath];
            }

            return null;
        }
        #endregion

        #region Class Methods
        /// <summary>
        /// Finalizes an instance of the ReflectionNode class.
        /// </summary>
        ~ReflectionNode()
        {
            Dispose(false);
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        /// <param name="disposing">True to also dispose of managed resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            UnhookEvents();
            DestroyChildren();
            Filter = null;
            m_SearchAttributes = null;

            if (disposing)
            {
                if (Disposed != null)
                {
                    Disposed(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// A function used to discard unwanted properties from appearing as nodes.
        /// </summary>
        /// <param name="property">The property to filter.</param>
        /// <returns>True if the property passes the filter, false otherwise.</returns>
        protected virtual bool FilterProperty(PropertyDescriptor property)
        {
            var filter = Filter;
            if (filter != null)
            {
                return filter(property);
            }

            return true;
        }

        /// <summary>
        /// Creates the child nodes.
        /// </summary>
        private List<ReflectionNode> EnsureChildren()
        {
            List<ReflectionNode> collection = m_Children;
            bool doCache = m_CacheChildren;
            if (!doCache || m_Children == null)
            {
                collection = new List<ReflectionNode>();
                FillChildren(collection);
                if (doCache)
                {
                    m_Children = collection;
                }
            }

            return collection;
        }

        /// <summary>
        /// Inherits settings from the specified parent node.
        /// </summary>
        /// <param name="parent">The parent to copy the settings from.</param>
        protected virtual void InheritFrom(ReflectionNode parent)
        {
            Parent = parent;
            m_InitializationFlags = parent.m_InitializationFlags;
            m_SearchAttributes = parent.m_SearchAttributes;
            m_Filter = parent.m_Filter;
            m_CacheChildren = parent.m_CacheChildren;
        }

        /// <summary>
        /// Fills the child collection with the values found in the current value.
        /// </summary>
        /// <param name="appendableCollection">The collection to fill with child nodes.</param>
        protected virtual void FillChildren(ICollection<ReflectionNode> appendableCollection)
        {
            var val = InternalValue as IEnumerable;
            if (val != null)
            {
                ReflectionNode enumerationNode = null;
                if (Descriptor != null && Parent != null)
                {
                    enumerationNode = EnumerableNode.BuildEnumerable(Parent, Descriptor);
                }
                else
                {
                    enumerationNode = EnumerableNode.BuildEnumerable(val, m_InitializationFlags, m_SearchAttributes);
                }

                if (enumerationNode != null)
                {
                    enumerationNode.InheritFrom(this);
                    appendableCollection.Add(enumerationNode);
                }
            }

            foreach (var property in Properties)
            {
                try
                {
                    appendableCollection.Add(ReflectionNode.Create(this, property));
                }
                catch (TargetInvocationException ex)
                {
                    Error.WriteEntry(
                        string.Format(
                        "Failed to create entry for property {0} on object {1}.",
                        property.Name,
                        m_Value),
                        ex);
                }
            }
        }

        /// <summary>
        /// Destroys all child nodes.
        /// </summary>
        private void DestroyChildren()
        {
            if (m_Children != null)
            {
                foreach (var child in m_Children)
                {
                    child.Dispose();
                }

                m_Children.Clear();
            }
        }

        /// <summary>
        /// Gets an enumerator used to iterate the children of this node.
        /// </summary>
        /// <returns>An enumerator used to iterate the children of this node.</returns>
        public IEnumerator<ReflectionNode> GetEnumerator()
        {
            return Children.GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator used to iterate the children of this node.
        /// </summary>
        /// <returns>An enumerator used to iterate the children of this node.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">The name of the property that has changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Registers for change events on the parent node.
        /// </summary>
        private void HookEvents()
        {
            if (m_Property != null && m_Property.SupportsChangeEvents)
            {
                if (Parent != null)
                {
                    m_Property.AddValueChanged(Parent.InternalValue, PerformValueChange);
                }
            }
        }


        /// <summary>
        /// Unregisters for change events from the parent node.
        /// </summary>
        private void UnhookEvents()
        {
            if (m_Property != null && m_Property.SupportsChangeEvents)
            {
                if (Parent != null)
                {
                    m_Property.RemoveValueChanged(Parent.InternalValue, PerformValueChange);
                }
            }
        }

        /// <summary>
        /// Updates the value of this node to the new value.
        /// </summary>
        /// <param name="sender">The parent object.</param>
        /// <param name="e">The Event Args.</param>
        private void PerformValueChange(object sender, EventArgs e)
        {
            Update();
        }

        /// <summary>
        /// Updates the value of this node to the current value.
        /// </summary>
        public void Update()
        {
            if (m_Property != null && Parent != null)
            {
                try
                {
                    m_Value = GetValue();
                    OnPropertyChanged("Value");
                    OnPropertyChanged("Children");
                }
                catch (Exception ex)
                {
                    Debug.WriteEntry("Failed to get value of property: " + m_Property.Name, ex);
                }
            }
        }

        /// <summary>
        /// Gets the last known value of the property for this node.
        /// </summary>
        /// <returns>The last known value of the property for this node.</returns>
        public object GetCachedValue()
        {
            return m_Value;
        }

        /// <summary>
        /// Gets the current value of the property for this node.
        /// </summary>
        /// <returns>The current value of this node's property, or null.</returns>
        protected virtual object GetValue()
        {
            if (Parent != null && m_Property != null)
            {
                return m_Property.GetValue(Parent.InternalValue);
            }

            return null;
        }

        /// <summary>
        /// Attempts to set this node's property value to the specified newValue.
        /// </summary>
        /// <param name="newValue">The value to set.</param>
        protected virtual void SetValue(object newValue)
        {
            if (!m_Property.IsReadOnly)
            {
                var p = Parent;
                if (p != null)
                {
                    var v = p.InternalValue;
                    if (v != null)
                    {
                        try
                        {
                            m_Property.SetValue(v, newValue);
                        }
                        catch (Exception e)
                        {
                            Error.WriteEntry("Error while attempting to set value for property: " + m_Property.Name, e);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the node with a property whose name matches the specified name.
        /// </summary>
        /// <param name="propertyName">The name of the property to get the node for.</param>
        /// <returns>A reflection node whose name matches the input name.</returns>
        protected virtual ReflectionNode GetChild(string propertyName)
        {
            if (!string.IsNullOrEmpty(propertyName))
            {
                var nodeNames = propertyName.Split('.');
                ReflectionNode cNode = this;
                for (int i = 0; i < nodeNames.Length; i++)
                {
                    var cProp = cNode.GetProperties()[nodeNames[i]];
                    if (cProp != null)
                    {
                        cNode = Create(cNode, cProp);
                    }
                }

                return cNode;
            }

            return this;
        }

        /// <summary>
        /// Gets a collection of properties for the current item.
        /// </summary>
        /// <returns>A collection of properties for the current item.</returns>
        protected virtual PropertyDescriptorCollection GetProperties()
        {
            if (m_Value != null)
            {
                var desc = new PrivateTypeDescriptor(m_Value.GetType(), m_InitializationFlags);
                if (m_SearchAttributes != null)
                {
                    return desc.GetProperties(m_SearchAttributes);
                }
                else
                {
                    return desc.GetProperties();
                }
            }

            return PropertyDescriptorCollection.Empty;
        }
        #endregion
        #endregion

        #region Events
        /// <summary>
        /// Raised when the value of this node changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raised when this node is disposed.
        /// </summary>
        public event EventHandler Disposed;
        #endregion
    }
}
