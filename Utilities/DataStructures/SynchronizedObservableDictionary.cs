#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures
{
    #region Using List
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Windows.Threading;
    #endregion
    /// <summary>
    /// This class provides a thread-safe, dispatcher-safe dictionary to bind to.
    /// </summary>
    /// <typeparam name="TKey">The type of key being stored.</typeparam>
    /// <typeparam name="TValue">The type of value being stored.</typeparam>
    public class SynchronizedObservableDictionary<TKey, TValue> : SynchronizedDictionary<TKey, TValue>, INotifyCollectionChanged
    {
        #region Internal Classes
        private class ObservableValueCollection : ICollection<TValue>, IList<TValue>, IEnumerable<TValue>, INotifyCollectionChanged
        {
            private SynchronizedObservableDictionary<TKey, TValue> m_OwningCollection;

            public ObservableValueCollection(SynchronizedObservableDictionary<TKey, TValue> owningCollection)
            {
                m_OwningCollection = owningCollection;
                m_OwningCollection.CollectionChanged += new NotifyCollectionChangedEventHandler(m_OwningCollection_CollectionChanged);
            }

            void m_OwningCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (CollectionChanged != null)
                {
                    NotifyCollectionChangedEventArgs e2 = null;
                    switch (e.Action)
                    {
                        case NotifyCollectionChangedAction.Add:
                            e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, ((KeyValuePair<TKey, TValue>)e.NewItems[0]).Value, e.NewStartingIndex);
                            break;
                        case NotifyCollectionChangedAction.Remove:
                            e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, ((KeyValuePair<TKey, TValue>)e.OldItems[0]).Value, e.OldStartingIndex);
                            break;
                        case NotifyCollectionChangedAction.Move:
                            e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, ((KeyValuePair<TKey, TValue>)e.NewItems[0]).Value, e.NewStartingIndex, e.OldStartingIndex);
                            break;
                        case NotifyCollectionChangedAction.Replace:
                            e2 = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, ((KeyValuePair<TKey, TValue>)e.NewItems[0]).Value, ((KeyValuePair<TKey, TValue>)e.OldItems[0]).Value, e.OldStartingIndex);
                            break;
                        case NotifyCollectionChangedAction.Reset:
                            e2 = e;
                            break;
                    }

                    OnCollectionChanged(e2);
                }
            }

            /// <summary>
            /// Raises the CollectionChanged event.
            /// </summary>
            /// <param name="e">Event Args describing how the collection changed.</param>
            private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                if (CollectionChanged != null)
                {
                    var disp = m_OwningCollection.Dispatcher;
                    if (disp != null && !disp.CheckAccess())
                    {
                        Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                        disp.Invoke(cb, e);
                    }
                    else
                    {
                        try
                        {
                            CollectionChanged(this, e);
                        }
                        catch
                        {

                        }
                    }
                }
            }

            int IList<TValue>.IndexOf(TValue item)
            {
                throw new NotSupportedException();
            }

            public void Insert(int index, TValue item)
            {
                throw new NotSupportedException();
            }

            public void RemoveAt(int index)
            {
                throw new NotSupportedException();
            }

            public TValue this[int index]
            {
                get
                {
                    return m_OwningCollection[index].Value;
                }
                set
                {
                    throw new NotSupportedException();
                }
            }

            public void Add(TValue item)
            {
                throw new NotSupportedException();
            }

            public void Clear()
            {
                throw new NotSupportedException();
            }

            public bool Contains(TValue item)
            {
                return m_OwningCollection.ContainsValue(item);
            }

            public void CopyTo(TValue[] array, int arrayIndex)
            {
                var temp = m_OwningCollection.Select(p => p.Value).ToArray();
                temp.CopyTo(array, arrayIndex);
            }

            public int Count
            {
                get { return m_OwningCollection.Count; }
            }

            public bool IsReadOnly
            {
                get { return true; }
            }

            public bool Remove(TValue item)
            {
                throw new NotSupportedException();
            }

            public IEnumerator<TValue> GetEnumerator()
            {
                return m_OwningCollection.Values.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public event NotifyCollectionChangedEventHandler CollectionChanged;
        }
        #endregion

        #region Variables
        /// <summary>
        /// The dispatcher which will process the collection changed event.
        /// </summary>
        private Dispatcher m_Dispatcher;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SynchronizedObservableDictionary class.
        /// </summary>
        public SynchronizedObservableDictionary()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SynchronizedObservableDictionary class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher which will process the collection changed event.</param>
        public SynchronizedObservableDictionary(Dispatcher observingDispatcher)
            : this()
        {
            m_Dispatcher = observingDispatcher;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the dispatcher to route updates through.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                Lock(); try
                {
                    return m_Dispatcher;
                }
                finally
                {
                    Unlock();
                }
            }

            set
            {
                Lock(); try
                {
                    m_Dispatcher = value;
                }
                finally
                {
                    Unlock();
                }
            }
        }
        #endregion
        #region INotifyCollectionChanged Members
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Event Args describing how the collection changed.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                if (m_Dispatcher != null && !m_Dispatcher.CheckAccess())
                {
                    Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                    m_Dispatcher.Invoke(cb, e);
                }
                else
                {
                    try
                    {
                        CollectionChanged(this, e);
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a collection used to provide notifications to WPF controls of the values collection.
        /// </summary>
        /// <returns></returns>
        public ICollection<TValue> GetValueNotifier()
        {
            return new ObservableValueCollection(this);
        }
        #endregion
        #region Overrides
        /// <summary>
        /// Adds a new or sets an existing key with the input value.
        /// </summary>
        /// <param name="key">The key to set the value of.</param>
        /// <param name="value">The value to set for the key.</param>
        /// <param name="index">The index that the item should be added with.</param>
        /// <param name="oldValue">The value that was replaced.</param>
        /// <returns>True if an item was added, false if an item was set.</returns>
        protected override bool AddOrSetInternal(TKey key, TValue value, int index, out TValue oldValue)
        {
            bool retVal = base.AddOrSetInternal(key, value, index, out oldValue);
            if (retVal)
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value), IndexOf(key)));
            }
            else
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                    NotifyCollectionChangedAction.Replace,              // action
                    new KeyValuePair<TKey, TValue>(key, value),         // new item
                    new KeyValuePair<TKey, TValue>(key, oldValue),
                    IndexOf(key)));    // old item
            }

            return retVal;
        }

        /// <summary>
        /// Attempts to retrieve an item by key, adding one using the value factory is necessary.
        /// </summary>
        /// <param name="key">The key to store the item under.</param>
        /// <param name="valueFactory">A function to call to generate the value.</param>
        /// <param name="value">The value that is being returned.</param>
        /// <returns>True if a value was added.</returns>
        protected override bool GetOrAddInternal(TKey key, Func<TKey, TValue> valueFactory, out TValue value)
        {
            bool ret = base.GetOrAddInternal(key, valueFactory, out value);
            if (ret)
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value), IndexOf(key)));
            }

            return ret;
        }

        /// <summary>
        /// Removes an item whose key is the input key from the collection.
        /// </summary>
        /// <param name="key">The key of the item to remove.</param>
        /// <param name="value">The removed value.</param>
        /// <param name="oldIndex">The index that the item previously resided at.</param>
        /// <returns>True if the item was removed.</returns>
        protected override bool RemoveInternal(TKey key, out TValue value, out int oldIndex)
        {
            bool retVal = base.RemoveInternal(key, out value, out oldIndex);
            if (oldIndex < 0)
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            else
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new KeyValuePair<TKey, TValue>(key, value), oldIndex));
            }

            return retVal;
        }

        /// <summary>
        /// Clears all items from the internal storage.
        /// </summary>
        protected override bool ClearInternal()
        {
            if (base.ClearInternal())
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                return true;
            }

            return false;
        }
        #endregion
    }
}
