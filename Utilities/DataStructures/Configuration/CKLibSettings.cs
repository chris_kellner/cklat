#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System.Linq;
    using System.Xml.Serialization;
    #endregion
    public class CKLibSettings
    {
        #region Variables
        /// <summary>
        /// List of setting groups.
        /// </summary>
        private SynchronizedDictionary<string, SettingGroup> m_SettingGroups;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CKLibSettings class.
        /// </summary>
        public CKLibSettings()
        {
            m_SettingGroups = new SynchronizedDictionary<string, SettingGroup>();
        }

        /// <summary>
        /// Initializes a new instance of the CKLibSettings class from another instance.
        /// </summary>
        /// <param name="other">The settings to copy from.</param>
        internal CKLibSettings(CKLibSettings other)
            : this()
        {
            Override(other);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Overrides the settings in this instance with those in the other instance.
        /// </summary>
        /// <param name="other">The instance whose settings have higher priority.</param>
        internal void Override(CKLibSettings other)
        {
            var otherSettings = other.SettingGroups;
            if (otherSettings != null)
            {
                for (int i = 0; i < otherSettings.Length; i++)
                {
                    var cOverride = otherSettings[i];
                    var myCurrent = this[cOverride.Name];
                    if (myCurrent != null)
                    {
                        myCurrent.Override(cOverride);
                    }
                    else
                    {
                        this[cOverride.Name] = cOverride;
                    }
                }
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the setting groups.
        /// </summary>
        [XmlArray("SettingGroups")]
        public SettingGroup[] SettingGroups
        {
            get
            {
                return m_SettingGroups.Values.ToArray();
            }

            set
            {
                ILockable locker = m_SettingGroups as ILockable;
                locker.Lock(); try
                {
                    m_SettingGroups.Clear();
                    if (value != null)
                    {
                        for (int i = 0; i < value.Length; i++)
                        {
                            this[value[i].Name] = value[i];
                        }
                    }
                }
                finally
                {
                    locker.Unlock();
                }
            }
        }

         /// <summary>
        /// Gets or sets the setting group by group name.
        /// </summary>
        /// <param name="groupName">The group name to get or set.</param>
        /// <param name="createIfNull">Creates the setting group if it does not exist.</param>
        /// <returns></returns>
        public SettingGroup this[string groupName, bool createIfNull]
        {
            get
            {
                SettingGroup group;
                if (m_SettingGroups.TryGetValue(groupName, out group))
                {
                    return group;
                }
                else if (createIfNull)
                {
                    this[groupName] = new SettingGroup(groupName);
                    return this[groupName];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the setting group by group name.
        /// </summary>
        /// <param name="groupName">The group name to get or set.</param>
        /// <returns></returns>
        public SettingGroup this[string groupName]
        {
            get
            {
                SettingGroup group;
                if (m_SettingGroups.TryGetValue(groupName, out group))
                {
                    return group;
                }

                return null;
            }

            set
            {
                if (!string.IsNullOrEmpty(groupName))
                {
                    SettingGroup group;
                    if (m_SettingGroups.TryGetValue(groupName, out group))
                    {
                        group.Override(value);
                    }
                    else
                    {
                        group = new SettingGroup(value);
                        if (group.Name != groupName)
                        {
                            group.Name = groupName;
                        }

                        m_SettingGroups[groupName] = group;
                    }
                }
            }
        }
        #endregion
    }
}
