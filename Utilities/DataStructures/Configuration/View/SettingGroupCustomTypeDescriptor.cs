﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.DataStructures.Configuration.View
{
    public class SettingGroupTypeDescriptorProvider : TypeDescriptionProvider
    {
    }

    public class SettingPropertyDescriptor : PropertyDescriptor
    {
        public SettingPropertyDescriptor(Setting setting)
            : base(setting.Name, null)
        {
        }

        public override bool CanResetValue(object component)
        {
            throw new NotImplementedException();
        }

        public override Type ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override object GetValue(object component)
        {
            throw new NotImplementedException();
        }

        public override bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public override Type PropertyType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ResetValue(object component)
        {
            throw new NotImplementedException();
        }

        public override void SetValue(object component, object value)
        {
            throw new NotImplementedException();
        }

        public override bool ShouldSerializeValue(object component)
        {
            throw new NotImplementedException();
        }
    }

    public class SettingGroupCustomTypeDescriptor : CustomTypeDescriptor
    {
        #region Variables
        private SettingGroup m_Group;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SettingGroupCustomTypeDescriptor class.
        /// </summary>
        /// <param name="group">The group to describe.</param>
        public SettingGroupCustomTypeDescriptor(SettingGroup group)
        {
            m_Group = group;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the setting group being described.
        /// </summary>
        public SettingGroup Group
        {
            get { return m_Group; }
            set { m_Group = value; }
        }
        #endregion

        #region Methods
        public override PropertyDescriptorCollection GetProperties()
        {
            return base.GetProperties();
        }

        public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return base.GetProperties(attributes);
        }
        #endregion
    }
}
