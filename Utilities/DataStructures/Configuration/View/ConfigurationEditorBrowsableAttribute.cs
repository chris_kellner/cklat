﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.DataStructures.Configuration.View
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Class, Inherited=true)]
    public class ConfigurationEditorBrowsableAttribute : Attribute
    {
        public ConfigurationEditorBrowsableAttribute(bool browsable)
        {
            Browsable = browsable;
        }

        public bool Browsable
        {
            get;
            set;
        }
    }
}
