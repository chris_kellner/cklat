#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System;
    using System.IO;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Diagnostics;
    #endregion
    /// <summary>
    /// The ConfigurationManager class is used to manage all of the default and user settings.
    /// </summary>
    public class ConfigurationManager
    {
        #region Variables
        /// <summary>
        /// Indicates whether the default settings have been loaded.
        /// </summary>
        private static bool m_IsLoaded;

        /// <summary>
        /// The name of the main product using this configuration.
        /// </summary>
        private static string m_ProductName;

        /// <summary>
        /// The version number of the main product.
        /// </summary>
        private static Version m_ProductVersion;

        /// <summary>
        /// Folder containing the default settings file.
        /// </summary>
        private static string m_DefaultSettingsFolder;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes static members of the CKLibSettings class.
        /// </summary>
        static ConfigurationManager()
        {
            m_ProductName = "CKLib";
            m_ProductVersion = new Version(1, 0, 0, 0);
            m_DefaultSettingsFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Default = new CKLibSettings();
            Current = new CKLibSettings();
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the path to the user directory.
        /// </summary>
        public static string CurrentSettingFilePath
        {
            get
            {
                return Path.Combine(AppDataFolder, FileName);
            }
        }

        /// <summary>
        /// The path to the default settings file.
        /// </summary>
        public static string DefaultSettingFilePath
        {
            get
            {
                return Path.Combine(DefaultSettingFolder, FileName);
            }
        }

        /// <summary>
        /// Path to the folder containing the default settings file.
        /// </summary>
        public static string DefaultSettingFolder
        {
            get
            {
                return m_DefaultSettingsFolder;
            }

            set
            {
                if (!m_IsLoaded)
                {
                    m_DefaultSettingsFolder = value;
                }
            }
        }

        /// <summary>
        /// Gets the path to the AppDataFolder.
        /// </summary>
        public static string AppDataFolder
        {
            get
            {
                string path = Path.Combine(
                    Environment.GetFolderPath(
                    Environment.SpecialFolder.LocalApplicationData),
                    string.Format("{0}\\", ProductName));
                path = Path.Combine(
                    path,
                    string.Format("{0}\\", ProductVersion.ToString(3)));
                return path;
            }
        }

        /// <summary>
        /// Gets the path to the file that log entries will be written to (if logging is enabled).
        /// </summary>
        public static string LogFilePath
        {
            get
            {
                string path = Path.Combine(
                    AppDataFolder,
                    string.Format("Log_Pid{0}_Domain{1}.txt", Process.GetCurrentProcess().Id, AppDomain.CurrentDomain.Id));
                return path;
            }
        }

        /// <summary>
        /// Gets or sets the name of the main product using this configuration.
        /// </summary>
        public static string ProductName
        {
            get
            {
                return m_ProductName;
            }

            set
            {
                if (!m_IsLoaded)
                {
                    m_ProductName = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the version of the main product.
        /// </summary>
        public static Version ProductVersion
        {
            get
            {
                return m_ProductVersion;
            }

            set
            {
                if (!m_IsLoaded && value != null)
                {
                    m_ProductVersion = value;
                }
            }
        }

        /// <summary>
        /// Gets the file name without directory information.
        /// </summary>
        public static string FileName
        {
            get
            {
                return string.Format(ProductName + "Settings.xml");
            }
        }

        /// <summary>
        /// Gets the default settings.
        /// </summary>
        private static CKLibSettings Default
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the current settings.
        /// </summary>
        public static CKLibSettings Current
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the default settings have been loaded.
        /// </summary>
        public static bool IsLoaded { get { return m_IsLoaded; } }
        #endregion
        #region Methods
        /// <summary>
        /// Gets the name of the file where this setting group should be saved.
        /// </summary>
        /// <param name="componentName">The name of the component to get the user settings file name for.</param>
        /// <returns></returns>
        private static string GetUserFileName(string componentName)
        {
            return Path.Combine(AppDataFolder, componentName + "Settings.xml");
        }

        /// <summary>
        /// Gets the name of the file where this setting group should be saved.
        /// </summary>
        /// <param name="componentName">The name of the component to get the default settings file name for.</param>
        /// <returns></returns>
        private static string GetDefaultFileName(string componentName)
        {
            return Path.Combine(DefaultSettingFolder, componentName + "Settings.xml");
        }

        /// <summary>
        /// Saves all settings for the current user.
        /// </summary>
        public static void Save()
        {
            var groups = Current.SettingGroups;
            if (groups != null)
            {
                for (int i = 0; i < groups.Length; i++)
                {
                    Save(groups[i]);
                }
            }
        }

        /// <summary>
        /// Saves all settings for the specified setting group for the current user.
        /// </summary>
        /// <param name="groupName">The name of the setting group to save.</param>
        public static void Save(string groupName)
        {
            var group = Current[groupName];
            Save(group);
        }

        /// <summary>
        /// Saves all settings for the specified setting group for the current user.
        /// </summary>
        /// <param name="group">The setting group to save.</param>
        private static void Save(SettingGroup group)
        {
            if (group != null)
            {
                string groupFilePath = GetUserFileName(group.ComponentName);
                string directory = Path.GetDirectoryName(groupFilePath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                CKLibSettings temp = new CKLibSettings();

                if (File.Exists(groupFilePath))
                {
                    using (FileStream fs = new FileStream(groupFilePath, FileMode.Open, FileAccess.Read))
                    {
                        XmlSerializer cereal = new XmlSerializer(typeof(CKLibSettings));
                        var obj = cereal.Deserialize(fs) as CKLibSettings;
                        if (obj != null)
                        {
                            temp.Override(obj);
                        }
                    }
                }

                temp[group.Name] = group;

                using (FileStream fs = new FileStream(groupFilePath, FileMode.Create, FileAccess.Write))
                {
                    XmlSerializer cereal = new XmlSerializer(typeof(CKLibSettings));
                    cereal.Serialize(fs, temp);
                }
            }
        }

        /// <summary>
        /// Reloads the configuration settings from disk.
        /// </summary>
        public static void Reload(bool force)
        {
            if (force || !m_IsLoaded)
            {
                CreateDefault();

                LoadSettingFile(ProductName);
                EnsureDefaults(Default);
                EnsureDefaults(Current);
                CKLib.Utilities.Diagnostics.FileChannel.Initialize();
                m_IsLoaded = true;
            }
        }

        /// <summary>
        /// Assingns the component name to each setting group.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="componentName"></param>
        private static void AssignComponentName(CKLibSettings settings, string componentName)
        {
            SettingGroup[] groups = settings.SettingGroups;
            if (groups != null)
            {
                for (int i = 0; i < groups.Length; i++)
                {
                    groups[i].ComponentName = componentName;
                }
            }
        }

        /// <summary>
        /// Loads configuration settings from disk.
        /// </summary>
        /// <param name="componentName"></param>
        public static void LoadSettingFile(string componentName)
        {
            string defaultPath = GetDefaultFileName(componentName);
            if (File.Exists(defaultPath))
            {
                using (FileStream fs = new FileStream(defaultPath, FileMode.Open, FileAccess.Read))
                {
                    XmlSerializer cereal = new XmlSerializer(typeof(CKLibSettings));
                    var obj = cereal.Deserialize(fs) as CKLibSettings;
                    if (obj != null)
                    {
                        AssignComponentName(obj, componentName);
                        Default.Override(obj);
                        Current.Override(obj);
                    }
                }
            }

            string currentPath = GetUserFileName(componentName);
            if (File.Exists(currentPath))
            {
                try
                {
                    using (FileStream fs = new FileStream(currentPath, FileMode.Open, FileAccess.Read))
                    {
                        XmlSerializer cereal = new XmlSerializer(typeof(CKLibSettings));
                        var obj = cereal.Deserialize(fs) as CKLibSettings;
                        if (obj != null)
                        {
                            AssignComponentName(obj, componentName);
                            Current.Override(obj);
                        }
                    }
                }
                catch (InvalidOperationException e)
                {
                    var ex = e.InnerException as XmlException;
                    if (ex != null && ex.Message == "Root element is missing.")
                    {
                        File.Delete(currentPath);
                    }
                }
            }
            else
            {
                Current[componentName, true].ComponentName = componentName;
            }
        }

        /// <summary>
        /// Initializes the default settings.
        /// </summary>
        private static void CreateDefault()
        {
            if (!File.Exists(DefaultSettingFilePath))
            {
                var def = new CKLibSettings();
                Default.Override(def);
                var group = Default[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP, true];
                group.SetValue(Setting.SERVER_NAME_SETTING, "Local");
                group.SetValue(Setting.SERVER_TCP_PORT, "6969");
                group.SetValue(Setting.LOG_LEVEL, CKLib.Utilities.Diagnostics.OutputLevel.Off);
                Default[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP] = group;

                using (FileStream fs = new FileStream(DefaultSettingFilePath, FileMode.Create, FileAccess.Write))
                {
                    XmlSerializer cereal = new XmlSerializer(typeof(CKLibSettings));
                    cereal.Serialize(fs, Default);
                }
            }
        }

        /// <summary>
        /// Ensures that the default settings are present.
        /// </summary>
        private static void EnsureDefaults(CKLibSettings settings)
        {
            if (settings != null)
            {
                var group = settings[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP, true];
                string test;
                if (!group.TryReadValue<string>(Setting.SERVER_NAME_SETTING, out test))
                {
                    group.SetValue(Setting.SERVER_NAME_SETTING, "Local");
                }

                if (!group.TryReadValue<string>(Setting.SERVER_TCP_PORT, out test))
                {
                    group.SetValue(Setting.SERVER_TCP_PORT, "6969");
                }

                CKLib.Utilities.Diagnostics.OutputLevel test2;
                if (!group.TryReadValue<CKLib.Utilities.Diagnostics.OutputLevel>(Setting.LOG_LEVEL, out test2))
                {
                    group.SetValue(Setting.LOG_LEVEL, CKLib.Utilities.Diagnostics.OutputLevel.Off);
                }
            }
        }

        /// <summary>
        /// Replaces the current settings with the default settings.
        /// </summary>
        public static void ResetDefaults()
        {
            Current.Override(Default);
        }
        #endregion
    }
}
