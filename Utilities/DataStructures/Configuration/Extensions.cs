#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.DataStructures.Configuration
{
    #region Using List
    using System;
    #endregion
    public static class Extensions
    {
        /// <summary>
        /// Converts an object to a setting value.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static SettingValue ToSettingValue(this object o)
        {
            return new SettingValue(o);
        }

        /// <summary>
        /// Converts an array into an array of setting values.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static SettingValue[] ToSettingValueArray(this Array a)
        {
            if (a != null)
            {
                SettingValue[] arr = new SettingValue[a.Length];
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = a.GetValue(i).ToSettingValue();
                }

                return arr;
            }

            return null;
        }

        /// <summary>
        /// Converts a setting value array to an array of the specified type.
        /// </summary>
        /// <typeparam name="TArray"></typeparam>
        /// <param name="a"></param>
        /// <returns></returns>
        public static TArray[] ToArray<TArray>(this SettingValue[] a)
        {
            if (a != null)
            {
                TArray[] arr = new TArray[a.Length];
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] != null)
                    {
                        arr[i] = a[i].Cast<TArray>();
                    }
                    else
                    {
                        arr[i] = default(TArray);
                    }
                }

                return arr;
            }

            return null;
        }
    }
}
