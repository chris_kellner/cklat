﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.DataStructures.Linq
{
    public static class Extensions
    {
        /// <summary>
        /// Executes the specified action on each element in the set.
        /// </summary>
        /// <typeparam name="T">The type of elements contained in the enumerable</typeparam>
        /// <param name="set">The enumerable to act upon</param>
        /// <param name="action">The action to perform.</param>
        /// <exception cref="System.ArgumentNullException">Throws an exception if the inputs are null.</exception>
        public static void Foreach<T>(this IEnumerable<T> set, Action<T> action)
        {
            if (set == null)
            {
                throw new ArgumentNullException("set");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (var val in set)
            {
                action(val);
            }
        }
    }
}
