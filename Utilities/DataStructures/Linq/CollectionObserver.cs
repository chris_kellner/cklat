#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.DataStructures.Linq
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Threading;
    using System.Threading;
    #endregion
    /// <summary>
    /// This class is intended to serve as a bindable component which represents data contained in several different collections all as one.
    /// </summary>
    /// <typeparam name="TCollection">The type contained in the collections.</typeparam>
    public class CollectionObserver<TCollection> : IEnumerable, IEnumerable<TCollection>, INotifyCollectionChanged, IList<TCollection>, IList, IDisposable, ILockable
    {
        #region Internal Classes
        /// <summary>
        /// This class is used to manage the observed collections.
        /// </summary>
        protected class Observer : IDisposable, IEnumerable<TCollection>, ILockable
        {
            #region Variables
            /// <summary>
            /// The wrapped collection.
            /// </summary>
            private INotifyCollectionChanged m_Collection;

            /// <summary>
            /// The parent collection observer.
            /// </summary>
            private CollectionObserver<TCollection> m_Parent;
            #endregion
            #region Constructors
            /// <summary>
            /// Initializes a new instance of the Observer class.
            /// </summary>
            /// <param name="parent">The collection observer that owns this observer.</param>
            /// <param name="collection">The collection being observed.</param>
            public Observer(CollectionObserver<TCollection> parent, INotifyCollectionChanged collection)
            {
                if (collection == null)
                {
                    throw new ArgumentNullException("collection");
                }

                m_Collection = collection;
                m_Parent = parent;
                m_Collection.CollectionChanged -= collection_CollectionChanged;
                m_Collection.CollectionChanged += collection_CollectionChanged;
            }
            #endregion

            #region Methods
            /// <summary>
            /// Occurs when the collection changes.
            /// </summary>
            /// <param name="sender">The collection that changed.</param>
            /// <param name="e">The change that was made.</param>
            void collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (m_Parent != null)
                {
                    m_Parent.OnObserverChanged(this, e);
                }
            }

            /// <summary>
            /// Gets a value indicating whether this collection contains the specified value.
            /// </summary>
            /// <param name="item">The item to check for.</param>
            /// <returns>The specified item.</returns>
            public bool Contains(TCollection item)
            {
                if (m_Collection is ICollection<TCollection>)
                {
                    return ((ICollection<TCollection>)m_Collection).Contains(item);
                }

                return this.Contains<TCollection>(item);
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets a value indicating how many items are in the collection.
            /// </summary>
            public int Count
            {
                get
                {
                    var collection = m_Collection as ICollection<TCollection>;
                    if (collection != null)
                    {
                        return collection.Count;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Gets the inner collection.
            /// </summary>
            public ICollection<TCollection> InnerCollection
            {
                get
                {
                    return m_Collection as ICollection<TCollection>;
                }
            }

            /// <summary>
            /// Gets the inner collection as a list.
            /// </summary>
            public IList<TCollection> InnerList
            {
                get
                {
                    return m_Collection as IList<TCollection>;
                }
            }

            #endregion

            #region IDisposable Members
            /// <summary>
            /// Releases resources held by this instance.
            /// </summary>
            public void Dispose()
            {
                Dispose(true);
            }

            /// <summary>
            /// Disposes of unmanaged, and optionally managed resources.
            /// </summary>
            /// <param name="disposing">True to dispose managed resources as well.</param>
            protected virtual void Dispose(bool disposing)
            {
                if (m_Parent != null)
                {
                    m_Parent.OnObserverChanged(
                        this,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    m_Parent = null;
                }

                if (m_Collection != null)
                {
                    m_Collection.CollectionChanged -= collection_CollectionChanged;
                    m_Collection = null;
                }
            }

            /// <summary>
            /// Finalizes an instance of the observer class.
            /// </summary>
            ~Observer()
            {
                Dispose(false);
            }
            #endregion

            #region IEnumerable Members
            /// <summary>
            /// Gets an enumerator that can be used to enumerate the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to enumerate the collection.</returns>
            public IEnumerator<TCollection> GetEnumerator()
            {
                IEnumerable<TCollection> collection = m_Collection as IEnumerable<TCollection>;
                if (collection != null)
                {
                    return collection.GetEnumerator();
                }

                return null;
            }

            /// <summary>
            /// Gets an enumerator that can be used to enumerate the collection.
            /// </summary>
            /// <returns>An enumerator that can be used to enumerate the collection.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
            #endregion

            #region ILockable Members
            private ILockable Locker { get { return m_Collection as ILockable; } }

            public void Lock()
            {
                var locker = Locker;
                if (locker != null)
                {
                    locker.Lock();
                }
            }

            public void Unlock()
            {
                var locker = Locker;
                if (locker != null)
                {
                    locker.Unlock();
                }
            }

            public bool IsSynchronized
            {
                get
                {
                    var locker = Locker;
                    if (locker != null)
                    {
                        return locker.IsSynchronized;
                    }

                    return false;
                }
            }

            public int LockId
            {
                get;
                set;
            }
            #endregion
        }
        #endregion

        #region Variables
        /// <summary>
        /// The dispatcher which will route the collection changed events.
        /// </summary>
        private Dispatcher m_Dispatcher;

        /// <summary>
        /// List of collections being observed.
        /// </summary>
        private List<Observer> m_Observed;

        /// <summary>
        /// A value indicating whether the count property is valid.
        /// </summary>
        private bool m_CountIsValid;

        /// <summary>
        /// The last valid count.
        /// </summary>
        private int m_CachedCount;

        /// <summary>
        /// Object key used to identify the collection changed event.
        /// </summary>
        private static object COLLECTION_CHANGED_KEY = new object();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CollectionObserver class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher to route the collection changes through.</param>
        /// <param name="observedCollection">The collection to begin observing.</param>
        public CollectionObserver(Dispatcher observingDispatcher, INotifyCollectionChanged observedCollection)
        {
            Events = new EventHandlerList();
            m_Dispatcher = observingDispatcher;
            m_Observed = new List<Observer>();
            ObserveCollection(observedCollection);
        }

        /// <summary>
        /// Initializes a new instance of the CollectionObserver class.
        /// </summary>
        /// <param name="observingDispatcher">The dispatcher to route the collection changes through.</param>
        /// <param name="observedCollections">The collections to begin observing.</param>
        public CollectionObserver(Dispatcher observingDispatcher, IEnumerable<INotifyCollectionChanged> observedCollections)
        {
            Events = new EventHandlerList();
            m_Dispatcher = observingDispatcher;
            m_Observed = new List<Observer>();
            if (observedCollections != null)
            {
                foreach (var observedCollection in observedCollections)
                {
                    ObserveCollection(observedCollection);
                }
            }
            
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a collection to the list of observed collections.
        /// </summary>
        /// <param name="collectionToObserve">The collection to being observing.</param>
        /// <returns>True if the collection was added to the list of observed collections, false otherwise.</returns>
        public bool ObserveCollection(INotifyCollectionChanged collectionToObserve)
        {
            if (collectionToObserve != null && !(collectionToObserve is CollectionObserver<TCollection>))
            {
                Lock(); try
                {
                    m_Observed.Add(new Observer(this, collectionToObserve));
                    return true;
                }
                finally
                {
                    Unlock();
                }
            }

            return false;
        }

        /// <summary>
        /// Occurs when an observer's collection changes.
        /// </summary>
        /// <param name="observer">The observer of the collection that has changed.</param>
        /// <param name="e">The change event args.</param>
        protected void OnObserverChanged(Observer observer, NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventArgs args = null;
            Lock(); try
            {
                int obsIdx = m_Observed.IndexOf(observer);
                int baseOffset = 0;
                for (int i = 0; i < obsIdx; i++)
                {
                    baseOffset += m_Observed[i].Count;
                }

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        args = new NotifyCollectionChangedEventArgs(e.Action, e.NewItems, e.NewStartingIndex + baseOffset);
                        break;
                    case NotifyCollectionChangedAction.Move:
                        args = new NotifyCollectionChangedEventArgs(e.Action, e.NewItems, e.NewStartingIndex + baseOffset, e.OldStartingIndex + baseOffset);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        args = new NotifyCollectionChangedEventArgs(e.Action, e.OldItems, e.OldStartingIndex + baseOffset);
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        args = new NotifyCollectionChangedEventArgs(e.Action, e.NewItems, e.OldItems, e.NewStartingIndex + baseOffset);
                        break;
                    case NotifyCollectionChangedAction.Reset:
                    default:
                        args = new NotifyCollectionChangedEventArgs(e.Action);
                        break;
                }

                m_CountIsValid = false;
            }
            finally
            {
                Unlock();
            }

            if (args != null)
            {
                OnCollectionChanged(args);
            }
        }

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Event Args describing how the collection changed.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (Events[COLLECTION_CHANGED_KEY] != null)
            {
                if (m_Dispatcher != null && !m_Dispatcher.CheckAccess())
                {
                    Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                    m_Dispatcher.Invoke(cb, e);
                }
                else
                {
                    RaiseCollectionChanged(e);
                }
            }
        }

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Event Args describing how the collection changed.</param>
        protected void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handler = Events[COLLECTION_CHANGED_KEY] as NotifyCollectionChangedEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the dispatcher used to observe the collection.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                return m_Dispatcher;
            }
        }

        /// <summary>
        /// Gets the list of events for this instance.
        /// </summary>
        protected EventHandlerList Events
        {
            get;
            private set;
        }
        #endregion

        #region Inherited Members
        #region INotifyCollectionChanged Members
        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add
            {
                Events.AddHandler(COLLECTION_CHANGED_KEY, value);
            }

            remove
            {
                Events.RemoveHandler(COLLECTION_CHANGED_KEY, value);
            }
        }
        #endregion

        #region IEnumerable Members
        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that iterates through the collection.</returns>
        public IEnumerator<TCollection> GetEnumerator()
        {
            return IterateInternals().GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that iterates through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IEnumerable<TCollection> IterateInternals()
        {
            Lock(); try
            {
                return m_Observed.SelectMany(c => c).ToArray();
            }
            finally
            {
                Unlock();
            }
        }
        #endregion

        #region ICollection Members
        /// <summary>
        /// This action is not supported.
        /// </summary>
        /// <param name="item"></param>
        void ICollection<TCollection>.Add(TCollection item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// This action is not supported.
        /// </summary>
        void  ICollection<TCollection>.Clear()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Gets value indicating whether the collection contains the specified value.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(TCollection item)
        {
            Lock(); try
            {
                for (int i = 0; i < m_Observed.Count; i++)
                {
                    if (m_Observed[i].Contains(item))
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                Unlock();
            }
        }

        public void CopyTo(TCollection[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the number of items being observed.
        /// </summary>
        public int Count
        {
            get 
            {
                if (!m_CountIsValid)
                {
                    Lock(); try
                    {
                        m_CachedCount = 0;
                        for (int i = 0; i < m_Observed.Count; i++)
                        {
                            m_CachedCount += m_Observed[i].Count;
                        }

                        m_CountIsValid = true;
                    }
                    finally
                    {
                        Unlock();
                    }
                }

                return m_CachedCount;
            }
        }

        /// <summary>
        /// Returns true.
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        bool  ICollection<TCollection>.Remove(TCollection item)
        {
            throw new NotSupportedException();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases any managed and unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases any unmanaged and optionally managed resources.
        /// </summary>
        /// <param name="disposing">True to release managed resources as well.</param>
        protected virtual void Dispose(bool disposing)
        {
            Events.Dispose();
            m_Dispatcher = null;
            if (m_Observed != null)
            {
                for (int i = 0; i < m_Observed.Count; i++)
                {
                    m_Observed[i].Dispose();
                }

                m_Observed.Clear();
            }
        }

        /// <summary>
        /// Finalizes an instance of the CollectionObserver class.
        /// </summary>
        ~CollectionObserver()
        {
            Dispose(false);
        }
        #endregion
        #endregion

        #region IList<TCollection> Members
        /// <summary>
        /// Finds the index in the collection of the specified item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(TCollection item)
        {
            int baseOffset = 0;
            for (int i = 0; i < m_Observed.Count; i++)
            {
                int index = m_Observed[i].InnerList.IndexOf(item);
                if (index > -1)
                {
                    return index + baseOffset;
                }

                baseOffset += m_Observed[i].Count;
            }

            return -1;
        }

        void IList<TCollection>.Insert(int index, TCollection item)
        {
            throw new NotSupportedException();
        }

        void IList<TCollection>.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Gets the item at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public TCollection this[int index]
        {
            get
            {
                Lock(); try
                {
                    for (int i = 0; i < m_Observed.Count; i++)
                    {
                        int count = m_Observed[i].Count;
                        if (index < count)
                        {
                            return m_Observed[i].InnerList[index];
                        }

                        index -= count;
                    }
                }
                finally
                {
                    Unlock();
                }

                throw new IndexOutOfRangeException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        #endregion

        #region IList Members
        int IList.Add(object value)
        {
            throw new NotSupportedException();
        }

        void IList.Clear()
        {
            throw new NotSupportedException();
        }

        bool IList.Contains(object value)
        {
            if (value is TCollection)
            {
                return Contains((TCollection)value);
            }

            return false;
        }

        int IList.IndexOf(object value)
        {
            if (value is TCollection)
            {
                return IndexOf((TCollection)value);
            }

            return -1;
        }

        void IList.Insert(int index, object value)
        {
            throw new NotSupportedException();
        }

        bool IList.IsFixedSize
        {
            get {return false; }
        }

        bool IList.IsReadOnly
        {
            get { return true; }
        }

        void IList.Remove(object value)
        {
            throw new NotSupportedException();
        }

        void IList.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        #endregion
        #region ICollectionMembers
        void ICollection.CopyTo(Array array, int index)
        {
            CopyTo((TCollection[])array, index);
        }

        int ICollection.Count
        {
            get { return Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return true; }
        }
        #endregion
        #region ILockable Members
        private readonly object m_Locker = new object();

        object ICollection.SyncRoot
        {
            get { return m_Locker; }
        }

        public void Lock()
        {
            Monitor.Enter(m_Locker);
            for (int i = 0; i < m_Observed.Count; i++)
            {
                m_Observed[i].Lock();
            }
        }

        public void Unlock()
        {
            for (int i = m_Observed.Count - 1; i >= 0; i--)
            {
                m_Observed[i].Unlock();
            }

            Monitor.Exit(m_Locker);
        }

        bool ILockable.IsSynchronized
        {
            get { return true; }
        }

        public int LockId
        {
            get;
            set;
        }
        #endregion
    }
}
