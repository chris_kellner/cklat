﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Evaluator;
using System.Xml.Serialization;

namespace CKLib.Utilities.Language
{
    public class ASTNode : AElement
    {
        #region Variables
        private Dictionary<string, object> m_EvaluatorCache;
        private INodeEvaluator m_EvaluatorOverride;
        #endregion

        public ASTNode(AElement fromElement)
            : base(fromElement.TokenType)
        {
            int i = 0;
            AddRoles(fromElement.GetRoles());
            Nodes = fromElement
                .Where(e => !e.NonExecutable)
                    .Select(e => new ASTNode(e) { NodeIndex = i++ })
                    .ToList();

            Position = fromElement.Position;
            Line = fromElement.Line;
            Column = fromElement.Column;

            Token tok = fromElement as Token;
            if (tok != null)
            {
                Value = tok.Value;
                IsTerminal = true;
            }
        }

        #region Properties
        /// <summary>
        /// Gets the child nodes of this node (if this node is non-terminal).
        /// </summary>
        public IList<ASTNode> Nodes
        {
            get;
            private set;
        }

        public override int Length
        {
            get
            {
                if (IsTerminal)
                {
                    return Value != null ? Value.Length : 0;
                }
                else
                {
                    int first = Position;
                    int last = Position;
                    if (Nodes != null && Nodes.Count > 0)
                    {
                        var node = Nodes.Last();
                        last = node.Position + node.Length;
                    }

                    return last - first;
                }
            }
        }

        /// <summary>
        /// Gets the terminal value of this node (if this node is terminal).
        /// </summary>
        public string Value
        {
            get;
            private set;
        }

        public bool IsTerminal { get; private set; }

        public int NodeIndex { get; private set; }

        public INodeEvaluator Evaluator
        {
            get
            {
                if (m_EvaluatorOverride == null)
                {
                    return TokenType.Evaluator;
                }

                return m_EvaluatorOverride;
            }

            set
            {
                m_EvaluatorOverride = value;
            }
        }
        #endregion

        public ASTNode FindNodeByType(string nodeType, int instance)
        {
            return Nodes.Where(n => n.IsType(nodeType)).Skip(instance).FirstOrDefault();
        }

        public ASTNode FindNodeByType(IGrammarElement nodeType, int instance)
        {
            return Nodes.Where(n => n.IsType(nodeType)).Skip(instance).FirstOrDefault();
        }

        /// <summary>
        /// Searches through the child nodes for ones that fulfill the specified role.
        /// </summary>
        /// <param name="roleName">The syntactical-role that the child node is fulfilling in this tree.</param>
        /// <param name="instance">The occurrence to retrieve when more than one node with the specified role is present.</param>
        /// <returns>An ASTNode that fulfills the specified syntactical-role in the tree, or null if one could not be found.</returns>
        public ASTNode FindNodeByRole(string roleName, int instance)
        {
            if (this.HasRole(roleName))
            {
                return this;
            }

            return Nodes.Where(n => n.HasRole(roleName)).Skip(instance).FirstOrDefault();
        }

        /// <summary>
        /// Searches through the child nodes for ones that fulfill the specified role.
        /// </summary>
        /// <param name="roleName">The syntactical-role that the child node is fulfilling in this tree.</param>
        /// <param name="mandatory">
        /// Specifies whether or not the node is mandatory.
        /// If set to <c>true</c>, an exception will be produced if the role cannot be found
        /// </param>
        /// <returns>An ASTNode that fulfills the specified syntactical-role in the tree, or null if one could not be found.</returns>
        /// <exception cref="ExecutionException"/>
        public ASTNode FindNodeByRole(string roleName, bool mandatory)
        {
            return FindNodeByRole(roleName, mandatory, true);
        }

        /// <summary>
        /// Searches through the child nodes for ones that fulfill the specified role.
        /// </summary>
        /// <param name="roleName">The syntactical-role that the child node is fulfilling in this tree.</param>
        /// <param name="mandatory">
        /// Specifies whether or not the node is mandatory.
        /// If set to <c>true</c>, an exception will be produced if the role cannot be found
        /// </param>
        /// <returns>An ASTNode that fulfills the specified syntactical-role in the tree, or null if one could not be found.</returns>
        /// <exception cref="ExecutionException"/>
        public ASTNode FindNodeByRole(string roleName, bool mandatory, bool includeSelf)
        {
            if (includeSelf && this.HasRole(roleName))
            {
                return this;
            }

            var node = Nodes.Where(n => n.HasRole(roleName)).FirstOrDefault();
            if (mandatory && node == null)
            {
                throw new ExecutionException("Missing required ROLE: " + roleName, this);
            }

            return node;
        }

        /// <summary>
        /// Searches through the child nodes for ones that fulfill the specified role.
        /// </summary>
        /// <param name="roleName">The syntactical-role that the child nodes are fulfilling in this tree.</param>
        /// <returns>The set of ASTNodes that fulfill the specified syntactical-role in the tree.</returns>
        public IEnumerable<ASTNode> FindNodesByRole(string roleName)
        {
            if (this.HasRole(roleName))
            {
                return new ASTNode[] { this };
            }
            else
            {
                return Nodes.Where(n => n.HasRole(roleName));
            }
        }

        public void CacheValue(string key, object value)
        {
            if (m_EvaluatorCache == null)
            {
                m_EvaluatorCache = new Dictionary<string, object>();
            }

            m_EvaluatorCache[key] = value;
        }

        public bool TryGetCachedValue<TValue>(string key, out TValue value)
        {
            if (m_EvaluatorCache == null)
            {
                value = default(TValue);
                return false;
            }

            object obj;
            if (m_EvaluatorCache.TryGetValue(key, out obj))
            {
                value = (TValue)obj;
                return true;
            }

            value = default(TValue);
            return false;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool includeChildren)
        {
            StringBuilder builder = new StringBuilder();
            ToString(builder, string.Empty, includeChildren);
            return builder.ToString();
        }

        private void ToString(StringBuilder builder, string indent, bool includeChildren)
        {
            builder.AppendFormat("{0}[{1}:{2}+{3}] {4} -> ({5}): {6}",
                indent,
                this.Line, this.Column,
                this.Length,
                this.TokenType.Name,
                string.Join(", ", this.GetRoles().ToArray()),
                this.Value != null ? "'" + this.Value + "'" : "[");
            if (Nodes != null && Nodes.Count > 0 && includeChildren)
            {
                builder.AppendLine();
                bool first = true;
                for(int i = 0;i<Nodes.Count;i++)
                {
                    var node = Nodes[i];
                    if (!first)
                    {
                        builder.Append("," + Environment.NewLine);
                    }
                    node.ToString(builder, indent + "    ", includeChildren);
                    first = false;
                }

                builder.AppendLine();
                builder.Append(indent + "]");
            }
        }

        public override IEnumerator<AElement> GetEnumerator()
        {
            return Nodes.Cast<AElement>().GetEnumerator();
        }
    }
}
