﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language;

namespace CKLib.Utilities.Language.Lex
{
    public class Terminal : AGrammarElement
    {
        #region Variables
        private Regex m_Regex;
        private bool m_Init;
        #endregion

        #region Constructors
        public Terminal(Grammar grammar)
            : base(grammar)
        {
        }

        public Terminal(Grammar grammar, string name, MatchType matchType, string pattern)
            : this(grammar)
        {
            Name = name;
            MatchType = matchType;
            Pattern = pattern;
        }
        #endregion

        #region Properties
        public MatchType MatchType { get; set; }
        public string Pattern { get; set; }
        public RegexOptions RegexOptions { get; set; }
        public bool NonParsed { get; set; }

        public List<Terminal> SubTokens { get; set; }
        #endregion

        public static Terminal MakeKeywords(Grammar grammar, IDictionary<string, string> nameValueLiterals)
        {
            var keywordRoot = new Terminal(grammar)
            {
                Name = "Keyword",
                NonExecutable = true,
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
            };

            foreach (var pair in nameValueLiterals)
            {
                keywordRoot.SubTokens.Add(new Terminal(grammar, pair.Key, MatchType.Regex, @"\b" + pair.Value + @"\b"));
            }

            return keywordRoot;
        }

        public override void Initialize()
        {
            if (!m_Init)
            {
                m_Init = true;
                GetCachedRegex();
                if (SubTokens != null)
                {
                    foreach (var token in SubTokens)
                    {
                        token.Parent = this;
                        token.Initialize();
                    }
                }
            }
        }

        public virtual Token ParseToken(ScriptSource source)
        {
            Initialize();
            Token t = null;
            switch (MatchType)
            {
                case MatchType.Literal:
                    t = MatchLiteral(source);
                    break;
                case MatchType.Group:
                    t = MatchGroup(source);
                    break;
                case MatchType.Regex:
                    t = MatchRegex(source);
                    break;
                case MatchType.Null:
                    if (source.CurrentPosition >= source.Input.Length)
                    {
                        t = new Token(this);
                    }

                    break;
            }

            if (t != null)
            {
                t.NonParsed = NonParsed;
            }

            return t;
        }

        protected virtual Token MatchLiteral(ScriptSource source)
        {
            int tLen = Pattern.Length;
            if (source.Input.Length - source.CurrentPosition >= tLen)
            {
                int cmp = string.Compare(source.Input, source.CurrentPosition, Pattern, 0, Pattern.Length);
                if (cmp == 0)
                {
                    return new Token(this, Pattern);
                }
            }

            return null;
        }

        private class RegexState{
            public int NextMatchIdx = -1;
            public string NextMatchValue = null;
            public bool HasMatch = true;
        }

        private RegexState m_RegexState = null;
        private ScriptSource m_lastSource = null;

        //private Dictionary<ScriptSource, RegexState> m_StateCache;

        protected virtual Token MatchRegex(ScriptSource source)
        {
            RegexState state = m_RegexState;
            if (m_lastSource != source)
            {
                m_lastSource = source;
                state = m_RegexState = new RegexState();
            }

            if (!state.HasMatch)
            {
                return null;
            }

            int cPos = source.CurrentPosition;
            if (cPos < state.NextMatchIdx)
            {
                return null;
            }
            else if (cPos == state.NextMatchIdx)
            {
                return new Token(this, state.NextMatchValue);
            }
            else
            {
                Regex rex = GetCachedRegex();

                Match m = rex.Match(source.Input, cPos);
                if (m.Success)
                {
                    if (m.Index == cPos)
                    {
                        state.NextMatchIdx = m.Index;
                        state.NextMatchValue = m.Value;
                        return new Token(this, m.Value);
                    }
                    else
                    {
                        state.NextMatchIdx = m.Index;
                        state.NextMatchValue = m.Value;
                    }
                }
                else
                {
                    state.HasMatch = false;
                }

                return null;
            }
        }

        protected virtual Token MatchGroup(ScriptSource source)
        {
            var toks = SubTokens.Select(def => def.ParseToken(source)).Where(t=>t != null);
            return toks.FirstOrDefault();
        }

        protected Regex GetCachedRegex()
        {
            if (m_Regex == null && MatchType == MatchType.Regex)
            {
                m_Regex = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions);
            }

            return m_Regex;
        }


        public override Parse.ParseMatch Matches(ElementList tokens, int index)
        {
            if (tokens[index].IsType(this))
            {
                return new ParseMatch(tokens[index], 1, null);
            }

            return ParseMatch.NoMatch;
        }

        public override string ToString()
        {
            return string.Format(
                "G->T{{ \"{0}\": -> {2} }}",
                Name,
                Parent != null ? Parent.Name : string.Empty);
        }
    }
}
