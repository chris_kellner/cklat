﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CKLib.Utilities.Language.Lex
{
    public class Lexer
    {
        private IGrammarElement m_EOL;

        public Lexer(Grammar grammar)
        {
            Grammar = grammar;
            m_EOL = grammar.FindGrammarElement("<EOL>");
        }

        public Grammar Grammar { get; private set; }

        public TimeSpan LastLexTime { get; private set; }

        public IEnumerable<AElement> LexTokens(string input)
        {
            LastLexTime = TimeSpan.Zero;
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            ScriptSource src = new ScriptSource(){
                Input = input,
                CurrentLine = 1,
                CurrentColumn = 1
            };

            Token lToken = null, cToken = null;
            do
            {
                var tokens = Grammar.RootTokens.Select(def => def.ParseToken(src)).Where(t => t != null);
                cToken = tokens.FirstOrDefault();
                if (cToken != null)
                {
                    lToken = cToken;
                    cToken.Position = src.CurrentPosition;
                    cToken.Line = src.CurrentLine;
                    cToken.Column = src.CurrentColumn;
                    // calculate line/column
                    sw.Stop();
                    LastLexTime = sw.Elapsed;
                    yield return cToken;
                    sw.Start();
                    src.CurrentPosition += cToken.Length;
                    src.CurrentColumn += cToken.Length;
                    if (cToken.Value != null)
                    {
                        var lines = cToken.Value.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                        var newLines = lines.Length - 1;
                        if (newLines > 0)
                        {
                            // New line start
                            src.CurrentLine += newLines;
                            src.CurrentColumn = 1;
                        }
                    }
                }
                else
                {
                    if (!char.IsWhiteSpace(src.Input[src.CurrentPosition]))
                    {
                        throw new LexerException(
                            string.Format("Unexpected character [{0}] at position: {1} ({2}:{3})",
                            src.Input[src.CurrentPosition], src.CurrentPosition, src.CurrentLine, src.CurrentColumn),
                            src.CurrentPosition);
                    }
                    src.CurrentPosition++;
                    src.CurrentColumn++;
                }
            } while (cToken == null || cToken.TokenType != Grammar.T_EOF);

            sw.Stop();
            LastLexTime = sw.Elapsed;
            yield break;
        }
    }
}
