﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Lex
{
    public class Token : AElement
    {
        public Token(Terminal type)
            : base(type)
        {
        }

        public Token(Terminal type, string value)
            : this(type)
        {
            Value = value;
        }

        public string Value { get; set; }
        
        public override int Length
        {
            get
            {
                if (Value == null)
                {
                    return 0;
                }

                return Value.Length;
            }
        }

        public override string ToString()
        {
            return string.Format("[{2}:{3}]{{\"{0}\": '{1}'}}",
                TokenType.Name, Value, Line, Column);
        }
    }
}
