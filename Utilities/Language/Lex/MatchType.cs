﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Lex
{
    public enum MatchType
    {
        Literal,
        Regex,
        Group,
        Null
    }
}
