﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class LanguageException : Exception
    {
        public LanguageException(string message)
            : base(message)
        {
        }

        public LanguageException(string message, AElement element)
            : base(message)
        {
            Element = element;
        }

        public LanguageException(string message, AElement element, Exception innerException)
            : base(message, innerException)
        {
            Element = element;
        }

        public AElement Element
        {
            get;
            private set;
        }

        public override string Message
        {
            get
            {
                if (Element != null)
                {
                    return base.Message + string.Format(" at: [{0}:{1}+{2}].", Element.Line, Element.Column, Element.Length);
                }

                return base.Message;
            }
        }
    }
}
