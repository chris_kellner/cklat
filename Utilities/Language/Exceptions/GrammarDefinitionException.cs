﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class GrammarDefinitionException : Exception
    {
        public GrammarDefinitionException(string message)
            : base(message)
        {
        }

        public GrammarDefinitionException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}
