﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class ParserException : LanguageException
    {
        public ParserException(string message, AElement element) : base(message, element) { }
    }
}
