﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class ExecutionException : LanguageException
    {
        public ExecutionException(string message, AElement element) : base(message, element) { }

        public ExecutionException(string message, AElement element, Exception innerException) 
            : base(message, element, innerException) { }
    }
}
