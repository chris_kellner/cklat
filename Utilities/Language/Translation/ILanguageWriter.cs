﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Translation
{
    public interface ILanguageWriter
    {
        void Write(ASTNode node);
    }
}
