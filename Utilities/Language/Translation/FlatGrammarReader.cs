﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Evaluator.Nodes;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Language.Samples;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CKLib.Utilities.Language.Translation
{
    public class FlatGrammarReader : IGrammarReader, IDisposable
    {
        #region Inner Classes
        private class ParsedGrammar : Grammar
        {
            protected override void DefineTokens()
            {
            }

            protected override void DefinePatterns()
            {
            }
        }

        private class TerminalNodeEvaluator : INodeEvaluator
        {
            public const string ROLE_NAME = "name";
            public const string ROLE_EXPRESSION = "expression";

            public TerminalNodeEvaluator()
            {
            }

            public TerminalNodeEvaluator(bool nonParsed)
                : this()
            {
                NonParsed = nonParsed;
            }

            public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
            {
                var nameNode = tree.FindNodeByRole(ROLE_NAME, 0);
                var valueNode = tree.FindNodeByRole(ROLE_EXPRESSION, 0);

                ScriptVariable @this = evaluator.Context.GetVariable(ScriptContext.RESERVED_NAME_THIS);
                Grammar grammar = @this.Value as Grammar;

                var name = nameNode.Value;
                Terminal rslt = null;
                switch (valueNode.TokenType.Name)
                {
                    case "regex":
                        rslt = new Terminal(grammar, name, MatchType.Regex,
                            evaluator.Evaluate<string>(valueNode))
                            {
                                NonParsed = NonParsed
                            };
                        break;
                    case "group":
                        rslt = new Terminal(grammar)
                        {
                            Name = name,
                            MatchType = MatchType.Group,
                            SubTokens = new List<Terminal>(
                                evaluator.Evaluate<IEnumerable<AGrammarElement>>(valueNode)
                                .Cast<Terminal>()),
                            NonParsed = NonParsed
                        };
                        break;
                    default:
                        rslt = new Terminal(grammar, name, MatchType.Literal,
                            evaluator.Evaluate<string>(valueNode))
                            {
                                NonParsed = NonParsed
                            };
                        break;
                }

                return new EvaluationResult(rslt);
            }

            public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
            {
                throw new NotSupportedException();
            }

            public bool NonParsed { get; set; }

            public bool CanAssign
            {
                get { return false; }
            }

            public bool HasValue
            {
                get { return true; }
            }
        }

        private class NonTerminalNodeEvaluator : INodeEvaluator
        {
            public const string ROLE_NAME = "name";
            public const string ROLE_EXPRESSION = "expression";

            public NonTerminalNodeEvaluator()
            {
            }

            public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
            {
                var nameNode = tree.FindNodeByRole(ROLE_NAME, 0);
                var valueNode = tree.FindNodeByRole(ROLE_EXPRESSION, 0);

                var name = nameNode.Value;
                NonTerminal rslt = null;
                ScriptVariable @this = evaluator.Context.GetVariable(ScriptContext.RESERVED_NAME_THIS);
                Grammar grammar = @this.Value as Grammar;
                switch (valueNode.TokenType.Name)
                {
                    case "group":
                        rslt = new NonTerminal(grammar, name)
                        {
                            SubPatterns = new List<NonTerminal>(evaluator
                                .Evaluate<IEnumerable<AGrammarElement>>(valueNode)
                                .Cast<NonTerminal>())
                        };
                        break;
                    default:
                        rslt = new NonTerminal(grammar, name, evaluator.Evaluate<string>(valueNode));
                        break;
                }

                return new EvaluationResult(rslt);
            }

            public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
            {
                throw new NotSupportedException();
            }

            public bool CanAssign
            {
                get { return false; }
            }

            public bool HasValue
            {
                get { return true; }
            }
        }

        private class RangeNodeEvaluator : INodeEvaluator
        {
            public const string ROLE_OPEN = "p_open";
            public const string ROLE_CLOSE = "p_close";

            public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
            {
                var openNode = tree.FindNodeByRole(ROLE_OPEN, 0);
                var closeNode = tree.FindNodeByRole(ROLE_CLOSE, 0);

                ScriptVariable @this = evaluator.Context.GetVariable(ScriptContext.RESERVED_NAME_THIS);
                Grammar grammar = @this.Value as Grammar;

                grammar.DefineTerminalGroup(openNode.Value, closeNode.Value);
                return EvaluationResult.Next;
            }

            public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
            {
                throw new NotSupportedException();
            }

            public bool CanAssign
            {
                get { return false; }
            }

            public bool HasValue
            {
                get { return true; }
            }
        }

        private class GroupNodeEvaluator : INodeEvaluator
        {
            public const string ROLE_CONTENT = "content";

            public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
            {
                List<AGrammarElement> elements = new List<AGrammarElement>();
                foreach (var node in tree.FindNodesByRole(ROLE_CONTENT))
                {
                    AGrammarElement elem = evaluator.Evaluate<AGrammarElement>(node);
                    elements.Add(elem);
                }

                return new EvaluationResult(elements);
            }

            public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
            {
                throw new NotSupportedException();
            }

            public bool CanAssign
            {
                get { return false; }
            }

            public bool HasValue
            {
                get { return true; }
            }
        }

        private class GrammarNodeEvaluator : INodeEvaluator
        {
            public const string ROLE_META = "meta";
            public const string ROLE_TERMINALS = "terminals";
            public const string ROLE_NONTERMINALS = "nonterminals";
            public const string ROLE_GROUPS = "groups";
            public const string ROLE_EVALUATORS = "evaluators";

            public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
            {
                var metaNode = tree.FindNodeByRole(ROLE_META, true);
                var termNode = tree.FindNodeByRole(ROLE_TERMINALS, true);
                var nontermNode = tree.FindNodeByRole(ROLE_NONTERMINALS, false);
                var groupNode = tree.FindNodeByRole(ROLE_GROUPS, false);
                var evalNode = tree.FindNodeByRole(ROLE_EVALUATORS, false);
                ParsedGrammar grammar = new ParsedGrammar();
                using (var scope = evaluator.Context.NewScope())
                {
                    evaluator.Context.SetLocal(ScriptContext.RESERVED_NAME_THIS, grammar);
                    evaluator.Evaluate(metaNode);
                    IEnumerable<Terminal> terminals = evaluator.Evaluate<IEnumerable<AGrammarElement>>(termNode)
                        .Cast<Terminal>();
                    grammar.RootTokens.AddRange(terminals);

                    if (nontermNode != null)
                    {
                        IEnumerable<NonTerminal> nonterminals = evaluator.Evaluate<IEnumerable<AGrammarElement>>(nontermNode)
                        .Cast<NonTerminal>();
                        grammar.RootPatterns.AddRange(nonterminals);
                    }

                    if (groupNode != null)
                    {
                        evaluator.Evaluate(groupNode);
                    }

                    if (evalNode != null)
                    {
                        
                    }
                }

                return new EvaluationResult(grammar);
            }

            public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
            {
                throw new NotSupportedException();
            }

            public bool CanAssign
            {
                get { return false; }
            }

            public bool HasValue
            {
                get { return true; }
            }
        }

        private class FlatGrammar : Grammar
        {
            protected override void DefineTokens()
            {
                var text = CTokens.Text(this);
                var json = CTokens.CodeBlock(this, "json", "<%Json", "%>", '%');
                var regex = CTokens.String(this, "regex", '/', true);
                regex.Evaluator = new PrimitiveNodeEvaluator(s =>
                {
                    return StringUtils.LanguageUnescape(s.Substring(1, s.Length - 2), false, '/');
                });

                var comment = new Terminal(this, "comment", MatchType.Regex, "(?m)#.*$")
                {
                    NonParsed = true
                };
                var literal = CTokens.Literal(this,
                    text,
                    json,
                    regex,
                    CTokens.Boolean(this),
                    CTokens.Number(this));
                var identifier = CTokens.Identifier(this);
                identifier.Evaluator = new MemberAccessorNodeEvaluator() { CanAssign = true, AccessType = AccessTypes.All };
                var op_assign = new Terminal(this)
                {
                    Name = "op_assign",
                    MatchType = Lex.MatchType.Group,
                    SubTokens = new List<Terminal>()
                    {
                        new Terminal(this, "op_assign_prop", MatchType.Literal, ":"),
                        new Terminal(this, "op_assign_term", MatchType.Literal, "->"),
                        new Terminal(this, "op_assign_nonterm", MatchType.Literal, "=>")
                    }
                };
                var op_brackets = new Terminal(this)
                {
                    Name = "Bracket",
                    MatchType = Lex.MatchType.Group,
                    SubTokens = new List<Terminal>()
                    {
                        new Terminal(this, "Bracket_Open", MatchType.Literal, "["),
                        new Terminal(this, "Bracket_Close", MatchType.Literal, "]")
                    }
                };

                var op_range = new Terminal(this, "op_range", MatchType.Literal, "...");
                var op_comma = new Terminal(this, "op_comma", MatchType.Literal, ",");
                var op_dot = new Terminal(this, "dot", MatchType.Literal, ".");

                var keywords = Terminal.MakeKeywords(this, new Dictionary<string, string>()
                {
                    { "KW_END", "End" }
                });

                RootTokens.Add(text);
                RootTokens.Add(json);
                RootTokens.Add(regex);
                RootTokens.Add(comment);
                RootTokens.Add(literal);
                RootTokens.Add(keywords);
                RootTokens.Add(identifier);
                RootTokens.Add(op_assign);
                RootTokens.Add(op_brackets);
                RootTokens.Add(op_range);
                RootTokens.Add(op_dot);
                RootTokens.Add(op_comma);

                DefineTerminalGroup("Bracket_Open", "Bracket_Close");
            }

            protected override void DefinePatterns()
            {
                var tag = new NonTerminal(this, "tag")
                {
                    NonExecutable = true,
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "tag_close", "Bracket_Open KW_END Bracket_Close"),
                        new NonTerminal(this, "tag_open", "Bracket_Open ('name'Identifier) Bracket_Close")
                    }
                };

                var range = new NonTerminal(this, "range", "('p_open'Identifier) op_range ('p_close'Identifier)")
                {
                    Evaluator = new RangeNodeEvaluator()
                };

                var assignment = new NonTerminal(this, "assignment")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "assignment_prop", "('lvalue|member'Identifier) op_assign_prop ('rvalue'Literal|Identifier)"){
                            Evaluator = new AssignmentNodeEvaluator()
                        },
                        new NonTerminal(this, "assignment_nonparsed", "dot ('name'Identifier) op_assign_term ('expression'(String|regex|group))"){
                            Evaluator = new TerminalNodeEvaluator(true)
                        },
                        new NonTerminal(this, "assignment_parsed", "('name'Identifier) op_assign_term ('expression'(String|regex|group))"){
                            Evaluator = new TerminalNodeEvaluator(false)
                        },
                        new NonTerminal(this, "assignment_nonterm", "('name'Identifier) op_assign_nonterm ('expression'(String|group))"){
                            Evaluator = new NonTerminalNodeEvaluator()
                        }
                    }
                };

                var useList = new NonTerminal(this, "useList", "Bracket_Open ('item'String) (op_comma ('item'Identifier))* Bracket_Close")
                {
                    Evaluator = new ObjectInitializerNodeEvaluator()
                    {
                        HasValue = true,
                        InitializerType = InitializerTypes.Array
                    }
                };

                var evaluator = new NonTerminal(this, "evaluator", "useList String json");

                //var value = new NonTerminal(this, "rvalue", "(?<=(op_assign)(String|regex|group)");
                var group = new NonTerminal(this, "group", "Bracket_Open ('content'assignment)+ Bracket_Close")
                {
                    Evaluator = new GroupNodeEvaluator()
                };

                var section = new NonTerminal(this, "section")
                {
                    SubPatterns = new List<NonTerminal>()
                    {
                        new NonTerminal(this, "section_meta", "('tag'tag_open) ('content'assignment_prop)* tag_close"){
                            Evaluator = new RootNodeEvaluator() { Filter = "content" }
                        },
                        new NonTerminal(this, "section_term", "('tag'tag_open) ('content'assignment_nonparsed|assignment_parsed)* tag_close"){
                            Evaluator = new GroupNodeEvaluator()
                        },
                        new NonTerminal(this, "section_nonterm", "('tag'tag_open) ('content'assignment_nonterm)* tag_close"){
                            Evaluator = new GroupNodeEvaluator()
                        },
                        new NonTerminal(this, "section_range", "('tag'tag_open) ('content'range)* tag_close"){
                            Evaluator = new RootNodeEvaluator() { Filter = "content" }
                        },
                        new NonTerminal(this, "section_eval", "('tag'tag_open) ('item'evaluator)* tag_close"){
                            Evaluator = new ObjectInitializerNodeEvaluator(){
                                InitializerType = InitializerTypes.Array,
                                HasValue = true
                            }
                        }
                    }
                };

                var grammar = new NonTerminal(this, "grammar",
                    "('meta'section_meta) ('terminals'section_term) ('nonterminals'section_nonterm)?" +
                    " ('groups'section_range)? ('evaluators'section_eval)?")
                {
                    Evaluator = new GrammarNodeEvaluator()
                };

                RootPatterns.Add(tag);
                RootPatterns.Add(range);
                RootPatterns.Add(assignment);
                RootPatterns.Add(group);
                RootPatterns.Add(useList);
                RootPatterns.Add(evaluator);
                RootPatterns.Add(section);
                RootPatterns.Add(grammar);

                DefineTerminalGroup("tag_open", "tag_close");
            }
        }
        #endregion

        private Grammar m_Grammar;

        public FlatGrammarReader()
            : this(false)
        {
        }

        public FlatGrammarReader(bool debugging)
            : base()
        {
            m_Grammar = CreateFlatGrammar(debugging);
        }

        public static Grammar CreateFlatGrammar(bool enableDebugging)
        {
            var grammar = new FlatGrammar();
            grammar.EnableParserDebugging = enableDebugging;
            grammar.AotCompile();
            return grammar;
        }

        public Grammar ReadGrammar(Stream inputStream)
        {
            using (var reader = new StreamReader(inputStream))
            {
                return m_Grammar.Evaluate<Grammar>(reader.ReadToEnd());
            }
        }

        public Grammar Grammar
        {
            get
            {
                return m_Grammar;
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~FlatGrammarReader()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_Grammar.Dispose();
            }
        }
        #endregion
    }
}
