﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Translation
{
    public abstract class AGrammarWriter : IGrammarWriter, IDisposable
    {
        private const string SECTION_META = "Meta";
        private const string SECTION_TERMINAL = "Terminals";
        private const string SECTION_NON_TERMINAL = "NonTerminals";
        private const string SECTION_GROUPS = "Groups";
        private const string SECTION_EVALUATORS = "Evaluators";

        protected Stream m_OutputStream;

        private Dictionary<INodeEvaluator, HashSet<AGrammarElement>> m_EvaluatorList;

        public AGrammarWriter(Stream outputStream)
        {
            if (!outputStream.CanWrite)
            {
                throw new ArgumentException("Stream is not writable", "outputStream");
            }

            m_OutputStream = outputStream;
        }

        public void WriteGrammar(Grammar grammar)
        {
            Reset();
            WriteMeta(grammar);
            WriteTerminals(grammar);
            WriteNonTerminals(grammar);
            WriteGroups(grammar);
            WriteEvaluators(grammar);
        }

        protected virtual void Reset()
        {
            m_EvaluatorList = new Dictionary<INodeEvaluator, HashSet<AGrammarElement>>();
        }

        private void MarkEvaluator(INodeEvaluator evaluator, AGrammarElement element)
        {
            if (evaluator != null)
            {
                HashSet<AGrammarElement> list;
                if (!m_EvaluatorList.TryGetValue(evaluator, out list))
                {
                    list = new HashSet<AGrammarElement>();
                    m_EvaluatorList[evaluator] = list;
                }

                list.Add(element);
            }
        }

        private void WriteMeta(Grammar grammar)
        {
            WriteSectionStart(SECTION_META);
            WriteMetaData(grammar);
            WriteSectionEnd();
        }

        private void WriteTerminals(Grammar grammar)
        {
            // Write header
            WriteSectionStart(SECTION_TERMINAL);

            // Write each terminal
            foreach (var terminal in grammar.RootTokens)
            {
                WriteTerminal(terminal);
            }

            WriteSectionEnd();
        }

        private void WriteNonTerminals(Grammar grammar)
        {
            // Write header
            WriteSectionStart(SECTION_NON_TERMINAL);

            // Write each non terminal
            foreach (var nonTerminal in grammar.RootPatterns)
            {
                WriteNonTerminal(nonTerminal);
            }

            WriteSectionEnd();
        }

        private void WriteGroups(Grammar grammar)
        {
            // Write Header
            WriteSectionStart(SECTION_GROUPS);

            // Write each group
            foreach (var pair in grammar.GetTerminalGroups())
            {
                WriteGroup(pair.Key, pair.Value);
            }

            WriteSectionEnd();
        }

        private void WriteEvaluators(Grammar grammar)
        {
            // Write Header
            WriteSectionStart(SECTION_EVALUATORS);

            // Write each evaluator
            foreach (var pair in m_EvaluatorList)
            {
                WriteEvaluator(pair.Key, pair.Value);
            }

            WriteSectionEnd();
        }

        protected abstract void WriteMetaData(Grammar grammar);

        protected abstract void WriteEvaluator(INodeEvaluator evaluator, IEnumerable<AGrammarElement> usedBy);

        protected virtual void WriteNonTerminal(NonTerminal nonterminal)
        {
            MarkEvaluator(nonterminal.Evaluator, nonterminal);
        }

        protected virtual void WriteTerminal(Terminal terminal)
        {
            MarkEvaluator(terminal.Evaluator, terminal);
        }

        protected abstract void WriteGroup(IGrammarElement start, IGrammarElement end);

        protected abstract void WriteSectionStart(string sectionName);

        protected abstract void WriteSectionEnd();

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~AGrammarWriter()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_OutputStream.Dispose();
            }
        }
        #endregion
    }
}
