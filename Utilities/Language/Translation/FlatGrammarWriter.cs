﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using CKLib.Utilities.Serialization.Json;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CKLib.Utilities.Language.Translation
{
    public class FlatGrammarWriter : AGrammarWriter
    {
        private const string PARSE_IGNORE_TOKEN = ".";

        private static Dictionary<RegexOptions, string> m_InlineOptions = new Dictionary<RegexOptions, string>()
        {
            { RegexOptions.Multiline, "m" },
            { RegexOptions.Singleline, "s" },
            { RegexOptions.ExplicitCapture, "n" },
            { RegexOptions.IgnoreCase, "i" },
            { RegexOptions.IgnorePatternWhitespace, "x" }
        };

        private StreamWriter m_StreamWriter;
        private int m_Indent;

        public FlatGrammarWriter(Stream outputStream)
            : base(outputStream)
        {
            m_StreamWriter = new StreamWriter(outputStream);
        }

        protected override void Reset()
        {
            base.Reset();
            m_Indent = 0;
        }

        protected override void WriteMetaData(Grammar grammar)
        {
            WriteLine("Name: " + StringUtils.ToStringLiteral(grammar.Name));
            WriteLine("Version: " + StringUtils.ToStringLiteral(grammar.Version.ToString()));
            WriteLine("CanEvaluate: " + grammar.CanEvaluate);
            WriteLine("CanCompile: " + grammar.CanCompile);
        }

        protected override void WriteNonTerminal(NonTerminal nonterminal)
        {
            base.WriteNonTerminal(nonterminal);
            if (nonterminal.Pattern != null)
            {
                WriteLine(nonterminal.Name + " => \"" + ToLiteral(nonterminal.Pattern, false, '\"') + "\"");
            }
            else
            {
                WriteLine(nonterminal.Name + " => [");
                if (nonterminal.SubPatterns.Count > 0)
                {
                    m_Indent++;
                    for (int i = 0; i < nonterminal.SubPatterns.Count - 1; i++)
                    {
                        // Write them with commas between
                        WriteNonTerminal(nonterminal.SubPatterns[i]);
                        //WriteLine(",");
                    }

                    // Write the last one
                    WriteNonTerminal(nonterminal.SubPatterns[nonterminal.SubPatterns.Count - 1]);
                    m_Indent--;
                }
                WriteLine("]");
            }
        }

        protected override void WriteTerminal(Terminal terminal)
        {
            base.WriteTerminal(terminal);
            if (terminal.Name.StartsWith("<"))
            {
                // Internal token
                return;
            }

            switch (terminal.MatchType)
            {
                case MatchType.Literal:
                    WriteLine(terminal.Name + " -> \"" + ToLiteral(terminal.Pattern, false, '\"') + "\"");
                    break;
                case MatchType.Regex:
                    WriteRegexTerminal(terminal);
                    break;
                case MatchType.Group:
                    {
                        WriteLine(terminal.Name + " -> [");
                        if (terminal.SubTokens.Count > 0)
                        {
                            m_Indent++;
                            for (int i = 0; i < terminal.SubTokens.Count - 1; i++)
                            {
                                // Write them with commas between
                                WriteTerminal(terminal.SubTokens[i]);
                                //WriteLine(",");
                            }

                            // Write the last one
                            WriteTerminal(terminal.SubTokens[terminal.SubTokens.Count - 1]);
                            m_Indent--;
                        }
                        WriteLine("]");
                    }
                    break;
                default:
                    break;
            }
        }

        private static string ToLiteral(string input, bool ignoreWhitespace, char quoteChar)
        {
            return StringUtils.LanguageEscape(input, ignoreWhitespace, quoteChar);
        }

        public static string GetRegexOptionsInline(RegexOptions opt)
        {
            string options = string.Empty;
            if (opt != RegexOptions.None)
            {
                foreach (var pair in m_InlineOptions)
                {
                    if ((opt & pair.Key) == pair.Key)
                    {
                        options += pair.Value;
                    }
                }
            }

            if (options.Length > 0)
            {
                options = "(?" + options + ")";
            }

            return options;
        }

        public static bool DoesRegexIgnoreWhitespace(string regex, RegexOptions options)
        {
            if ((options & RegexOptions.IgnorePatternWhitespace) == RegexOptions.IgnorePatternWhitespace)
            {
                return true;
            }

            var match = Regex.Match(regex, @"^\(\?([mnxis-])\)");
            if(match.Success)
            {
                if (match.Value.Contains("x"))
                {
                    return true;
                }
            }

            return false;
        }

        private void WriteRegexTerminal(Terminal terminal)
        {
            string options = GetRegexOptionsInline(terminal.RegexOptions);
            bool ignoreWhitespace = DoesRegexIgnoreWhitespace(terminal.Pattern, terminal.RegexOptions);
            string fmt = string.Format("{0}{1} -> /{2}{3}/",
                terminal.NonParsed ? PARSE_IGNORE_TOKEN : string.Empty,
                terminal.Name, options, ToLiteral(terminal.Pattern, ignoreWhitespace, '/'));
            WriteLine(fmt);
        }

        protected override void WriteGroup(IGrammarElement start, IGrammarElement end)
        {
            WriteLine(string.Format("{0} ... {1}", start.Name, end.Name));
        }

        protected override void WriteEvaluator(INodeEvaluator evaluator, IEnumerable<AGrammarElement> usedBy)
        {
            var used = "[" + string.Join(", ", usedBy.Select(g => JsonUtils.JsonEncode(g.Name)).ToArray()) + "]";
            WriteLine(used);
            WriteLine(JsonUtils.JsonEncode(evaluator.GetType().ToString()));
            var json = JsonFormatter.Stringify(evaluator);
            json = json + Environment.NewLine;
            json = StringUtils.ToCodeBlock(json, "<%Json", "%>", '%');
            WriteLine(json);
            WriteLine();
        }

        private void WriteProperty(string name, object value)
        {
            WriteLine(name + ": " + Convert.ToString(value));
        }

        protected override void WriteSectionStart(string sectionName)
        {
            WriteLine();
            WriteLine("[" + sectionName + "]");
        }

        protected override void WriteSectionEnd()
        {
            WriteLine("[End]");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_StreamWriter.Dispose();
            }
        }

        private void WriteLine()
        {
            m_StreamWriter.WriteLine();
        }

        private void WriteLine(string line)
        {
            m_StreamWriter.WriteLine(new string('\t', m_Indent) + line);
        }

        private void Write(string line, bool indented)
        {
            if (indented)
            {
                m_StreamWriter.Write(new string('\t', m_Indent) + line);
            }
            else
            {
                m_StreamWriter.Write(line);
            }
        }
    }
}
