﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class LanguagePattern : AElement
    {
        public LanguagePattern(NonTerminal def)
            : base(def)
        {
        }

        public LanguagePattern(NonTerminal def, IList<AElement> value)
            : this(def)
        {
            Value = value;
        }

        public IList<AElement> Value { get; set; }

        public override int Position
        {
            get
            {
                if (Value != null && Value.Count > 0)
                {
                    return Value.First().Position;
                }

                return -1;
            }
        }

        public override int Line
        {
            get
            {
                if (Value != null && Value.Count > 0)
                {
                    return Value.First().Line;
                }

                return -1;
            }
        }

        public override int Column
        {
            get
            {
                if (Value != null && Value.Count > 0)
                {
                    return Value.First().Column;
                }

                return -1;
            }
        }


        public override int Length
        {
            get
            {
                int first = Position;
                int last = Position;
                if (Value != null && Value.Count > 0)
                {
                    var node = Value.Last();
                    last = node.Position + node.Length;
                }

                return last - first;
            }
        }

        public override IEnumerator<AElement> GetEnumerator()
        {
            if (Value != null)
            {
                return Value.GetEnumerator();
            }

            return base.GetEnumerator();
        }

        public override string ToString()
        {
            return string.Format("P{{ \"{0}\": [{1}] }}",
                TokenType.Name, String.Join(", ", Value.Select(v=>v.ToString()).ToArray()));
        }
    }
}
