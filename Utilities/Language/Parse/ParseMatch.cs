﻿using CKLib.Utilities.Language.Lex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class ParseMatch
    {
        public static readonly ParseMatch NoMatch = new ParseMatch();
        public static readonly ParseMatch NonConsummateMatch = new ParseMatch(null, 0, null);

        private Dictionary<string, ParseGroup> m_NamedGroups = new Dictionary<string, ParseGroup>();
        private List<FailureReason> m_Reasons;

        private ParseMatch()
        {
        }

        public ParseMatch(AElement element, int elementsConsumed, ParseGroup group)
        {
            Element = element;
            Success = true;
            ElementsConsumed = elementsConsumed;
            if (group != null)
            {
                SetGroup(group);
            }
        }

        private ParseMatch(NonTerminal parent, bool consume,  IEnumerable<ParseMatch> others)
            : this(
            new LanguagePattern(parent, Merge(consume, others)),
            others.Sum(o=>o.ElementsConsumed),
            null)
        {
            foreach (var other in others)
            {
                MergeGroups(other);
            }
        }

        private void MergeGroups(ParseMatch other)
        {
            foreach (var group in other.m_NamedGroups)
            {
                ParseGroup grp;
                if (m_NamedGroups.TryGetValue(group.Key, out grp))
                {
                    foreach (var elem in group.Value.Elements)
                    {
                        grp.Elements.Add(elem);
                    }
                }
                else
                {
                    m_NamedGroups[group.Key] = group.Value;
                }
            }
        }

        public static ParseMatch Fail(FailureReason reason)
        {
            ParseMatch match = new ParseMatch();
            match.AddReason(reason);
            return match;
        }


        public void AddReasons(IEnumerable<FailureReason> reason)
        {
            if (reason != null)
            {
                if (m_Reasons == null)
                {
                    m_Reasons = new List<FailureReason>();
                }

                m_Reasons.AddRange(reason);
            }
        }

        public void AddReason(FailureReason reason)
        {
            if (reason != null)
            {
                if (m_Reasons == null)
                {
                    m_Reasons = new List<FailureReason>();
                }

                m_Reasons.Add(reason);
            }
        }

        public static ParseMatch Aggregate(NonTerminal parent, bool consume, params ParseMatch[] others)
        {
            return Aggregate(parent, consume, (IEnumerable<ParseMatch>)others);
        }

        public static ParseMatch Aggregate(NonTerminal parent, bool consume, IEnumerable<ParseMatch> others)
        {
            if (others.Any() && others.All(m => m != ParseMatch.NoMatch))
            {
                return new ParseMatch(parent, consume, others);
            }

            return ParseMatch.NoMatch;
        }

        private static List<AElement> Merge(bool consume, IEnumerable<ParseMatch> matches)
        {
            List<AElement> elements = new List<AElement>();
            foreach(var match in matches)
            {
                if (!consume)
                {
                    elements.Add(match.Element);
                }
                else
                {
                    elements.AddRange(ExtractElements(match.Element));
                }
            }

            return elements;
        }

        private static IEnumerable<AElement> ExtractElements(AElement element)
        {
            if (element is Token)
            {
                yield return element;
            }
            else if (element is LanguagePattern)
            {
                foreach (var item in ((LanguagePattern)element).Value)
                {
                    yield return item;
                }
            }
        }

        public void Accept()
        {
            foreach (var group in m_NamedGroups)
            {
                foreach (var elem in group.Value.Elements)
                {
                    elem.AddRole(group.Key);
                }
            }
        }

        public void SetGroupName(IEnumerable<string> names)
        {
            if (names.Any())
            {
                var grp = new ParseGroup(names, Element);
                SetGroup(grp);
            }
        }

        private void SetGroup(ParseGroup grp)
        {
            foreach (var name in grp.GroupNames)
            {
                m_NamedGroups[name] = grp;
            }
        }

        public bool Success { get; private set; }
        public AElement Element { get; private set; }
        public int ElementsConsumed { get; private set; }
        public int Index { get; set; }

        public IDictionary<string, ParseGroup> Groups
        {
            get
            {
                return m_NamedGroups;
            }
        }

        public IEnumerable<FailureReason> FailureReasons
        {
            get
            {
                return m_Reasons;
            }
        }

        public static bool operator ==(ParseMatch left, ParseMatch right)
        {
            if (object.ReferenceEquals(null, left))
            {
                return object.ReferenceEquals(null, right);
            }
            else if (object.ReferenceEquals(null, right))
            {
                return false;
            }
            else if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else
            {
                return left.Success == false && right.Success == false ||
                    left.ElementsConsumed == 0 && right.ElementsConsumed == 0;
            }
        }

        public static bool operator !=(ParseMatch left, ParseMatch right)
        {
            return !(left == right);
        }
    }
}
