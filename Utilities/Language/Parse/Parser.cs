﻿using CKLib.Utilities.Language.Exceptions;
using CKLib.Utilities.Language.Lex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class Parser
    {
        public Parser(Grammar grammar)
        {
            Grammar = grammar;
        }

        public Grammar Grammar { get; private set; }
        public TimeSpan LastParseTime { get; private set; }

        public IEnumerable<FailureReason> LastErrors { get; private set; }

        public ElementList Parse(IEnumerable<AElement> tokens)
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            List<FailureReason> reasons = new List<FailureReason>();
            var tElements = ParseInternal(tokens, reasons);
            LastErrors = reasons;
            sw.Stop();
            LastParseTime = sw.Elapsed;
            return tElements;
        }

        private ElementList ParseInternal(IEnumerable<AElement> tokens, List<FailureReason> reasons)
        {
            var tElements = new ElementList(tokens.Where(t => !t.NonParsed));
            var groups = Grammar.GetTerminalGroups();
            for (int i = 0; i < tElements.Count; i++)
            {
                var pair = groups.FirstOrDefault(p => tElements[i].IsType(p.Key));
                if (pair.Key != null)
                {
                    int count = ParseGroup(tElements, i, pair.Key, pair.Value, reasons);
                    i += count;
                }
            }

            bool foundOne;
            do
            {
                foundOne = false;
                for (int i = 0; i < Grammar.RootPatterns.Count; i++)
                {
                    IEnumerable<FailureReason> errors;
                    while (Grammar.RootPatterns[i].Find(tElements, out errors))
                    {
                        foundOne = true;
                    }

                    if (errors != null)
                    {
                        reasons.AddRange(errors);
                    }
                }
            } while (foundOne);
            return tElements;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokens"></param>
        /// <param name="startIndex"></param>
        /// <param name="openToken"></param>
        /// <param name="closeToken"></param>
        /// <returns>The number of elements processed for the specified group.</returns>
        private int ParseGroup(ElementList tokens, int startIndex, IGrammarElement openToken, IGrammarElement closeToken, List<FailureReason> reasons)
        {
            int openIndex = startIndex;
            int closeIndex = -1;
            int count = 1;
            for (int i = startIndex + 1; i < tokens.Count; i++)
            {
                if (tokens[i].IsType(closeToken))
                {
                    count--;
                }

                if (count < 0)
                {
                    throw new ParserException("Too many close tokens", tokens[i]);
                }
                else if (count == 0)
                {
                    closeIndex = i;
                    if (closeIndex > openIndex)
                    {
                        var subset = tokens.Slice(openIndex + 1, closeIndex);
                        var parsed = ParseInternal(subset, reasons);
                        tokens.ReplaceRange(openIndex + 1, subset.Length, parsed);
                        return parsed.Count + 1;
                    }
                }

                if (tokens[i].IsType(openToken))
                {
                    count++;
                }
            }

            if (count > 0)
            {
                throw new UnclosedGroupException(tokens[startIndex]);
            }

            return 0;
        }
    }
}
