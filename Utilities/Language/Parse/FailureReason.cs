﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class FailureReason
    {
        public FailureReason(NonTerminal pattern, IGrammarElement expected, AElement actual, int index)
        {
            Pattern = pattern;
            Expected = expected;
            Actual = actual;
            PatternIndex = index;
        }

        public NonTerminal Pattern { get; private set; }
        public IGrammarElement Expected { get; private set; }
        public AElement Actual { get; private set; }
        public int PatternIndex { get; private set; }

        public override string ToString()
        {
            return string.Format("Pattern: {0} -> Expected: {1}, but received: {2}[{3}:{4}]", Pattern.Name,
                Expected.Name, Actual.TokenType.Name, Actual.Line, Actual.Column);
        }
    }
}
