﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Evaluator;

namespace CKLib.Utilities.Language.Parse
{
    public class PatternRule
    {
        #region Inner Classes
        private class RuleGrammar : Grammar
        {
            public const string TN_OP_OR = "Op_Or",
                TN_OP_AND = "Op_And",
                TN_OP_REPEAT = "Op_Repeat",
                TN_OP_GROUP_START = "Op_Group_Start",
                TN_OP_GROUP_START_PLAIN = "Op_Group_Start_Plain",
                TN_OP_GROUP_START_LA = "Op_Group_Start_Look_Ahead",
                TN_OP_GROUP_START_LA_N = "Op_Group_Start_Look_Ahead_Neg",
                TN_OP_GROUP_START_LB = "Op_Group_Start_Look_Behind",
                TN_OP_GROUP_START_LB_N = "Op_Group_Start_Look_Behind_Neg",
                TN_OP_GROUP_END = "Op_Group_End",
                TN_OP_ANY = "Op_Any",
                TN_GROUP_NAME = "Group_Name";

            protected override void DefineTokens()
            {
                var @string = CTokens.String(this, TN_GROUP_NAME, '\'', false);
                var operation = new Terminal(this)
                {
                    Name = "Operation",
                    MatchType = MatchType.Group,
                    SubTokens = new List<Terminal>()
                    {
                        new Terminal(this, TN_OP_OR, MatchType.Literal, "|"),
                        new Terminal(this, TN_OP_AND, MatchType.Regex, "[, ]"),
                        new Terminal(this, TN_OP_ANY, MatchType.Literal, "."),
                        new Terminal(this)
                        {
                            Name = TN_OP_GROUP_START,
                            MatchType = MatchType.Group,
                            SubTokens = new List<Terminal>()
                            {
                                new Terminal(this, TN_OP_GROUP_START_LA, MatchType.Literal, "(?="),
                                new Terminal(this, TN_OP_GROUP_START_LA_N, MatchType.Literal, "(?!"),
                                new Terminal(this, TN_OP_GROUP_START_LB, MatchType.Literal, "(?<="),
                                new Terminal(this, TN_OP_GROUP_START_LB_N, MatchType.Literal, "(?<!"),
                                new Terminal(this, TN_OP_GROUP_START_PLAIN, MatchType.Literal, "("),
                            }
                        },
                        new Terminal(this, TN_OP_GROUP_END, MatchType.Literal, ")"),
                        new Terminal(this, TN_OP_REPEAT, MatchType.Regex, @"[*+?]"),
                    }
                };

                var identifier = CTokens.Identifier(this);
                RootTokens.Add(@string);
                RootTokens.Add(operation);
                RootTokens.Add(identifier);
            }

            public static IMatchRule Reduce(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                while (GroupMatchRule.Consume(grammar, rules)) { }
                while (ProcessModifiers(grammar, rules)) { }
                while (ExactMatchRule.Consume(grammar, rules)) { }
                while (MultiMatchRule.Consume(grammar, rules)) { }
                while (OptionMatchRule.Consume(grammar, rules)) { }
                IMatchRule rule = rules.First();
                int idx = 0;
                NumberRecursive((AMatchRule)rule, ref idx);
                return rule;
            }

            private static void NumberRecursive(AMatchRule matchRule, ref int cIndex)
            {
                if (matchRule is ExactMatchRule)
                {
                    matchRule.MatchIndex = cIndex;
                }
                else if (matchRule is OptionMatchRule)
                {
                    foreach (var rule in matchRule)
                    {
                        NumberRecursive((AMatchRule)rule, ref cIndex);
                    }
                }
                else if (matchRule is MultiMatchRule)
                {
                    foreach (var rule in matchRule)
                    {
                        NumberRecursive((AMatchRule)rule, ref cIndex);
                        cIndex++;
                    }
                }
            }

            private static bool ProcessModifiers(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                for (int i = 1; i < rules.Count; i++)
                {
                    var rule = rules[i] as UnmatchedRule;
                    if (rule != null)
                    {
                        if (rule.Name == TN_OP_REPEAT)
                        {
                            switch (rule.Token.Value)
                            {
                                case "*":
                                    rules[i - 1].Optional = true;
                                    rules[i - 1].AllowMultiple = true;
                                    break;
                                case "+":
                                    rules[i - 1].AllowMultiple = true;
                                    break;
                                case "?":
                                    rules[i - 1].Optional = true;
                                    break;
                            }

                            rules.ReplaceRange(i, 1, null);
                        }
                    }
                }

                return false;
            }

            protected override void DefinePatterns()
            {
            }
        }

        private interface IMatchRule : IEnumerable<IMatchRule>
        {
            ParseMatch Matches(ElementList tokens, int index);

            bool AllowMultiple { get; set; }

            bool Optional { get; set; }

            bool HasPriority { get; set; }
            bool IsNegative { get; set; }
            bool RunBackwards { get; set; }
            bool EnableDebugging { get; set; }

            IMatchRule LookAhead { get; set; }
            IMatchRule LookBehind { get; set; }
            IMatchRule NegativeLookAhead { get; set; }
            IMatchRule NegativeLookBehind { get; set; }

            IEnumerable<string> GetGroupNames();
            void AddGroupName(string groupName);
            bool HasGroupNames { get; }
        }

        private class UnmatchedRule : AMatchRule
        {
            public Token Token { get; private set; }

            public string Name { get { return Token.TokenType.Name; } }

            public UnmatchedRule(NonTerminal parent, Token t)
                : base(parent)
            {
                Token = t;
            }

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                throw new NotImplementedException();
            }

            public override string ToString()
            {
                return Name;
            }
        }

        private abstract class AMatchRule : IMatchRule
        {
            private HashSet<string> m_GroupNames = new HashSet<string>();

            public AMatchRule(NonTerminal parent)
            {
                Parent = parent;
                MatchIndex = -1;
            }

            public NonTerminal Parent { get; private set; }

            public void AddGroupName(string groupName)
            {
                m_GroupNames.Add(groupName);
            }

            public IEnumerable<string> GetGroupNames()
            {
                return m_GroupNames;
            }

            public bool HasGroupNames
            {
                get
                {
                    return m_GroupNames.Count > 0;
                }
            }

            public abstract ParseMatch Matches(ElementList tokens, int index);

            protected virtual bool CheckLookahead(ElementList tokens, int index)
            {
                if (LookAhead != null)
                {
                    var match = LookAhead.Matches(tokens, index);
                    return match.Success;
                }
                else if (NegativeLookAhead != null)
                {
                    var match = NegativeLookAhead.Matches(tokens, index);
                    return !match.Success;
                }

                return true;
            }

            protected virtual bool CheckLookbehind(ElementList tokens, int index)
            {
                if (LookBehind != null)
                {
                    var match = LookBehind.Matches(tokens, index);
                    return match.Success;
                }
                else if (NegativeLookBehind != null)
                {
                    var match = NegativeLookBehind.Matches(tokens, index);
                    return !match.Success;
                }

                return true;
            }

            public bool AllowMultiple { get; set; }
            public bool Optional { get; set; }
            public NonTerminal LanguagePatternDefinition { get; set; }
            public bool HasPriority { get; set; }
            public bool IsNegative { get; set; }
            public bool RunBackwards { get; set; }
            public int MatchIndex { get; set; }
            public bool EnableDebugging { get; set; }

            public virtual IEnumerator<IMatchRule> GetEnumerator()
            {
                yield break;
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public virtual void CopyTo(IMatchRule other)
            {
                other.AllowMultiple = this.AllowMultiple;
                other.HasPriority = this.HasPriority;
                other.IsNegative = this.IsNegative;
                other.LookAhead = this.LookAhead;
                other.LookBehind = this.LookBehind;
                other.NegativeLookAhead = this.NegativeLookAhead;
                other.NegativeLookBehind = this.NegativeLookBehind;
                other.Optional = this.Optional;
                other.RunBackwards = this.RunBackwards;
                other.EnableDebugging = this.EnableDebugging;
                foreach (var grp in m_GroupNames)
                {
                    other.AddGroupName(grp);
                }
            }

            public IMatchRule LookAhead { get; set; }
            public IMatchRule NegativeLookAhead { get; set; }

            public IMatchRule LookBehind { get; set; }
            public IMatchRule NegativeLookBehind { get; set; }
        }

        private abstract class AMultiMatchRule : AMatchRule, IMultiMatchRule
        {
            public AMultiMatchRule(NonTerminal parent)
                : base(parent)
            {
                Rules = new List<IMatchRule>();
            }

            public bool PushRule(IMatchRule rule)
            {
                Rules.Add(rule);
                return true;
            }

            public IMatchRule PopRule()
            {
                if (Rules.Count > 0)
                {
                    IMatchRule rule = Rules[Rules.Count - 1];
                    Rules.RemoveAt(Rules.Count - 1);
                    return rule;
                }

                return null;
            }

            public IList<IMatchRule> Rules { get; set; }

            public override IEnumerator<IMatchRule> GetEnumerator()
            {
                return Rules.GetEnumerator();
            }

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                throw new NotImplementedException();
            }
        }

        private interface IMultiMatchRule : IMatchRule
        {
            IList<IMatchRule> Rules { get; }

            bool PushRule(IMatchRule rule);

            IMatchRule PopRule();
        }

        private class ExactMatchRule : AMatchRule
        {
            public ExactMatchRule(NonTerminal parent, IGrammarElement element)
                : base(parent)
            {
                m_Element = element;
            }

            private IGrammarElement m_Element;

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                if (!CheckLookbehind(tokens, index - 1)) { return ParseMatch.NoMatch; }

                List<AElement> elements = new List<AElement>();
                while (index >= 0 && index < tokens.Count)
                {
                    if (m_Element == null && CheckLookahead(tokens, index + 1))
                    {
                        break;
                    }
                    else
                    {
                        bool isType = tokens[index].IsType(m_Element);
                        bool collect = isType ^ IsNegative;
                        if (!collect)
                        {
                            break;
                        }
                    }

                    elements.Add(tokens[index]);
                    if (!AllowMultiple)
                    {
                        break;
                    }

                    if (RunBackwards)
                    {
                        index--;
                    }
                    else
                    {
                        index++;
                    }
                }

                if (elements.Count > 0)
                {
                    if (CheckLookahead(tokens, index + 1))
                    {
                        ParseGroup grp = null;
                        if (HasGroupNames)
                        {
                            grp = new ParseGroup(GetGroupNames(), elements);
                        }

                        return new ParseMatch(new LanguagePattern(Parent, elements), elements.Count, grp);
                    }
                }

                if (EnableDebugging && index >= 0 && index < tokens.Count)
                {
                    return ParseMatch.Fail(new FailureReason(Parent, m_Element, tokens[index], MatchIndex));
                }

                return ParseMatch.NoMatch;
            }

            public override string ToString()
            {
                return m_Element.Name + (AllowMultiple ? "*" : string.Empty);
            }

            public static bool Consume(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                for (int i = 0; i < rules.Count; i++)
                {
                    UnmatchedRule rule = rules[i] as UnmatchedRule;
                    if (rule != null)
                    {
                        if (rule.Name == CTokens.TN_IDENTIFIER)
                        {
                            var element = grammar.FindGrammarElement(rule.Token.Value);
                            var match = new ExactMatchRule(rule.Parent, element);
                            rule.CopyTo(match);
                            rules.ReplaceRange(i, 1, match);
                        }
                        else if (rule.Name == RuleGrammar.TN_OP_ANY)
                        {
                            var match = new ExactMatchRule(rule.Parent, null);
                            rule.CopyTo(match);
                            rules.ReplaceRange(i, 1, match);
                        }
                    }
                }

                return false;
            }
        }

        private class MultiMatchRule : AMultiMatchRule
        {
            public MultiMatchRule(NonTerminal parent)
                : base(parent)
            {
            }

            public static bool Consume(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                for (int i = 0; i < rules.Count; i++)
                {
                    UnmatchedRule rule = rules[i] as UnmatchedRule;
                    if (rule != null)
                    {
                        if (rule.Token.TokenType.Name == RuleGrammar.TN_OP_AND)
                        {
                            MultiMatchRule match = new MultiMatchRule(rule.Parent);
                            rule.CopyTo(match);
                            match.Rules.Add(rules[i - 1]);
                            match.Rules.Add(rules[i + 1]);
                            rules.ReplaceRange(i - 1, 3, match);
                            return true;
                        }
                    }
                }

                return false;
            }

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                List<ParseMatch> elements = new List<ParseMatch>();
                int j = 0;
                foreach (var rule in (RunBackwards ? Rules.Reverse() : Rules))
                {
                    ParseMatch match = rule.Matches(tokens, index + (j * (RunBackwards ? -1 : 1)));
                    if (!match.Success)
                    {
                        if (rule.Optional)
                        {
                            continue;
                        }

                        return match;
                    }
                    else
                    {
                        while (match.Success)
                        {
                            elements.Add(match);
                            j += match.ElementsConsumed;
                            if (rule.AllowMultiple)
                            {
                                match = rule.Matches(tokens, index + (j * (RunBackwards ? -1 : 1)));
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                var retVal = ParseMatch.Aggregate(Parent, true, elements);
                retVal.SetGroupName(GetGroupNames());
                return retVal;
            }

            public override string ToString()
            {
                return string.Format("({0}){1}",
                    string.Join(" ", Rules.Select(r => r.ToString()).ToArray()),
                    (AllowMultiple ? "*" : string.Empty));
            }
        }

        private class GroupMatchRule : AMatchRule
        {
            private static readonly char[] GROUP_NAME_SEPARATORS = { '|' };

            public GroupMatchRule(NonTerminal parent)
                : base(parent)
            {
            }

            public IMatchRule Rule { get; set; }

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                return Rule.Matches(tokens, index);
            }

            public static bool Consume(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                int i = 0;
                int openIdx = -1;
                int endIdx = -1;
                int groupNameIdx = -1;
                do
                {
                    UnmatchedRule rule = rules[i] as UnmatchedRule;
                    if (rule != null)
                    {
                        if (rule.Token.IsType(RuleGrammar.TN_OP_GROUP_START))
                        {
                            openIdx = i;
                            groupNameIdx = -1;
                        }
                        else if (rule.Token.IsType(RuleGrammar.TN_GROUP_NAME))
                        {
                            groupNameIdx = i;
                        }
                        else if (rule.Name == RuleGrammar.TN_OP_GROUP_END)
                        {
                            if (openIdx < 0)
                            {
                                throw new GrammarDefinitionException("Invalid pattern at: " + rule.Token.Position);
                            }

                            if (groupNameIdx >= 0 && groupNameIdx != openIdx + 1)
                            {
                                throw new GrammarDefinitionException("Group name must follow Open Group token at: " + rule.Token.Position);
                            }

                            endIdx = i;
                            //GroupMatchRule match = new GroupMatchRule(rule.Parent);
                            ConsumableList<IMatchRule> tmp = new ConsumableList<IMatchRule>();
                            var oRule = (UnmatchedRule)rules[openIdx];
                            bool isNegative = false;
                            bool isConsume = false;
                            bool isBehind = false;
                            switch (oRule.Name)
                            {
                                case RuleGrammar.TN_OP_GROUP_START_LA:
                                    break;
                                case RuleGrammar.TN_OP_GROUP_START_LA_N:
                                    isNegative = true;
                                    break;
                                case RuleGrammar.TN_OP_GROUP_START_LB:
                                    isBehind = true;
                                    break;
                                case RuleGrammar.TN_OP_GROUP_START_LB_N:
                                    isBehind = true;
                                    isNegative = true;
                                    break;
                                case RuleGrammar.TN_OP_GROUP_START_PLAIN:
                                    isConsume = true;
                                    break;
                            }

                            for (int j = Math.Max(openIdx, groupNameIdx) + 1; j < endIdx; j++)
                            {
                                tmp.Add(rules[j]);
                            }

                            int count = tmp.Count + (groupNameIdx > 0 ? 3 : 2); // Open + Close
                            //match.Rule = ;
                            var reduced = RuleGrammar.Reduce(grammar, tmp);
                            //reduced.IsNegative = isNegative;
                            if (groupNameIdx > 0)
                            {
                                var nameRule = rules[groupNameIdx] as UnmatchedRule;
                                string name = nameRule.Token.Value;
                                var names = name.Trim('\'').Split(GROUP_NAME_SEPARATORS, StringSplitOptions.RemoveEmptyEntries);
                                for (int n = 0; n < names.Length; n++)
                                {
                                    reduced.AddGroupName(names[n]);
                                }
                            }

                            if (isConsume)
                            {
                                rules.ReplaceRange(openIdx, count, reduced);
                            }
                            else
                            {
                                rules.ReplaceRange(openIdx, count, null);
                                if (isBehind)
                                {
                                    reduced.RunBackwards = true;
                                    if (isNegative)
                                    {
                                        rules[openIdx].NegativeLookBehind = reduced;
                                    }
                                    else
                                    {
                                        rules[openIdx].LookBehind = reduced;
                                    }
                                }
                                else
                                {
                                    if (isNegative)
                                    {
                                        rules[openIdx - 1].NegativeLookAhead = reduced;
                                    }
                                    else
                                    {
                                        rules[openIdx - 1].LookAhead = reduced;
                                    }
                                }
                            }
                            i = 0;
                            return true;
                        }
                    }

                    i++;
                } while (i < rules.Count);
                return false;
            }

            public override string ToString()
            {
                return "(" + Rule + ")";
            }
        }

        private class OptionMatchRule : AMultiMatchRule
        {
            public OptionMatchRule(NonTerminal parent)
                : base(parent)
            {
                Rules = new List<IMatchRule>();
                HasPriority = true;
            }

            public override ParseMatch Matches(ElementList tokens, int index)
            {
                //if (Parent.Name == "expr_bool_cmp") { System.Diagnostics.Debugger.Break(); }
                if (!CheckLookbehind(tokens, index - 1)) { return ParseMatch.NoMatch; }

                List<ParseMatch> elements = new List<ParseMatch>();
                ParseMatch match = null;
                ParseMatch failure = null;
                foreach (var rule in (RunBackwards ? Rules.Reverse() : Rules))
                {
                    match = rule.Matches(tokens, index);
                    if ((match.Success ^ IsNegative) && CheckLookahead(tokens, index + match.ElementsConsumed))
                    {
                        match.SetGroupName(GetGroupNames());
                        break;
                    }
                    else
                    {
                        if (failure != null && EnableDebugging)
                        {
                            failure.AddReasons(match.FailureReasons);
                        }
                        else
                        {
                            failure = match;
                        }
                    }
                }

                if (IsNegative)
                {
                    match = ParseMatch.NonConsummateMatch;
                }

                if (match != null)
                {
                    return match;
                }

                return failure;
            }

            public override string ToString()
            {
                return string.Format("({0}){1}",
                    string.Join(" | ", Rules.Select(r => r.ToString()).ToArray()),
                    (AllowMultiple ? "*" : string.Empty));
            }

            public static bool Consume(Grammar grammar, ConsumableList<IMatchRule> rules)
            {
                for (int i = 0; i < rules.Count; i++)
                {
                    UnmatchedRule rule = rules[i] as UnmatchedRule;
                    if (rule != null)
                    {
                        if (rule.Token.TokenType.Name == RuleGrammar.TN_OP_OR)
                        {
                            OptionMatchRule match = new OptionMatchRule(rule.Parent);
                            rule.CopyTo(match);
                            var r0 = rules[i - 1];
                            var r1 = rules[i + 1];
                            match.Rules.Add(r0);
                            match.Rules.Add(r1);
                            rules.ReplaceRange(i - 1, 3, match);
                            return true;
                        }
                    }
                }

                return false;
            }
        }
        #endregion

        private static readonly RuleGrammar S_RULE_GRAMMAR;
        private List<IMatchRule> m_MatchRules;
        private bool m_DebuggingEnabled;

        static PatternRule()
        {
            S_RULE_GRAMMAR = new RuleGrammar();
            S_RULE_GRAMMAR.AotCompile();
        }

        public PatternRule(NonTerminal languagePatternDefinition, string pattern)
        {
            LanguagePatternDefinition = languagePatternDefinition;
            Grammar = languagePatternDefinition.Grammar;
            Pattern = pattern;
            m_DebuggingEnabled = Grammar.EnableParserDebugging;
            try
            {
                Lexer lex = S_RULE_GRAMMAR.CreateLexer();
                AElement[] tokens = lex.LexTokens(pattern).ToArray();
                ConsumableList<IMatchRule> rules = new ConsumableList<IMatchRule>(
                    tokens.Where(t => !t.NonParsed).Select(t => (IMatchRule)(new UnmatchedRule(this.LanguagePatternDefinition, t as Token)
                    {
                        EnableDebugging = m_DebuggingEnabled
                    }))
                    );
                var root = RuleGrammar.Reduce(Grammar, rules);
                m_MatchRules = rules;
            }
            catch (LexerException lex)
            {
                throw new GrammarDefinitionException("Failed to parse pattern: " + pattern, lex);
            }
            catch (GrammarDefinitionException ex)
            {
                throw new GrammarDefinitionException("Failed to parse pattern: " + pattern, ex);
            }
        }

        public Grammar Grammar { get; private set; }
        public NonTerminal LanguagePatternDefinition { get; private set; }
        public string Pattern { get; private set; }

        public ParseMatch Find(ElementList tokens, int startIndex)
        {
            ParseMatch failure = null;
            foreach (var rule in m_MatchRules)
            {
                for (int i = startIndex; i < tokens.Count; i++)
                {
                    var match = rule.Matches(tokens, i);
                    if (match.Success)
                    {
                        match.Index = i;
                        return match;
                    }
                    else if (failure == null)
                    {
                        failure = match;
                    }
                    else if (m_DebuggingEnabled)
                    {
                        failure.AddReasons(match.FailureReasons);
                    }
                }
            }

            return failure ?? ParseMatch.NoMatch;
        }
    }
}

