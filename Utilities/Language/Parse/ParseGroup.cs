﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Parse
{
    public class ParseGroup
    {
        public ParseGroup(IEnumerable<string> groupNames, IEnumerable<AElement> elements)
        {
            if (groupNames == null || !groupNames.Any())
            {
                throw new ArgumentNullException("groupName");
            }

            GroupNames = groupNames;
            Elements = new List<AElement>(elements);
        }

        public IEnumerable<string> GroupNames { get; private set; }
        public IList<AElement> Elements { get; private set; }

        public override string ToString()
        {
            return string.Join(",", Elements.Select(e => e.ToString()).ToArray());
        }
    }
}
