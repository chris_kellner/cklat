﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Parse;

namespace CKLib.Utilities.Language
{
    public interface IGrammarElement
    {
        string Name { get; }
        Grammar Grammar { get; }
        IGrammarElement Parent { get; }
        INodeEvaluator Evaluator { get; set; }
        bool NonExecutable { get; set; }

        ParseMatch Matches(ElementList tokens, int index);
    }

    public abstract class AGrammarElement : IGrammarElement
    {
        private INodeEvaluator m_Evaluator;
        private bool? m_NonExecutable;

        public AGrammarElement(Grammar owner)
        {
            Grammar = owner;
        }

        public INodeEvaluator Evaluator
        {
            get
            {
                if (m_Evaluator != null)
                {
                    return m_Evaluator;
                }
                else if (Parent != null)
                {
                    return Parent.Evaluator;
                }

                return null;
            }
            set
            {
                m_Evaluator = value;
            }
        }

        public string Name
        {
            get;
            set;
        }

        public Grammar Grammar
        {
            get;
            protected set;
        }

        public IGrammarElement Parent
        {
            get;
            protected set;
        }

        public bool NonExecutable
        {
            get
            {
                if (m_NonExecutable.HasValue)
                {
                    return m_NonExecutable.Value;
                }

                if (Parent != null)
                {
                    return Parent.NonExecutable;
                }

                return false;
            }
            set
            {
                m_NonExecutable = value;
            }
        }

        public abstract void Initialize();

        public abstract ParseMatch Matches(ElementList tokens, int index);
    }
}
