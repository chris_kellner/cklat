﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public abstract class AElement : IEnumerable<AElement>
    {
        #region Variables
        private HashSet<string> m_Roles = new HashSet<string>();
        #endregion

        #region Constructors
        public AElement(IGrammarElement type)
        {
            TokenType = type;
            Grammar = type.Grammar;
        }
        #endregion

        #region Properties
        public Grammar Grammar { get; private set; }
        public IGrammarElement TokenType { get; private set; }
        public bool NonParsed { get; set; }
        public bool NonExecutable { get { return TokenType.NonExecutable; } }

        public virtual int Line { get; set; }
        public virtual int Column { get; set; }
        public virtual int Position { get; set; }

        public abstract int Length
        {
            get;
        }
        #endregion

        public virtual bool IsType(IGrammarElement type)
        {
            IGrammarElement cType = TokenType;
            while (cType != null)
            {
                if (type == cType)
                {
                    return true;
                }

                cType = cType.Parent;
            }

            return false;
        }

        public bool IsType(string typeName)
        {
            IGrammarElement cType = Grammar.FindGrammarElement(typeName);
            return IsType(cType);
        }

        public virtual IEnumerator<AElement> GetEnumerator()
        {
            yield break;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddRole(string roleName)
        {
            m_Roles.Add(roleName);
        }

        public void AddRoles(IEnumerable<string> roleNames)
        {
            foreach (var name in roleNames)
            {
                m_Roles.Add(name);
            }
        }

        public bool HasRole(string roleName)
        {
            return m_Roles.Contains(roleName);
        }

        public IEnumerable<string> GetRoles()
        {
            return m_Roles;
        }
    }
}
