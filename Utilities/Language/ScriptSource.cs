﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class ScriptSource
    {
        public string Input { get; set; }
        public int CurrentPosition { get; set; }
        public int CurrentLine { get; set; }
        public int CurrentColumn { get; set; }
    }
}
