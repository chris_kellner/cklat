﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Evaluator.Runtime;
using CKLib.Utilities.Language.Exceptions;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language
{
    public class REPL
    {
        private Grammar m_Grammar;
        private ScriptContext m_Context;
        private Lexer m_Lexer;
        private Parser m_Parser;
        private ScriptEvaluator m_Evaluator;
        private CompilationUnit m_CompilationUnit;
        private string m_ScriptDirectory;

        public REPL(Grammar grammar)
        {
            m_Grammar = grammar;
            m_Lexer = m_Grammar.CreateLexer();
            m_Parser = m_Grammar.CreateParser();
            m_Context = new ScriptContext();
            m_Evaluator = m_Grammar.CreateEvaluator(m_Context);
            m_CompilationUnit = new CompilationUnit(m_Grammar);
        }

        public ScriptContext Context { get { return m_Context; } }

        public string ScriptDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(m_ScriptDirectory))
                {
                    m_ScriptDirectory = Environment.CurrentDirectory;
                }

                return m_ScriptDirectory;
            }
            set
            {
                m_ScriptDirectory = value;
            }
        }

        private void PrintVersion(TextWriter writer)
        {
            writer.WriteLine(string.Format("Grammar: {0} Version: {1}", m_Grammar.Name, m_Grammar.Version));
        }

        private static void Describe(object o, TextWriter writer)
        {
            if (o == null) { writer.WriteLine("null"); }
            else if (o.GetType().IsPrimitive || o is string) { writer.WriteLine(o.GetType().Name + ": " + o.ToString()); }
            else if (o is Type && o.GetType().IsEnum)
            {
                writer.WriteLine("Enum Values: {");
                foreach (var val in Enum.GetValues((Type)o))
                {
                    writer.WriteLine("  " + val);
                }
                writer.WriteLine("}");
            }
            else if (o is IDictionary<string, object>)
            {
                foreach (var key in ((IDictionary<string, object>)o).Keys)
                {
                    writer.WriteLine("Dictionary Keys: {");
                    writer.WriteLine("  " + key);
                    writer.WriteLine("}");
                }
            }
            else
            {
                Type t = null;
                if (o is Type) t = (Type)o;
                else if (o is ClassName) t = ((ClassName)o).Class;
                else t = o.GetType();
                writer.WriteLine("Methods: {");
                foreach (var method in t.GetMethods(MemberInvoker.ALL_FLAGS))
                {
                    writer.WriteLine("  " + method);
                }
                writer.WriteLine("}");

                writer.WriteLine("Properties: {");
                foreach (var method in t.GetProperties(MemberInvoker.ALL_FLAGS))
                {
                    writer.WriteLine("  " + method);
                }
                writer.WriteLine("}");

                writer.WriteLine("Fields: {");
                foreach (var method in t.GetFields(MemberInvoker.ALL_FLAGS))
                {
                    writer.WriteLine("  " + method);
                }
                writer.WriteLine("}");
            }
        }

        public object RunFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                fileName = Path.Combine(
                    ScriptDirectory,
                    Path.GetFileName(fileName)
                    );
            }

            return m_Grammar.Evaluate(Context, null, File.ReadAllText(fileName));
        }

        public void Run(TextReader reader, TextWriter writer)
        {
            Stopwatch sw = new Stopwatch();
            string buffer = null;
            ASTNode node = null;
            IEnumerable<AElement> tokens = null;
            Context.SetGlobal("exit", new Action(() => Environment.Exit(0)));
            Context.SetGlobal("describe", new Action<object>(o => Describe(o, writer)));
            Context.SetGlobal("repl", this);

            PrintVersion(writer);
            m_Evaluator.EvaluateNode += (s, e) =>
            {
                writer.WriteLine("--> " + e.CurrentNode.ToString(false));
                e.Step();
            };
            while (buffer != "exit")
            {
                sw.Reset();
                if (string.IsNullOrEmpty(buffer))
                {
                    writer.Write(">>> ");
                }
                else
                {
                    writer.Write("... ");
                }

                var cmd = reader.ReadLine();
                switch (cmd)
                {
                    case "exit":
                        return;
                    case "debug":
                        m_Evaluator.DebugEnabled = !m_Evaluator.DebugEnabled;
                        continue;
                    case "version":
                        PrintVersion(writer);
                        continue;
                    default:
                        if (string.IsNullOrEmpty(buffer))
                        {
                            buffer = cmd;
                        }
                        else
                        {
                            buffer = string.Join(Environment.NewLine, new string[] { buffer, cmd });
                        }
                        break;
                }

                try
                {
                    node = null;
                    tokens = m_Lexer.LexTokens(buffer);
                    var parsed = m_Parser.Parse(tokens);
                    node = m_CompilationUnit.MakeRoot(parsed);
                    sw.Start();
                    var result = m_Evaluator.Evaluate(node);
                    sw.Stop();
                    if (result.IsValue)
                    {
                        writer.WriteLine(result.Value);
                        writer.WriteLine("Elapsed: " + (m_Lexer.LastLexTime + m_Parser.LastParseTime + sw.Elapsed));
                    }

                    buffer = null;
                }
                catch (UnclosedGroupException)
                {
                    // Waiting on the group to be closed, most likely a block of code
                    continue;
                }
                catch (ExecutionException ex)
                {
                    writer.WriteLine(ex.Message);
                    buffer = null;
                }
                catch (Exception ex)
                {
                    writer.WriteLine(ex);
                    buffer = null;
                }
                finally
                {
                    Context.SetGlobal("ast", node);
                    Context.SetGlobal("tokens", tokens);
                }
            }
        }
    }
}
