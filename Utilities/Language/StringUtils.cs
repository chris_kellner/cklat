﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CKLib.Utilities.Language
{
    public static class StringUtils
    {
        private static Dictionary<string, string> m_EscapeCharacters = new Dictionary<string, string>()
        {
            { "\0", @"\0" },
            { "\r", @"\r" },
            { "\n", @"\n" },
            { "\t", @"\t" },
            { "\f", @"\f" },
            { "\a", @"\a" },
            { "\b", @"\b" },
            { "\v", @"\v" },
            { "\\", @"\\" },
            //{ "\"", @"\""" }
        };

        private static Dictionary<string, string> m_EscapeCharactersReverse = new Dictionary<string, string>()
        {
            { @"\0", "\0" },
            { @"\r", "\r" },
            { @"\n", "\n" },
            { @"\t", "\t" },
            { @"\f", "\f" },
            { @"\a", "\a" },
            { @"\b", "\b" },
            { @"\v", "\v" },
            { @"\\", "\\" }
        };

        private const string WHITESPACE_CHARACTERS = "\r\n\t\f\a\b\v\0";
        private const string WHITESPACE_CHARACTERS_REVERSE = @"\r\n\t\f\a\b\v\0";

        private const string NON_WHITESPACE_CHARACTERS = "\\";
        private const string NON_WHITESPACE_CHARACTERS_REVERSE = @"\\";

        private const string BASE_CHARACTERS = WHITESPACE_CHARACTERS + NON_WHITESPACE_CHARACTERS;
        private const string BASE_CHARACTERS_REVERSE = WHITESPACE_CHARACTERS_REVERSE + NON_WHITESPACE_CHARACTERS_REVERSE;

        private static HashSet<string> m_WhiteSpaceEscaped = new HashSet<string>(
            WHITESPACE_CHARACTERS.ToCharArray().Select(c => c.ToString()));
        private static HashSet<string> m_WhiteSpaceEscapedReverse = new HashSet<string>(
            WHITESPACE_CHARACTERS_REVERSE.ToCharArray().Select(c => c.ToString()));

        public static string LanguageEscape(string unescapedString, bool ignoreWhitespace, params char[] quoteChars)
        {
            if (unescapedString == null) return null;
            string pattern = BASE_CHARACTERS;
            if (quoteChars.Length > 0)
            {
                pattern += new string(quoteChars);
            }

            pattern = "[" + pattern + "]";
            string output = Regex.Replace(unescapedString, pattern, m =>
            {
                if (ignoreWhitespace && m_WhiteSpaceEscaped.Contains(m.Value))
                {
                    return m.Value;
                }

                string replacement;
                if (m_EscapeCharacters.TryGetValue(m.Value, out replacement))
                {
                    return replacement;
                }

                return "\\" + m.Value;
            });

            return output;
        }

        public static string ToCodeBlock(string sourceCode, string openMark, string closeMark, char escapeChar)
        {
            sourceCode = Regex.Replace(sourceCode, openMark + "|" + closeMark, m =>
            {
                return escapeChar + m.Value;
            });

            return openMark + sourceCode + closeMark;
        }

        public static string FromCodeBlock(string codeBlock, string openMark, string closeMark, char escapeChar)
        {
            string regex = string.Format(@"(?ns){0}(?<code>({2}{1}|.*(?={1}))*?){1}", openMark, closeMark, escapeChar);
            var match = Regex.Match(codeBlock, regex);
            if (match.Success)
            {
                var grp = match.Groups["code"];
                codeBlock = grp.Value;
            }

            codeBlock = codeBlock.Replace(escapeChar + closeMark, closeMark);
            return codeBlock;
        }

        public static string ToStringLiteral(string unescapedString)
        {
            if (unescapedString == null) return null;
            string str = LanguageEscape(unescapedString, false, '"');
            return "\"" + str + "\"";
        }

        public static string ToStringLiteral(string unescapedString, params char[] quoteChars)
        {
            if (unescapedString == null) return null;
            string str = LanguageEscape(unescapedString, false, quoteChars);
            return "\"" + str + "\"";
        }

        public static string FromStringLiteral(string escapedString, char quoteChar)
        {
            if (string.IsNullOrEmpty(escapedString)) { return escapedString; }
            if (escapedString[0] == quoteChar)
            {
                if (escapedString[escapedString.Length - 1] == quoteChar)
                {
                    escapedString = escapedString.Substring(1, escapedString.Length - 2);
                }
            }

            string str = LanguageUnescape(escapedString, false, quoteChar);
            return str;
        }

        public static string LanguageUnescape(string escapedString, bool ignoreWhitespace, params char[] quoteChars)
        {
            string pattern = BASE_CHARACTERS_REVERSE;
            if (quoteChars.Length > 0)
            {
                pattern += string.Join("\\", quoteChars.Select(c => c.ToString()).ToArray());
            }

            pattern = "[" + pattern + "]";
            string output = Regex.Replace(escapedString, pattern, m =>
            {
                if (ignoreWhitespace && m_WhiteSpaceEscapedReverse.Contains(m.Value))
                {
                    return m.Value;
                }

                string replacement;
                if (m_EscapeCharactersReverse.TryGetValue(m.Value, out replacement))
                {
                    return replacement;
                }

                return m.Value.Substring(1);
            });

            return output;
        }
    }
}
