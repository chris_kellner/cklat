﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class ScriptVariable
    {
        protected object m_Value;

        public string Name { get; set; }
        public Type Type { get; set; }
        public object Value
        {
            get
            {
                return m_Value;
            }

            set
            {
                TrySet(value);
            }
        }

        public bool IsDefined { get; set; }

        public virtual bool TrySet(object value)
        {
            if (Type == null)
            {
                /* Dynamic type assignment
                 * Wasn't set in the declaration
                 */
                if (value != null)
                {
                    Type = value.GetType();
                }
                else
                {
                    Type = typeof(object);
                }

                m_Value = value;
                IsDefined = true;
            }
            else if (Type.IsInstanceOfType(value))
            {
                m_Value = value;
                IsDefined = true;
            }
            else if (value == null)
            {
                m_Value = null;
                IsDefined = true;
            }
            else if (value.GetType().IsPrimitive)
            {
                value = ExpressionProcessor.SystemUpscale(value, Type);
                if (Type.IsInstanceOfType(value))
                {
                    m_Value = value;
                    IsDefined = true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}: {2}", Type.FullName, Name, Value);
        }
    }
}
