﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class SwitchNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_TARGET = "target";
        public const string ROLE_CASE = "case";
        public const string ROLE_CASE_CONDITION = "condition";
        public const string ROLE_CASE_CONTENT = "content";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var targetNode = tree.FindNodeByRole(ROLE_TARGET, true);
            var caseNodes = tree.FindNodesByRole(ROLE_CASE);
            var target = evaluator.Evaluate<object>(targetNode);
            bool fallthrough = false;
            foreach (var caseNode in caseNodes)
            {
                var conditionNode = caseNode.FindNodeByRole(ROLE_CASE_CONDITION, false);
                object condition = null;
                if (conditionNode != null)
                {
                    condition = evaluator.Evaluate<object>(conditionNode);
                }

                if (fallthrough ||  // falling through from previous case
                    conditionNode == null ||    // default
                    target != null && target.Equals(condition) ||
                    target == condition){
                    fallthrough = false;
                    var rslt = evaluator.Evaluate(caseNode);
                    if (rslt.IsLoopExit)
                    {
                        return rslt;
                    }

                    if (AllowFallThrough || caseNode.FindNodeByRole(ROLE_CASE_CONTENT, false) == null)
                    {
                        fallthrough = true;
                    }
                }
            }

            return EvaluationResult.Next;
        }

        public bool AllowFallThrough { get; set; }
    }
}
