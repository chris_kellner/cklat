﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class TypeNameNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_TYPE = "type";
        public const string ROLE_GENERIC = "generics";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var typeNode = tree.FindNodeByRole(ROLE_TYPE, true, false);
            var genericNode = tree.FindNodeByRole(ROLE_GENERIC, false, false);
            Type[] genericTypes = null;
            Type type = null;
            if (genericNode != null)
            {
                if (!typeNode.IsTerminal)
                {
                    throw new ExecutionException("Type node isn't terminal for generic declaration", tree);
                }

                var genericClasses = evaluator.Evaluate<IEnumerable<ClassName>>(genericNode);
                genericTypes = genericClasses.Select(t => t.Class).ToArray();
                string typeName = typeNode.Value + "`" + genericTypes.Length;
                type = evaluator.Context.References.BindToType(typeName);
                if (type == null)
                {
                    throw new ExecutionException("Type not found: " + typeName, tree);
                }

                if (genericTypes != null && genericTypes.Length > 0)
                {
                    type = type.MakeGenericType(genericTypes);
                }
            }
            else
            {
                type = evaluator.Evaluate<ClassName>(typeNode).Class;
            }

            if (IsArray)
            {
                type = type.MakeArrayType();
            }

            return new EvaluationResult(new ClassName(type));
        }

        public bool IsArray { get; set; }
    }
}
