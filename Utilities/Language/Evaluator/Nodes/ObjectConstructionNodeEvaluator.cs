﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class ObjectConstructionNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_TYPE = "type";
        public const string ROLE_PARAMETERS = "parameters";
        public const string ROLE_PARAM = "param";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            Type type = expectedType;
            object[] parameters = null;
            int[] indexers = null;
            var typeNode = tree.FindNodeByRole(ROLE_TYPE, type == null);
            if (typeNode != null)
            {
                var @class = evaluator.Evaluate<ClassName>(typeNode);
                type = @class.Class;
            }

            var paramsNode = tree.FindNodeByRole(ROLE_PARAMETERS, IsArray);
            if (paramsNode != null)
            {
                if (IsArray)
                {
                    indexers = paramsNode.FindNodesByRole(ROLE_PARAM)
                       .Select(p => evaluator.Evaluate<int>(p))
                       .ToArray();
                }
                else
                {
                    parameters = paramsNode.FindNodesByRole(ROLE_PARAM)
                        .Select(p => evaluator.Evaluate<object>(p))
                        .ToArray();
                }
            }

            if (IsArray)
            {
                var arr = Array.CreateInstance(type, indexers);
                return new EvaluationResult(arr);
            }
            else
            {
                var obj = Activator.CreateInstance(type, BindingFlags.CreateInstance, Binder, parameters, null);
                return new EvaluationResult(obj);
            }
        }

        public Binder Binder { get; set; }
        public bool IsArray { get; set; }
    }
}
