﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class ForeachNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_DECLARATION = "s1";
        public const string ROLE_COLLECTION = "collection";
        public const string ROLE_CONTENTS = "contents";
        public const string ROLE_NAME = "name";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var collectionNode = tree.FindNodeByRole(ROLE_COLLECTION, true);
            var collection = evaluator.Evaluate<IEnumerable>(collectionNode);

            var declarationNode = tree.FindNodeByRole(ROLE_DECLARATION, true);
            var lcvNode = declarationNode.FindNodeByRole(ROLE_NAME, true);
            var contentNode = tree.FindNodeByRole(ROLE_CONTENTS, true);

            var enumerator = collection.GetEnumerator();
            using (var outerScope = evaluator.Context.NewScope())
            {
                evaluator.Evaluate(declarationNode);
                while (enumerator.MoveNext())
                {
                    evaluator.MakeAssignment(lcvNode, enumerator.Current);
                    using (var scope = evaluator.Context.NewScope())
                    {
                        var rslt = evaluator.Evaluate(contentNode);
                        if (rslt.IsLoopExit)
                        {
                            if (rslt.Direction == DirectionTypes.Break)
                            {
                                rslt = new EvaluationResult(rslt.Value);
                                break;
                            }

                            return rslt;
                        }
                    }
                }
            }

            return EvaluationResult.Next;
        }
    }
}
