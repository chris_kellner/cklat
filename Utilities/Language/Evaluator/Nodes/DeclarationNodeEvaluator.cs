﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class DeclarationNodeEvaluator : INodeEvaluator
    {
        public const string ROLE_TYPE_NAME = "type";
        public const string ROLE_VAR_NAME = "name";

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            var typeNode = tree.FindNodeByRole(ROLE_TYPE_NAME, true);
            Type t = null;
            if (!typeNode.IsTerminal || !typeNode.IsType("KW_VAR"))
            {
                ClassName typeName = evaluator.Evaluate<ClassName>(typeNode);
                if (typeName != null)
                {
                    t = typeName.Class;
                }
                else
                {
                    throw new ExecutionException("Unknown Type '" + typeNode.Value + "'.", tree);
                }
            }

            foreach (var nameNode in tree.FindNodesByRole(ROLE_VAR_NAME))
            {
                string name = nameNode.Value;
                ScriptVariable var;
                if (evaluator.Context.TryGetVariable(name, out var))
                {
                    throw new ExecutionException("Variable '" + name + "' is already declared", tree);
                }

                evaluator.Context.DeclareLocal(name, t);
            }

            return EvaluationResult.Next;
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            var nameNode = tree.FindNodeByRole(ROLE_VAR_NAME, true);
            Evaluate(evaluator, tree);
            evaluator.MakeAssignment(nameNode, value);
        }

        public bool CanAssign
        {
            get { return true; }
        }

        public bool HasValue
        {
            get { return false; }
        }
    }
}
