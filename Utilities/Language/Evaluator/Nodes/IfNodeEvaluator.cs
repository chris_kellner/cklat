﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class IfNodeEvaluator : INodeEvaluator
    {
        public const string ROLE_CONDITION = "s2";
        public const string ROLE_BODY = "body";
        public const string ROLE_ELSE_IF = "elif";
        public const string ROLE_ELSE = "else";

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            var @else = tree.FindNodeByRole(ROLE_ELSE, false, false);
            EvaluationResult rslt = EvaluationResult.Next;

            List<ASTNode> ifList = new List<ASTNode>();
            ifList.AddRange(tree.FindNodesByRole(ROLE_ELSE_IF));
            bool handled = false;
            for(int i=0;i<ifList.Count;i++)
            {
                var condition = ifList[i].FindNodeByRole(ROLE_CONDITION, true, false);
                var body = ifList[i].FindNodeByRole(ROLE_BODY, false, false);

                bool isTrue = evaluator.Evaluate<bool>(condition);
                if (isTrue)
                {
                    handled = true;
                    if (body != null)
                    {
                        rslt = evaluator.Evaluate(body);
                        if (rslt.IsLoopExit) { return rslt; }
                    }

                    break;
                }
            }

            if (!handled && @else != null)
            {
                rslt = evaluator.Evaluate(@else);
            }

            return rslt;
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return false; }
        }
    }
}
