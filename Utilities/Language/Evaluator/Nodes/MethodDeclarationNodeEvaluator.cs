﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class MethodDeclarationNodeEvaluator : INodeEvaluator
    {
        public const string ROLE_RETURN_TYPE = "return-type";
        public const string ROLE_METHOD_NAME = "method-name";
        public const string ROLE_METHOD_ATTRIBUTE = "method-attribute";
        public const string ROLE_METHOD_BODY = "method-body";
        public const string ROLE_PARAMETERS_GROUP = "p-group";
        public const string ROLE_PARAMETER = "param";
        public const string ROLE_PARAMETER_NAME = "name";
        public const string ROLE_PARAMETER_TYPE = "type";
        public const string ROLE_PARAMETER_MODIFIER = "p-modifier";
        public const string ROLE_PARAMETER_ATTRIBUTE = "p-attribute";

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            var returnTypeNode = tree.FindNodeByRole(ROLE_RETURN_TYPE, 0);
            var methodNameNode = tree.FindNodeByRole(ROLE_METHOD_NAME, 0);
            var parametersNode = tree.FindNodeByRole(ROLE_PARAMETERS_GROUP, 0);
            var bodyNode = tree.FindNodeByRole(ROLE_METHOD_BODY, 0);

            Type returnType = typeof(object);
            if (returnTypeNode != null)
            {
                returnType = evaluator.Context.BindToType(returnTypeNode.Value);
            }

            string methodName = null;
            if (methodNameNode != null)
            {
                methodName = methodNameNode.Value;
            }

            List<ScriptMethodParameter> parameters = new List<ScriptMethodParameter>();
            var paramNodes = parametersNode.FindNodesByRole(ROLE_PARAMETER);
            foreach (var parameter in paramNodes)
            {
                var nameNode = parameter.FindNodeByRole(ROLE_PARAMETER_NAME, 0);
                var typeNode = parameter.FindNodeByRole(ROLE_PARAMETER_TYPE, 0);
                Type type = typeNode != null ? evaluator.Context.BindToType(typeNode.Value) : typeof(object);
                string name = nameNode.Value;
                ScriptMethodParameter param = new ScriptMethodParameter(name, type);
                parameters.Add(param);
            }

            var method = new ScriptMethod(evaluator.Context, bodyNode, methodName, returnType, parameters.ToArray());
            if (IsLambda)
            {
                return new EvaluationResult(method.CreateDelegate());
            }
            else
            {
                return new EvaluationResult(method);
            }
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return true; }
        }

        public bool IsLambda { get; set; }
    }
}
