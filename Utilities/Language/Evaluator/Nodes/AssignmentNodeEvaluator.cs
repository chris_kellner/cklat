﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class AssignmentNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_LVALUE = "lvalue";
        public const string ROLE_RVALUE = "rvalue";

        private static readonly string[] REQUIRED_ROLES = { ROLE_LVALUE, ROLE_RVALUE };

        public AssignmentNodeEvaluator()
        {
            HasValue = true;
            CanAssign = false;
        }

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var lValueNode = tree.FindNodeByRole(ROLE_LVALUE, true);
            var rValueNode = tree.FindNodeByRole(ROLE_RVALUE, true);

            // Evaluate rValue first so we can assign an assignment
            var right = evaluator.Evaluate<object>(rValueNode);

            // Perform the assignment
            evaluator.MakeAssignment(lValueNode, right);
            return new EvaluationResult(DirectionTypes.Next, right);
        }

        protected override string[] GetRequiredRoleNames()
        {
            return REQUIRED_ROLES;
        }
    }
}
