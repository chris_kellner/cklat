﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class ExpressionNodeEvaluator : INodeEvaluator
    {
        #region Variables
        private int m_LeftIdx;
        private int m_RightIdx;
        private int m_OpIdx;
        private ExpressionProcessor m_Processor;
        #endregion

        #region Constructors
        public ExpressionNodeEvaluator()
            :this(0,2,1)
        {
        }
        
        public ExpressionNodeEvaluator(int leftIdx, int rightIdx, int opIdx)
        {
            m_LeftIdx = leftIdx;
            m_RightIdx = rightIdx;
            m_OpIdx = opIdx;
        }
        #endregion

        #region Properties
        public bool CanAssign { get { return false; } }

        public bool HasValue { get { return true; } }

        public ExpressionProcessor Processor
        {
            get
            {
                if (m_Processor == null)
                {
                    m_Processor = new ExpressionProcessor();
                }

                return m_Processor;
            }
            set { m_Processor = value; }
        }
        #endregion

        #region Methods
        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            ASTNode left = null, right = null;
            // Some operators only take 1 operand, in these cases the index should be -1.
            if (m_LeftIdx >= 0)
            {
                left = tree.Nodes[m_LeftIdx];
            }
            
            if (m_RightIdx >= 0)
            {
                right = tree.Nodes[m_RightIdx];
            }

            string op = tree.Nodes[m_OpIdx].Value;
            var operation = GetOperationType(left, right, op);
            var retVal = Processor.Evaluate(evaluator, left, right, operation);
            return new EvaluationResult(retVal);
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        protected virtual OperatorType GetOperationType(ASTNode left, ASTNode right, string operation)
        {
            switch (operation)
            {
                case "+":
                    return OperatorType.op_Addition;
                case "-":
                    if (left == null) { return OperatorType.op_UnaryNegation; }
                    return OperatorType.op_Subtraction;
                case "*":
                    return OperatorType.op_Multiply;
                case "/":
                    return OperatorType.op_Division;
                case "%":
                    return OperatorType.op_Modulus;
                case "|":
                    return OperatorType.op_BitwiseOr;
                case "&":
                    return OperatorType.op_BitwiseAnd;
                case "^":
                    return OperatorType.op_ExclusiveOr;
                case "==":
                    return OperatorType.op_Equality;
                case "!=":
                    return OperatorType.op_Inequality;
                case ">":
                    return OperatorType.op_GreaterThan;
                case "<":
                    return OperatorType.op_LessThan;
                case ">=":
                    return OperatorType.op_GreaterThanOrEqual;
                case "<=":
                    return OperatorType.op_LessThanOrEqual;
                case "&&":
                    return OperatorType.op_LogicalAnd;
                case "||":
                    return OperatorType.op_LogicalOr;
                case "!":
                    return OperatorType.op_LogicalNot;
                case "~":
                    return OperatorType.op_OnesComplement;
                case "++":
                    return OperatorType.op_Increment;
                case "--":
                    return OperatorType.op_Decrement;
            }

            return OperatorType.Unknown;
        }
        #endregion
    }
}
