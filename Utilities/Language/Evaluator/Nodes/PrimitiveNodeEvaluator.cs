﻿using CKLib.Utilities.Language.Lex;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class PrimitiveNodeEvaluator : INodeEvaluator
    {
        private static Dictionary<Type, Func<string, object>> s_Converters = new Dictionary<Type, Func<string, object>>()
        {
            { typeof(float), (_1) => float.Parse(_1.TrimEnd('f','F'), NumberStyles.Float) },
            { typeof(double), (_1) => double.Parse(_1.TrimEnd('d','D'), NumberStyles.Float) },
            { typeof(byte), (_1) => byte.Parse(_1.TrimEnd('b','B'), NumberStyles.Integer) },
            { typeof(short), (_1) => short.Parse(_1.TrimEnd('s','S'), NumberStyles.Integer) },
            { typeof(ushort), (_1) => ushort.Parse(_1.TrimEnd('s','S','u','U'), NumberStyles.Integer) },
            { typeof(int), (_1) => int.Parse(_1.TrimEnd('i'), NumberStyles.Integer) },
            { typeof(uint), (_1) => uint.Parse(_1.TrimEnd('u','U'), NumberStyles.Integer) },
            { typeof(long), (_1) => long.Parse(_1.TrimEnd('l','L'), NumberStyles.Integer) },
            { typeof(ulong), (_1) => ulong.Parse(_1.TrimEnd('u','U','l','L'), NumberStyles.Integer) },
            { typeof(char), (_1) => _1.Trim('\'')[0] },
            { typeof(string), (_1) => _1.Trim('"') },
            { typeof(bool), (_1) => bool.Parse(_1) }
        };

        private Func<string, object> m_Converter;

        public PrimitiveNodeEvaluator()
        {
        }

        public PrimitiveNodeEvaluator(Type primitiveType)
        {
            PrimitiveType = primitiveType;
        }

        public PrimitiveNodeEvaluator(Func<string, object> valueConverter)
        {
            m_Converter = valueConverter;
        }

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            if (tree.IsTerminal)
            {
                Terminal def = tree.TokenType as Terminal;
                if (def != null)
                {
                    if (m_Converter == null)
                    {
                        Func<string, object> cvt;
                        if (!s_Converters.TryGetValue(PrimitiveType, out cvt))
                        {
                            cvt = s => Convert.ChangeType(s, PrimitiveType);
                        }

                        m_Converter = cvt;
                    }

                    try
                    {
                        return new EvaluationResult(m_Converter(tree.Value));
                    }
                    catch (Exception ex)
                    {
                        throw new ExecutionException("Cannot convert NonTerminal: " + tree.Value + " to primitive type.", tree, ex);
                    }
                }
            }

            throw new ExecutionException("Cannot convert NonTerminal: " + tree.Value + " to primitive type.", tree);
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            throw new NotSupportedException();
        }

        public Type PrimitiveType
        {
            get;
            set;
        }

        public bool CanAssign
        {
            get { return false; }
        }

        public bool HasValue
        {
            get { return true; }
        }
    }
}
