﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    [Flags]
    public enum AccessTypes
    {
        Sited = 0,
        This = 1,
        Class = 2,
        Variable = 4,
        All = 7
    }

    public enum MemberTypes
    {
        None = 0,
        Member = 1,
        Method = 2,
        Both = 3
    }

    public class MemberAccessorNodeEvaluator : INodeEvaluator
    {
        public const string ROLE_SITE = "site";
        public const string ROLE_ACCESSOR = "accessor";
        public const string ROLE_MEMBER_NAME = "member";
        public const string ROLE_PARAMETERS = "parameters";
        public const string ROLE_PARAMETER = "param";
        public const string ROLE_VALUE = "value";
        public const string ROLE_TYPE_PARAMS = "type_params";

        //private MemberInvoker m_Invoker = new MemberInvoker(null);

        public EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree)
        {
            object last = null;
            var siteNode = tree.FindNodeByRole(ROLE_SITE, false, false);
            var acc = tree.FindNodeByRole(ROLE_ACCESSOR, false, false);
            if (siteNode != null)
            {
                var site = evaluator.Evaluate<object>(siteNode);
                last = EvaluateAccessor(evaluator, site, acc ?? tree);
            }
            else
            {
                // Treat it like a variable
                last = EvaluateAccessor(evaluator, null, acc ?? tree);
            }

            return new EvaluationResult(last);
        }

        public void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value)
        {
            object last = null;
            var siteNode = tree.FindNodeByRole(ROLE_SITE, false, false);
            var acc = tree.FindNodeByRole(ROLE_ACCESSOR, false, false);
            if (siteNode != null)
            {
                last = evaluator.Evaluate<object>(siteNode);
            }

            EvaluateAssignment(evaluator, last, acc ?? tree, value);
        }

        private object EvaluateAccessor(ScriptEvaluator evaluator, object callSite, ASTNode tree)
        {
            var invoker = evaluator.Context.MemberInvoker;
            var nameNode = tree.FindNodeByRole(ROLE_MEMBER_NAME, ImpliedMemberName == null);
            var paramNode = tree.FindNodeByRole(ROLE_PARAMETERS, false);
            var typeParamsNode = tree.FindNodeByRole(ROLE_TYPE_PARAMS, false);

            string memberName = ImpliedMemberName ?? (EvalName ? evaluator.Evaluate<string>(nameNode) : nameNode.Value);

            Type[] typeParams = null;
            object[] parameters = null;
            if (paramNode != null)
            {
                parameters = paramNode.FindNodesByRole(ROLE_PARAMETER)
                    .Select(n => evaluator.Evaluate<object>(n))
                    .ToArray();
            }

            if (typeParamsNode != null)
            {
                typeParams = evaluator.Evaluate<IEnumerable<ClassName>>(typeParamsNode)
                    .Select(t => t.Class).ToArray();
            }

            if (callSite == null)
            {
                if ((AccessType & AccessTypes.Variable) == AccessTypes.Variable)
                {
                    TryGetVariableValue(evaluator, nameNode, out callSite);
                    #region Explanation
                    /*
                        * Its a variable:
                        * 
                        * // Previously:
                        *      int x = 4;
                        *      Func<int> y = p => p + 4;
                        * // Now:
                        *      x = y(x)
                        *      - at y(x):
                        *          - callsite = null
                        *          - member = y
                        *          - params = x
                        *          - at x
                        *              - callsite = null
                        *              - member = x
                        *              - params = null
                        *          - invoke y(x)
                        */
                    #endregion
                }

                if (callSite == null && (AccessType & AccessTypes.This) == AccessTypes.This)
                {
                     ScriptVariable var;
                     if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_THIS, out var))
                     {
                         callSite = var;
                         var obj = AccessMember(invoker, callSite, null, memberName, typeParams, parameters);
                         return obj;
                     }
                }

                if ((AccessType & AccessTypes.Class) == AccessTypes.This)
                {
                    ScriptVariable var;
                    if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_CLASS, out var))
                    {
                        callSite = var;
                        var obj = AccessMember(invoker, null, (Type)callSite, memberName, typeParams, parameters);
                        return obj;
                    }
                }

                var val = evaluator.Evaluate<object>(nameNode);
                var del = val as Delegate;
                if (del != null && paramNode != null)
                {
                    val = del.DynamicInvoke(parameters);
                }
                else if (val is ScriptMethod && paramNode != null)
                {
                    val = ((ScriptMethod)val).Invoke(null, parameters);
                }
                else if (paramNode != null)
                {
                    val = AccessMember(invoker, callSite, null, "Item", typeParams, parameters);
                }

                return val;
            }
            else
            {
                var obj = AccessMember(invoker, callSite, null, memberName, typeParams, parameters);
                return obj;
            }
        }

        public void EvaluateAssignment(ScriptEvaluator evaluator, object callSite, ASTNode tree, object value)
        {
            var invoker = evaluator.Context.MemberInvoker;
            var nameNode = tree.FindNodeByRole(ROLE_MEMBER_NAME, ImpliedMemberName == null);
            var paramNode = tree.FindNodeByRole(ROLE_PARAMETERS, false);

            string memberName = ImpliedMemberName ?? (EvalName ? evaluator.Evaluate<string>(nameNode) : nameNode.Value);

            object[] parameters = null;
            if (paramNode != null)
            {
                parameters = paramNode.FindNodesByRole(ROLE_PARAMETER)
                    .Select(n => evaluator.Evaluate<object>(n))
                    .ToArray();
            }

            var hasValue = false;
            if (callSite == null)
            {
                ScriptVariable var = null;
                if ((AccessType & AccessTypes.Variable) == AccessTypes.Variable)
                {
                    if (evaluator.Context.TryGetVariable(memberName, out var))
                    {
                        if (!var.TrySet(value))
                        {
                            throw new ExecutionException(string.Format("Cannot implicitly convert type '{0}' to type'{1}'.", value.GetType().FullName, var.Type.FullName), tree);
                        }

                        hasValue = true;
                    }
                }

                if (!hasValue && (AccessType & AccessTypes.This) == AccessTypes.This)
                {
                    if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_THIS, out var))
                    {
                        hasValue = invoker.TrySetMember(var.Value, memberName, parameters, value);
                    }
                }

                if (!hasValue && (AccessType & AccessTypes.Class) == AccessTypes.This)
                {
                    if (evaluator.Context.TryGetVariable(ScriptContext.RESERVED_NAME_CLASS, out var))
                    {
                        hasValue = invoker.TrySetMember(var.Value, memberName, parameters, value);
                    }
                }
            }
            else
            {
                invoker.SetMember(callSite, memberName, parameters, value);
            }
        }

        protected object AccessMember(MemberInvoker invoker, object callSite, Type declaredType, string memberName, Type[] genericParameters, object[] parameters)
        {
            if ((MemberType & MemberTypes.Member) == MemberTypes.Member)
            {
                return invoker.GetMember(callSite, memberName, parameters);
            }
            else if ((MemberType & MemberTypes.Method) == MemberTypes.Method)
            {
                MethodBase method;
                return invoker.InvokeMethod(callSite, declaredType, memberName, genericParameters, null, parameters, out method);
            }
            else
            {
                return null;
            }
        }

        protected bool TryGetVariableValue(ScriptEvaluator evaluator, ASTNode tree, out object value)
        {
            string name = EvalName ? evaluator.Evaluate<string>(tree) : tree.Value;
            var context = evaluator.Context;
            value = null;
            ScriptVariable var;
            if (!context.TryGetVariable(name, out var))
            {
                var type = context.BindToType(name);
                if (type != null)
                {
                    value = type;
                    return true;
                }

                throw new ExecutionException("Variable '" + name + "' is not declared", tree);
            }

            if (!var.IsDefined)
            {
                throw new ExecutionException("Variable '" + name + "' is not defined", tree);
            }

            value = var.Value;
            return true;
        }

        public bool CanAssign
        {
            get;
            set;
        }

        public Binder Binder
        {
            get;
            set;
        }

        public bool HasValue
        {
            get { return true; }
        }

        public AccessTypes AccessType
        {
            get;
            set;
        }

        public MemberTypes MemberType { get; set; }

        public bool EvalName { get; set; }

        public string ImpliedMemberName { get; set; }
    }
}
