﻿using CKLib.Utilities.Language.Evaluator.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Nodes
{
    public class GenericArgsNodeEvaluator : ANodeEvaluator
    {
        public const string ROLE_TYPE = "type";

        public override EvaluationResult Evaluate(ScriptEvaluator evaluator, Type expectedType, ASTNode tree)
        {
            var types = tree.FindNodesByRole(ROLE_TYPE);
            IEnumerable<ClassName> classes = types.Select(t => evaluator.Evaluate<ClassName>(t));
            return new EvaluationResult(classes.ToArray());
        }
    }
}
