﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public enum DirectionTypes : int
    {
        Next = 0,
        Return,
        Top,
        Break,
        Jump
    }

    public class EvaluationResult
    {
        public static readonly EvaluationResult Next = new EvaluationResult(null);
        public static readonly EvaluationResult Break = new EvaluationResult(DirectionTypes.Break, null);
        public static readonly EvaluationResult Continue = new EvaluationResult(DirectionTypes.Top, null);
        public static readonly EvaluationResult Void_Return = new EvaluationResult(DirectionTypes.Return, null);

        private DirectionTypes m_Direction;
        private object m_Value;

        public EvaluationResult(DirectionTypes direction, object value)
        {
            m_Direction = direction;
            m_Value = value;
        }

        public EvaluationResult(object value)
            : this(DirectionTypes.Next, value)
        {
        }
        
        public DirectionTypes Direction
        {
            get { return m_Direction; }
            private set { m_Direction = value; }
        }

        public object Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
        
        public T GetValue<T>()
        {
            return (T)Value;
        }

        public bool IsJump
        {
            get
            {
                return Direction > DirectionTypes.Next;
            }
        }

        public bool IsLoopExit
        {
            get
            {
                return Direction != DirectionTypes.Next && Direction != DirectionTypes.Top;
            }
        }

        public bool IsValue
        {
            get
            {
                return Direction <= DirectionTypes.Return;
            }
        }
    }
}
