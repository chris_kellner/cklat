﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptEvent : EventInfo
    {
        private string m_Name;
        private Type m_EventType;
        private EventAttributes m_Attributes;
        private MethodInfo m_Adder;
        private MethodInfo m_Remover;
        private MethodInfo m_Invoker;

        protected ScriptEvent(string name, Type eventType, EventAttributes attributes)
        {
            m_Name = name;
            m_EventType = eventType;
            m_Attributes = attributes;
        }

        public ScriptEvent(Type owningType, string name, Type eventType)
            : this(name, eventType, EventAttributes.None)
        {
            OwningType = owningType;
        }

        public Type OwningType { get; set; }

        public Type EventType { get { return m_EventType; } }

        public FieldInfo CreateBackingField()
        {
            var fld = new ScriptField(OwningType, "<>_backingField_" + Name, m_EventType,
                FieldAttributes.Private | FieldAttributes.SpecialName);
            m_Adder = new ScriptMethod(
                (t, p) => { 
                    fld.SetValue(t, p[0]); 
                    return null; 
                },
                "add_" + Name,
                typeof(void),
                new ScriptMethodParameter("value", m_EventType));
            m_Remover = new ScriptMethod(
                (t, p) => {
                    var val = (Delegate)fld.GetValue(t);
                    fld.SetValue(t, Delegate.Combine(val, (Delegate)p[0]));
                    return null;
                },
                "remove_" + Name,
                typeof(void),
                new ScriptMethodParameter("value", m_EventType));

            CreateInvoker(fld);
            return fld;
        }

        protected virtual MethodInfo CreateInvoker(FieldInfo eventField)
        {
            var invokeMethod = m_EventType.GetMethod("Invoke", BindingFlags.Public | BindingFlags.Instance);
            m_Invoker = new ScriptMethod(
               (t, p) =>
               {
                   var val = (Delegate)eventField.GetValue(t);
                   return val.DynamicInvoke(p);
               },
               "invoke_" + Name,
               invokeMethod.ReturnType,
               ScriptMethodParameter.FromParameters(invokeMethod.GetParameters()));
            return m_Invoker;
        }

        public override EventAttributes Attributes
        {
            get { return m_Attributes; }
        }

        public override MethodInfo GetAddMethod(bool nonPublic)
        {
            return m_Adder;
        }

        public override MethodInfo GetRaiseMethod(bool nonPublic)
        {
            return m_Invoker;
        }

        public override MethodInfo GetRemoveMethod(bool nonPublic)
        {
            return m_Remover;
        }

        public override Type DeclaringType
        {
            get { return OwningType; }
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return m_Name; }
        }

        public override Type ReflectedType
        {
            get { return OwningType; }
        }
    }
}
