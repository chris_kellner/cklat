﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    #region Inner Types
    [Flags]
    public enum OperatorType
    {
        /// <summary>
        /// The operation is undefined.
        /// </summary>
        Unknown = 0,
        Unary = 1 << 0,
        Binary = 1 << 1,
        #region Unary
        op_UnaryNegation = (1 << 2) | Unary,       // - _
        op_UnaryPlus = (1 << 4) | Unary,           // + _
        op_LogicalNot = (1 << 5) | Unary,          // ! _
        op_Decrement = (1 << 6) | Unary,           // _ -- or -- _
        op_Increment = (1 << 7) | Unary,           // _++ or ++_
        op_OnesComplement = (1 << 8) | Unary,      // ~ _
        #endregion
        #region Binary
        op_Addition = (1 << 9) | Binary,            // _ + _
        op_BitwiseAnd = (1 << 10) | Binary,          // _ & _
        op_BitwiseOr = (1 << 11) | Binary,           // _ | _
        op_Division = (1 << 12) | Binary,            // _ / _
        op_ExclusiveOr = (1 << 13) | Binary,         // _ ^ _
        op_Modulus = (1 << 14) | Binary,             // _ % _
        op_Multiply = (1 << 15) | Binary,            // _ * _
        op_ShiftRight = (1 << 16) | Binary,          // _ >> _
        op_ShiftLeft = (1 << 17) | Binary,           // _ << _
        op_Subtraction = (1 << 18) | Binary,         // _ - _
        #endregion
        #region Comparison
        op_Equality = (1 << 19) | Binary,            // _ == _
        op_GreaterThan = (1 << 20) | Binary,         // _ > _
        op_GreaterThanOrEqual = (1 << 21) | Binary,  // _ >= _
        op_Inequality = (1 << 22) | Binary,          // _ != _
        op_LessThan = (1 << 23) | Binary,            // _ < _
        op_LessThanOrEqual = (1 << 24) | Binary,     // _ <= _
        op_LogicalAnd = (1 << 25) | Binary,          // _ && _
        op_LogicalOr = (1 << 26) | Binary,           // _ || _
        #endregion
    }
    #endregion

    public class ExpressionProcessor
    {
        #region Static Members
        private static Dictionary<Type, Func<object, object, bool>> s_Equality = new Dictionary<Type, Func<object, object, bool>>()
        {
            { typeof(double), (_1,_2)=> (double)_1 == (double)_2 },
            { typeof(float), (_1,_2)=> (float)_1 == (float)_2 },
            { typeof(char), (_1,_2) => (char)_1 == (char)_2 },
            { typeof(short), (_1,_2) => (short)_1 == (short)_2 },
            { typeof(ushort), (_1,_2) => (ushort)_1 == (ushort)_2 },
            { typeof(int), (_1,_2) => (int)_1 == (int)_2 },
            { typeof(uint), (_1,_2) => (uint)_1 == (uint)_2 },
            { typeof(long), (_1,_2) => (long)_1 == (long)_2 },
            { typeof(ulong), (_1,_2) => (ulong)_1 == (ulong)_2 },
            { typeof(string), (_1,_2)=> (string)_1 == (string)_2 }
        };

        private static Dictionary<Type, Func<object, object, object>> s_Adders = new Dictionary<Type, Func<object, object, object>>()
        {
            { typeof(double), (_1,_2)=> (double)_1 + (double)_2 },
            { typeof(float), (_1,_2)=> (float)_1 + (float)_2 },
            { typeof(char), (_1,_2) => (char)_1 + (char)_2 },
            { typeof(short), (_1,_2) => (short)_1 + (short)_2 },
            { typeof(ushort), (_1,_2) => (ushort)_1 + (ushort)_2 },
            { typeof(int), (_1,_2) => (int)_1 + (int)_2 },
            { typeof(uint), (_1,_2) => (uint)_1 + (uint)_2 },
            { typeof(long), (_1,_2) => (long)_1 + (long)_2 },
            { typeof(ulong), (_1,_2) => (ulong)_1 + (ulong)_2 },
            { typeof(string), (_1,_2)=> String.Concat(_1, _2) }
        };

        private static Dictionary<Type, Func<object, object>> s_Incrementers = new Dictionary<Type, Func<object, object>>()
        {
            { typeof(double), (_1)=> (double)_1 + 1 },
            { typeof(float), (_1)=> (float)_1 + 1},
            { typeof(char), (_1) => (char)_1 + 1},
            { typeof(short), (_1) => (short)_1 + 1 },
            { typeof(ushort), (_1) => (ushort)_1 + 1 },
            { typeof(int), (_1) => (int)_1 + 1 },
            { typeof(uint), (_1) => (uint)_1 + 1 },
            { typeof(long), (_1) => (long)_1 + 1},
            { typeof(ulong), (_1) => (ulong)_1 + 1 },
        };

        private static Dictionary<Type, Func<object, object>> s_Decrementers = new Dictionary<Type, Func<object, object>>()
        {
            { typeof(double), (_1)=> (double)_1 - 1 },
            { typeof(float), (_1)=> (float)_1 - 1},
            { typeof(char), (_1) => (char)_1 - 1},
            { typeof(short), (_1) => (short)_1 - 1 },
            { typeof(ushort), (_1) => (ushort)_1 - 1 },
            { typeof(int), (_1) => (int)_1 - 1 },
            { typeof(uint), (_1) => (uint)_1 - 1 },
            { typeof(long), (_1) => (long)_1 - 1},
            { typeof(ulong), (_1) => (ulong)_1 - 1 },
        };

        private static Dictionary<Type, Func<object, object, object>> s_Subtracters = new Dictionary<Type, Func<object, object, object>>()
        {
            { typeof(double), (_1,_2)=> (double)_1 - (double)_2 },
            { typeof(float), (_1,_2)=> (float)_1 - (float)_2 },
            { typeof(char), (_1,_2) => (char)_1 - (char)_2 },
            { typeof(short), (_1,_2) => (short)_1 - (short)_2 },
            { typeof(ushort), (_1,_2) => (ushort)_1 - (ushort)_2 },
            { typeof(int), (_1,_2) => (int)_1 - (int)_2 },
            { typeof(uint), (_1,_2) => (uint)_1 - (uint)_2 },
            { typeof(long), (_1,_2) => (long)_1 - (long)_2 },
            { typeof(ulong), (_1,_2) => (ulong)_1 - (ulong)_2 }
        };

        private static Dictionary<Type, Func<object, object, object>> s_Multipliers = new Dictionary<Type, Func<object, object, object>>()
        {
            { typeof(double), (_1,_2)=> (double)_1 * (double)_2 },
            { typeof(float), (_1,_2)=> (float)_1 * (float)_2 },
            { typeof(char), (_1,_2) => (char)_1 * (char)_2 },
            { typeof(short), (_1,_2) => (short)_1 * (short)_2 },
            { typeof(ushort), (_1,_2) => (ushort)_1 * (ushort)_2 },
            { typeof(int), (_1,_2) => (int)_1 * (int)_2 },
            { typeof(uint), (_1,_2) => (uint)_1 * (uint)_2 },
            { typeof(long), (_1,_2) => (long)_1 * (long)_2 },
            { typeof(ulong), (_1,_2) => (ulong)_1 * (ulong)_2 }
        };

        private static Dictionary<Type, Func<object, object, object>> s_Dividers = new Dictionary<Type, Func<object, object, object>>()
        {
            { typeof(double), (_1,_2)=> (double)_1 / (double)_2 },
            { typeof(float), (_1,_2)=> (float)_1 / (float)_2 },
            { typeof(char), (_1,_2) => (char)_1 / (char)_2 },
            { typeof(short), (_1,_2) => (short)_1 / (short)_2 },
            { typeof(ushort), (_1,_2) => (ushort)_1 / (ushort)_2 },
            { typeof(int), (_1,_2) => (int)_1 / (int)_2 },
            { typeof(uint), (_1,_2) => (uint)_1 / (uint)_2 },
            { typeof(long), (_1,_2) => (long)_1 / (long)_2 },
            { typeof(ulong), (_1,_2) => (ulong)_1 / (ulong)_2 }
        };
        #endregion

        #region Properties
        public Binder Binder { get; set; }
        #endregion

        protected virtual OperatorType GetOperationType(ASTNode left, ASTNode right, string operation)
        {
            switch (operation)
            {
                case "+":
                    return OperatorType.op_Addition;
                case "-":
                    if (left == null) { return OperatorType.op_UnaryNegation; }
                    return OperatorType.op_Subtraction;
                case "*":
                    return OperatorType.op_Multiply;
                case "/":
                    return OperatorType.op_Division;
                case "%":
                    return OperatorType.op_Modulus;
                case "|":
                    return OperatorType.op_BitwiseOr;
                case "&":
                    return OperatorType.op_BitwiseAnd;
                case "^":
                    return OperatorType.op_ExclusiveOr;
                case "==":
                    return OperatorType.op_Equality;
                case "!=":
                    return OperatorType.op_Inequality;
                case ">":
                    return OperatorType.op_GreaterThan;
                case "<":
                    return OperatorType.op_LessThan;
                case ">=":
                    return OperatorType.op_GreaterThanOrEqual;
                case "<=":
                    return OperatorType.op_LessThanOrEqual;
                case "&&":
                    return OperatorType.op_LogicalAnd;
                case "||":
                    return OperatorType.op_LogicalOr;
                case "!":
                    return OperatorType.op_LogicalNot;
                case "~":
                    return OperatorType.op_OnesComplement;
                case "++":
                    return OperatorType.op_Increment;
                case "--":
                    return OperatorType.op_Decrement;
            }

            return OperatorType.Unknown;
        }


        public virtual object Evaluate(ScriptEvaluator evaluator, ASTNode left, ASTNode right, OperatorType operation)
        {
            OperatorType op = operation;
            if ((op & OperatorType.Unary) == OperatorType.Unary)
            {
                return EvaluateUnary(evaluator, left, right, op);
            }
            else
            {
                return EvaluateBinary(evaluator, evaluator.Evaluate<object>(left), right, op);
            }
        }

        public object EvaluateUnary(ScriptEvaluator evaluator, ASTNode left, ASTNode right, OperatorType operation)
        {
            var target = evaluator.Evaluate<object>(left ?? right);
            switch (operation)
            {
                case OperatorType.op_UnaryNegation:
                    return Subtract(0, target);
                case OperatorType.op_LogicalNot:
                    return !((bool)(target));
                case OperatorType.op_OnesComplement:
                    break;
                case OperatorType.op_Increment:
                case OperatorType.op_Decrement:
                    // Save the old value for postfix operations
                    var retVal = target;
                    if (operation == OperatorType.op_Increment)
                    {
                        target = Increment(target);
                    }
                    else
                    {
                        target = Decrement(target);
                    }

                    // Store the new value
                    evaluator.MakeAssignment(left ?? right, target);

                    // If left is null it is a prefix operation
                    if (left == null)
                    {
                        retVal = target;
                    }

                    return retVal;
            }

            return null;
        }

        public object EvaluateBinary(ScriptEvaluator evaluator, object left, ASTNode rightNode, OperatorType operation)
        {
            // Logical grouping operators must short-circuit
            if (operation == OperatorType.op_LogicalAnd)
            {
                return Convert.ToBoolean(left) && evaluator.Evaluate<bool>(rightNode);
            }
            else if (operation == OperatorType.op_LogicalOr)
            {
                return Convert.ToBoolean(left) || evaluator.Evaluate<bool>(rightNode);
            }
            else
            {
                var right = evaluator.Evaluate<object>(rightNode);
                switch (operation)
                {
                    case OperatorType.op_Addition:
                        return Add(left, right);
                    case OperatorType.op_Subtraction:
                        return Subtract(left, right);
                    case OperatorType.op_Multiply:
                        return Multiply(left, right);
                    case OperatorType.op_Division:
                        return Divide(left, right);
                    case OperatorType.op_Modulus:
                    case OperatorType.op_BitwiseOr:
                    case OperatorType.op_BitwiseAnd:
                    case OperatorType.op_ExclusiveOr:
                        break;
                    case OperatorType.op_Equality:
                        return CheckEquals(left, right);
                    case OperatorType.op_Inequality:
                        return CheckNotEquals(left, right);
                    case OperatorType.op_GreaterThan:
                        return Compare(left, right) > 0;
                    case OperatorType.op_LessThan:
                        return Compare(left, right) < 0;
                    case OperatorType.op_GreaterThanOrEqual:
                        return Compare(left, right) >= 0;
                    case OperatorType.op_LessThanOrEqual:
                        return Compare(left, right) <= 0;
                }
            }

            return null;
        }

        #region Helpers
        protected virtual int Compare(object left, object right)
        {
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            var cLeft = left as IComparable;
            var cRight = right as IComparable;
            if (cLeft == null)
            {
                if (cRight == null)
                {
                    return 0;
                }

                return -1;
            }
            else if (cRight == null)
            {
                return 1;
            }

            return cLeft.CompareTo(cRight);
        }

        protected virtual object Add(object left, object right)
        {
            Func<object, object, object> adder;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Adders.TryGetValue(left.GetType(), out adder))
            {
                return adder(left, right);
            }

            return left.GetType().InvokeMember("op_addition",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual object Subtract(object left, object right)
        {
            Func<object, object, object> subtracter;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Subtracters.TryGetValue(left.GetType(), out subtracter))
            {
                return subtracter(left, right);
            }

            return left.GetType().InvokeMember("op_subtraction",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual object Multiply(object left, object right)
        {
            Func<object, object, object> subtracter;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Multipliers.TryGetValue(left.GetType(), out subtracter))
            {
                return subtracter(left, right);
            }

            return left.GetType().InvokeMember("op_multiply",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual object Divide(object left, object right)
        {
            Func<object, object, object> subtracter;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Dividers.TryGetValue(left.GetType(), out subtracter))
            {
                return subtracter(left, right);
            }

            return left.GetType().InvokeMember("op_division",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual object Increment(object left)
        {
            Func<object, object> incrementer;
            var lType = left.GetType();
            if (s_Incrementers.TryGetValue(left.GetType(), out incrementer))
            {
                return incrementer(left);
            }

            return left.GetType().InvokeMember("op_Increment",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left });
        }

        protected virtual object Decrement(object left)
        {
            Func<object, object> incrementer;
            var lType = left.GetType();
            if (s_Decrementers.TryGetValue(left.GetType(), out incrementer))
            {
                return incrementer(left);
            }

            return left.GetType().InvokeMember("op_Decrement",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left });
        }

        protected virtual bool CheckEquals(object left, object right)
        {
            Func<object, object, bool> subtracter;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Equality.TryGetValue(left.GetType(), out subtracter))
            {
                return subtracter(left, right);
            }

            return (bool)left.GetType().InvokeMember("op_Equality",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual bool CheckNotEquals(object left, object right)
        {
            Func<object, object, bool> subtracter;
            var lType = left.GetType();
            var rType = right.GetType();
            left = Upscale(left, rType);
            right = Upscale(right, lType);
            if (s_Equality.TryGetValue(left.GetType(), out subtracter))
            {
                return !subtracter(left, right);
            }

            return (bool)left.GetType().InvokeMember("op_Inequality",
                BindingFlags.InvokeMethod, Binder, null, new object[] { left, right });
        }

        protected virtual object Upscale(object obj, Type tOther)
        {
            return SystemUpscale(obj, tOther);
        }

        public static object SystemUpscale(object obj, Type tOther)
        {
            var code = GetTargetTypeCode(obj != null ? obj.GetType() : typeof(void), tOther);
            if (code != TypeCode.DBNull && code != TypeCode.Empty && code != TypeCode.Object)
            {
                if (Type.GetTypeCode(obj.GetType()) != code)
                {
                    return Convert.ChangeType(obj, code);
                }
            }

            return obj;
        }

        private static TypeCode GetTargetTypeCode(Type tLeft, Type tRight)
        {
            TypeCode lCode = Type.GetTypeCode(tLeft);
            TypeCode rCode = Type.GetTypeCode(tRight);
            if (rCode > lCode)
            {
                return rCode;
            }

            return lCode;
        }
        #endregion
    }
}
