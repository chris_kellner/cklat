﻿using CKLib.Utilities.Interception;
using CKLib.Utilities.Interception.Behaviors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptBinder : Binder
    {
        private class BinderState
        {
            public object[] args;
        }

        private static readonly IInterceptorFactory m_Interceptor = new ProxyInterceptorFactory();

        public bool AllowCodeGeneration { get; set; }

        public override FieldInfo BindToField(BindingFlags bindingAttr, FieldInfo[] match, object value, CultureInfo culture)
        {
            return Type.DefaultBinder.BindToField(bindingAttr, match, value, culture);
        }

        public override MethodBase BindToMethod(BindingFlags bindingAttr, MethodBase[] match, ref object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] names, out object state)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            BinderState theState = new BinderState();
            state = theState;
            args = args ?? new object[0];
            object[] newArgs = (object[])args.Clone();
            theState.args = newArgs;
            MethodBase bestMethod = null;
            int bestChange = Int32.MaxValue;
            for (int i = 0; i < match.Length; i++)
            {
                // Count the number of indexes that match. 
                int count = 0;
                int changeCount = 0;
                ParameterInfo[] parameters = match[i].GetParameters();
                // Go on to the next property if the number of indexes do not match. 
                if (args.Length != parameters.Length)
                    continue;
                // Match each of the indexes that the user expects the property to have. 
                for (int j = 0; j < args.Length; j++)
                {
                    // Determine whether the types specified by the user can be converted to index type. 
                    object newValue;
                    if (TryChangeType(newArgs[j], parameters[j].ParameterType, culture, out newValue))
                    {
                        if (newValue != null)
                        {
                            args[j] = newValue;
                            ++changeCount;
                        }
                        else
                        {
                            args[j] = newArgs[j];
                        }

                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                // Determine whether the method has been found. 
                if (count == args.Length && changeCount < bestChange)
                {
                    if (changeCount == 0)
                    {
                        return match[i];
                    }
                    else
                    {
                        bestMethod = match[i];
                        bestChange = changeCount;
                    }
                }
            }

            if (bestMethod != null)
            {
                return bestMethod;
            }

            state = null;
            return null;
        }

        public override object ChangeType(object value, Type toType, CultureInfo culture)
        {
            object newValue;
            TryChangeType(value, toType, culture, out newValue);
            return newValue;
        }

        public bool TryChangeType(object value, Type toType, CultureInfo culture, out object newValue)
        {
            newValue = null;
            if (toType.IsInstanceOfType(value)) { return true; }

            // Determine whether the value parameter can be converted to a value of type myType.
            if (value.GetType().IsPrimitive && toType.IsPrimitive)
            {
                try
                {
                    newValue = Convert.ChangeType(value, toType);
                    return true;
                }
                catch (InvalidCastException) { return false; }
            }

            if (typeof(Delegate).IsAssignableFrom(toType))
            {
                var sm = value as ScriptMethod;
                if (sm != null)
                {
                    Delegate d = ScriptMethodExtensions.MakeDelegate(toType, sm, null, false);
                    if (d != null)
                    {
                        newValue = d;
                        return true;
                    }
                }

                var func = value as ScriptFunction;
                if (func != null)
                {
                    Delegate d = ScriptMethodExtensions.CastDelegate(toType, func);
                    if (d != null)
                    {
                        newValue = d;
                        return true;
                    }
                }
            }

            /*
            var cvt = TypeDescriptor.GetConverter(value);
            if (cvt.CanConvertTo(toType))
            {
                newValue = cvt.ConvertTo(value, toType);
                return true;
            }

            cvt = TypeDescriptor.GetConverter(toType);
            if (cvt.CanConvertFrom(value.GetType()))
            {
                newValue = cvt.ConvertFrom(value);
                return true;
            }*/

            if (AllowCodeGeneration && m_Interceptor.IsCompatible(value, toType))
            {
                newValue = m_Interceptor.CreateInterceptor(value, toType, null);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CanConvertFrom(Type fromType, Type toType)
        {
            if (fromType.IsPrimitive && toType.IsPrimitive)
            {
                return System.ComponentModel.TypeDescriptor.GetConverter(fromType).CanConvertTo(toType);
            }

            return AllowCodeGeneration && m_Interceptor.IsCompatible(fromType, toType);
        }

        public override void ReorderArgumentArray(ref object[] args, object state)
        {
            if (state != null)
            {
                args = ((BinderState)state).args;
            }
        }

        public override MethodBase SelectMethod(BindingFlags bindingAttr, MethodBase[] match, Type[] types, ParameterModifier[] modifiers)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            for (int i = 0; i < match.Length; i++)
            {
                // Count the number of indexes that match. 
                int count = 0;
                ParameterInfo[] parameters = match[i].GetParameters();
                // Go on to the next property if the number of indexes do not match. 
                if (types.Length != parameters.Length)
                    continue;
                // Match each of the indexes that the user expects the property to have. 
                for (int j = 0; j < types.Length; j++)
                    // Determine whether the types specified by the user can be converted to index type. 
                    if (CanConvertFrom(types[j], parameters[j].ParameterType))
                        count += 1;
                    else
                        break;
                // Determine whether the method has been found. 
                if (count == types.Length)
                {
                    return match[i];
                }
            }
            return null;
        }

        public override PropertyInfo SelectProperty(BindingFlags bindingAttr, PropertyInfo[] match, Type returnType, Type[] indexes, ParameterModifier[] modifiers)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            for (int i = 0; i < match.Length; i++)
            {
                // Count the number of indexes that match. 
                int count = 0;
                ParameterInfo[] parameters = match[i].GetIndexParameters();
                // Go on to the next property if the number of indexes do not match. 
                if (indexes.Length != parameters.Length)
                    continue;
                // Match each of the indexes that the user expects the property to have. 
                for (int j = 0; j < indexes.Length; j++)
                    // Determine whether the types specified by the user can be converted to index type. 
                    if (CanConvertFrom(indexes[j], parameters[j].ParameterType))
                        count += 1;
                    else
                        break;
                // Determine whether the property has been found. 
                if (count == indexes.Length)
                    // Determine whether the return type can be converted to the properties type. 
                    if (CanConvertFrom(returnType, match[i].PropertyType))
                        return match[i];
                    else
                        continue;
            }
            return null;
        }
    }
}
