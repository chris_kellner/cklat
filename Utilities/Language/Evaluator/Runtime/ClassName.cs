﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ClassName : IMetadataAccessible
    {
        private static readonly MemberInvoker s_Invoker = new MemberInvoker(null, MemberInvoker.CLASS_FLAGS);

        public ClassName(Type type)
        {
            Class = type;
        }

        public Type Class { get; private set; }

        public void SetMember(string name, object value)
        {
            s_Invoker.SetMember(null, Class, name, null, value);
        }

        public TValue GetMember<TValue>(string name)
        {
            return (TValue)s_Invoker.GetMember(null, Class, name, null);
        }

        public TValue InvokeMember<TValue>(string name, params object[] parameters)
        {
            MethodBase method;
            return (TValue)s_Invoker.InvokeMethod(null, Class, name, null, parameters, out method);
        }
    }
}
