﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptProperty : PropertyInfo
    {
        #region Variables
        private readonly string m_Name;
        private readonly Type m_PropertyType;
        private MethodInfo m_Getter;
        private MethodInfo m_Setter;
        private ParameterInfo[] m_Indexers;
        private AttributeCollection m_CustomAttributes;
        #endregion

        #region Constructors
        protected ScriptProperty(string name, Type propertyType)
        {
            m_Name = name;
            m_PropertyType = propertyType;
            m_CustomAttributes = new AttributeCollection();
        }

        public ScriptProperty(Type owningType, string name, Type propertyType)
            : this(name, propertyType)
        {
            OwningType = owningType;
        }

        public ScriptProperty(Type owningType, Type propertyType, ParameterInfo[] indexParameters)
            : this(owningType, "Item", propertyType)
        {
            m_Indexers = indexParameters;
        }
        #endregion

        public Type OwningType { get; set; }

        public FieldInfo CreateBackingField()
        {
            var fld = new ScriptField(OwningType, "<>_backingField_" + Name, PropertyType,
                FieldAttributes.Private | FieldAttributes.SpecialName);
            m_Setter = new ScriptMethod(
                (t, p) => { fld.SetValue(t, p[0]); return null; }, 
                "set_" + Name, 
                typeof(void),
                new ScriptMethodParameter("value", PropertyType));
            m_Getter = new ScriptMethod(
                (t, p) => fld.GetValue(t),
                "get_" + Name,
                PropertyType);
            return fld;
        }

        #region Base
        public override PropertyAttributes Attributes
        {
            get { return PropertyAttributes.None; }
        }

        public override bool CanRead
        {
            get { return m_Getter != null; }
        }

        public override bool CanWrite
        {
            get { return m_Setter != null; }
        }

        public override MethodInfo[] GetAccessors(bool nonPublic)
        {
            throw new NotImplementedException();
        }

        public override MethodInfo GetGetMethod(bool nonPublic)
        {
            if (m_Getter != null && m_Getter.IsPublic || nonPublic)
            {
                return m_Getter;
            }

            return null;
        }

        public override ParameterInfo[] GetIndexParameters()
        {
            return m_Indexers;
        }

        public override MethodInfo GetSetMethod(bool nonPublic)
        {
            if (m_Setter != null && m_Setter.IsPublic || nonPublic)
            {
                return m_Setter;
            }

            return null;
        }

        public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, System.Globalization.CultureInfo culture)
        {
            return m_Getter.Invoke(obj, invokeAttr, binder, index, culture); 
        }

        public override Type PropertyType
        {
            get { return m_PropertyType; }
        }

        public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, System.Globalization.CultureInfo culture)
        {
            object[] args = null;
            if (index != null && index.Length > 0)
            {
                args = new object[index.Length + 1];
                index.CopyTo(args, 0);
                args[args.Length - 1] = value;
            }
            else
            {
                args = new object[] { value };
            }

            m_Setter.Invoke(obj, invokeAttr, binder, args, culture);
        }

        public override Type DeclaringType
        {
            get { return OwningType; }
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            return m_CustomAttributes
                .Cast<Attribute>()
                .Where(a => attributeType.IsInstanceOfType(a))
                .ToArray();
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return m_Name; }
        }

        public override Type ReflectedType
        {
            get { return OwningType; }
        }
        #endregion
    }
}
