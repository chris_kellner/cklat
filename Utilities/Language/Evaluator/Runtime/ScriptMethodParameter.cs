﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptMethodParameter : ParameterInfo
    {
        public ScriptMethodParameter(string name)
        {
            if (name == null)
            {
                throw new ArgumentException("Cannot have unnamed parameter", "name");
            }

            NameImpl = name;
            this.AttrsImpl |= ParameterAttributes.In;
        }

        public ScriptMethodParameter(string name, Type type)
            : this(name)
        {
            ClassImpl = type;
        }

        public ScriptMethodParameter(string name, object defaultValue)
            : this(name)
        {
            DefaultValueImpl = defaultValue;
            this.AttrsImpl |= (ParameterAttributes.HasDefault | ParameterAttributes.Optional);
        }

        public ScriptMethodParameter(string name, Type type, ParameterAttributes attributes)
            : this(name, type)
        {
            this.AttrsImpl = attributes;
        }

        public ScriptMethodParameter(string name, Type type, object defaultValue)
            : this(name, type)
        {
            DefaultValueImpl = defaultValue;
            this.AttrsImpl |= (ParameterAttributes.HasDefault | ParameterAttributes.Optional);
        }

        public ScriptMethodParameter(ParameterInfo fromParameter)
            : this(fromParameter.Name, fromParameter.ParameterType, fromParameter.Attributes)
        {
            DefaultValueImpl = fromParameter.DefaultValue;
            PositionImpl = fromParameter.Position;
        }

        public static ScriptMethodParameter[] FromParameters(ParameterInfo[] parameters)
        {
            ScriptMethodParameter[] newParams = new ScriptMethodParameter[parameters.Length];
            for (int i = 0; i < newParams.Length; i++)
            {
                newParams[i] = new ScriptMethodParameter(parameters[i]);
            }
            return newParams;
        }
    }
}
