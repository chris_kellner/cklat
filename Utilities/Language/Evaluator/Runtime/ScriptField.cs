﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class ScriptField : FieldInfo
    {
        #region Fields
        private readonly string m_Name;
        private readonly Type m_Type;
        private readonly FieldAttributes m_Attributes;
        #endregion

        #region Constructors
        protected ScriptField(string fieldName, Type fieldType)
            : this(null, fieldName, fieldType, FieldAttributes.Public | FieldAttributes.Static)
        {
        }

        public ScriptField(Type owningType, string fieldName, Type fieldType)
            : this(owningType, fieldName, fieldType, FieldAttributes.Public)
        {
        }

        public ScriptField(Type owningType, string fieldName, Type fieldType, FieldAttributes attributes)
        {
            m_Name = fieldName;
            m_Type = fieldType;
            m_Attributes = attributes;
            OwningType = owningType;
        }
        #endregion

        public Type OwningType { get; set; }

        #region Base
        public override FieldAttributes Attributes
        {
            get { return m_Attributes; }
        }

        public override RuntimeFieldHandle FieldHandle
        {
            get { throw new NotImplementedException(); }
        }

        public override Type FieldType
        {
            get { return m_Type; }
        }

        public override object GetValue(object obj)
        {
            if ((Attributes & FieldAttributes.Static) == FieldAttributes.Static)
            {
                if (OwningType is IMetadataAccessible)
                {
                    return ((IMetadataAccessible)OwningType).GetMember<object>(m_Name);
                }

                throw new MemberAccessException("Owning type does not implement IMetadataAccessible");
            }
            else
            {
                var meta = obj as IMetadataAccessible;
                if (meta == null)
                {
                    throw new MemberAccessException(
                        string.Format("Cannot access instance field '{0}' on non metadata object", m_Name));
                }
                else
                {
                    return meta.GetMember<object>(m_Name);
                }
            }
        }

        public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, System.Globalization.CultureInfo culture)
        {
            if ((Attributes & FieldAttributes.Static) == FieldAttributes.Static)
            {
                if (OwningType is IMetadataAccessible)
                {
                    ((IMetadataAccessible)OwningType).SetMember(m_Name, value);
                }

                throw new MemberAccessException("Owning type does not implement IMetadataAccessible");
            }
            else
            {
                var meta = obj as IMetadataAccessible;
                if (meta == null)
                {
                    throw new MemberAccessException(
                        string.Format("Cannot access instance field '{0}' on non metadata object", m_Name));
                }
                else
                {
                    meta.SetMember(m_Name, value);
                }
            }
        }

        public override Type DeclaringType
        {
            get { return OwningType; }
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return m_Name; }
        }

        public override Type ReflectedType
        {
            get { return OwningType; }
        }
        #endregion
    }
}
