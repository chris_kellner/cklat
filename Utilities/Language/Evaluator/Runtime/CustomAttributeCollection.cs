﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public class CustomAttributeCollection : IEnumerable<Attribute>, ICollection<Attribute>
    {
        #region Variables
        private List<Attribute> m_Attributes;
        #endregion

        #region Constructors
        public CustomAttributeCollection()
        {
            m_Attributes = new List<Attribute>();
        }

        public CustomAttributeCollection(IEnumerable<Attribute> items)
        {
            m_Attributes = new List<Attribute>(items);
        }
        #endregion

        #region ICollection
        public void Add(Attribute item)
        {
            m_Attributes.Add(item);
        }

        public void Clear()
        {
            m_Attributes.Clear();
        }

        public bool Contains(Attribute item)
        {
            return m_Attributes.Contains(item);
        }

        public void CopyTo(Attribute[] array, int arrayIndex)
        {
            m_Attributes.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return m_Attributes.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Attribute item)
        {
            return m_Attributes.Remove(item);
        }

        public IEnumerator<Attribute> GetEnumerator()
        {
            return m_Attributes.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
