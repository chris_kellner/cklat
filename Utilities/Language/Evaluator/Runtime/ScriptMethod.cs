﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public delegate object ScriptFunction(params object[] parameters);
    public delegate object ScriptInstanceFunction(object target, params object[] parameters);

    public class ScriptMethod : MethodInfo
    {
        private static long s_Anonymous_Method_Index = 0;
        private static readonly object S_ANON_LOCK = new object();
        private ScriptContext m_Context;
        private ScriptVariable[] m_Closure;
        private ScriptMethodParameter[] m_Parameters;
        private Type[] m_ParameterTypes;
        private Type m_ReturnType;
        private string m_Name;
        private readonly ASTNode m_MethodBody;
        private readonly ScriptFunction m_WrappedFunction;
        private readonly ScriptInstanceFunction m_WrappedInstanceFunction;
        private bool m_HasOutParameters;
        private MethodAttributes m_MethodAttributes;

        protected ScriptMethod(string name, Type returnType, ScriptMethodParameter[] parameters)
        {
            m_ReturnType = returnType ?? typeof(object);
            m_Parameters = parameters ?? new ScriptMethodParameter[0];
            m_Name = name ?? string.Format("<{0}>_anonymous", NextAnonymousIndex());
            for (int i = 0; i < m_Parameters.Length; i++)
            {
                if (m_Parameters[i].IsRetval || m_Parameters[i].ParameterType.IsByRef)
                {
                    m_HasOutParameters = true;
                    break;
                }
            }
        }

        public ScriptMethod(ScriptInstanceFunction methodBody, string name, Type returnType, params ScriptMethodParameter[] parameters)
            : this(name, returnType, parameters)
        {
            m_WrappedInstanceFunction = methodBody;
        }

        public ScriptMethod(ScriptFunction methodBody, string name, Type returnType, params ScriptMethodParameter[] parameters)
            : this(name, returnType, parameters)
        {
            m_MethodAttributes |= MethodAttributes.Static;
            m_WrappedFunction = methodBody;
        }

        public ScriptMethod(ASTNode methodBody, string name, Type returnType, params ScriptMethodParameter[] parameters)
            : this(name, returnType, parameters)
        {
            m_MethodBody = methodBody;
        }

        public ScriptMethod(ScriptContext context, ASTNode methodBody, string name, Type returnType, params ScriptMethodParameter[] parameters)
            : this(methodBody, name, returnType, parameters)
        {
            m_Context = context;
        }

        private static long NextAnonymousIndex()
        {
            lock (S_ANON_LOCK)
            {
                return s_Anonymous_Method_Index++;
            }
        }

        public override string Name
        {
            get
            {
                return m_Name;
            }
        }

        public override Type ReturnType
        {
            get
            {
                return m_ReturnType;
            }
        }

        public ScriptVariable[] Closure
        {
            get
            {
                return m_Closure;
            }
            set
            {
                m_Closure = value;
            }
        }

        public override MethodAttributes Attributes
        {
            get
            {
                return m_MethodAttributes;
            }
        }

        public override MethodImplAttributes GetMethodImplementationFlags()
        {
            return MethodImplAttributes.Managed;
        }

        public override ParameterInfo[] GetParameters()
        {
            return m_Parameters;
        }

        public virtual TReturn Invoke<TReturn>(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, System.Globalization.CultureInfo culture)
        {
            var ret = Invoke(obj, invokeAttr, binder, parameters, culture);

            return (TReturn)ret;
        }

        public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, System.Globalization.CultureInfo culture)
        {
            var ctx = m_Context;
            using (var key = ctx.NewStackFrame())
            {
                ctx.SetLocal(ScriptContext.RESERVED_NAME_THIS, obj);
                if (m_Closure != null)
                {
                    // Push the closure variable references into the context
                    for (int i = 0; i < m_Closure.Length; i++)
                    {
                        ctx.SetLocal(m_Closure[i]);
                    }
                }

                using (var scope = ctx.NewScope())
                {
                    object rslt = null;
                    if (m_WrappedFunction != null)
                    {
                        rslt = m_WrappedFunction(parameters);
                    }
                    else if (m_WrappedInstanceFunction != null)
                    {
                        rslt = m_WrappedInstanceFunction(obj, parameters);
                    }
                    else
                    {
                        // Set the parameters into the context
                        for (int i = 0; i < m_Parameters.Length; i++)
                        {
                            // Be sure to declare the local first in the new scope
                            //  so as not to override a closure variable
                            var cParam = m_Parameters[i];
                            var variable = ctx.DeclareLocal(cParam.Name, cParam.ParameterType);
                            variable.Value = parameters[i];
                        }

                        // Evaluate the method
                        var eval = new ScriptEvaluator(ctx);
                        rslt = eval.Evaluate(ReturnType, m_MethodBody);
                    }

                    // Assign out parameters
                    if (m_HasOutParameters)
                    {
                        for (int i = 0; i < m_Parameters.Length; i++)
                        {
                            var cParam = m_Parameters[i];
                            if (cParam.IsRetval || m_Parameters[i].ParameterType.IsByRef)
                            {
                                var variable = ctx.GetVariable(cParam.Name);
                                parameters[i] = variable.Value;
                            }
                        }
                    }

                    return rslt;
                }
            }
        }

        public ScriptFunction CreateDelegate()
        {
            return (p) => Invoke(null, p);
        }

        public ScriptFunction CreateDelegate(object target)
        {
            return (p) => Invoke(target, p);
        }

        public override RuntimeMethodHandle MethodHandle
        {
            get { throw new NotSupportedException(); }
        }

        private ScriptType m_DeclaringType;
        public override Type DeclaringType
        {
            get
            {
                return m_DeclaringType;
            }
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }

        public override object[] GetCustomAttributes(bool inherit)
        {
            throw new NotImplementedException();
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            throw new NotImplementedException();
        }


        public override Type ReflectedType
        {
            get
            {
                // Todo...
                return DeclaringType;
            }
        }

        public override MethodInfo GetBaseDefinition()
        {
            var type = DeclaringType.BaseType;
            if (type != null)
            {
                return type.GetMethod(Name, GetParameterTypes());
            }

            return null;
        }

        internal virtual Type[] GetParameterTypes()
        {
            if (m_ParameterTypes == null)
            {
                m_ParameterTypes = GetParameters().Select(p => p.ParameterType).ToArray();
            }

            return m_ParameterTypes;
        }

        public override ICustomAttributeProvider ReturnTypeCustomAttributes
        {
            get { throw new NotImplementedException(); }
        }
    }
}
