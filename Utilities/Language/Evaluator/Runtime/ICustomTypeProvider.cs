﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public interface ICustomTypeProvider
    {
        Type GetCustomType();
    }
}
