﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator.Runtime
{
    public static class ReflectionExtensions
    {
        public static bool IsExtensionMethod(this MethodBase method)
        {
            if (method == null)
            {
                throw new ArgumentNullException("method");
            }

            return method.IsDefined(typeof(System.Runtime.CompilerServices.ExtensionAttribute), true);
        }
    }
}
