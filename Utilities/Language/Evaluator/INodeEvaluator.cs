﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public interface INodeEvaluator
    {
        EvaluationResult Evaluate(ScriptEvaluator evaluator, ASTNode tree);

        void MakeAssignment(ScriptEvaluator evaluator, ASTNode tree, object value);

        bool CanAssign { get; }
        bool HasValue { get; }
    }
}
