﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Evaluator
{
    public class EvaluationStack : IEnumerable<object>
    {
        private Stack<object> m_EvaluationStack = new Stack<object>();

        public object Push(object obj)
        {
            m_EvaluationStack.Push(obj);
            return obj;
        }

        public T Push<T>(T obj)
        {
            m_EvaluationStack.Push(obj);
            return obj;
        }

        public object Pop()
        {
            return m_EvaluationStack.Pop();
        }

        public T Pop<T>()
        {
            return (T)m_EvaluationStack.Pop();
        }

        public object Peek()
        {
            return m_EvaluationStack.Peek();
        }

        public T Peek<T>()
        {
            return (T)m_EvaluationStack.Peek();
        }

        public bool Contains(object obj)
        {
            return m_EvaluationStack.Contains(obj);
        }

        public void Clear()
        {
            m_EvaluationStack.Clear();
        }

        public bool IsEmpty
        {
            get
            {
                return m_EvaluationStack.Count == 0;
            }
        }

        public int Count
        {
            get
            {
                return m_EvaluationStack.Count;
            }
        }

        public IEnumerator<object> GetEnumerator()
        {
            return m_EvaluationStack.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
