﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Evaluator.Nodes;

namespace CKLib.Utilities.Language.Samples
{
    public static class CTokens
    {
        #region constants
        private const string REG_FLOAT = @"\b
      -?
      (    \d+(\.[0-9]+)?   #number with optional floating point
      |    \.[0-9]+         #or just starting with floating point
      )
      ([eE][+-]?[0-9]+)? # optional exponent
      [fF]\b  #signifies float";

        private const string REG_DOUBLE = @"\b
      -?
      (
          (
            (    \d+\.[0-9]+  #number with optional floating point
            |    \.[0-9]+        #or just starting with floating point
            )
            ([eE][+-]?[0-9]+)? # optional exponent
            [dD]?
          )
          |
          (
            \d+
            (
                ([eE][+-]?[0-9]+) # optional exponent
                [dD]?  #signifies double
                |
                [dD]  #signifies double
            )
          )
      )\b";
        #endregion

        public const string TN_COMMENT = "Comment",
            TN_COMMENT_LINE = "Line_Comment",
            TN_COMMENT_BLOCK = "Block_Comment",
            TN_NUMBER = "Number",
            TN_LITERAL = "Literal",
            TN_TEXT = "Text",
            TN_STRING = "String",
            TN_CHAR = "Char",
            TN_IDENTIFIER = "Identifier";

        

        public static Terminal Comment(Grammar grammar)
        {
            Terminal comment = new Terminal(grammar)
            {
                Name = TN_COMMENT,
                MatchType = MatchType.Group,
                NonParsed = true,
                NonExecutable = true,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(grammar, TN_COMMENT_LINE, MatchType.Regex, "//.*$"){
                        RegexOptions = RegexOptions.Multiline, NonParsed = true
                    }
                    ,new Terminal(grammar, TN_COMMENT_BLOCK, MatchType.Regex, @"/\*(.*?)\*/"){
                        RegexOptions = RegexOptions.Singleline, NonParsed = true
                    }
                }
            };

            return comment;
        }

        public static Terminal Null(Grammar grammar)
        {
            Terminal @null = new Terminal(grammar, "null", MatchType.Regex, @"\bnull\b")
            {
                Evaluator = new PrimitiveNodeEvaluator((Type)null)
            };

            return @null;
        }

        public static Terminal EndOfLine(Grammar grammar)
        {
            Terminal eol = new Terminal(grammar, "<EOL>", MatchType.Regex, "(\r\n)|(\n\r)|(\n)|(\r)")
            {
                NonParsed = true, NonExecutable = true
            };
            return eol;
        }

        public static Terminal Identifier(Grammar grammar)
        {
            Terminal identifier = new Terminal(grammar, TN_IDENTIFIER, MatchType.Regex, @"\b([a-zA-Z_][\w]*)\b")
            {
                Evaluator = new IdentifierNodeEvaluator() { RequiresDeclaration = true }
            };
            return identifier;
        }

        public static Terminal Boolean(Grammar grammar)
        {
            Terminal boolean = new Terminal(grammar, "Boolean", MatchType.Regex, "[Tt]rue|[Ff]alse")
            {
                Evaluator = new PrimitiveNodeEvaluator(typeof(bool))
            };
            return boolean;
        }

        public static Terminal Number(Grammar grammar)
        {
            Terminal number = new Terminal(grammar)
            {
                Name = TN_NUMBER,
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(grammar, "Float", MatchType.Regex, REG_FLOAT) {
                        Evaluator = new PrimitiveNodeEvaluator(typeof(float)),
                        RegexOptions = RegexOptions.IgnorePatternWhitespace
                    },
                    new Terminal(grammar, "Double", MatchType.Regex, REG_DOUBLE){
                        Evaluator = new PrimitiveNodeEvaluator(typeof(double)),
                        RegexOptions = RegexOptions.IgnorePatternWhitespace
                    },
                    new Terminal(grammar, "Byte", MatchType.Regex, @"\b(\d{1,3}[bB])\b"){
                        Evaluator = new PrimitiveNodeEvaluator(typeof(byte))
                    },
                    new Terminal(grammar, "Int16", MatchType.Regex, @"\b(\d{1,5}[sS])\b"){
                       Evaluator = new PrimitiveNodeEvaluator(typeof(short)),
                    },
                    new Terminal(grammar, "UInt16", MatchType.Regex, @"\b(\d{1,5}[uU][sS])\b"){
                       Evaluator = new PrimitiveNodeEvaluator(typeof(ushort)),
                    },
                    new Terminal(grammar, "Int32", MatchType.Regex, @"\b(\d{1,10})\b"){ 
                       Evaluator = new PrimitiveNodeEvaluator(typeof(int)),
                    },
                    new Terminal(grammar, "UInt32", MatchType.Regex, @"\b((\d{1,10}[uU])|(0[xX][0-9a-fA-F]+))\b"){ 
                        Evaluator = new PrimitiveNodeEvaluator(typeof(uint)),
                    },
                    new Terminal(grammar, "Int64", MatchType.Regex, @"\b(\d{1,19}[lL])\b"){
                       Evaluator = new PrimitiveNodeEvaluator(typeof(long)),
                    },
                    new Terminal(grammar, "UInt64", MatchType.Regex, @"\b(\d{1,19}[uU][lL])\b"){
                       Evaluator = new PrimitiveNodeEvaluator(typeof(ulong)), 
                    },
                }
            };
            return number;
        }

        public static Terminal Text(Grammar grammar)
        {
            Terminal @string = new Terminal(grammar)
            {
                Name = TN_TEXT,
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    String(grammar, TN_STRING, '"', false),
                    new Terminal(grammar, TN_CHAR, MatchType.Regex, "'.'") {
                        Evaluator = new PrimitiveNodeEvaluator(typeof(char))
                    },
                }
            };

            return @string;
        }

        public static Terminal String(Grammar grammar, string name, char quoteChar, bool multiline)
        {
            return new Terminal(grammar, name, MatchType.Regex,
                string.Format(@"(?n){0}([^{0}{1}\\]|\\.)*{0}", quoteChar, multiline ? string.Empty : "\r\n"))
            {
                Evaluator = new StringNodeEvaluator()
                {
                    QuoteChar = quoteChar
                }
            };
        }

        public static Terminal CodeBlock(Grammar grammar, string name, string openMark, string closeMark, char escapeChar)
        {
            return new Terminal(grammar, name, MatchType.Regex,
                string.Format(@"(?ns){0}({2}{1}|.*(?={1}))*?{1}", openMark, closeMark, escapeChar))
            {
                Evaluator = new CodeBlockEvaluator()
                {
                    OpenMark = openMark,
                    CloseMark = closeMark,
                    EscapeChar = escapeChar
                }
            };
        }

        public static Terminal Literal(Grammar grammar, params Terminal[] literalTypes)
        {
            Terminal literal = new Terminal(grammar)
            {
                Name = TN_LITERAL,
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>(literalTypes)
            };

            return literal;
        }
    }
}
