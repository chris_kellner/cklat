﻿using CKLib.Utilities.Language.Evaluator;
using CKLib.Utilities.Language.Evaluator.Nodes;
using CKLib.Utilities.Language.Lex;
using CKLib.Utilities.Language.Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Samples
{
    public class JsonGrammar : Grammar
    {
        public JsonGrammar()
        {
            Name = "Json";
            Version = "1.0";
            CanEvaluate = true;
        }

        protected override void DefineTokens()
        {
            var @string = CTokens.String(this, "string", '"', false);
            var literal = CTokens.Literal(this,
                @string,
                CTokens.Number(this),
                CTokens.Boolean(this),
                CTokens.Null(this));
            var operators = new Terminal(this)
            {
                Name = "operator",
                MatchType = Lex.MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "op_assign", MatchType.Literal, ":"),
                    new Terminal(this, "op_comma", MatchType.Literal, ",")
                }
            };

            var obj = new Terminal(this)
            {
                Name = "Braces",
                MatchType = Lex.MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Brace_Open", MatchType.Literal, "{"),
                    new Terminal(this, "Brace_Close", MatchType.Literal, "}")
                }
            };

            var array = new Terminal(this)
            {
                Name = "Arrays",
                MatchType = Lex.MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Array_Open", MatchType.Literal, "["),
                    new Terminal(this, "Array_Close", MatchType.Literal, "]")
                }
            };

            RootTokens.Add(literal);
            RootTokens.Add(operators);
            RootTokens.Add(obj);
            RootTokens.Add(array);

            DefineTerminalGroup("Array_Open", "Array_Close");
            DefineTerminalGroup("Brace_Open", "Brace_Close");
        }

        protected override void DefinePatterns()
        {
            var memberName = new NonTerminal(this, "membername")
            {
                Pattern = "('member'string)(?=op_assign)",
                Evaluator = new MemberAccessorNodeEvaluator()
                {
                    AccessType = AccessTypes.This,
                    CanAssign = true,
                    EvalName = true
                }
            };

            var pair = new NonTerminal(this, "assignment", "('lvalue'membername) op_assign ('rvalue'Literal|object|array)")
            {
                Evaluator = new AssignmentNodeEvaluator()
            };

            var obj = new NonTerminal(this, "object")
            {
                Evaluator = new ObjectInitializerNodeEvaluator()
                {
                    InitializerType = InitializerTypes.Object,
                    HasValue = true
                },
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "obj_empty", "Brace_Open Brace_Close"),
                    new NonTerminal(this, "obj_full", "Brace_Open ('assignment'assignment) (op_comma ('assignment'assignment))* Brace_Close")
                }
            };
            var array = new NonTerminal(this, "array")
            {
                Evaluator = new ObjectInitializerNodeEvaluator()
                {
                    HasValue = true,
                    InitializerType = InitializerTypes.Array
                },
                SubPatterns = new List<NonTerminal>()
                {
                    new NonTerminal(this, "arr_empty", "Array_Open Array_Close"),
                    new NonTerminal(this, "arr_full", "Array_Open ('item'Literal|object|array) (op_comma ('item'Literal|object|array))* Array_Close")
                }
            };

            RootPatterns.Add(memberName);
            RootPatterns.Add(pair);
            RootPatterns.Add(obj);
            RootPatterns.Add(array);
        }
    }
}
