﻿using CKLib.Utilities.Language.Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Samples
{
    public class CSharpGrammar : MathGrammar
    {
        protected override void DefineTokens()
        {
            base.DefineTokens();
        }

        protected override void DefinePatterns()
        {
            base.DefinePatterns();
            NonTerminal field = new NonTerminal(this, "field")
            {
            };

            NonTerminal method = new NonTerminal(this, "method")
            {
            };

            NonTerminal property = new NonTerminal(this, "property")
            {
            };

            NonTerminal @event = new NonTerminal(this, "event")
            {
            };

            NonTerminal @class = new NonTerminal(this, "class")
            {
                Pattern = @"class Identifier Brace_Open (field|method|constructor|property|event|class)* Brace_Close"
            };
        }
    }
}
