﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Samples;
using CKLib.Utilities.Language.Lex;

namespace CKLib.Utilities.Language
{
    public class ToyGrammar : Grammar
    {
        protected override void DefineTokens()
        {
            Terminal identifier = new Terminal(this, "Identifier", MatchType.Regex, @"\b([a-zA-Z_][\w]*)\b");
            Terminal operation = new Terminal(this)
            {
                Name = "Operation",
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Op_Plus", MatchType.Literal, "+"),
                    new Terminal(this, "Op_Minus", MatchType.Literal, "-"),
                    new Terminal(this, "Op_Multiply", MatchType.Literal, "*"),
                    new Terminal(this, "Op_Divide", MatchType.Literal, "/"),
                }
            };

            Terminal comparison = new Terminal(this)
            {
                Name = "Comparison",
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Cmp_Equals", MatchType.Literal, "=="),
                    new Terminal(this, "Cmp_Inequals", MatchType.Literal, "!="),
                    new Terminal(this, "Cmp_Gt_Or_Eq", MatchType.Literal, ">="),
                    new Terminal(this, "Cmp_Lt_Or_Eq", MatchType.Literal, "<="),
                    new Terminal(this, "Cmp_Greater", MatchType.Literal, ">"),
                    new Terminal(this, "Cmp_Less", MatchType.Literal, "<"),
                }
            };

            Terminal brace = new Terminal(this)
            {
                Name = "Brace",
                MatchType = MatchType.Group,
                SubTokens = new List<Terminal>()
                {
                    new Terminal(this, "Scope_In", MatchType.Literal, "{"),
                    new Terminal(this, "Scope_Out", MatchType.Literal, "}"),
                    new Terminal(this, "Paren_Open", MatchType.Literal, "("),
                    new Terminal(this, "Paren_Close", MatchType.Literal, ")"),
                    new Terminal(this, "Idx_Open", MatchType.Literal, "["),
                    new Terminal(this, "Idx_Close", MatchType.Literal, "]"),
                }
            };

            Terminal memberAccess = new Terminal(this, "Accessor", MatchType.Literal, ".");
            Terminal assignment = new Terminal(this, "Assignment", MatchType.Literal, "=");
            Terminal eos = new Terminal(this, "<EOS>", MatchType.Literal, ";");
            Terminal number = CTokens.Number(this);
            Terminal @string = CTokens.Text(this);
            Terminal comment = CTokens.Comment(this);
        }

        protected override void DefinePatterns()
        {
        }
    }
}
