﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Validation
{
    public struct CodeSample
    {
        public CodeSample(string code)
            : this(code, 0)
        {
        }

        public CodeSample(string code, int targetIndex)
        {
            this = default(CodeSample);
            Code = code;
            TargetIndex = targetIndex;
        }

        public string Code { get; private set; }
        public int TargetIndex { get; private set; }
    }
}
