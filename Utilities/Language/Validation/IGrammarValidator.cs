﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Validation
{
    public interface IGrammarValidator
    {
        ValidationResult Validate();
        IGrammarElement GrammarElement { get; }
    }
}
