﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Validation
{
    public class ValidationResult
    {
        public IGrammarElement Element { get; protected set; }
        public ValidationStatus Status { get; set; }

        public override string ToString()
        {
            return string.Format("Validation for element: [{0}] -> {1}", Element.Name, Status);
        }
    }
}
