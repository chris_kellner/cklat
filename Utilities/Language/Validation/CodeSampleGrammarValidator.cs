﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Language.Validation
{
    public class CodeSampleGrammarValidator : IGrammarValidator
    {
        private class CodeSampleValidationResult : ValidationResult
        {
            public CodeSampleValidationResult(IGrammarElement element, ValidationStatus status, CodeSample failedSample, ASTNode parseTree)
            {
                Element = element;
                Status = status;
                FailedSample = failedSample;
                ParseTree = parseTree;
            }

            public CodeSample FailedSample { get; private set; }
            public ASTNode ParseTree {get; private set;}

            public override string ToString()
            {
                if (Status == ValidationStatus.Fail)
                {
                    return string.Join(
                        Environment.NewLine,
                        new string[] { 
                            base.ToString(),
                            "\tCode: " + FailedSample.Code, 
                            "\tParseTree: " + (ParseTree != null ? ParseTree.ToString(true) : "[null]") 
                        });
                }
                else
                {
                    return base.ToString();
                }
            }
        }

        private CodeSample m_Sample;

        public CodeSampleGrammarValidator(IGrammarElement element, CodeSample sample)
        {
            GrammarElement = element;
            m_Sample = sample;
        }

        public IGrammarElement GrammarElement { get; private set; }
        
        public CodeSample Sample { get { return m_Sample; } }

        public bool AcceptSubnodes { get; set; }

        private bool CheckType(AElement element)
        {
            if (!element.IsType(GrammarElement))
            {
                if (AcceptSubnodes)
                {
                    AElement node = element.FirstOrDefault();
                    while (node != null && node != element)
                    {
                        if (node.IsType(GrammarElement))
                        {
                            return true;
                        }

                        node = node.FirstOrDefault();
                    }
                }

                return false;
            }

            return true;
        }

        public ValidationResult Validate()
        {
            var grammar = GrammarElement.Grammar;
            var lexer = grammar.CreateLexer();
            var parser = grammar.CreateParser();
            var sample = Sample;
            var elements = parser.Parse(lexer.LexTokens(sample.Code));
            if (elements.Count > sample.TargetIndex)
            {
                if (elements.Count > 1 && sample.TargetIndex < 0)
                {
                    return new CodeSampleValidationResult(
                     GrammarElement,
                     ValidationStatus.Fail,
                        sample,
                        grammar.Parse(sample.Code));
                }
                else
                {
                    var idx = sample.TargetIndex > 0 ? sample.TargetIndex : 0;
                    if (!CheckType(elements[idx]))
                    {
                        return new CodeSampleValidationResult(
                            GrammarElement,
                            ValidationStatus.Fail,
                            sample,
                            grammar.Parse(sample.Code));
                    }
                }
            }
            else
            {
                return new CodeSampleValidationResult(
                     GrammarElement,
                     ValidationStatus.Fail,
                        sample,
                        grammar.Parse(sample.Code));
            }

            return new CodeSampleValidationResult(
                GrammarElement,
                ValidationStatus.Pass,
                default(CodeSample),
                null);
        }
    }
}
