#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Proxy
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using CKLib.Utilities.Diagnostics;
    using CKLib.Utilities.Interception;
    #endregion
    /// <summary>
    /// Class used to validate a type for the proxy builder.
    /// </summary>
    public class ProxyTypeValidator
    {
        #region Delegates
        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped is compatible with the specified interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        public delegate bool ProxyValidationRule(ITypeCache typeCache, Type typeOfObjectBeingWrapped, Type interfaceToValidate);
        #endregion
        #region Variables
        /// <summary>
        /// The set of rules used to validate types when creating a proxy.
        /// </summary>
        private SynchronizedCollection<ProxyValidationRule> m_Rules;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ProxyTypeValidator class.
        /// </summary>
        public ProxyTypeValidator()
        {
            m_Rules = new SynchronizedCollection<ProxyValidationRule>();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Adds the default set of validation rules to this validator.
        /// </summary>
        protected virtual void AddDefaultRules()
        {
            m_Rules.Add(MethodValidationRule);
            m_Rules.Add(PropertyValidationRule);
            m_Rules.Add(EventValidationRule);
        }

        /// <summary>
        /// Initializes this instance with the validation rules that it will use to validate the requested proxies.
        /// </summary>
        public void Initialize()
        {
            AddDefaultRules();
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Release()
        {
            m_Rules.Clear();
        }

        /// <summary>
        /// Checks to see if the input typeOfObjectBeingWrapped defines members compatible with those defined
        /// in the interfaces.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The Type of the object that the proxy is being made for.</param>
        /// <param name="interfaces">The interfaces which the proxy will implement.</param>
        /// <param name="throwOnError">If set to <c>true</c> this method will throw an exception if the wrapped object cannot
        /// implement all of the required interfaces..</param>
        /// <returns>True if the types are compatible, false otherwise.</returns>
        /// <exception cref="InvalidOperationException">If the types are incompatible and <c>throwOnError</c>
        /// is set then an InvalidOperationException is thrown.</exception>
        public bool EnsureProxyCompatibility(ITypeCacheProvider provider, Type typeOfObjectBeingWrapped, Type[] interfaces, bool throwOnError)
        {
            return EnsureProxyCompatibility(provider.GetTypeCache(), typeOfObjectBeingWrapped, interfaces, throwOnError);
        }

        /// <summary>
        /// Checks to see if the input typeOfObjectBeingWrapped defines members compatible with those defined
        /// in the interfaces.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The Type of the object that the proxy is being made for.</param>
        /// <param name="interfaces">The interfaces which the proxy will implement.</param>
        /// <param name="throwOnError">If set to <c>true</c> this method will throw an exception if the wrapped object cannot
        /// implement all of the required interfaces..</param>
        /// <returns>True if the types are compatible, false otherwise.</returns>
        /// <exception cref="InvalidOperationException">If the types are incompatible and <c>throwOnError</c>
        /// is set then an InvalidOperationException is thrown.</exception>
        public virtual bool EnsureProxyCompatibility(ITypeCache typeCache, Type typeOfObjectBeingWrapped, Type[] interfaces, bool throwOnError)
        {
            if (typeOfObjectBeingWrapped != null && interfaces != null && interfaces.Length > 0)
            {
                foreach (var ifc in FactoryHelpers.ExpandTypeInheritance(interfaces))
                {
                    if (ifc != null && ifc.IsInterface)
                    {
                        if (!m_Rules.All(validate => validate(typeCache, typeOfObjectBeingWrapped, ifc)))
                        {
                            if (throwOnError)
                            {
                                throw new InvalidOperationException(
                                    string.Format("Object type {0} does not match the definition of interface: {1}, unable to create Proxy.",
                                    typeOfObjectBeingWrapped.Name,
                                    ifc.Name));
                            }

                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the methods required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        private bool MethodValidationRule(ITypeCache typeCache, Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetAllMethods().
                Where(meth=>!meth.IsSpecialName).
                All(meth => FindBestMatchingMethodInWrapType(typeCache, typeOfObjectBeingWrapped, meth) != null);
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the properties required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        /// <returns>True if the base type is compatible with the interface type, false otherwise.</returns>
        private bool PropertyValidationRule(ITypeCache typeCache, Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetProperties().All(prop => typeOfObjectBeingWrapped.GetProperty(prop.Name) != null);
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the events required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        /// <returns>True if the base type is compatible with the interface type, false otherwise.</returns>
        private bool EventValidationRule(ITypeCache typeCache, Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetEvents().All(evt => typeOfObjectBeingWrapped.GetEventRecursive(evt.Name) != null);
        }

        /// <summary>
        /// Finds the method in the specified wrappedType that best matches the specified interfaceMethod.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory requesting the method.</param>
        /// <param name="typeOfObjectBeingWrapped">The type being wrapped.</param>
        /// <param name="interfaceMethod">The interface method being matched.</param>
        /// <returns>The best matched method or null.</returns>
        public MethodInfo FindBestMatchingMethodInWrapType(ITypeCacheProvider provider, Type typeOfObjectBeingWrapped, MethodInfo interfaceMethod)
        {
            return FindBestMatchingMethodInWrapType(provider.GetTypeCache(), typeOfObjectBeingWrapped, interfaceMethod);
        }

        /// <summary>
        /// Finds the method in the specified wrappedType that best matches the specified interfaceMethod.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory requesting the method.</param>
        /// <param name="typeOfObjectBeingWrapped">The type being wrapped.</param>
        /// <param name="interfaceMethod">The interface method being matched.</param>
        /// <returns>The best matched method or null.</returns>
        public MethodInfo FindBestMatchingMethodInWrapType(ITypeCache typeCache, Type typeOfObjectBeingWrapped, MethodInfo interfaceMethod)
        {
            if (typeOfObjectBeingWrapped != null && interfaceMethod != null)
            {
                var sourceMethods = typeOfObjectBeingWrapped.GetAllMethods();
                var interfaceParameterTypes = FactoryHelpers.GetMethodParameterTypes(interfaceMethod);
                MethodInfo bestMethod = typeOfObjectBeingWrapped.GetMethod(interfaceMethod.Name, interfaceParameterTypes);
                int bestWeight = 0;
                if (bestMethod == null)
                {
                    foreach (var sourceMethod in sourceMethods)
                    {
                        if (sourceMethod.Name == interfaceMethod.Name)
                        {
                            var parameters = sourceMethod.GetParameters();
                            var sourceParameterTypes = FactoryHelpers.GetMethodParameterTypes(sourceMethod);
                            if (parameters.Length == interfaceParameterTypes.Length)
                            {
                                int currentWeight = ComputeTypeCompatibility(
                                    typeCache,
                                    interfaceMethod.ReturnType,
                                    sourceMethod.ReturnType,
                                    interfaceMethod.DeclaringType);
                                if (currentWeight > 0)
                                {
                                    for (int i = 0; i < parameters.Length; i++)
                                    {
                                        var tempWeight =
                                            ComputeTypeCompatibility(
                                            typeCache,
                                            sourceParameterTypes[i],
                                            interfaceParameterTypes[i],
                                            typeOfObjectBeingWrapped);

                                        if (tempWeight > 0)
                                        {
                                            currentWeight += tempWeight;
                                        }
                                        else
                                        {
                                            currentWeight = 0;
                                            break;
                                        }
                                    }
                                }

                                if (currentWeight > bestWeight)
                                {
                                    bestMethod = sourceMethod;
                                }
                            }
                        }
                    }
                }

                return bestMethod;
            }

            return null;
        }

        /// <summary>
        /// Gets a weight used for determining how well a given type matches the desired type.
        /// </summary>
        /// <param name="factory">The proxy factory making the request.</param>
        /// <param name="desiredType">The type that we need.</param>
        /// <param name="providedType">The type that we have.</param>
        /// <param name="typeOwningDesiredType">The type that owns the desiredType,
        /// this will only happen if a type declares a method whose return type or parameter type IS that type.</param>
        /// <returns>A value from 0 to 5 which indicates how well a given type matches the desired type.</returns>
        private int ComputeTypeCompatibility(ITypeCache typeCache, Type desiredType, Type providedType, Type typeOwningDesiredType)
        {
            int weight = 0;

            if (desiredType == providedType)
            {
                // IVehicle == IVehicle
                weight = 5;
            }
            // Check for inheritance
            else if (desiredType.IsAssignableFrom(providedType))
            {
                // IVehicle IsAssignableFrom Car
                weight = 4;
            }
            else if (
                // Check to see if the desired type is the interface type
                desiredType == typeOwningDesiredType ||

                //Check to see if we have already created a proxy for it
                typeCache.GetProxyConstructor(providedType, desiredType) != null ||

                // Check to see if whats being passed in Might be a proxy
                typeCache.GetProxyConstructor(desiredType, providedType) != null)
            {
                weight = 3;
            }
            else if (providedType.IsAssignableFrom(desiredType))
            {
                // desiredType is a subclass of what we are providing
                //  this can get dangerous so only use this method as a last resort
                //  Compatibility in this way will require a cast when making the proxy.
                weight = 1;
            }
            else if (
                    //Check to see if we can create a proxy for it
                    EnsureProxyCompatibility(typeCache, providedType, new Type[] { desiredType }, false) ||
                    // whats being passed in Might be a proxy
                    EnsureProxyCompatibility(typeCache, desiredType, new Type[] { providedType }, false))
            {
                // Can be proxied
                weight = 2;
            }

            return weight;
        }
        #endregion
    }
}
