#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy.ObjectSharing
{
    #region Using List
    using System;
    using System.Threading;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.Messages;
    #endregion
    /// <summary>
    /// This is the base class for all shared objects.
    /// </summary>
    public interface ISharedObject : IDisposable
    {
        /// <summary>
        /// Gets or sets a value indicating whether the shared object is owned locally.
        /// </summary>
        bool IsOwnedLocally { get; set; }

        /// <summary>
        /// Gets or sets the id of the connection that owns the shared object.
        /// </summary>
        Guid OwnerId { get; set; }

        /// <summary>
        /// Gets the unique instance id of this shared object.
        /// </summary>
        Guid InstanceId { get; }

        /// <summary>
        /// Gets the type of object being shared.
        /// </summary>
        Type ObjectType { get; }

        /// <summary>
        /// Gets or sets the IMessenger used to communicate.
        /// </summary>
        IMessenger Messenger { get; set; }

        /// <summary>
        /// Gets or sets the SynchronizationContext that this object was shared from.
        /// </summary>
        SynchronizationContext SynchronizationContext { get; set; }

        /// <summary>
        /// Notifies all connections that this shared object is available for use or not.
        /// </summary>
        /// <param name="available">True if the object is available, false if it is disposing.</param>
        void NotifyProxyAvailability(bool available);

        /// <summary>
        /// Initializes the shared object.
        /// </summary>
        void InitializeProxy();

        /// <summary>
        /// Gets the object that was shared if it is owned locally.
        /// </summary>
        object WrappedObject { get; }

        /// <summary>
        /// Gets the WaitHandle used for making remote calls synchronous with the calling thread.
        /// </summary>
        EventWaitHandle MethodCallWaiter { get; }

        /// <summary>
        /// Invokes a method based on a remote call if the object is owned locally, or sends a request to the
        /// remote object for the call to me made if it is owned remotely.
        /// </summary>
        /// <param name="methodName">The name of the method to call.</param>
        /// <param name="arguments">The parameters to pass to the method.</param>
        /// <returns>The result of the method call.</returns>
        object CallMethod(string methodName, object[] arguments);

        /// <summary>
        /// Sends a message to the owner of the object to invoke a method.
        /// </summary>
        /// <param name="rqst">The message to send.</param>
        void SendMethodCallMessage(APICallRequestMessage rqst);
        
        /// <summary>
        /// Receives and processes a remote procedure call message.
        /// </summary>
        /// <param name="rqst">The request to process.</param>
        void ReceiveMethodCallMessage(APICallRequestMessage rqst);

        /// <summary>
        /// Sends a response to a remote procedure call.
        /// </summary>
        /// <param name="requestor">The connection that requested the remote call.</param>
        /// <param name="rspns">The response message to be sent.</param>
        void SendMethodResponseMessage(Guid requestor, APICallResponseMessage rspns);

        /// <summary>
        /// Receives and processes a response message.
        /// </summary>
        /// <param name="rspns">The message to process.</param>
        void ReceiveMethodResponseMessage(APICallResponseMessage rspns);
    }

    /// <summary>
    /// This interface defines a generic ISharedObject.
    /// </summary>
    /// <typeparam name="TInterface">The interface type that the object is being shared as.</typeparam>
    public interface ISharedObject<TInterface> : ISharedObject where TInterface : class
    {
        /// <summary>
        /// The object being shared as the interface it is being shared as.
        /// </summary>
        new TInterface WrappedObject { get; }
    }

    public class SynchronizedCallRequest
    {
        public SynchronizedCallRequest(APICallRequestMessage rqst)
        {
            Request = rqst;
        }

        public APICallRequestMessage Request { get; private set; }
        public object Result { get; set; }
    }
}
