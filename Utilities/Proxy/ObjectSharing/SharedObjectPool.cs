#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy.ObjectSharing
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Threading;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Messages;
    #endregion
    /// <summary>
    /// This class is used as an event argument for the shared object pool events.
    /// </summary>
    public class SharedObjectAvailabilityEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SharedObjectAvailabilityEventArgs class.
        /// </summary>
        /// <param name="obj">The ISharedObject.</param>
        /// <param name="isAvailable">A value indicating whether the share is available or disposing.</param>
        public SharedObjectAvailabilityEventArgs(ISharedObject obj, bool isAvailable)
        {
            SharedObject = obj;
            IsAvailable = isAvailable;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the ISharedObject.
        /// </summary>
        public ISharedObject SharedObject { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the object is available or disposing.
        /// </summary>
        public bool IsAvailable { get; private set; }
        #endregion
    }

    /// <summary>
    /// This class is used as the manager for all objects which are shared accross a connection.
    /// </summary>
    public sealed class SharedObjectPool : ISharedObjectPool, IDisposable
    {
        #region Variables
        /// <summary>
        /// A collection of objects which have been shared by this connection or a remote one.
        /// </summary>
        private SynchronizedDictionary<Guid, ISharedObject> m_SharedObjects;

        /// <summary>
        /// The IMessenger that owns this instance.
        /// </summary>
        private IMessenger m_Messenger;
        
        /// <summary>
        /// The ProxyFactory used for creating the shares.
        /// </summary>
        private ProxyFactory m_SharedObjectFactory;

        /// <summary>
        /// Event handler key for the SharedObjectReceived event.
        /// </summary>
        private readonly object m_sharedObjectReceivedKey = new object();

        /// <summary>
        /// Event handler key for the SharedObjectDisposed event.
        /// </summary>
        private readonly object m_sharedObjectDisposedKey = new object();

        /// <summary>
        /// This queue is used as a backlog for shared objects which are shared before the messenger has been connected.
        /// </summary>
        private SynchronizedQueue<ISharedObject> m_PendingNotificationQueue = new SynchronizedQueue<ISharedObject>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the SharedObjectPool class.
        /// </summary>
        public SharedObjectPool()
        {
            m_SharedObjects = new SynchronizedDictionary<Guid, ISharedObject>();
            m_SharedObjectFactory = new ProxyFactory();
            Events = new EventHandlerList();
        }
        #endregion

        /// <summary>
        /// Initializes the ISharedObject pool
        /// </summary>
        /// <param name="messenger">The IMessenger that owns this ISharedObjectPool.</param>
        /// <returns>True if the initialization was successful, false otherwise.</returns>
        public bool Initialize(IMessenger messenger)
        {
            m_Messenger = messenger;
            if (m_Messenger != null)
            {
                m_Messenger.AddCallback(typeof(APICallRequestMessage), ProcessCallback);
                m_Messenger.AddCallback(typeof(APICallResponseMessage), ProcessCallback);
                m_Messenger.AddCallback(typeof(SharedObjectAvailabilityMessage), ProcessCallback);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Releases all resources held by this instance.
        /// </summary>
        public void Release()
        {
            if (m_Messenger != null)
            {
                m_Messenger.RemoveCallback(typeof(APICallRequestMessage), ProcessCallback);
                m_Messenger.RemoveCallback(typeof(APICallResponseMessage), ProcessCallback);
                m_Messenger.RemoveCallback(typeof(SharedObjectAvailabilityMessage), ProcessCallback);
            }
        }

        /// <summary>
        /// Shares the specified object as the specified interface type.
        /// </summary>
        /// <typeparam name="T">The interface to expose the object as.</typeparam>
        /// <param name="objectToShare">The object to be shared.</param>
        /// <returns>An ISharedObject, or null if the object failed to be shared.</returns>
        public ISharedObject ShareObject<T>(object objectToShare) where T : class
        {
            // Make a new proxy with base class: ASharedObject<T>,
            //  define constructor which and object and calls base.
            //  Construct the new object.
            //  Add the proxy to the list of shared objects
            //  
            //  return the new object.

            object created = m_SharedObjectFactory.CreateProxy<T>(new SharedObjectBuilder(), objectToShare, typeof(ASharedObject<T>), false);
            var obj = created as ISharedObject;
            if (obj != null)
            {
                obj.Messenger = m_Messenger;
                obj.OwnerId = m_Messenger.MessengerId;
                obj.InitializeProxy();
                obj.SynchronizationContext = SynchronizationContext.Current;
                m_SharedObjects.Add(obj.InstanceId, obj);
                if (m_Messenger.ConnectionStatus == System.ServiceModel.CommunicationState.Opened)
                {
                    obj.NotifyProxyAvailability(true);
                }
                else
                {
                    m_PendingNotificationQueue.Enqueue(obj);
                }

                return obj;
            }

            return null;
        }

        /// <summary>
        /// Unshares an object by share reference.
        /// </summary>
        /// <param name="obj">An object that was previously shared.</param>
        public void UnshareObject(object obj)
        {
            if (obj != null)
            {
                foreach (var share in m_SharedObjects.TryRemoveWhere(
                    p => p.Value.IsOwnedLocally && p.Value.WrappedObject == obj))
                {
                    share.Value.NotifyProxyAvailability(false);
                    OnDisposeSharedObject(share.Value);
                    share.Value.Dispose();
                }
            }
        }

        /// <summary>
        /// Unshares an object by its wrapping ISharedObject.
        /// </summary>
        /// <param name="objectToUnshare">The ISharedObject to unshare.</param>
        public void UnshareObject(ISharedObject objectToUnshare)
        {
            if (objectToUnshare != null)
            {
                objectToUnshare.NotifyProxyAvailability(false);
                m_SharedObjects.Remove(objectToUnshare.InstanceId);
            }
        }

        /// <summary>
        /// Gets a shared object by instance id.
        /// </summary>
        /// <param name="instanceId">The instance id of the object.</param>
        /// <returns>The shared object, or null.</returns>
        public ISharedObject GetSharedObject(Guid instanceId)
        {
            ISharedObject obj;
            if (m_SharedObjects.TryGetValue(instanceId, out obj))
            {
                return obj;
            }
            return null;
        }

        /// <summary>
        /// Gets a shared object by instance id.
        /// </summary>
        /// <typeparam name="TShare">The type to return the object as.</typeparam>
        /// <param name="instanceId">The instance id of the object.</param>
        /// <returns>The shared object, or null.</returns>
        public TShare GetSharedObject<TShare>(Guid instanceId) where TShare : class
        {
            var share = GetSharedObject(instanceId);
            return share as TShare;
        }

        /// <summary>
        /// Called when a shared object is received.
        /// </summary>
        /// <param name="obj">The shared object.</param>
        private void OnReceiveSharedObject(ISharedObject obj)
        {
            EventHandler<SharedObjectAvailabilityEventArgs> handler;
            handler = Events[m_sharedObjectReceivedKey] as EventHandler<SharedObjectAvailabilityEventArgs>;
            if (handler != null)
            {
                handler(this, new SharedObjectAvailabilityEventArgs(obj, true));
            }
        }

        /// <summary>
        /// Called when a shared object is disposed.
        /// </summary>
        /// <param name="obj">The shared object that was disposed.</param>
        private void OnDisposeSharedObject(ISharedObject obj)
        {
            EventHandler<SharedObjectAvailabilityEventArgs> handler;
            handler = Events[m_sharedObjectDisposedKey] as EventHandler<SharedObjectAvailabilityEventArgs>;
            if (handler != null)
            {
                handler(this, new SharedObjectAvailabilityEventArgs(obj, true));
            }
        }

        /// <summary>
        /// Distributes all pending shared object notifications.
        /// </summary>
        public void SendPendingNotifications()
        {
            int count = m_PendingNotificationQueue.Count;
            while (count-- > 0)
            {
                var obj = m_PendingNotificationQueue.Dequeue();
                obj.NotifyProxyAvailability(true);
            }
        }

        /// <summary>
        /// Processes messages related to shared objects.
        /// </summary>
        /// <param name="pair">The object containing information about the message to process.</param>
        private void ProcessCallback(MessageEnvelope pair)
        {
            var info = pair;
            if (info.Message is APICallRequestMessage)
            {
                APICallRequestMessage notice = info.Message as APICallRequestMessage;
                var obj = GetSharedObject(notice.InstanceId);
                if (obj != null)
                {
                    obj.ReceiveMethodCallMessage(notice);
                }
            }
            else if (info.Message is APICallResponseMessage)
            {
                APICallResponseMessage notice = info.Message as APICallResponseMessage;
                var obj = GetSharedObject(notice.InstanceId);
                if (obj != null)
                {
                    obj.ReceiveMethodResponseMessage(notice);
                }
            }
            else if (info.Message is SharedObjectAvailabilityMessage)
            {
                SharedObjectAvailabilityMessage notice = info.Message as SharedObjectAvailabilityMessage;
                if (notice.IsAvailable)
                {
                    if (!m_SharedObjects.ContainsKey(notice.InstanceId))
                    {
                        Type t = notice.ObjectType;
                        if (t != null)
                        {
                            Type type = m_SharedObjectFactory.CreateType(new SharedObjectBuilder(), typeof(ASharedObject<>).MakeGenericType(t), t);
                            if (type != null)
                            {
                                ISharedObject obj = Activator.CreateInstance(type, notice.InstanceId) as ISharedObject;
                                if (obj != null)
                                {
                                    obj.Messenger = m_Messenger;
                                    obj.OwnerId = notice.SenderId;
                                    obj.InitializeProxy();
                                    m_SharedObjects[obj.InstanceId] = obj;

                                    OnReceiveSharedObject(obj);
                                }
                            }
                        }
                    }
                }
                else
                {
                    ISharedObject obj;
                    if (m_SharedObjects.TryGetValue(notice.InstanceId, out obj))
                    {
                        OnDisposeSharedObject(obj);

                        obj.Dispose();
                        m_SharedObjects.Remove(notice.InstanceId);
                    }
                }
            }
        }

        /// <summary>
        /// Gets all objects shared by the specified owner.
        /// </summary>
        /// <param name="ownerId">The connection id of the owner to get the objects for.</param>
        /// <returns>An enumeration of ISharedObjects.</returns>
        public IEnumerable<ISharedObject> GetObjectsSharedByOwner(Guid ownerId)
        {
            var shared = from share in m_SharedObjects.Values
                         where share.OwnerId == ownerId
                         select share;
            return shared;
        }

        #region Properties
        /// <summary>
        /// Gets a list of events used by this instance.
        /// </summary>
        public EventHandlerList Events
        {
            get;
            private set;
        }
        #endregion
        #region Events

        public event EventHandler<SharedObjectAvailabilityEventArgs> SharedObjectReceived
        {
            add
            {
                Events.AddHandler(m_sharedObjectReceivedKey, value);
            }
            remove
            {
                Events.RemoveHandler(m_sharedObjectDisposedKey, value);
            }
        }

        /// <summary>
        /// Called when a shared object is disposed.
        /// </summary>
        public event EventHandler<SharedObjectAvailabilityEventArgs> SharedObjectDisposing
        {
            add
            {
                Events.AddHandler(m_sharedObjectDisposedKey, value);
            }
            remove
            {
                Events.RemoveHandler(m_sharedObjectDisposedKey, value);
            }
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Disposes of all remote objects owned by the specified connection.
        /// </summary>
        /// <param name="ownerId">The id of the connecton to dispose the objects of.</param>
        public void DisposeObjectsForOwner(Guid ownerId)
        {
            var objects = GetObjectsSharedByOwner(ownerId).ToArray();
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].Dispose();
                m_SharedObjects.Remove(objects[i].InstanceId);
            }
        }

        /// <summary>
        /// Releases all resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            Events.Dispose();
            m_SharedObjectFactory.Release();
            foreach (var pair in m_SharedObjects)
            {
                pair.Value.Dispose();
            }
            m_SharedObjects.Clear();
        }

        #endregion
    }
}
