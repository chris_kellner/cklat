#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy.ObjectSharing
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Security;
    #endregion

    /// <summary>
    /// This is the base class for all shared objects.
    /// </summary>
    /// <typeparam name="TShare">The interface used to share the object.</typeparam>
    public class ASharedObject<TShare> : ISharedObject<TShare> where TShare : class
    {
        #region Variables
        /// <summary>
        /// Dictionary of functions keyed by name visible through the TShare interface.
        /// </summary>
        private Dictionary<string, HashSet<MethodInfo>> m_functions;

        /// <summary>
        /// The instance id of this shared object.
        /// </summary>
        private readonly Guid m_InstanceId;

        /// <summary>
        /// Reference to the IMessenger used to communicate.
        /// </summary>
        private IMessenger m_Messenger;

        /// <summary>
        /// Event handle used to synchronize method calls.
        /// </summary>
        private ManualResetEvent m_SynchronizationObject;

        /// <summary>
        /// The inner object to make the method calls on, only used if the object is owned locally.
        /// </summary>
        private TShare m_WrappedObject;

        /// <summary>
        /// A collection of method call responses keyed by request id.
        /// </summary>
        private Dictionary<Guid, object> m_CallResponses;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ASharedObject class.
        /// </summary>
        /// <param name="id">The instance id of the remotely shared object.</param>
        public ASharedObject(Guid id)
        {
            m_functions = new Dictionary<string, HashSet<MethodInfo>>();
            m_InstanceId = id;
            m_SynchronizationObject = new ManualResetEvent(true);
            m_CallResponses = new Dictionary<Guid, object>();
            ((ISharedObject)this).IsOwnedLocally = false;
        }

        /// <summary>
        /// Initializes a new instance of the ASharedObject class.
        /// </summary>
        /// <param name="wrappedObject">The local object to be shared.</param>
        public ASharedObject(object wrappedObject)
        {
            m_functions = new Dictionary<string, HashSet<MethodInfo>>();
            m_InstanceId = Guid.NewGuid();
            if (wrappedObject is TShare)
            {
                m_WrappedObject = (TShare)wrappedObject;
            }
            else
            {
                m_WrappedObject = ProxyFactory.Instance.CreateProxy<TShare>(wrappedObject, false);
            }

            m_SynchronizationObject = new ManualResetEvent(true);
            ((ISharedObject)this).IsOwnedLocally = true;
        }
        #endregion
        #region ISharedObject Members
        void ISharedObject.InitializeProxy()
        {
            foreach (var method in typeof(TShare).GetAllMethods())
            {
                HashSet<MethodInfo> namedSet;
                if (!m_functions.TryGetValue(method.Name, out namedSet))
                {
                    namedSet = new HashSet<MethodInfo>();
                    m_functions.Add(method.Name, namedSet);
                }

                namedSet.Add(method);
            }
        }

        IMessenger ISharedObject.Messenger
        {
            get
            {
                return m_Messenger;
            }
            set
            {
                m_Messenger = value;
            }
        }

        SynchronizationContext ISharedObject.SynchronizationContext
        {
            get;
            set;
        }

        bool ISharedObject.IsOwnedLocally
        {
            get;
            set;
        }

        Guid ISharedObject.OwnerId
        {
            get;
            set;
        }

        Guid ISharedObject.InstanceId
        {
            get { return m_InstanceId; }
        }

        Type ISharedObject.ObjectType
        {
            get { return typeof(TShare); }
        }

        void ISharedObject.NotifyProxyAvailability(bool available)
        {
            var share = (ISharedObject)this;
            if (share.IsOwnedLocally)
            {
                SharedObjectAvailabilityMessage msg = new SharedObjectAvailabilityMessage(
                    share.InstanceId,
                    share.ObjectType,
                    available);
                msg.SenderId = share.OwnerId;
                m_Messenger.SendNetworkMessage(msg);
            }
        }

        System.Threading.EventWaitHandle ISharedObject.MethodCallWaiter
        {
            get { return m_SynchronizationObject; }
        }

        /// <summary>
        /// TODO: Redo this with dynamic method instead of reflection call.
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        object ISharedObject.CallMethod(string methodName, object[] arguments)
        {
            HashSet<MethodInfo> theSet;
            if (m_functions.TryGetValue(methodName, out theSet))
            {
                MethodInfo mInfo = theSet.FirstOrDefault(meth =>
                {
                    var types = FactoryHelpers.GetMethodParameterTypes(meth);
                    if (types.Length == arguments.Length)
                    {
                        for (int i = 0; i < types.Length; i++)
                        {
                            if (arguments[i] == null)
                            {
                                continue;
                            }

                            if (arguments[i].GetType() != arguments[i].GetType())
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    return false;
                });
                if (mInfo != null)
                {
                    var share = (ISharedObject)this;
                    if (share.IsOwnedLocally)
                    {
                        return mInfo.Invoke(m_WrappedObject, arguments);
                    }
                    else
                    {
                        if (mInfo.ReturnType != typeof(void))
                        {
                            m_SynchronizationObject.Reset();
                        }
                        else
                        {
                            m_SynchronizationObject.Set();
                        }

                        APICallRequestMessage msg = new APICallRequestMessage(
                            share.ObjectType.AssemblyQualifiedName,
                            share.InstanceId,
                            methodName,
                            arguments);
                        msg.SenderId = share.OwnerId;
                        share.SendMethodCallMessage(msg);

                        m_SynchronizationObject.WaitOne(TimeSpan.FromSeconds(10));

                        object o;
                        if (m_CallResponses.TryGetValue(msg.RequestId, out o))
                        {
                            System.Diagnostics.Debug.WriteLine(string.Format("Method {0} returned value {1}", methodName, o));
                            return o;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Invokes the method if the requestor has the appropriate rights.
        /// </summary>
        /// <param name="requestor"></param>
        /// <param name="methodName"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private object InvokeMethod(Guid requestor, string methodName, object[] arguments)
        {
            var share = (ISharedObject)this;
            if (share.IsOwnedLocally)
            {
                HashSet<MethodInfo> theSet;
                if (m_functions.TryGetValue(methodName, out theSet))
                {
                    MethodInfo mInfo = theSet.FirstOrDefault(meth =>
                    {
                        var types = FactoryHelpers.GetMethodParameterTypes(meth);
                        if (types.Length == arguments.Length)
                        {
                            for (int i = 0; i < types.Length; i++)
                            {
                                if (arguments[i] == null)
                                {
                                    continue;
                                }

                                if (arguments[i].GetType() != arguments[i].GetType())
                                {
                                    return false;
                                }
                            }
                            return true;
                        }
                        return false;
                    });
                    if (mInfo != null)
                    {
                        if (LocalOnlyAttribute.CheckAccess(mInfo, m_Messenger, requestor))
                        {
                            return mInfo.Invoke(m_WrappedObject, arguments);
                        }
                    }
                }
            }
            return null;
        }

        void ISharedObject.SendMethodCallMessage(CKLib.Utilities.Messages.APICallRequestMessage rqst)
        {
            var share = (ISharedObject)this;
            if (!share.IsOwnedLocally)
            {
                m_Messenger.SendNetworkMessage(share.OwnerId, rqst);
            }
        }

        void ISharedObject.ReceiveMethodCallMessage(CKLib.Utilities.Messages.APICallRequestMessage rqst)
        {
            var share = (ISharedObject)this;
            if (share.IsOwnedLocally)
            {
                if (share.SynchronizationContext != null)
                {
                    SynchronizedCallRequest sync = new SynchronizedCallRequest(rqst);
                    share.SynchronizationContext.Post(p =>
                    {
                        SynchronizedCallRequest synch = (SynchronizedCallRequest)p;
                        try
                        {
                            synch.Result = InvokeMethod(rqst.SenderId, synch.Request.FunctionName, synch.Request.Parameters);
                        }
                        finally
                        {
                            var rspns = new APICallResponseMessage(
                            rqst.RequestId,
                            share.ObjectType.AssemblyQualifiedName,
                            share.InstanceId,
                            synch.Result);
                            rspns.SenderId = share.OwnerId;
                            share.SendMethodResponseMessage(rqst.SenderId, rspns);
                        }
                    }, sync);
                }
                else
                {
                    object o = null;
                    try
                    {
                        o = InvokeMethod(rqst.SenderId, rqst.FunctionName, rqst.Parameters);
                    }
                    finally
                    {
                        var rspns = new APICallResponseMessage(
                            rqst.RequestId,
                            share.ObjectType.AssemblyQualifiedName,
                            share.InstanceId,
                            o);
                        rspns.SenderId = share.OwnerId;
                        share.SendMethodResponseMessage(rqst.SenderId, rspns);
                    }
                }
            }
        }

        void ISharedObject.SendMethodResponseMessage(Guid requestor, CKLib.Utilities.Messages.APICallResponseMessage rspns)
        {
            var share = (ISharedObject)this;
            if (share.IsOwnedLocally)
            {
                rspns.SenderId = share.OwnerId;
                m_Messenger.SendNetworkMessage(requestor, rspns);
            }
        }

        void ISharedObject.ReceiveMethodResponseMessage(CKLib.Utilities.Messages.APICallResponseMessage rspns)
        {
            var share = (ISharedObject)this;
            if (!share.IsOwnedLocally)
            {
                m_CallResponses[rspns.RequestId] = rspns.Response;
                m_SynchronizationObject.Set();
            }
        }

        #endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            m_CallResponses.Clear();
            m_functions.Clear();
            m_Messenger = null;
            m_SynchronizationObject.Close();
            m_WrappedObject = null;
        }
        #endregion

        #region ISharedObject<TShare> Members

        TShare ISharedObject<TShare>.WrappedObject
        {
            get { return m_WrappedObject; }
        }

        object ISharedObject.WrappedObject
        {
            get
            {
                return m_WrappedObject;
            }
        }

        #endregion
    }
}
