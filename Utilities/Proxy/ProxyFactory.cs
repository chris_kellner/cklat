#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Proxy
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using CKLib.Utilities.Patterns;
    using CKLib.Utilities.Proxy.Builder;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Interception;
    #endregion
    /// <summary>
    /// The ProxyFactory class is designed to create a lightweight wrapper to an object where either
    /// the object type or the interface types are not defined in this assembly.
    /// </summary>
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Assert, Name="Full Trust")]
    public class ProxyFactory : IProxyFactory
    {
        #region Variables
        private TypeCache m_TypeCache;

        /// <summary>
        /// The default proxy builder for this factory.
        /// </summary>
        public IProxyBuilder m_DefaultBuilder;

        /// <summary>
        /// The proxy type vaidator for this instance.
        /// </summary>
        private ProxyTypeValidator m_Validator;

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes the <see cref="ProxyFactory"/> class.
        /// </summary>
        public ProxyFactory()
            : this(new ProxyTypeValidator())
        {
            m_Validator.Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the ProxyFactory class.
        /// </summary>
        /// <param name="customValidator">Objects used for validating proxy creation.</param>
        public ProxyFactory(ProxyTypeValidator customValidator)
        {
            m_Validator = customValidator;
            m_TypeCache = new TypeCache();
            m_DefaultBuilder = new PassThroughTypeConversionBuilder();
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton proxy factory for the application.
        /// </summary>
        public static ProxyFactory Instance
        {
            get
            {
                return Singleton<ProxyFactory>.Instance;
            }
        }

        /// <summary>
        /// Gets the ProxyTypeValidator used by this factory.
        /// </summary>
        public ProxyTypeValidator Validator
        {
            get
            {
                return m_Validator;
            }
        }
        #endregion
        #region Methods
        
        #region Type Creation API
        /// <summary>
        /// Creates a proxy to the specified object using the default proxy factory instance.
        /// </summary>
        /// <param name="objectToWrap">The object to wrap as the interace type.</param>
        /// <param name="interfaceToReturn">The interface to wrap the object as.</param>
        /// <returns>A Type proxy for the specified object of the interfaceToReturn type, or null.</returns>
        public static object CreateObjectTypeProxy(object objectToWrap, Type interfaceToReturn)
        {
            return ProxyFactory.Instance.CreateProxy(objectToWrap, interfaceToReturn, true);
        }

        /// <summary>
        /// Creates a proxy to the specified object using the default IProxyBuilder.
        /// </summary>
        /// <param name="objToWrap">The object to wrap as the interace type.</param>
        /// <param name="interfaceType">The interface to wrap the object as.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>A Type proxy for the specified object of the interfaceToReturn type, or null.</returns>
        public object CreateProxy(object objToWrap, Type interfaceType, bool throwOnError)
        {
            var obj = CreateProxy(null, objToWrap, null, new Type[] { interfaceType }, throwOnError);
            return obj;
        }

        /// <summary>
        /// Creates a proxy to the specified object using the default IProxyBuilder.
        /// </summary>
        /// <typeparam name="TInterface">The interface to wrap the object as.</typeparam>
        /// <param name="objToWrap">The object to wrap as the interace type.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>A Type proxy for the specified object of the TInterface type, or null.</returns>
        public TInterface CreateProxy<TInterface>(object objToWrap, bool throwOnError) where TInterface : class
        {
            var obj = CreateProxy(null, objToWrap, null, new Type[] { typeof(TInterface) }, throwOnError);
            if (throwOnError || obj is TInterface)
            {
                return (TInterface)obj;
            }

            return default(TInterface);
        }

        /// <summary>
        /// Creates a proxy to the specified object using the specified IProxyBuilder.
        /// </summary>
        /// <typeparam name="TInterface">The interface to wrap the object as.</typeparam>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="objToWrap">The object to wrap as the interace type.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>A Type proxy for the specified object of the TInterface type, or null.</returns>
        public TInterface CreateProxy<TInterface>(IProxyBuilder builder, object objToWrap, bool throwOnError) where TInterface : class
        {
            var obj = CreateProxy(builder, objToWrap, null, new Type[] { typeof(TInterface) }, throwOnError);
            if (throwOnError || obj is TInterface)
            {
                return (TInterface)obj;
            }

            return default(TInterface);
        }

        /// <summary>
        /// Creates a proxy to the specified object using the specified IProxyBuilder.
        /// </summary>
        /// <typeparam name="TInterface">The interface to wrap the object as.</typeparam>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="objToWrap">The object to wrap as the interace type.</param>
        /// <param name="inheritedType">The type to have the proxy inherit from. This allows the requestor
        /// to provide a base set of functionality for the proxy.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>A Type proxy for the specified object of the TInterface type, or null.</returns>
        public TInterface CreateProxy<TInterface>(IProxyBuilder builder, object objToWrap, Type inheritedType, bool throwOnError) where TInterface : class
        {
            var obj = CreateProxy(builder, objToWrap, inheritedType, new Type[] { typeof(TInterface) }, throwOnError);
            if (throwOnError || obj is TInterface)
            {
                return (TInterface)obj;
            }

            return default(TInterface);
        }

        /// <summary>
        /// Creates a proxy for the specified type using the specified IProxyBuilder.
        /// </summary>
        /// <typeparam name="TInterface">The interface to wrap the object as.</typeparam>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="typeOfObjectToWrap">The type which the proxy should be prepared to be constructed with
        /// to wrap.</param>
        /// <returns>A Type proxy of the TInterface type, or null.</returns>
        public Type CreateType<TInterface>(IProxyBuilder builder, Type typeOfObjectToWrap) where TInterface : class
        {
            var type = CreateType(
                builder,
                typeOfObjectToWrap, typeof(TInterface),
                new Type[] { typeof(TInterface) },
                true);
            return type;
        }

        /// <summary>
        /// Creates a proxy for the specified type using the specified IProxyBuilder.
        /// </summary>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="inheritedType">The type to have the proxy inherit from. This allows the requestor
        /// to provide a base set of functionality for the proxy.</param>
        /// <param name="interfaceToReturn">The interface to wrap the object as.</param>
        /// <returns>A Type proxy of the interfaceToReturn type, or null.</returns>
        public Type CreateType(IProxyBuilder builder, Type inheritedType, Type interfaceToReturn)
        {
            var type = CreateType(
                builder,
                inheritedType, interfaceToReturn,
                new Type[] { interfaceToReturn },
                true);
            return type;
        }

        /// <summary>
        /// Creates a proxy to the specified object using the specified IProxyBuilder.
        /// </summary>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="objToWrap">The object to wrap as the interace type.</param>
        /// <param name="inheritedType">The type to have the proxy inherit from. This allows the requestor
        /// to provide a base set of functionality for the proxy.</param>
        /// <param name="interfacesToImplement">A set of interfaces to implement on the proxy.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>A Type proxy for the specified object or null.</returns>
        public object CreateProxy(IProxyBuilder builder, object objToWrap, Type inheritedType, Type[] interfacesToImplement, bool throwOnError)
        {
            if (objToWrap != null)
            {
                Type typeOfWrappedObject = objToWrap.GetType();
                var type = CreateType(builder, inheritedType, typeOfWrappedObject, interfacesToImplement, throwOnError);
                if (type != null)
                {
                    object o = Activator.CreateInstance(type, objToWrap);
                    return o;
                }
            }

            return null;
        }

        /// <summary>
        /// The method that performs the actual proxy creation, this method will attempt to avoid duplicate type creations.
        /// If a similar type has already been make it will return that one instead of making a new one.
        /// </summary>
        /// <param name="builder">The IProxyBuilder implementation that should be used to produce the proxy,
        /// passing null will use the default PassThroughTypeConversionBuilder.</param>
        /// <param name="inheritedType">The type to have the proxy inherit from. This allows the requestor
        /// to provide a base set of functionality for the proxy.</param>
        /// <param name="typeofWrappedObject">The type which the proxy should be prepared to be constructed with
        /// to wrap.</param>
        /// <param name="interfaceExposure">A set of interfaces to implement on the proxy.</param>
        /// <param name="throwOnError">If set to true an exception will be generated if the proxy fails to be
        /// created, if false this method will return null.</param>
        /// <returns>The Type based on the specified parameters, or null.</returns>
        private Type CreateType(IProxyBuilder builder, Type inheritedType, Type typeofWrappedObject, Type[] interfaceExposure, bool throwOnError)
        {
            // Check to see if the type has previously been created:
            var type = m_TypeCache.GetTypeFromCache(typeofWrappedObject, interfaceExposure);
            if (type != null)
            {
                return type;
            }

            // Check to make sure that the types are compatible.
            if (m_Validator.EnsureProxyCompatibility(m_TypeCache, typeofWrappedObject, interfaceExposure, throwOnError))
            {
                // Create the dynamic Type
                if (builder == null)
                {
                    builder = m_DefaultBuilder;
                }

                if (builder != null)
                {
                    string typeName = typeofWrappedObject.Name + "Proxy" + Guid.NewGuid();
                    var allInterfaces = FactoryHelpers.ExpandTypeInheritance(interfaceExposure);
                    var typeBuilder = FactoryHelpers.GetTypeBuilder(typeName, inheritedType, allInterfaces.ToArray());

                    // Build instruction set:
                    ProxyBuildingSchematic args = new ProxyBuildingSchematic(typeBuilder, typeofWrappedObject, m_Validator);

                    // Build storage member
                    BuildStorageMember(this, args);
                    args.AddDefinedMember(args.BuilderStorageMember);
                    ImplementIProxyObject(typeBuilder, args);

                    // Add constructors
                    var wrapper = BuildWrapperConstructor(builder, args);
                    m_TypeCache.AddConstructor(typeofWrappedObject, wrapper);
                    try
                    {
                        foreach (var ctor in DefineBaseClassConstructors(builder, args))
                        {
                            args.AddDefinedMember(ctor);
                        }

                        // Implement interfaces:
                        foreach(var ifc in allInterfaces)
                        {
                            // implement the interface
                            args.InterfaceBeingImplemented = ifc;
                            ImplementInterface(builder, args, ifc);
                        }

                        if (!typeofWrappedObject.IsInterface)
                        {
                            DefineToString(builder, args);
                        }

                        type = typeBuilder.CreateType();
                    }
                    catch (Exception)
                    {
                        // If anything goes wrong, clean up
                        m_TypeCache.RemoveConstructor(typeofWrappedObject, wrapper);
                        return null;
                    }

                    if (type != null)
                    {
                        m_TypeCache.CacheType(type, typeofWrappedObject, interfaceExposure);
                    }

                    return type;
                }
            }

            return null;
        }
        #endregion
        #region Emit
        /// <summary>
        /// This method is expected to assign the StorageMember property of the event args.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments used to provide the storage member.</param>
        private void BuildStorageMember(IProxyFactory proxyFactory, IProxyBuildingSchematic eventArgs)
        {
            eventArgs.BuilderStorageMember =
                eventArgs.TypeBuilder.DefineField(
                "m_WrappedObject",
                eventArgs.TypeOfObjectBeingWrapped,
                FieldAttributes.Private);
        }

        /// <summary>
        /// Implements the <see cref="CKLib.Utilities.Proxy.IProxyObject"/> interface explicitly on the proxy.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that is building the rest of the proxy.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        private void ImplementIProxyObject(TypeBuilder builder, IProxyBuildingSchematic args)
        {
            var method = builder.DefineMethod("GetWrappedObject", FactoryHelpers.ExplicitImplementation, typeof(object), null);
            if (method != null)
            {
                builder.AddInterfaceImplementation(typeof(IProxyObject));

                var ilGen = method.GetILGenerator();
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldfld, args.BuilderStorageMember);
                if (args.BuilderStorageMember.FieldType.IsValueType)
                {
                    ilGen.Emit(OpCodes.Box, args.BuilderStorageMember.FieldType);
                }

                ilGen.Emit(OpCodes.Ret);
                builder.DefineMethodOverride(method, typeof(IProxyObject).GetMethod("GetWrappedObject"));
            }

             method = builder.DefineMethod("GetWrapperType", FactoryHelpers.ExplicitImplementation, typeof(Type), null);
             if (method != null)
             {
                 var ilGen = method.GetILGenerator();
                 ProxyBuildingPatterns.TypeOf(ilGen, args.BuilderStorageMember.FieldType);
                 ilGen.Emit(OpCodes.Ret);
                 builder.DefineMethodOverride(method, typeof(IProxyObject).GetMethod("GetWrapperType"));
             }
        }

        /// <summary>
        /// Provides a pass-through implementation of "ToString" which calls the ToString method on the object being wrapped.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that is building the rest of the proxy.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>A MethodBuilder object for the ToString method.</returns>
        private MethodBuilder DefineToString(IProxyBuilder builder, IMethodBuildingSchematic args)
        {
            string methodName = "ToString";
            Type returnType = typeof(string);
            Type[] parameterTypes = Type.EmptyTypes;
            var methodBuilder = FactoryHelpers.GetMethodBuilder(args.TypeBuilder, methodName, returnType, parameterTypes);
            if (methodBuilder != null)
            {
                var innerFunction = args.BuilderStorageMember.FieldType.GetMethod("ToString", Type.EmptyTypes);
                var ilGen = methodBuilder.GetILGenerator();
                args.CurrentMethodBuilder = methodBuilder;
                args.MethodGenerator = ilGen;
                args.ParameterTypes = parameterTypes;
                args.ReturnType = returnType;
                args.InterfaceMemberToBuild = innerFunction;
                ProxyBuildingPatterns.CallStorageMemberPassthrough(this, args, methodBuilder);

                ilGen.Emit(OpCodes.Ret);                    // return
                args.TypeBuilder.DefineMethodOverride(methodBuilder, typeof(object).GetMethod("ToString"));
            }

            return methodBuilder;
        }

        /// <summary>
        /// Provides the builder with an opportunity to fill in the methods on the interface.
        /// </summary>
        /// <param name="builder">The proxy builder which will be used to create the methods.</param>
        /// <param name="args">The schematic which describes the type being built.</param>
        /// <param name="cInterface">The interface currently being implemented.</param>
        /// <returns>True if the interface was successfully implemented, false otherwise.</returns>
        private bool ImplementInterface(IProxyBuilder builder, ProxyBuildingSchematic args, Type cInterface)
        {
            try
            {
                // Implement all of the properties required by the current interface
                var properties = cInterface.GetProperties();
                for (int p = 0; p < properties.Length; p++)
                {
                    var cProperty = properties[p];
                    BuildProperty(builder, args, cProperty);
                }

                // Implement all of the events required by the current interface
                var events = cInterface.GetEvents();
                for (int e = 0; e < events.Length; e++)
                {
                    var cEvent = events[e];
                    BuildEvent(builder, args, cEvent);
                }

                // Implement all of the methods required by the current interface
                var methods = cInterface.GetMethods();
                for (int m = 0; m < methods.Length; m++)
                {
                    // Skip the "special names" (Constructors, properties, and events)
                    var cMethod = methods[m];
                    if (!cMethod.IsSpecialName)
                    {
                        var body = BuildMethod(builder, args, cMethod);
                        args.AddDefinedMember(body, ((IMethodBuildingSchematic)args).ParameterTypes);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Adds an implementation for each of the constructors found on the base object so that the proxy may be constructed using
        /// each of them.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create each constructor.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>An enumeration of constructors which can later be used to construct the proxy.</returns>
        private IEnumerable<ConstructorBuilder> DefineBaseClassConstructors(IProxyBuilder builder, ProxyBuildingSchematic args)
        {
            if (args.TypeBuilder != null)
            {
                var baseType = args.TypeBuilder.BaseType;
                if (baseType != null)
                {
                    var constructors = baseType.GetConstructors();
                    for (int i = 0; i < constructors.Length; i++)
                    {
                        var cInfo = constructors[i];
                        var ctor = BuildBaseConstructor(builder, args, cInfo);
                        if (ctor != null)
                        {
                            yield return ctor;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a constructor for the proxy type which accepts an object whose type is compatible with the type
        /// being wrapped.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create the constructor.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <returns>A ConstructorBuilder object, or null.</returns>
        private ConstructorBuilder BuildWrapperConstructor(IProxyBuilder builder, IConstructorBuildingSchematic args)
        {
            if (builder != null)
            {
                Type[] parameters = new Type[] { args.TypeOfObjectBeingWrapped };
                var constructorBuilder = args.TypeBuilder.DefineConstructor(
                   FactoryHelpers.ConstructorImplementation,
                    CallingConventions.HasThis | CallingConventions.Standard,
                    parameters);
                args.ParameterTypes = parameters;
                if (constructorBuilder != null)
                {
                    var ilGen = constructorBuilder.GetILGenerator();
                    args.CurrentConstructorBuilder = constructorBuilder;
                    args.MethodGenerator = ilGen;
                    builder.BuildConstructor(this, args);
                }
                
                return constructorBuilder;
            }

            return null;
        }

        /// <summary>
        /// Builds a constructor for the proxy type whose signiture matches that of the specified ConstructorInfo.
        /// </summary>
        /// <param name="builder">The IProxyBuilder that will be used to create the constructor.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="info">The base class constructor which should be called by the constructor being built.</param>
        /// <returns>A ConstructorBuilder object, or null.</returns>
        private ConstructorBuilder BuildBaseConstructor(IProxyBuilder builder, IConstructorBuildingSchematic args, ConstructorInfo info)
        {
            if (builder != null && info != null)
            {
                Type[] parameterTypes = FactoryHelpers.GetMethodParameterTypes(info);
                if (!args.HasMemberDefined(info))
                {
                    args.InterfaceMemberToBuild = info;
                    var constructorBuilder = args.TypeBuilder.DefineConstructor(
                      FactoryHelpers.ConstructorImplementation,
                       CallingConventions.HasThis | CallingConventions.Standard,
                       parameterTypes);
                    args.ParameterTypes = parameterTypes;
                    if (constructorBuilder != null)
                    {
                        var ilGen = constructorBuilder.GetILGenerator();
                        args.CurrentConstructorBuilder = constructorBuilder;
                        args.MethodGenerator = ilGen;
                        builder.BuildConstructor(this, args);
                    }

                    return constructorBuilder;
                }
            }

            return null;
        }

        /// <summary>
        /// Builds a method for the proxy type whose signature matches that of the specified MethodInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the method.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="interfaceMethod">The method on the interface which should be built by this method.</param>
        private MethodBuilder BuildMethod(IProxyBuilder proxyBuilder, IMethodBuildingSchematic args, MethodInfo interfaceMethod)
        {
            string methodName = interfaceMethod.Name;
            Type returnType = interfaceMethod.ReturnType;
            Type[] parameterTypes = FactoryHelpers.GetMethodParameterTypes(interfaceMethod);
            var builder = FactoryHelpers.GetMethodBuilder(args.TypeBuilder, methodName, returnType, parameterTypes);
            if (builder != null)
            {
                args.CurrentMethodBuilder = builder;
                args.MethodGenerator = builder.GetILGenerator();
                args.InterfaceMemberToBuild = interfaceMethod;
                args.ParameterTypes = parameterTypes;
                args.ReturnType = returnType;
                proxyBuilder.BuildMethod(this, args);
                args.TypeBuilder.DefineMethodOverride(builder, interfaceMethod);
            }

            return builder;
        }

        /// <summary>
        /// Builds a property for the proxy type whose signiture matches that of the specified PropertyInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the property.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="cProperty">The property on the interface which should be built by this method.</param>
        private void BuildProperty(IProxyBuilder proxyBuilder, IPropertyBuildingSchematic args, PropertyInfo cProperty)
        {
            if (proxyBuilder != null && args != null && cProperty != null)
            {
                string propertyName = cProperty.Name;
                Type propertyType = cProperty.PropertyType;
                var propBuilder = FactoryHelpers.GetPropertyBuilder(args.TypeBuilder, propertyName, propertyType);
                args.CurrentPropertyBuilder = propBuilder;
                if (propBuilder != null)
                {
                    var innerProperty = args.BuilderStorageMember.FieldType.GetProperty(propertyName);
                    args.InterfaceMemberToBuild = innerProperty;

                    if (innerProperty.CanRead && cProperty.CanRead)
                    {
                        string getName = string.Format("get_{0}", propertyName);
                        var method1 = FactoryHelpers.GetPropertyMethodBuilder(args.TypeBuilder, getName, propertyType, Type.EmptyTypes);
                        if (method1 != null)
                        {
                            propBuilder.SetGetMethod(method1);
                            var ilGen1 = method1.GetILGenerator();
                            args.CurrentMethodBuilder = method1;
                            args.MethodGenerator = ilGen1;
                            args.ReturnType = propertyType;
                            args.ParameterTypes = Type.EmptyTypes;
                            proxyBuilder.BuildPropertyGetter(this, args);
                            args.TypeBuilder.DefineMethodOverride(method1, cProperty.GetGetMethod());
                        }
                    }

                    if (innerProperty.CanWrite && cProperty.CanWrite)
                    {
                        string setName = string.Format("set_{0}", propertyName);
                        var method2 = FactoryHelpers.GetPropertyMethodBuilder(args.TypeBuilder, setName, null, new Type[] { propertyType });
                        if (method2 != null)
                        {
                            propBuilder.SetSetMethod(method2);
                            var ilGen1 = method2.GetILGenerator();
                            args.CurrentMethodBuilder = method2;
                            args.MethodGenerator = ilGen1;
                            args.ReturnType = typeof(void);
                            args.ParameterTypes = new Type[] { propertyType };
                            proxyBuilder.BuildPropertySetter(this, args);
                            args.TypeBuilder.DefineMethodOverride(method2, cProperty.GetSetMethod());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a event for the proxy type whose signature matches that of the specified EventInfo.
        /// </summary>
        /// <param name="proxyBuilder">The IProxyBuilder that will be used to create the event.</param>
        /// <param name="args">The instructions used to describe to the builder how the member should be built.</param>
        /// <param name="cEvent">The event on the interface which should be built by this method.</param>
        private void BuildEvent(IProxyBuilder proxyBuilder, IEventBuildingSchematic args, EventInfo cEvent)
        {
            var typeBuilder = args.TypeBuilder;
            var fieldObject = args.BuilderStorageMember;
            if (typeBuilder != null && cEvent != null)
            {
                string eventName = cEvent.Name;
                Type eventHandlerType = cEvent.EventHandlerType;
                var eventBuilder = FactoryHelpers.GetEventBuilder(typeBuilder, eventName, eventHandlerType);
                if (eventBuilder != null)
                {
                    var innerEvent = fieldObject.FieldType.GetEventRecursive(eventName);
                    args.CurrentEventBuilder = eventBuilder;
                    args.InterfaceMemberToBuild = innerEvent;

                    string addName = string.Format("add_{0}", eventName);
                    var method1 = FactoryHelpers.GetPropertyMethodBuilder(typeBuilder, addName, null, new Type[] { eventHandlerType });
                    if (method1 != null)
                    {
                        args.CurrentMethodBuilder = method1;
                        args.MethodGenerator = method1.GetILGenerator();
                        args.ReturnType = null;
                        args.ParameterTypes = new Type[] { eventHandlerType };
                        eventBuilder.SetAddOnMethod(method1);
                        proxyBuilder.BuildEventAdder(this, args);
                        typeBuilder.DefineMethodOverride(method1, cEvent.GetAddMethod());
                    }

                    string removeName = string.Format("remove_{0}", eventName);
                    var method2 = FactoryHelpers.GetPropertyMethodBuilder(typeBuilder, removeName, null, new Type[] { eventHandlerType });
                    if (method2 != null)
                    {
                        args.CurrentMethodBuilder = method2;
                        args.MethodGenerator = method2.GetILGenerator();
                        args.ReturnType = null;
                        args.ParameterTypes = new Type[] { eventHandlerType };
                        eventBuilder.SetRemoveOnMethod(method2);
                        proxyBuilder.BuildEventRemover(this, args);
                        typeBuilder.DefineMethodOverride(method2, cEvent.GetRemoveMethod());
                    }
                }
            }
        }

        #endregion
        #endregion

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Release()
        {
            m_DefaultBuilder = null;
            m_TypeCache.Clear();
            if (m_Validator != null)
            {
                m_Validator.Release();
                m_Validator = null;
            }
        }

        public ConstructorInfo GetProxyConstructor(Type typeOfObjectToWrap, Type exposedType)
        {
            return m_TypeCache.GetProxyConstructor(typeOfObjectToWrap, exposedType);
        }

        public ITypeCache GetTypeCache()
        {
            return m_TypeCache;
        }
    }
}
