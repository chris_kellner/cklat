#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL CHRISTOPHER KELLNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Proxy.Builder
{
    #region Using List
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Diagnostics;
    #endregion
    public static class ProxyBuildingPatterns
    {
        /// <summary>
        /// Loads all arguments (and optionally translates them) from the parameter list onto the stack.
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="args"></param>
        public static void LoadAndTransformParameterTypes(IProxyFactory factory, IMethodBuildingSchematic args, MethodInfo bestMatchingWrappedFunction)
        {
            Type[] parameterTypes = args.ParameterTypes;
            var innerFunction = bestMatchingWrappedFunction;
            Type[] innerParameterTypes = FactoryHelpers.GetMethodParameterTypes(innerFunction);
            var ilGen = args.MethodGenerator;
            for (int i = 0; i < parameterTypes.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldarg, i + 1);
                var cIParam = innerParameterTypes[i];
                if (!cIParam.IsAssignableFrom(parameterTypes[i]))
                {
                    if (cIParam.IsSubclassOf(parameterTypes[i]))
                    {
                        BoxUnboxIt(ilGen, parameterTypes[i], cIParam);
                        Warning.WriteEntry(string.Format(
                            LogStrings.WARN_PARAM_CAST_CLASS,
                            parameterTypes[i].Name,
                            cIParam.Name, args.CurrentMethodBuilder.Name, args.TypeBuilder.Name));
                    }
                    else
                    {
                        // Skip validation and wrap premade types
                        var checkConst = factory.GetProxyConstructor(parameterTypes[i], cIParam);
                        if (checkConst != null)
                        {
                            ilGen.Emit(OpCodes.Newobj, checkConst);
                        }
                        // Convert TO (wrap)
                        else if (args.Validator.EnsureProxyCompatibility(factory, parameterTypes[i], new Type[] { cIParam }, false))
                        {
                            if (parameterTypes[i].IsValueType)
                            {
                                ilGen.Emit(OpCodes.Box, parameterTypes[i]);
                            }
                            MethodInfo info = typeof(ProxyFactory).GetMethod("CreateObjectTypeProxy", BindingFlags.Static | BindingFlags.Public);
                            TypeOf(ilGen, cIParam);
                            ilGen.Emit(OpCodes.Call, info);
                        }
                        // Convert BACK (unwrap)
                        else if (args.Validator.EnsureProxyCompatibility(factory, cIParam, new Type[] { parameterTypes[i] }, false))
                        {
                            MethodInfo info = typeof(ProxyBuildingPatterns).GetMethod("GenericUnwrap").MakeGenericMethod(parameterTypes[i], cIParam);
                            ilGen.Emit(OpCodes.Call, info);
                        }
                        else
                        {
                            // Try casting
                            BoxUnboxIt(ilGen, parameterTypes[i], cIParam);
                            Warning.WriteEntry(string.Format(
                                LogStrings.WARN_PARAM_CAST_CLASS,
                                parameterTypes[i].Name,
                                cIParam.Name, args.CurrentMethodBuilder.Name, args.TypeBuilder.Name));
                        }
                    }
                }
                else
                {
                    BoxUnboxIt(ilGen, parameterTypes[i], cIParam);
                }
            }
        }

        /// <summary>
        /// Adds a typeof(type) call to the ILGenerator.
        /// </summary>
        /// <param name="ilGen">The ILGenerator to add the call to.</param>
        /// <param name="type">The type to get the type of.</param>
        public static void TypeOf(ILGenerator ilGen, Type type)
        {
            MethodInfo getTypeInfo = typeof(Type).GetMethod("GetTypeFromHandle");
            ilGen.Emit(OpCodes.Ldtoken, type);
            ilGen.Emit(OpCodes.Call, getTypeInfo);
        }

        /// <summary>
        /// Pushes the default value for the specified type onto the top of the evaluation stack.
        /// </summary>
        /// <param name="ilGen">The ILGenerator to add the call to.</param>
        /// <param name="type">The type to get the default value of.</param>
        public static void Default(ILGenerator ilGen, Type type)
        {
            if (type.IsValueType)
            {
                var local = ilGen.DeclareLocal(type);
                ilGen.Emit(OpCodes.Ldloca_S, local);
                ilGen.Emit(OpCodes.Initobj, type);
                ilGen.Emit(OpCodes.Ldloc, local);
            }
            else
            {
                ilGen.Emit(OpCodes.Ldnull);
            }
        }

        /// <summary>
        /// This function returns the unwrapped version of the wrapped object (and wraps it again if it needs to),
        /// as a conversion to the targetType.
        /// </summary>
        /// <param name="obj">A Proxy object.</param>
        /// <param name="targetType">The type that should be returned from this function.</param>
        /// <returns>The wrapped object contained in the proxy.</returns>
        public static TOut GenericUnwrap<TIn, TOut>(TIn obj)
        {
            if (typeof(TOut).IsAssignableFrom(typeof(TIn)))
            {
                object tmp = obj;
                return (TOut)tmp;
            }

            IProxyObject proxy = obj as IProxyObject;
            if (proxy != null)
            {
                var wrapped = proxy.GetWrappedObject();
                if (wrapped != null)
                {
                    var wrappedType = proxy.GetWrapperType();
                    if (typeof(TOut).IsAssignableFrom(wrappedType))
                    {
                        return (TOut)wrapped;
                    }
                    else if (typeof(TOut).IsInterface && 
                        ProxyFactory.Instance.Validator.EnsureProxyCompatibility(ProxyFactory.Instance, wrappedType, new Type[] { typeof(TOut) }, false))
                    {
                        return (TOut)ProxyFactory.CreateObjectTypeProxy(wrappedType, typeof(TOut));
                    }
                }
            }

            return default(TOut);
        }

        /// <summary>
        /// This function returns the unwrapped version of the wrapped object (and wraps it again if it needs to),
        /// as a conversion to the targetType.
        /// </summary>
        /// <param name="obj">A Proxy object.</param>
        /// <param name="targetType">The type that should be returned from this function.</param>
        /// <returns>The wrapped object contained in the proxy.</returns>
        public static object Unwrap(object obj, Type targetType)
        {
            if (targetType.IsInstanceOfType(obj))
            {
                return obj;
            }

            IProxyObject proxy = obj as IProxyObject;
            if (proxy != null)
            {
                var wrapped = proxy.GetWrappedObject();
                if (wrapped != null)
                {
                    var wrappedType = proxy.GetWrapperType();
                    if (targetType.IsAssignableFrom(wrappedType))
                    {
                        return wrapped;
                    }
                    else if (ProxyFactory.Instance.Validator.EnsureProxyCompatibility(ProxyFactory.Instance, wrappedType, new Type[] { targetType }, false))
                    {
                        return ProxyFactory.CreateObjectTypeProxy(wrappedType, targetType);
                    }
                }
            }

            if (targetType.IsValueType)
            {
                return Activator.CreateInstance(targetType);
            }

            return null;
        }

        /// <summary>
        /// This function performs post function call type transformations to ensure that the proper type is on the stack to be returned.
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="args"></param>
        /// <param name="bestMatchingWrappedFunction"></param>
        /// <param name="returnTypeLocalVar">If this parameter is > -1, the return value is stored into a local variable at the specified
        /// index and then pushed back onto the stack at the end of this method.</param>
        public static void TransformReturnType(IProxyFactory factory, IMethodBuildingSchematic args, MethodInfo bestMatchingWrappedFunction, int returnTypeLocalVar)
        {
            Type returnType = args.ReturnType;
            var innerFunction = bestMatchingWrappedFunction;
            var ilGen = args.MethodGenerator;
            if (returnType != typeof(void))
            {
                if (!returnType.IsAssignableFrom(innerFunction.ReturnType))
                {
                    if (returnType.IsSubclassOf(innerFunction.ReturnType))
                    {
                        if (returnType.IsValueType)
                        {
                            ilGen.Emit(OpCodes.Castclass, returnType);
                        }
                        else
                        {
                            ilGen.Emit(OpCodes.Isinst, returnType);
                        }
                    }
                    else
                    {
                        var checkConst = factory.GetProxyConstructor(innerFunction.ReturnType, returnType);
                        // Skip validation and wrap
                        if (checkConst != null)
                        {
                            ilGen.Emit(OpCodes.Newobj, checkConst);
                        }
                        // Convert TO (wrap)
                        else if (args.Validator.EnsureProxyCompatibility(factory, innerFunction.ReturnType, new Type[] { returnType }, false))
                        {
                            if (innerFunction.ReturnType.IsValueType)
                            {
                                ilGen.Emit(OpCodes.Box, innerFunction.ReturnType);
                            }
                            MethodInfo info = typeof(ProxyFactory).GetMethod("CreateObjectTypeProxy", BindingFlags.Static | BindingFlags.Public);
                            TypeOf(ilGen, returnType);
                            ilGen.Emit(OpCodes.Call, info);
                        }
                        // Try casting
                        else
                        {
                            ilGen.Emit(OpCodes.Castclass, returnType);
                        }
                    }
                }
                else
                {
                    BoxUnboxIt(ilGen, innerFunction.ReturnType, returnType);
                }

                if (returnTypeLocalVar >= 0)
                {
                    ilGen.Emit(OpCodes.Stloc, returnTypeLocalVar);
                    ilGen.Emit(OpCodes.Ldloc, returnTypeLocalVar);
                }
            }
            //ilGen.Emit(OpCodes.Ret);                    // return
        }

        /// <summary>
        /// Boxes, Unboxes, or casts the specfied tIn type to the tOut type.
        /// </summary>
        /// <param name="ilGen">The ilGenerator to emit instructions to.</param>
        /// <param name="tIn">The type you are starting with.</param>
        /// <param name="tOut">The type you want to end up with.</param>
        public static void BoxUnboxIt(ILGenerator ilGen, Type tIn, Type tOut)
        {
            if (tIn.IsValueType)
            {
                if (!tOut.IsValueType)
                {
                    ilGen.Emit(OpCodes.Box, tIn);
                }
            }
            else if (tOut.IsValueType)
            {
                ilGen.Emit(OpCodes.Unbox_Any, tOut);
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, tOut);
            }
        }

        public static LocalBuilder ConstructEventHandler(ILGenerator ilGen, Type eventHandlerType, LocalBuilder eventHandlerTarget, MethodInfo eventHandlerMethod)
        {
            LocalBuilder handlerLocal = ilGen.DeclareLocal(eventHandlerType);
            ilGen.Emit(OpCodes.Ldloc, eventHandlerTarget);
            ilGen.Emit(OpCodes.Ldftn, eventHandlerMethod);
            ilGen.Emit(OpCodes.Newobj, eventHandlerType.GetConstructor(new Type[] { typeof(object), typeof(IntPtr) }));
            ilGen.Emit(OpCodes.Stloc, handlerLocal);
            return handlerLocal;
        }

        public static void CallFunctionWithLocals(ILGenerator ilGen, LocalBuilder localOwningMethod, MethodInfo fieldMethodToCall, params LocalBuilder[] localsToPassToFunction)
        {
            ilGen.Emit(OpCodes.Ldloc, localOwningMethod);
            for (int i = 0; i < localsToPassToFunction.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldloc, localsToPassToFunction[i]);
            }
            ilGen.Emit(OpCodes.Callvirt, fieldMethodToCall);
        }

        public static void CallFieldFunctionWithLocals(ILGenerator ilGen, FieldInfo fieldOwningMethod, MethodInfo fieldMethodToCall, params LocalBuilder[] localsToPassToFunction)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldfld, fieldOwningMethod);
            for (int i = 0; i < localsToPassToFunction.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldloc, localsToPassToFunction[i]);
            }
            ilGen.Emit(OpCodes.Callvirt, fieldMethodToCall);
        }

        public static void CallFieldFunctionWithArguments(ILGenerator ilGen, FieldInfo fieldOwningMethod, MethodInfo fieldMethodToCall, params int[] indexesOfArgumentsToPassToFunction)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldfld, fieldOwningMethod);
            for (int i = 0; i < indexesOfArgumentsToPassToFunction.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldarg, indexesOfArgumentsToPassToFunction[i]);
            }
            ilGen.Emit(OpCodes.Callvirt, fieldMethodToCall);
        }

        public static void GotoLabelIfLocalIsNull(ILGenerator ilGen, LocalBuilder localVariableToNullCheck, Label labelToGoToIfVariableIsNull)
        {
            ilGen.Emit(OpCodes.Ldloc, localVariableToNullCheck);
            GotoLabelIfStackPopIsFalse(ilGen, labelToGoToIfVariableIsNull);
        }

        public static void GotoLabelIfStackPopIsFalse(ILGenerator ilGen, Label labelToGoToIfVariableIsNull)
        {
            //  Assumes item is already on stack....
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Ceq);
            ilGen.Emit(OpCodes.Ldc_I4_0);
            ilGen.Emit(OpCodes.Ceq);
            ilGen.Emit(OpCodes.Brfalse_S, labelToGoToIfVariableIsNull);
        }

        public static void GotoLabelIfStackPopIsTrue(ILGenerator ilGen, Label labelToGoToIfVariableIsNull)
        {
            //  Assumes item is already on stack....
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Ceq);
            ilGen.Emit(OpCodes.Ldc_I4_0);
            ilGen.Emit(OpCodes.Ceq);
            ilGen.Emit(OpCodes.Brtrue_S, labelToGoToIfVariableIsNull);
        }

        /// <summary>
        /// Adds IL to the generator equivalent to: m_fieldObject.[Func](params);
        /// Does not add a return statement.
        /// 
        /// Loads 'this', the field, and all method arguments onto the stack, then calls the inner function.
        /// If the inner function has a return value other than void, that value is pushed onto the stack.
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="args"></param>
        /// <param name="myMethod"></param>
        public static void CallStorageMemberPassthrough(IProxyFactory factory, IMethodBuildingSchematic args, MethodInfo myMethod)
        {
            Type[] parameters = args.ParameterTypes;
            MethodInfo innerFunction = null;
            var ilGen = args.MethodGenerator;
            if (args.InterfaceMemberToBuild is PropertyInfo)
            {
                IPropertyBuildingSchematic propSchem = (IPropertyBuildingSchematic)args;
                // Setter:
                if (myMethod.ReturnType == typeof(void))
                {
                    innerFunction = ((PropertyInfo)propSchem.InterfaceMemberToBuild).GetSetMethod();
                }

                // Getter:
                else
                {
                    innerFunction = ((PropertyInfo)propSchem.InterfaceMemberToBuild).GetGetMethod();
                }
            }
            else
            {
                innerFunction = args.Validator.FindBestMatchingMethodInWrapType(factory, args.TypeOfObjectBeingWrapped, (MethodInfo)args.InterfaceMemberToBuild);
            }

            if (innerFunction != null)
            {
                ilGen.Emit(OpCodes.Ldarg_0);                // this
                if (args.BuilderStorageMember.FieldType.IsValueType)
                {
                    ilGen.Emit(OpCodes.Ldflda, args.BuilderStorageMember);     // .fieldObject
                }
                else
                {
                    ilGen.Emit(OpCodes.Ldfld, args.BuilderStorageMember);     // .fieldObject
                }

                LoadAndTransformParameterTypes(factory, args, innerFunction);
                ilGen.Emit(OpCodes.Callvirt, innerFunction); // .myMethod()
                TransformReturnType(factory, args, innerFunction, -1);
            }
        }

        /// <summary>
        /// Adds IL to the generator equivalent to: base.[Func](params);
        /// Does not add a return statement.
        /// 
        /// Loads 'this' and all arguments onto the stack, then calls the base function.
        /// If the base function has a return type other than void then that value is pushed onto the stack.
        /// </summary>
        /// <param name="ilGen"></param>
        /// <param name="myMethod"></param>
        /// <param name="parameterTypes"></param>
        public static void CallBasePassthrough(ILGenerator ilGen, MethodBase myMethod, Type[] parameterTypes)
        {
            var myType = myMethod.DeclaringType;
            if (myType != null)
            {
                Type[] parameters = parameterTypes;
                var baseType = myType.BaseType;
                if (baseType != null)
                {
                    MethodBase baseMethod = null;
                    bool isCtor = false;
                    if (myMethod.Name == ConstructorInfo.ConstructorName)
                    {
                        baseMethod = baseType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, parameters, null);
                        isCtor = true;
                    }
                    else
                    {
                        baseMethod = baseType.GetMethod(myMethod.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, parameters, null);
                    }

                    if (baseMethod != null)
                    {
                        ilGen.Emit(OpCodes.Ldarg_0);    // this
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            ilGen.Emit(OpCodes.Ldarg, i + 1);
                        }

                        if (isCtor)
                        {
                            ilGen.Emit(OpCodes.Call, (ConstructorInfo)baseMethod);
                        }
                        else
                        {
                            ilGen.Emit(OpCodes.Call, (MethodInfo)baseMethod);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates a passthrough constructor to the matching constructor on the base type.
        /// </summary>
        /// <param name="typeBuilder"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public static ConstructorBuilder BuildBaseConstructor(TypeBuilder typeBuilder, ConstructorInfo info)
        {
            if (typeBuilder != null && info != null)
            {

                Type[] parameterTypes = FactoryHelpers.GetMethodParameterTypes(info);
                var builder = typeBuilder.DefineConstructor(
                  FactoryHelpers.ConstructorImplementation,
                   CallingConventions.HasThis | CallingConventions.Standard,
                   parameterTypes);

                if (builder != null)
                {
                    var ilGen = builder.GetILGenerator();
                    CallBasePassthrough(ilGen, builder, parameterTypes);
                    ilGen.Emit(OpCodes.Ret);
                }
                return builder;
            }
            return null;
        }
    }
}
