﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Proxy.Builder
{
    public class DelegatingProxyBuilder : IProxyBuilder
    {
        public enum SchematicType
        {
            Constructor,
            Method,
            PropertyGetter,
            PropertySetter,
            EventAdder,
            EventRemover
        }

        public class SchematicEventArgs : EventArgs
        {
            public SchematicEventArgs(IProxyFactory factory, IProxyBuildingSchematic schematic, SchematicType type)
            {
                Factory = factory;
                Schematic = schematic;
                EventType = type;
            }

            public SchematicType EventType { get; private set; }
            public IProxyFactory Factory { get; private set; }
            public IProxyBuildingSchematic Schematic { get; private set; }
        }

        public void BuildConstructor(IProxyFactory proxyFactory, IConstructorBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.Constructor));
        }

        public void BuildMethod(IProxyFactory proxyFactory, IMethodBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.Method));
        }

        public void BuildPropertyGetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.PropertyGetter));
        }

        public void BuildPropertySetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.PropertySetter));
        }

        public void BuildEventAdder(IProxyFactory proxyFactory, IEventBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.EventAdder));
        }

        public void BuildEventRemover(IProxyFactory proxyFactory, IEventBuildingSchematic eventArgs)
        {
            OnBuildEvent(new SchematicEventArgs(proxyFactory, eventArgs, SchematicType.EventRemover));
        }

        protected virtual void OnBuildEvent(SchematicEventArgs args)
        {
            if (BuildEvent != null)
            {
                BuildEvent(this, args);
            }
        }

        public event EventHandler<SchematicEventArgs> BuildEvent;
    }
}
