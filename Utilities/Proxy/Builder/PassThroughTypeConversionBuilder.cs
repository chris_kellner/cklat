#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL CHRISTOPHER KELLNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Proxy.Builder
{
    #region Using List
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    #endregion
    /// <summary>
    /// This builder is used for creating a type conversion proxy.
    /// </summary>
    public class PassThroughTypeConversionBuilder : IProxyBuilder
    {
        #region IProxyBuilder Members

        /// <summary>
        /// Builds the constructor described by the input schematic.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the constructor.</param>
        public virtual void BuildConstructor(IProxyFactory proxyFactory, IConstructorBuildingSchematic eventArgs)
        {
            var info = eventArgs.CurrentConstructorBuilder;
            if (info != null)
            {
                Type[] parameters = eventArgs.ParameterTypes;

                ProxyBuildingPatterns.CallBasePassthrough(
                    eventArgs.MethodGenerator,
                    info, eventArgs.ParameterTypes);

                // Check to see if it is the special constructor
                if (parameters.Length == 1 && parameters[0].IsAssignableFrom(eventArgs.TypeOfObjectBeingWrapped))
                {
                    eventArgs.MethodGenerator.Emit(OpCodes.Ldarg_0);    // push this
                    eventArgs.MethodGenerator.Emit(OpCodes.Ldarg_1);    // push wrap object
                    eventArgs.MethodGenerator.Emit(OpCodes.Stfld, eventArgs.BuilderStorageMember);    // store wrap object in the field.
                }

                eventArgs.MethodGenerator.Emit(OpCodes.Ret);
            }
        }

        /// <summary>
        /// Builds the method.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the method.</param>
        public virtual void BuildMethod(IProxyFactory proxyFactory, IMethodBuildingSchematic eventArgs)
        {
            ProxyBuildingPatterns.CallStorageMemberPassthrough(proxyFactory, eventArgs, (MethodInfo)eventArgs.InterfaceMemberToBuild);
            eventArgs.MethodGenerator.Emit(OpCodes.Ret);
        }

        /// <summary>
        /// Builds the property getter.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the property getter.</param>
        public virtual void BuildPropertyGetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            var ilGen1 = eventArgs.MethodGenerator;
            var propertyInfo = eventArgs.InterfaceMemberToBuild as PropertyInfo;
            if (propertyInfo != null)
            {
                //ProxyBuildingPatterns.CallBasePassthrough(ilGen1, propertyInfo.GetGetMethod(), eventArgs.ParameterTypes);
                ProxyBuildingPatterns.CallStorageMemberPassthrough(proxyFactory, eventArgs, propertyInfo.GetGetMethod());
                ilGen1.Emit(OpCodes.Ret);  // return
            }
        }

        /// <summary>
        /// Builds the property setter.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the property setter.</param>
        public virtual void BuildPropertySetter(IProxyFactory proxyFactory, IPropertyBuildingSchematic eventArgs)
        {
            var ilGen1 = eventArgs.MethodGenerator;
            var propertyInfo = eventArgs.InterfaceMemberToBuild as PropertyInfo;
            if (propertyInfo != null)
            {
                //ProxyBuildingPatterns.CallBasePassthrough(ilGen1, propertyInfo.GetSetMethod(), eventArgs.ParameterTypes);
                ProxyBuildingPatterns.CallStorageMemberPassthrough(proxyFactory, eventArgs, propertyInfo.GetSetMethod());
                ilGen1.Emit(OpCodes.Ret);  // return
            }
        }

        /// <summary>
        /// Builds the event adder.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the event adder.</param>
        public virtual void BuildEventAdder(IProxyFactory proxyFactory, IEventBuildingSchematic eventArgs)
        {
            var innerEvent = eventArgs.InterfaceMemberToBuild as EventInfo;
            if (innerEvent != null)
            {
                var ilGen1 = eventArgs.MethodGenerator;
                if (innerEvent.EventHandlerType.IsAssignableFrom(eventArgs.ParameterTypes[0]))
                {
                    ProxyBuildingPatterns.CallFieldFunctionWithArguments(
                        ilGen1,
                        eventArgs.BuilderStorageMember,
                        innerEvent.GetAddMethod(),
                        1);
                }
                else
                {
                    var castMethod = typeof(FactoryHelpers).GetMethod("CastDelegate", BindingFlags.Static | BindingFlags.Public,
                        null, CallingConventions.Any, new Type[] { typeof(Type), typeof(Delegate) }, null);
                    LocalBuilder convertedHandler = ilGen1.DeclareLocal(typeof(Delegate));

                    // Need to cast the argument as the desired type
                    ProxyBuildingPatterns.TypeOf(ilGen1, innerEvent.EventHandlerType);
                    ilGen1.Emit(OpCodes.Ldarg_1);
                    ilGen1.Emit(OpCodes.Call, castMethod);
                    ilGen1.Emit(OpCodes.Stloc, convertedHandler);

                    // Add it in
                    ProxyBuildingPatterns.CallFieldFunctionWithLocals(
                        ilGen1,
                        eventArgs.BuilderStorageMember,
                        innerEvent.GetAddMethod(),
                        convertedHandler);
                }

                ilGen1.Emit(OpCodes.Ret);
            }
        }

        /// <summary>
        /// Builds the event remover.
        /// </summary>
        /// <param name="proxyFactory">The proxy factory.</param>
        /// <param name="eventArgs">The arguments describing how to build the event remover.</param>
        public virtual void BuildEventRemover(IProxyFactory proxyFactory, IEventBuildingSchematic eventArgs)
        {
             var innerEvent = eventArgs.InterfaceMemberToBuild as EventInfo;
             if (innerEvent != null)
             {
                 var ilGen1 = eventArgs.MethodGenerator;
                 if (innerEvent.EventHandlerType.IsAssignableFrom(eventArgs.ParameterTypes[0]))
                 {
                     ProxyBuildingPatterns.CallFieldFunctionWithArguments(
                         ilGen1,
                         eventArgs.BuilderStorageMember,
                         innerEvent.GetRemoveMethod(),
                         1);
                 }
                 else
                 {
                     var castMethod = typeof(FactoryHelpers).GetMethod("CastDelegate", BindingFlags.Static | BindingFlags.Public,
                         null, CallingConventions.Any, new Type[] { typeof(Type), typeof(Delegate) }, null);
                     LocalBuilder convertedHandler = ilGen1.DeclareLocal(typeof(Delegate));

                     // Need to cast the argument as the desired type
                     ProxyBuildingPatterns.TypeOf(ilGen1, innerEvent.EventHandlerType);
                     ilGen1.Emit(OpCodes.Ldarg_1);
                     ilGen1.Emit(OpCodes.Call, castMethod);
                     ilGen1.Emit(OpCodes.Stloc, convertedHandler);

                     // Add it in
                     ProxyBuildingPatterns.CallFieldFunctionWithLocals(
                         ilGen1,
                         eventArgs.BuilderStorageMember,
                         innerEvent.GetRemoveMethod(),
                         convertedHandler);
                 }

                 ilGen1.Emit(OpCodes.Ret);
             }
        }
        #endregion
    }
}
