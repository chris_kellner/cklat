﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public class MethodDefinition
    {
        private MethodBase m_Method;
        private Type[] m_ParameterTypes;

        public MethodDefinition(ConstructorBuilder cons, Type[] parameterTypes)
        {
            m_Method = cons;
            m_ParameterTypes = parameterTypes;
        }

        public MethodDefinition(MethodBuilder cons, Type[] parameterTypes)
        {
            m_Method = cons;
            m_ParameterTypes = parameterTypes;
        }

        public MethodDefinition(DynamicMethod cons)
        {
            m_Method = cons;
            m_ParameterTypes = cons.GetParameters().Select(p=>p.GetType()).ToArray();
        }

        public MethodBase Method { get { return m_Method; } }

        public Type[] GetParameterTypes()
        {
            return m_ParameterTypes;
        }

        public MethodImplAttributes GetMethodImplementationFlags()
        {
            return m_Method.GetMethodImplementationFlags();
        }

        public Type DeclaringType
        {
            get { return m_Method.DeclaringType; }
        }

        public Type ReturnType
        {
            get
            {
                if (m_Method is MethodInfo)
                {
                    return ((MethodInfo)m_Method).ReturnType;
                }
                else
                {
                    return DeclaringType;
                }
            }
        }

        public string Name
        {
            get { return m_Method.Name; }
        }

        public ILGenerator GetILGenerator()
        {
            if (m_Method is MethodBuilder)
            {
                return ((MethodBuilder)m_Method).GetILGenerator();
            }
            else if (m_Method is ConstructorBuilder)
            {
                return ((ConstructorBuilder)m_Method).GetILGenerator();
            }
            else if (m_Method is DynamicMethod)
            {
                return ((DynamicMethod)m_Method).GetILGenerator();
            }

            throw new NotSupportedException("Invalid method type was detected");
        }
    }
}
