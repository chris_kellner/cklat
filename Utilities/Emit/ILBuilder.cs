﻿using CKLib.Utilities.Emit.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public class ParameterLoadedEventArgs : LoadEventArgs
    {
        public ParameterLoadedEventArgs(ILGenerator ilGen, ICallParameter parameter, Type neededType, int parameterIndex)
            : base(ilGen)
        {
            Parameter = parameter;
            NeededType = neededType;
            ParameterIndex = parameterIndex;
        }

        public ICallParameter Parameter { get; private set; }
        public Type NeededType { get; private set; }
        public int ParameterIndex { get; private set; }
    }

    public class ILBuilder
    {
        #region Variables
        private ILGenerator m_ILGen;
        private MethodDefinition m_Method;
        #endregion

        #region Constructors
        public ILBuilder(MethodDefinition method)
        {
            m_ILGen = method.GetILGenerator();
            m_Method = method;
        }
        #endregion

        #region Properties
        public ILGenerator InnerGenerator
        {
            get
            {
                return m_ILGen;
            }
        }

        public MethodDefinition Method
        {
            get
            {
                return m_Method;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a <c>typeof(type)</c> call to the ILGenerator.
        /// </summary>
        /// <param name="ilGen">The ILGenerator to add the call to.</param>
        /// <param name="type">The type to get the type of.</param>
        public ILBuilder TypeOf(Type type)
        {
            new LiteralCallParameter(type).Load(m_ILGen);
            return this;
        }

        /// <summary>
        /// Pushes the default value for the specified type onto the top of the evaluation stack.
        /// </summary>
        /// <param name="ilGen">The ILGenerator to add the call to.</param>
        /// <param name="type">The type to get the default value of.</param>
        public ILBuilder Default(Type type)
        {
            if (type.IsValueType)
            {
                var local = m_ILGen.DeclareLocal(type);
                m_ILGen.Emit(OpCodes.Ldloca_S, local);
                m_ILGen.Emit(OpCodes.Initobj, type);
                m_ILGen.Emit(OpCodes.Ldloc, local);
            }
            else
            {
                m_ILGen.Emit(OpCodes.Ldnull);
            }

            return this;
        }

        public ILBuilder This()
        {
            m_ILGen.Emit(OpCodes.Ldarg_0);
            return this;
        }

        public ILBuilder Pop()
        {
            m_ILGen.Emit(OpCodes.Pop);
            return this;
        }

        public ILBuilder Return()
        {
            m_ILGen.Emit(OpCodes.Ret);
            return this;
        }

        public ILBuilder Box(Type valueType)
        {
            if (valueType.IsValueType)
            {
                m_ILGen.Emit(OpCodes.Box, valueType);
            }

            return this;
        }

        public ILBuilder Unbox(Type currentType, Type valueType)
        {
            if (valueType.IsValueType)
            {
                if (currentType.IsByRef)
                {
                    m_ILGen.Emit(OpCodes.Unbox, valueType);
                }
                else
                {
                    m_ILGen.Emit(OpCodes.Unbox_Any, valueType);
                }
            }

            return this;
        }

        public ILBuilder Unbox(Type valueType)
        {
            if (valueType.IsValueType)
            {
                m_ILGen.Emit(OpCodes.Unbox_Any, valueType);
            }

            return this;
        }

        public ILBuilder Call(MethodInfo method, params ICallParameter[] parameters)
        {
            return Call(method, null, parameters);
        }

        public ILBuilder Call(MethodInfo method, EventHandler<LoadEventArgs> methodCallLoadedHandler, params ICallParameter[] parameters)
        {
            MethodCallParameter call = new MethodCallParameter()
            {
                Method = method,
                Parameters = parameters
            };

            if (methodCallLoadedHandler != null)
            {
                call.Loaded += methodCallLoadedHandler;
            }

            call.Load(m_ILGen);
            return this;
        }

        public ILBuilder Equals(Type lValue, Type rValue)
        {
            var equality = lValue.GetMethod("op_Equality", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            if (equality == null)
            {
                equality = rValue.GetMethod("op_Equality", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            }

            if (equality != null)
            {
                m_ILGen.Emit(OpCodes.Call, equality);
            }
            else
            {
                m_ILGen.Emit(OpCodes.Ceq);
            }

            return this;
        }

        /// <summary>
        /// Calls the default constructor for the specified type.
        /// </summary>
        /// <param name="typeToCreate"></param>
        /// <returns></returns>
        public ILBuilder New(Type typeToCreate)
        {
            var cons = typeToCreate.GetConstructor(Type.EmptyTypes);
            if (cons == null)
            {
                throw new ArgumentException("Specified type does not define a default constructor");
            }
            m_ILGen.Emit(OpCodes.Newobj, cons);
            return this;
        }

        public ILBuilder New(ConstructorInfo method, params ICallParameter[] parameters)
        {
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    parameters[i].Load(m_ILGen);
                }
            }

            m_ILGen.Emit(OpCodes.Newobj, method);
            return this;
        }

        public ILBuilder InitArray(LocalBuilder local, int size)
        {
            if (!local.LocalType.IsArray)
            {
                throw new ArgumentException("Specified local is not an array type");
            }

            if (size < 0)
            {
                throw new ArgumentException("Specified size cannot be less than 0");
            }

            m_ILGen.Emit(OpCodes.Ldc_I4, size);
            m_ILGen.Emit(OpCodes.Newarr, local.LocalType.GetElementType());
            m_ILGen.Emit(OpCodes.Stloc, local);
            return this;
        }

        public ILBuilder LoadField(FieldInfo field)
        {
            FieldCallParameter tgt = new FieldCallParameter()
            {
                Field = field
            };

            tgt.Load(m_ILGen);
            return this;
        }

        public ILBuilder LoadProperty(PropertyInfo prop)
        {
            PropertyCallParameter tgt = new PropertyCallParameter()
            {
                Property = prop
            };

            tgt.Load(m_ILGen);
            return this;
        }

        public ILBuilder LoadMember(MemberInfo member)
        {
            if (member is FieldInfo)
            {
                return LoadField((FieldInfo)member);
            }
            else if (member is PropertyInfo)
            {
                return LoadProperty((PropertyInfo)member);
            }
            else
            {
                throw new NotSupportedException("Received unsupported member type: " + member.GetType());
            }
        }

        public ILBuilder LoadArgument(int index)
        {
            ArgumentCallParameter arg = new ArgumentCallParameter(m_Method, index);
            arg.Load(m_ILGen);
            return this;
        }

        public ILBuilder LoadLocal(LocalBuilder local)
        {
            LocalCallParameter arg = new LocalCallParameter(local);
            arg.Load(m_ILGen);
            return this;
        }

        public ILBuilder LoadLiteral(object literal)
        {
            LiteralCallParameter arg = new LiteralCallParameter(literal);
            arg.Load(m_ILGen);
            return this;
        }

        public ILBuilder StoreMember(MemberInfo targetMember, ICallParameter value)
        {
            value.Load(m_ILGen);
            return StoreMember(targetMember);
        }

        public ILBuilder StoreLocal(LocalBuilder targetLocal, ICallParameter value)
        {
            value.Load(m_ILGen);
            return StoreLocal(targetLocal);
        }

        public ILBuilder StoreArg(int targetArgIndex, ICallParameter value)
        {
            value.Load(m_ILGen);
            return StoreArg(targetArgIndex);
        }

        public ILBuilder StoreField(FieldInfo field)
        {
            m_ILGen.Emit(OpCodes.Stfld, field);
            return this;
        }

        public ILBuilder StoreProperty(PropertyInfo prop)
        {
            MethodCallParameter call = new MethodCallParameter()
            {
                Method = prop.GetSetMethod()
            };

            call.Load(m_ILGen);
            return this;
        }

        public ILBuilder StoreMember(MemberInfo member)
        {
            if (member is FieldInfo)
            {
                return StoreField((FieldInfo)member);
            }
            else if (member is PropertyInfo)
            {
                return StoreProperty((PropertyInfo)member);
            }
            else
            {
                throw new NotSupportedException("Received unsupported member type: " + member.GetType());
            }
        }

        public ILBuilder StoreLocal(LocalBuilder local)
        {
            m_ILGen.Emit(OpCodes.Stloc_S, local);
            return this;
        }

        public ILBuilder StoreElement(LocalBuilder array, int index, ICallParameter value)
        {
            m_ILGen.Emit(OpCodes.Ldloc, array);
            m_ILGen.Emit(OpCodes.Ldc_I4, index);
            value.Load(m_ILGen);
            var tArray = array.LocalType;
            if (tArray.IsArray)
            {
                tArray = tArray.GetElementType();
            }

            if (!tArray.IsValueType)
            {
                if (value.ParameterType.IsValueType)
                {
                    m_ILGen.Emit(OpCodes.Box, value.ParameterType);
                }
            }

            m_ILGen.Emit(OpCodes.Stelem, value.ParameterType);
            return this;
        }

        public ILBuilder StoreArg(int index)
        {
            m_ILGen.Emit(OpCodes.Starg_S, index);
            return this;
        }

        public ILBuilder Is(Type type)
        {
            m_ILGen.Emit(OpCodes.Isinst, type);
            m_ILGen.Emit(OpCodes.Ldnull);
            m_ILGen.Emit(OpCodes.Ceq);
            m_ILGen.Emit(OpCodes.Ldc_I4_0);
            m_ILGen.Emit(OpCodes.Ceq);
            return this;
        }

         public ILBuilder As(Type type)
        {
            m_ILGen.Emit(OpCodes.Isinst, type);
            return this;
        }

        public ILBuilder Cast(Type currentType, Type targetType)
        {
            if (currentType != targetType)
            {
                if (currentType.IsValueType)
                {
                    if (!targetType.IsValueType)
                    {
                        Box(currentType);
                        m_ILGen.Emit(OpCodes.Castclass, targetType);
                    }
                    else
                    {
                        // Both value types... what do?
                        throw new InvalidOperationException(
                            string.Format("Desired cast is not supported ({0}){1}", targetType, currentType));
                    }
                }
                else
                {
                    if (targetType.IsValueType)
                    {
                        Unbox(targetType);
                    }
                    else
                    {
                        m_ILGen.Emit(OpCodes.Castclass, targetType);
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Implements a for-loop by accepting a function which recieves the ILBuilder and
        /// the Loop-Control variable (type int), and the number of iterations.
        /// </summary>
        /// <param name="iteration"></param>
        /// <param name="numIterations"></param>
        /// <returns></returns>
        public ILBuilder For(int numIterations, Action<ILBuilder, LocalBuilder> iteration)
        {
            var lcv = m_ILGen.DeclareLocal(typeof(int));
            var start = m_ILGen.DefineLabel();
            var end = m_ILGen.DefineLabel();
            // int i = 0
            m_ILGen.Emit(OpCodes.Ldc_I4_0);
            m_ILGen.Emit(OpCodes.Stloc, lcv);
            m_ILGen.MarkLabel(start);
            // i < numIterations
            m_ILGen.Emit(OpCodes.Ldloc, lcv);
            m_ILGen.Emit(OpCodes.Ldc_I4, numIterations);
            m_ILGen.Emit(OpCodes.Clt);
            m_ILGen.Emit(OpCodes.Brfalse_S, end);
            // Body
            iteration(this, lcv);
            // i++;
            m_ILGen.Emit(OpCodes.Ldloc, lcv);
            m_ILGen.Emit(OpCodes.Ldc_I4_1);
            m_ILGen.Emit(OpCodes.Add);
            m_ILGen.Emit(OpCodes.Stloc, lcv);
            // Back to start
            m_ILGen.Emit(OpCodes.Br_S, start);
            m_ILGen.MarkLabel(end);
            return this;
        }

        public ILBuilder If(Action<ILBuilder> then, Action<ILBuilder> @else)
        {
            Label lbl = m_ILGen.DefineLabel();
            Label lbl2 = m_ILGen.DefineLabel();
            m_ILGen.Emit(OpCodes.Brfalse_S, lbl2);
            if (then != null)
            {
                then(this);
            }
            m_ILGen.Emit(OpCodes.Br_S, lbl);
            m_ILGen.MarkLabel(lbl2);
            if (@else != null)
            {
                @else(this);
            }

            m_ILGen.MarkLabel(lbl);
            return this;
        }

        public ILBuilder Load(ILoadable loadable)
        {
            loadable.Load(InnerGenerator);
            return this;
        }

        public ILBuilder If(ICallParameter rValue, OpCode comparison, Action<ILBuilder> then, Action<ILBuilder> @else)
        {
            rValue.Load(m_ILGen);
            m_ILGen.Emit(comparison);
            Label lbl = m_ILGen.DefineLabel();
            Label lbl2 = m_ILGen.DefineLabel();
            m_ILGen.Emit(OpCodes.Brfalse_S, lbl);
            then(this);
            m_ILGen.MarkLabel(lbl2);
            @else(this);
            m_ILGen.MarkLabel(lbl);
            return this;
        }

         /// <summary>
        /// Delegates the current method to the return value of the target.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public ILBuilder DelegateTo(ICallParameter target)
        {
            return DelegateTo(target, null, null, null);
        }

        /// <summary>
        /// Delegates the current method to the return value of the target.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="binder"></param>
        /// <param name="parameterLoadedHandler"></param>
        /// <returns></returns>
        public ILBuilder DelegateTo(ICallParameter target, Binder binder, EventHandler<ParameterLoadedEventArgs> parameterLoadedHandler, EventHandler<LoadEventArgs> methodCallLoadedHandler)
        {
            MethodInfo meth = null;
            try
            {
                meth = target.ParameterType.GetMethod(
                    Method.Name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy,
                    binder,
                    Method.GetParameterTypes(),
                    null);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Could not locate an appropriate match on the target type", ex);
            }

            if (meth == null)
            {
                throw new InvalidOperationException("Could not locate an appropriate match on the target type");
            }

            target.Load(m_ILGen);
            return Call(meth, methodCallLoadedHandler, Method.GetCallParameters((p, e) =>
            {
                if (parameterLoadedHandler != null)
                {
                    ArgumentCallParameter arg = p as ArgumentCallParameter;
                    int index = arg.Index;
                    if (!meth.IsStatic)
                    {
                        index--;
                    }

                    parameterLoadedHandler(this, new ParameterLoadedEventArgs(
                        m_ILGen,
                        arg,
                        meth.GetParameters()[index].ParameterType,
                        index));
                }
            }));
        }

        public ILBuilder Throw<TException>(string message) where TException : Exception
        {
            var cons = typeof(TException).GetConstructor(new Type[] { typeof(string) });
            New(cons, new LiteralCallParameter(message));
            m_ILGen.Emit(OpCodes.Throw);
            return this;
        }

        public ILBuilder WriteLine(string message)
        {
            m_ILGen.EmitWriteLine(message);
            return this;
        }

        public ILBuilder WriteLine(FieldInfo field)
        {
            m_ILGen.EmitWriteLine(field);
            return this;
        }

        public ILBuilder WriteLine(LocalBuilder local)
        {
            m_ILGen.EmitWriteLine(local);
            return this;
        }
        #endregion
    }
}
