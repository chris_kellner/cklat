﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public interface ITargetedCallParameter : ICallParameter
    {
        ICallParameter Target { get; set; }
    }
}
