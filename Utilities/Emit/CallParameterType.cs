﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public enum CallParameterType
    {
        Literal = 0,
        Local,
        Argument,
        Element,
        Field,
        Property,
        MethodResult
    }
}
