﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public interface ICallParameter : ILoadable
    {
        CallParameterType CallParameterType { get; }
        Type ParameterType { get; }
    }
}
