﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit
{
    public class LoadEventArgs : EventArgs
    {
        public LoadEventArgs(ILGenerator ilGen)
        {
            Generator = ilGen;
        }

        public ILGenerator Generator
        {
            get;
            private set;
        }
    }

    public interface ILoadable
    {
        void Load(ILGenerator ilGen);
        event EventHandler<LoadEventArgs> Loaded;
    }
}
