﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class PropertyCallParameter : ITargetedCallParameter
    {
        public ICallParameter Target { get; set; }

        public PropertyInfo Property { get; set; }

        public CallParameterType CallParameterType
        {
            get { return CallParameterType.Property; }
        }

        public Type ParameterType { get { return Property.PropertyType; } }

        public void Load(ILGenerator ilGen)
        {
            if (Target != null)
            {
                Target.Load(ilGen);
            }

            ilGen.Emit(OpCodes.Callvirt, Property.GetGetMethod());
            OnLoaded(ilGen);
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
