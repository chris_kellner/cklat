﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class FieldCallParameter : ITargetedCallParameter
    {
        public CallParameterType CallParameterType
        {
            get { return CallParameterType.Field; }
        }

        public Type ParameterType { get { return Field.FieldType; } }

        public FieldCallParameter()
        {
        }

        public FieldCallParameter(ICallParameter target, FieldInfo info)
        {
            Target = target;
            Field = info;
        }

        public ICallParameter Target { get; set; }
        public FieldInfo Field { get; set; }
        public bool IsAddress { get; set; }

        public void Load(ILGenerator ilGen)
        {
            if (Target != null)
            {
                Target.Load(ilGen);
            }

            if (!IsAddress)
            {
                ilGen.Emit(OpCodes.Ldfld, Field);
            }
            else
            {
                ilGen.Emit(OpCodes.Ldflda, Field);
            }

            OnLoaded(ilGen);
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
