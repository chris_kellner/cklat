﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class ArgumentCallParameter : ICallParameter
    {
        private static readonly OpCode[] ldarg_codes = { OpCodes.Ldarg_0, OpCodes.Ldarg_1, OpCodes.Ldarg_2, OpCodes.Ldarg_3 };

        public ArgumentCallParameter()
        {
        }

        public ArgumentCallParameter(Type argumentType, int index)
        {
            ParameterType = argumentType;
            Index = index;
        }

        public ArgumentCallParameter(MethodDefinition method, int index)
        {
            Index = index;
            if (!method.Method.IsStatic)
            {
                index -= 1;
            }

            Type t = method.GetParameterTypes()[index];
            ParameterType = t;
            IsByRef = ParameterType.IsByRef;
        }

        public CallParameterType CallParameterType
        {
            get { return CallParameterType.Argument; }
        }

        public int Index { get; set; }
        public bool IsByRef { get; set; }
        public Type ParameterType { get; set; }

        public static ArgumentCallParameter This(Type thisType)
        {
            return new ArgumentCallParameter(thisType, 0);
        }

        public void Load(ILGenerator ilGen)
        {
            if (!IsByRef)
            {
                if (Index < ldarg_codes.Length)
                {
                    ilGen.Emit(ldarg_codes[Index]);
                }
                else
                {
                    ilGen.Emit(OpCodes.Ldarg_S, Index);
                }
            }
            else
            {
                ilGen.Emit(OpCodes.Ldarg, Index);
                ilGen.Emit(OpCodes.Ldind_Ref);
            }

            OnLoaded(ilGen);
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
