﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Emit.Parameters
{
    public class LocalCallParameter : ICallParameter
    {
        public LocalCallParameter()
        {
        }

        public LocalCallParameter(LocalBuilder local)
        {
            Value = local;
        }

        public CallParameterType CallParameterType
        {
            get { return CallParameterType.Local; }
        }

        public Type ParameterType { get { return Value.LocalType; } }

        public LocalBuilder Value
        {
            get;
            set;
        }

        public void Load(ILGenerator ilGen)
        {
            ilGen.Emit(OpCodes.Ldloc, Value);
            OnLoaded(ilGen);
        }

        protected virtual void OnLoaded(ILGenerator ilGen)
        {
            EventHandler<LoadEventArgs> h = Loaded;
            if (h != null)
            {
                h(this, new LoadEventArgs(ilGen));
            }
        }

        public event EventHandler<LoadEventArgs> Loaded;
    }
}
