﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public abstract class AExtendedMethodBuilderArgs : IMethodBuilderArgs
    {
        private IMethodBuilderArgs m_Inner;
        public AExtendedMethodBuilderArgs(IMethodBuilderArgs inner)
        {
            m_Inner = inner;
        }

        public FieldInfo BuilderStorageMember
        {
            get
            {
                return m_Inner.BuilderStorageMember;
            }
            set
            {
                m_Inner.BuilderStorageMember = value;
            }
        }

        public MethodBuilder CurrentMethodBuilder
        {
            get
            {
                return m_Inner.CurrentMethodBuilder;
            }
            set
            {
                m_Inner.CurrentMethodBuilder = value;
            }
        }

        public Type InterfaceBeingImplemented
        {
            get { return m_Inner.InterfaceBeingImplemented; }
        }

        public MemberInfo InterfaceMemberToBuild
        {
            get
            {
                return m_Inner.InterfaceMemberToBuild;
            }
            set
            {
                m_Inner.InterfaceMemberToBuild = value;
            }
        }

        public MethodInfo InterfaceMethodToBuild
        {
            get
            {
                return m_Inner.InterfaceMethodToBuild;
            }
            set
            {
                m_Inner.InterfaceMethodToBuild = value;
            }
        }

        public Type[] ParameterTypes
        {
            get
            {
                return m_Inner.ParameterTypes;
            }
            set
            {
                m_Inner.ParameterTypes = value;
            }
        }

        public Type ReturnType
        {
            get
            {
                return m_Inner.ReturnType;
            }
            set
            {
                m_Inner.ReturnType = value;
            }
        }

        public System.Reflection.Emit.TypeBuilder TypeBuilder
        {
            get { return m_Inner.TypeBuilder; }
        }

        public Type TypeOfObjectBeingWrapped
        {
            get { return m_Inner.TypeOfObjectBeingWrapped; }
        }


        public Emit.ILBuilder GetILBuilder()
        {
            return m_Inner.GetILBuilder();
        }


        public FieldInfo CreatorFactoryField
        {
            get
            {
                return m_Inner.CreatorFactoryField;
            }
            set
            {
                m_Inner.CreatorFactoryField = value;
            }
        }
    }
}
