﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public interface IInterceptor
    {
        /// <summary>
        /// Gets the root object which has been wrapped. If this instance wraps another IInterceptor,
        /// the root of that IIntercepter is returned.
        /// </summary>
        /// <returns>The object at the root of the interception.</returns>
        object GetRoot();

        /// <summary>
        /// Gets the object which has been wrapped.
        /// </summary>
        /// <returns>The object which has been wrapped by this interceptor.</returns>
        object GetInner();

        /// <summary>
        /// Gets the IInterceptorFactory that created this interceptor.
        /// </summary>
        IInterceptorFactory CreatorFactory { get; set; }
    }
}
