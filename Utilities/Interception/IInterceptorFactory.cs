﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public interface IInterceptorFactory
    {
        /// <summary>
        /// Gets a value indicating whether or not the specified object is compatible with the specified interface.
        /// </summary>
        /// <typeparam name="TInterface">The interface type to validate the object against.</typeparam>
        /// <param name="inner">The object which would be wrapped.</param>
        /// <returns>A value indicating whether or not the specified object is compatible with the specified interface.</returns>
        bool IsCompatible(object inner, Type tInterface);

        /// <summary>
        /// Gets a value indicating whether or not the specified object is compatible with the specified interface.
        /// </summary>
        /// <typeparam name="TInterface">The interface type to validate the object against.</typeparam>
        /// <param name="innerType">The object type which would be wrapped.</param>
        /// <returns>A value indicating whether or not the specified object is compatible with the specified interface.</returns>
        bool IsCompatible(Type innerType, Type tInterface);
        
        /// <summary>
        /// When overridden in a derived class, this function creates a wrapper around the inner object
        /// exposing the methods defined by <c>TInterface</c>.
        /// </summary>
        /// <typeparam name="TInterface">The interface to expose.</typeparam>
        /// <param name="inner">The inner object to be wrapped by the interface.</param>
        /// <returns>An instance of <c>TInterface</c> which routes its logic through to the <c>inner</c> object.</returns>
        object CreateInterceptor(object inner, Type tInterface, Type tBase);
    }
}
