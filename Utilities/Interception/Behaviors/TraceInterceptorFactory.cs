﻿using CKLib.Utilities.Diagnostics;
using CKLib.Utilities.Emit.Parameters;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Emit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Interception.Behaviors
{
    public class TraceInterceptorFactory : AInterceptorFactory
    {
        private class TraceMethodBuilderArgs : AExtendedMethodBuilderArgs
        {
            public TraceMethodBuilderArgs(IMethodBuilderArgs argsInner)
                : base(argsInner)
            {
            }

            public MethodInfo OnTraceMethod { get; set; }
        }

        protected override IMethodBuilderArgs DefineType(Type innerType, Type @interface, Type @base)
        {
            IMethodBuilderArgs args = base.DefineType(innerType, @interface, @base);
            if (args != null)
            {
                args = new TraceMethodBuilderArgs(args);
                ImplementITraceable(args);
            }

            return args;
        }

        protected override bool IsObjectAlreadyWrapped(object inner, System.Type @interface, System.Type @base)
        {
            if (base.IsObjectAlreadyWrapped(inner, @interface, @base))
            {
                return typeof(ITraceable).IsInstanceOfType(inner);
            }

            return false;
        }

        private void ImplementITraceable(IMethodBuilderArgs args)
        {
            TraceMethodBuilderArgs tArgs = args as TraceMethodBuilderArgs;
            TypeBuilder builder = args.TypeBuilder;
            builder.AddInterfaceImplementation(typeof(ITraceable));
            Type eventHandlerType = typeof(EventHandler<TraceEventArgs>);
            EventInfo innerEvent = typeof(ITraceable).GetEvent("TraceEvent");
            // Define event backing field
            FieldInfo eventBacker = builder.DefineField("<k>_TraceEvent_<BackingField>", eventHandlerType, FieldAttributes.Private | FieldAttributes.SpecialName);
            // Define PropertyChanged Event:
            EventBuilder @event = FactoryHelpers.GetEventBuilder(builder, "TraceEvent", eventHandlerType);
            Type[] parameterTypes = new Type[] { eventHandlerType };
            MethodBuilder addMethod = FactoryHelpers.GetPropertyMethodBuilder(builder, "add_TraceEvent", null, parameterTypes);
            MethodBuilder removeMethod = FactoryHelpers.GetPropertyMethodBuilder(builder, "remove_TraceEvent", null, parameterTypes);
            addMethod.GetILBuilder(parameterTypes)
                .This()
                .Call(typeof(Delegate).GetMethod("Combine", new Type[] { typeof(Delegate), typeof(Delegate) }),
                    new FieldCallParameter() { Field = eventBacker, Target = ArgumentCallParameter.This(builder) },
                    new ArgumentCallParameter() { Index = 1 })
                .Cast(typeof(Delegate), eventHandlerType)
                .StoreField(eventBacker)
                .Return();
            @event.SetAddOnMethod(addMethod);
            removeMethod.GetILBuilder(parameterTypes)
                .This()
                .Call(typeof(Delegate).GetMethod("Remove", new Type[] { typeof(Delegate), typeof(Delegate) }),
                    new FieldCallParameter() { Field = eventBacker, Target = ArgumentCallParameter.This(builder) },
                    new ArgumentCallParameter() { Index = 1 })
                .Cast(typeof(Delegate), eventHandlerType)
                .StoreField(eventBacker)
                .Return();
            @event.SetRemoveOnMethod(removeMethod);

            builder.DefineMethodOverride(addMethod, innerEvent.GetAddMethod());
            builder.DefineMethodOverride(removeMethod, innerEvent.GetRemoveMethod());
            // Define property changed handler
            MethodBuilder raiseMethod = FactoryHelpers.GetMethodBuilder(builder, "OnTrace", null, new Type[] { typeof(TraceEventArgs) });
            LocalBuilder tmp = raiseMethod.GetILGenerator().DeclareLocal(eventHandlerType);
            raiseMethod.GetILBuilder(new Type[] { typeof(string) })
                .This().LoadField(eventBacker).StoreLocal(tmp)
                .Call(typeof(Object).GetMethod("ReferenceEquals"),
                    new LocalCallParameter(tmp),
                    new LiteralCallParameter(null))
                .If(
                    then: null,
                    @else: t =>
                    {
                        t
                        .LoadLocal(tmp)
                        .Call(eventHandlerType.GetMethod("Invoke"),
                            ArgumentCallParameter.This(args.TypeBuilder),
                            new ArgumentCallParameter(typeof(TraceEventArgs), 1));
                    }
                )
                .Return();
            tArgs.OnTraceMethod = raiseMethod;
        }

        protected override void DefineMethodBody(IMethodBuilderArgs args)
        {
            // Pass through, no type conversion
            var ilB = args.GetILBuilder();
            LocalBuilder retVal = null;
            if (args.ReturnType != null && args.ReturnType != typeof(void))
            {
                retVal = ilB.InnerGenerator.DeclareLocal(args.ReturnType);
            }

            LocalBuilder stopwatch = ilB.InnerGenerator.DeclareLocal(typeof(System.Diagnostics.Stopwatch));
            ilB.New(typeof(System.Diagnostics.Stopwatch))
                .StoreLocal(stopwatch)
                .LoadLocal(stopwatch)
                .Call(typeof(System.Diagnostics.Stopwatch).GetMethod("Start"));
            
            var src = MakeTraceSource(args);
            LogEnterEvent(args, src, stopwatch);
            if (retVal != null)
            {
                ilB.DelegateTo(
                        new FieldCallParameter(ArgumentCallParameter.This(args.TypeBuilder), args.BuilderStorageMember))
                    .StoreLocal(retVal);
                LogExitEvent(args, src, stopwatch, retVal);
                ilB.LoadLocal(retVal).Return();
            }
            else
            {
                ilB.DelegateTo(
                        new FieldCallParameter(ArgumentCallParameter.This(args.TypeBuilder), args.BuilderStorageMember));
                LogExitEvent(args, src, stopwatch, retVal);
                ilB.Return();
            }

        }

        private void LogEnterEvent(IMethodBuilderArgs args, LocalBuilder source, LocalBuilder stopwatch)
        {
            var tArgs = args as TraceMethodBuilderArgs;
            var ilB = args.GetILBuilder();
            var eArgs = MakeEventArgs(args, source, TraceEvent.Enter, string.Empty);
            var pArray = MakeParameterArray(args);
            ilB
                .LoadLocal(eArgs).StoreMember(
                    typeof(TraceEventArgs).GetProperty("Data"),
                    new LocalCallParameter(pArray))
                .LoadLocal(eArgs).StoreMember(
                    typeof(TraceEventArgs).GetProperty("Elapsed"),
                    new PropertyCallParameter()
                    {
                        Target = new LocalCallParameter(stopwatch),
                        Property = typeof(System.Diagnostics.Stopwatch).GetProperty("Elapsed")
                    })
                .This().Call(tArgs.OnTraceMethod, new LocalCallParameter(eArgs));
        }

        private void LogExitEvent(IMethodBuilderArgs args, LocalBuilder source, LocalBuilder stopwatch, LocalBuilder retVal)
        {
            var tArgs = args as TraceMethodBuilderArgs;
            var ilB = args.GetILBuilder();
            var eArgs = MakeEventArgs(args, source, TraceEvent.Exit, string.Empty);
            if (retVal != null)
            {
                ilB.LoadLocal(eArgs)
                    .StoreMember(
                        typeof(TraceEventArgs).GetProperty("Data"),
                        new LocalCallParameter(retVal))
                    .LoadLocal(eArgs).StoreMember(
                        typeof(TraceEventArgs).GetProperty("Elapsed"),
                        new PropertyCallParameter()
                        {
                            Target = new LocalCallParameter(stopwatch),
                            Property = typeof(System.Diagnostics.Stopwatch).GetProperty("Elapsed")
                        })
                    .This().Call(tArgs.OnTraceMethod, new LocalCallParameter(eArgs))
                    .LoadLocal(stopwatch).Call(typeof(System.Diagnostics.Stopwatch).GetMethod("Stop"));
            }
            else
            {
                ilB.LoadLocal(eArgs).StoreMember(
                        typeof(TraceEventArgs).GetProperty("Elapsed"),
                        new PropertyCallParameter()
                        {
                            Target = new LocalCallParameter(stopwatch),
                            Property = typeof(System.Diagnostics.Stopwatch).GetProperty("Elapsed")
                        })
                    .This().Call(tArgs.OnTraceMethod, new LocalCallParameter(eArgs))
                    .LoadLocal(stopwatch).Call(typeof(System.Diagnostics.Stopwatch).GetMethod("Stop"));
            }
        }

        private LocalBuilder MakeParameterArray(IMethodBuilderArgs args)
        {
            var ilB = args.GetILBuilder();
            var pArray = ilB.InnerGenerator.DeclareLocal(typeof(object[]));
            ilB.InitArray(pArray, args.ParameterTypes.Length);
            for (int i = 0; i < args.ParameterTypes.Length; i++)
            {
                ilB.StoreElement(pArray, i, new ArgumentCallParameter(args.ParameterTypes[i], i + 1));
            }

            return pArray;
        }

        private LocalBuilder MakeEventArgs(IMethodBuilderArgs args, LocalBuilder source, TraceEvent evt, string message)
        {
            var ilB = args.GetILBuilder();
            var eArgs = ilB.InnerGenerator.DeclareLocal(typeof(TraceEventArgs));
            var cons = typeof(TraceEventArgs).GetConstructor(new Type[] { typeof(TraceSource), typeof(TraceEvent), typeof(string) });
            ilB
                .New(cons,
                    new LocalCallParameter(source),
                    new LiteralCallParameter(evt),
                    new LiteralCallParameter(message))
                .StoreLocal(eArgs);
            return eArgs;
        }

        private LocalBuilder MakeTraceSource(IMethodBuilderArgs args)
        {
            var ilB = args.GetILBuilder();
            var src = ilB.InnerGenerator.DeclareLocal(typeof(TraceSource));
            ilB
                .This().Call(typeof(Object).GetMethod("GetType"))
                .New(typeof(TraceSource).GetConstructor(new Type[] { typeof(Type), typeof(MethodBase) }),
                    new MethodCallParameter(typeof(MethodBase), "GetCurrentMethod"))
                .StoreLocal(src);
            return src;
        }

        protected override bool ValidateCompatibility(Type innerType, Type[] interfaces, bool throwOnError)
        {
            Type firstInterface = interfaces.First();
            if (!firstInterface.IsAssignableFrom(innerType))
            {
                if (throwOnError)
                {
                    throw new InvalidOperationException(string.Format(
                        "Received incompatible type {0}, must be {1}",
                        innerType, firstInterface));
                }

                return false;
            }

            return true;
        }
    }
}
