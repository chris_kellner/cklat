﻿using CKLib.Utilities.Interception;
using CKLib.Utilities.Interception.Behaviors;
using CKLib.Utilities.Proxy;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Interception.Behaviors
{
    internal class ConversionBinder : Binder
    {
        private class BinderState
        {
            public object[] args { get; set; }
        }

        public ConversionBinder(ProxyInterceptorFactory factory)
        {
            m_Factory = factory;
        }

        #region Binder Methods
        public override FieldInfo BindToField(
          BindingFlags bindingAttr,
          FieldInfo[] match,
          object value,
          CultureInfo culture)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            // Get a field for which the value parameter can be converted to the specified field type. 
            for (int i = 0; i < match.Length; i++)
                if (ChangeType(value, match[i].FieldType, culture) != null)
                    return match[i];
            return null;
        }

        public override MethodBase BindToMethod(
          BindingFlags bindingAttr,
          MethodBase[] match,
          ref object[] args,
          ParameterModifier[] modifiers,
          CultureInfo culture,
          string[] names,
          out object state)
        {
            // Store the arguments to the method in a state object.
            BinderState myBinderState = new BinderState();
            object[] arguments = new Object[args.Length];
            args.CopyTo(arguments, 0);
            myBinderState.args = arguments;
            state = myBinderState;
            if (match == null)
                throw new ArgumentNullException("match");
            return FindBestMatchingMethod(match, null, Type.GetTypeArray(args));
        }

        public override object ChangeType(object value, Type toType, System.Globalization.CultureInfo culture)
        {
            // Determine whether the value parameter can be converted to a value of type myType.
            if (value.GetType().IsPrimitive && toType.IsPrimitive)
            {
                return Convert.ChangeType(value, toType);
            }

            if (CanConvertFrom(value.GetType(), toType))
            {
                return m_Factory.CreateInterceptor(value, toType, null);
            }
            else
                // Return null. 
                return null;
        }

        public override void ReorderArgumentArray(ref object[] args, object state)
        {
            ((BinderState)state).args.CopyTo(args, 0);
        }

        public override MethodBase SelectMethod(BindingFlags bindingAttr, MethodBase[] match, Type[] types, ParameterModifier[] modifiers)
        {
            if (match == null)
                throw new ArgumentNullException("match");

            return FindBestMatchingMethod(match, null, types);
        }

        public override PropertyInfo SelectProperty(BindingFlags bindingAttr, PropertyInfo[] match, Type returnType, Type[] indexes, ParameterModifier[] modifiers)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            for (int i = 0; i < match.Length; i++)
            {
                // Count the number of indexes that match. 
                int count = 0;
                ParameterInfo[] parameters = match[i].GetIndexParameters();
                // Go on to the next property if the number of indexes do not match. 
                if (indexes.Length != parameters.Length)
                    continue;
                // Match each of the indexes that the user expects the property to have. 
                for (int j = 0; j < indexes.Length; j++)
                    // Determine whether the types specified by the user can be converted to index type. 
                    if (CanConvertFrom(indexes[j], parameters[j].ParameterType))
                        count += 1;
                    else
                        break;
                // Determine whether the property has been found. 
                if (count == indexes.Length)
                    // Determine whether the return type can be converted to the properties type. 
                    if (CanConvertFrom(returnType, match[i].PropertyType))
                        return match[i];
                    else
                        continue;
            }
            return null;
        }

        private bool CanConvertFrom(Type fromType, Type toType)
        {
            if (fromType.IsPrimitive && toType.IsPrimitive)
            {
                return System.ComponentModel.TypeDescriptor.GetConverter(fromType).CanConvertTo(toType);
            }

            return EnsureProxyCompatibility(fromType, FactoryHelpers.ExpandTypeInheritance(toType).ToArray(), false);
        }
        #endregion

        #region Validation Methods
        #region Delegates
        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped is compatible with the specified interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        public delegate bool ValidationRule(Type typeOfObjectBeingWrapped, Type interfaceToValidate);
        #endregion
        #region Variables
        /// <summary>
        /// The set of rules used to validate types when creating a proxy.
        /// </summary>
        private SynchronizedCollection<ValidationRule> m_Rules = new SynchronizedCollection<ValidationRule>();

        private ProxyInterceptorFactory m_Factory;
        #endregion
        #region Methods
        /// <summary>
        /// Adds the default set of validation rules to this validator.
        /// </summary>
        protected virtual void AddDefaultRules()
        {
            m_Rules.Add(MethodValidationRule);
            m_Rules.Add(PropertyValidationRule);
            m_Rules.Add(EventValidationRule);
        }

        /// <summary>
        /// Initializes this instance with the validation rules that it will use to validate the requested proxies.
        /// </summary>
        public void Initialize()
        {
            AddDefaultRules();
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Release()
        {
            m_Rules.Clear();
        }

        /// <summary>
        /// Checks to see if the input typeOfObjectBeingWrapped defines members compatible with those defined
        /// in the interfaces.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The Type of the object that the proxy is being made for.</param>
        /// <param name="interfaces">The interfaces which the proxy will implement.</param>
        /// <param name="throwOnError">If set to <c>true</c> this method will throw an exception if the wrapped object cannot
        /// implement all of the required interfaces..</param>
        /// <returns>True if the types are compatible, false otherwise.</returns>
        /// <exception cref="InvalidOperationException">If the types are incompatible and <c>throwOnError</c>
        /// is set then an InvalidOperationException is thrown.</exception>
        public virtual bool EnsureProxyCompatibility(Type typeOfObjectBeingWrapped, Type[] interfaces, bool throwOnError)
        {
            if (typeOfObjectBeingWrapped != null && interfaces != null && interfaces.Length > 0)
            {
                foreach (var ifc in FactoryHelpers.ExpandTypeInheritance(interfaces))
                {
                    if (ifc != null && ifc.IsInterface)
                    {
                        if (!m_Rules.All(validate => validate(typeOfObjectBeingWrapped, ifc)))
                        {
                            if (throwOnError)
                            {
                                throw new InvalidOperationException(
                                    string.Format("Object type {0} does not match the definition of interface: {1}, unable to create Proxy.",
                                    typeOfObjectBeingWrapped.Name,
                                    ifc.Name));
                            }

                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the methods required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        private bool MethodValidationRule(Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetAllMethods().
                Where(meth => !meth.IsSpecialName).
                All(meth => FindBestMatchingMethodInType(typeOfObjectBeingWrapped, meth) != null);
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the properties required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        /// <returns>True if the base type is compatible with the interface type, false otherwise.</returns>
        private bool PropertyValidationRule(Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetProperties().All(prop => typeOfObjectBeingWrapped.GetProperty(prop.Name) != null);
        }

        /// <summary>
        /// Checks to see if the specified typeOfObjectBeingWrapped defines all of the events required by the specified
        /// interface.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory that is requesting the validation to ensure that the proxy will be valid.</param>
        /// <param name="typeOfObjectBeingWrapped">The type of the object that is to be wrapped.</param>
        /// <param name="interfaceToValidate">The interface to check compatibility for.</param>
        /// <returns>True if the typeOfObjectBeingWrapped is compatible with the interface type, false otherwise.</returns>
        /// <returns>True if the base type is compatible with the interface type, false otherwise.</returns>
        private bool EventValidationRule(Type typeOfObjectBeingWrapped, Type interfaceToValidate)
        {
            return interfaceToValidate.GetEvents().All(evt => typeOfObjectBeingWrapped.GetEventRecursive(evt.Name) != null);
        }

        /// <summary>
        /// Finds the method in the specified wrappedType that best matches the specified interfaceMethod.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory requesting the method.</param>
        /// <param name="typeOfObjectBeingWrapped">The type being wrapped.</param>
        /// <param name="interfaceMethod">The interface method being matched.</param>
        /// <returns>The best matched method or null.</returns>
        public MethodBase FindBestMatchingMethod(MethodBase[] matches, string name, Type[] parameterTypes)
        {
            MethodBase bestMethod = null;
            int bestWeight = 0;
            var interfaceParameterTypes = parameterTypes;
            foreach (var sourceMethod in matches)
            {
                if (string.IsNullOrEmpty(name) || sourceMethod.Name == name)
                {
                    var parameters = sourceMethod.GetParameters();
                    var sourceParameterTypes = FactoryHelpers.GetMethodParameterTypes(sourceMethod);
                    if (parameters.Length == interfaceParameterTypes.Length)
                    {
                        int currentWeight = 0;
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            var tempWeight =
                                ComputeTypeCompatibility(
                                sourceParameterTypes[i],
                                interfaceParameterTypes[i],
                                sourceMethod.DeclaringType);

                            if (tempWeight > 0)
                            {
                                currentWeight += tempWeight;
                            }
                            else
                            {
                                currentWeight = 0;
                                break;
                            }
                        }

                        /*
                        currentWeight = ComputeTypeCompatibility(
                            interfaceMethod.ReturnType,
                            sourceMethod.ReturnType,
                            interfaceMethod.DeclaringType);
                        */

                        if (currentWeight > bestWeight)
                        {
                            bestMethod = sourceMethod;
                        }
                    }
                }
            }

            return bestMethod;
        }

        /// <summary>
        /// Finds the method in the specified wrappedType that best matches the specified interfaceMethod.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory requesting the method.</param>
        /// <param name="searchType">The type being wrapped.</param>
        /// <param name="interfaceMethod">The interface method being matched.</param>
        /// <returns>The best matched method or null.</returns>
        public MethodBase FindBestMatchingMethodInType(Type searchType, string name, Type[] parameterTypes)
        {
            if (searchType != null)
            {
                var sourceMethods = searchType.GetAllMethods();
                MethodBase bestMethod = searchType.GetMethod(name, parameterTypes);
                if (bestMethod == null)
                {
                    bestMethod = FindBestMatchingMethod(sourceMethods.ToArray(), name, parameterTypes);
                }

                return bestMethod;
            }

            return null;
        }

        /// <summary>
        /// Finds the method in the specified wrappedType that best matches the specified interfaceMethod.
        /// </summary>
        /// <param name="validatingFactory">The IProxyFactory requesting the method.</param>
        /// <param name="searchType">The type being wrapped.</param>
        /// <param name="interfaceMethod">The interface method being matched.</param>
        /// <returns>The best matched method or null.</returns>
        public MethodBase FindBestMatchingMethodInType(Type searchType, MethodInfo interfaceMethod)
        {
            return FindBestMatchingMethodInType(
                searchType,
                interfaceMethod.Name,
                FactoryHelpers.GetMethodParameterTypes(interfaceMethod));
        }

        /// <summary>
        /// Gets a weight used for determining how well a given type matches the desired type.
        /// </summary>
        /// <param name="factory">The proxy factory making the request.</param>
        /// <param name="desiredType">The type that we need.</param>
        /// <param name="providedType">The type that we have.</param>
        /// <param name="typeOwningDesiredType">The type that owns the desiredType,
        /// this will only happen if a type declares a method whose return type or parameter type IS that type.</param>
        /// <returns>A value from 0 to 5 which indicates how well a given type matches the desired type.</returns>
        private int ComputeTypeCompatibility(Type desiredType, Type providedType, Type typeOwningDesiredType)
        {
            int weight = 0;

            if (desiredType == providedType)
            {
                // IVehicle == IVehicle
                weight = 5;
            }
            // Check for inheritance
            else if (desiredType.IsAssignableFrom(providedType))
            {
                // IVehicle IsAssignableFrom Car
                weight = 4;
            }
            else if (
                // Check to see if the desired type is the interface type
                desiredType == typeOwningDesiredType ||

                //Check to see if we have already created a proxy for it
                m_Factory.GetTypeCache().GetProxyConstructor(providedType, desiredType) != null ||

                // Check to see if whats being passed in Might be a proxy
                m_Factory.GetTypeCache().GetProxyConstructor(desiredType, providedType) != null)
            {
                weight = 3;
            }
            else if (providedType.IsAssignableFrom(desiredType))
            {
                // desiredType is a subclass of what we are providing
                //  this can get dangerous so only use this method as a last resort
                //  Compatibility in this way will require a cast when making the proxy.
                weight = 1;
            }
            else if (
                //Check to see if we can create a proxy for it
                    EnsureProxyCompatibility(providedType, new Type[] { desiredType }, false) ||
                // whats being passed in Might be a proxy
                    EnsureProxyCompatibility(desiredType, new Type[] { providedType }, false))
            {
                // Can be proxied
                weight = 2;
            }

            return weight;
        }
        #endregion
        #endregion
    }
}
