﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CKLib.Utilities.Emit;
using CKLib.Utilities.Emit.Parameters;
using System.Reflection.Emit;
using System.ComponentModel;
using CKLib.Utilities.Proxy;

namespace CKLib.Utilities.Interception.Behaviors
{
    public class PropertyChangedInterceptorFactory : AInterceptorFactory
    {
        private class PropertyChangedMethodBuilderArgs : AExtendedMethodBuilderArgs
        {
            public PropertyChangedMethodBuilderArgs(IMethodBuilderArgs argsInner)
                : base(argsInner)
            {
            }

            public MethodInfo NotifyPropertyChangedMethod { get; set; }
        }

        protected override bool IsObjectAlreadyWrapped(object inner, System.Type @interface, System.Type @base)
        {
            if (base.IsObjectAlreadyWrapped(inner, @interface, @base))
            {
                return typeof(INotifyPropertyChanged).IsInstanceOfType(inner);
            }

            return false;
        }

        protected override void DefineMethodBody(IMethodBuilderArgs args)
        {
            PropertyChangedMethodBuilderArgs pArgs = (PropertyChangedMethodBuilderArgs)args;
            PropertyInfo info = args.InterfaceMemberToBuild as PropertyInfo;
            if (info == null || !args.InterfaceMethodToBuild.Name.StartsWith("set_"))
            {
                // Pass through, no type conversion
                args.GetILBuilder()
                    .This().LoadField(args.BuilderStorageMember)
                    .Call(args.InterfaceMethodToBuild, args.InterfaceMethodToBuild.GetCallParameters())
                    .Return();
            }
            else // Only deal with property Setters
            {
                Console.WriteLine("Making property: " + info.Name);
                args.GetILBuilder()
                    .This().LoadField(args.BuilderStorageMember)
                    .Call(info.GetGetMethod())
                    .LoadArgument(1)
                    .Equals(info.PropertyType, info.PropertyType)
                    // Check to see if the new value is different
                    .If(
                        then: null,
                        @else: t =>
                        {
                            // Invoke the "setter"
                            args.GetILBuilder()
                                .This().LoadField(args.BuilderStorageMember)
                                .Call(args.InterfaceMethodToBuild, args.InterfaceMethodToBuild.GetCallParameters());
                            // Call NotifyPropertyChanged
                            args.GetILBuilder()
                                .This()
                                .Call(pArgs.NotifyPropertyChangedMethod, new LiteralCallParameter(pArgs.InterfaceMemberToBuild.Name));
                        }
                    )
                    .Return();
            }
        }

        protected override IMethodBuilderArgs DefineType(Type innerType, Type @interface, Type @base)
        {
            IMethodBuilderArgs args = base.DefineType(innerType, @interface, @base);
            if (args != null)
            {
                args = new PropertyChangedMethodBuilderArgs(args);
                ImplementINotifyPropertyChanged(args);
            }

            return args;
        }

        protected override Type GetInnerObjectType(object inner, Type interfaceType)
        {
            /*
            if (interfaceType.IsAssignableFrom(inner.GetType()))
            {
                return interfaceType;
            }*/

            return base.GetInnerObjectType(inner, interfaceType);
        }

        protected override bool ValidateCompatibility(Type innerType, Type[] interfaces, bool throwOnError)
        {
            Type firstInterface = interfaces.First();
            if (!firstInterface.IsAssignableFrom(innerType))
            {
                if (throwOnError)
                {
                    throw new InvalidOperationException(string.Format(
                        "Received incompatible type {0}, must be {1}",
                        innerType, firstInterface));
                }

                return false;
            }

            return true;
        }

        private void ImplementINotifyPropertyChanged(IMethodBuilderArgs args)
        {
            TypeBuilder builder = args.TypeBuilder;
            builder.AddInterfaceImplementation(typeof(INotifyPropertyChanged));
            EventInfo innerEvent = typeof(INotifyPropertyChanged).GetEvent("PropertyChanged");
            // Define event backing field
            FieldInfo eventBacker = builder.DefineField("<k>_PropertyChanged_<BackingField>", typeof(PropertyChangedEventHandler), FieldAttributes.Private | FieldAttributes.SpecialName);
            // Define PropertyChanged Event:
            EventBuilder @event = FactoryHelpers.GetEventBuilder(builder, "PropertyChanged", typeof(PropertyChangedEventHandler));
            Type[] parameterTypes = new Type[] { typeof(PropertyChangedEventHandler) };
            MethodBuilder addMethod = FactoryHelpers.GetPropertyMethodBuilder(builder, "add_PropertyChanged", null, parameterTypes);
            MethodBuilder removeMethod = FactoryHelpers.GetPropertyMethodBuilder(builder, "remove_PropertyChanged", null, parameterTypes);
            addMethod.GetILBuilder(parameterTypes)
                .This()
                .Call(typeof(Delegate).GetMethod("Combine", new Type[] {typeof(Delegate), typeof(Delegate) }),
                    new FieldCallParameter() { Field = eventBacker, Target = ArgumentCallParameter.This(builder) },
                    new ArgumentCallParameter() { Index = 1 })
                .Cast(typeof(Delegate), typeof(PropertyChangedEventHandler))
                .StoreField(eventBacker)
                .Return();
            @event.SetAddOnMethod(addMethod);
            removeMethod.GetILBuilder(parameterTypes)
                .This()
                .Call(typeof(Delegate).GetMethod("Remove", new Type[] { typeof(Delegate), typeof(Delegate) }),
                    new FieldCallParameter() { Field = eventBacker, Target = ArgumentCallParameter.This(builder) },
                    new ArgumentCallParameter() { Index = 1 })
                .Cast(typeof(Delegate), typeof(PropertyChangedEventHandler))
                .StoreField(eventBacker)
                .Return();
            @event.SetRemoveOnMethod(removeMethod);

            builder.DefineMethodOverride(addMethod, innerEvent.GetAddMethod());
            builder.DefineMethodOverride(removeMethod, innerEvent.GetRemoveMethod());
            // Define property changed handler
            MethodBuilder raiseMethod = FactoryHelpers.GetMethodBuilder(builder, "OnPropertyChanged", null, new Type[] { typeof(string) });
            LocalBuilder tmp = raiseMethod.GetILGenerator().DeclareLocal(typeof(PropertyChangedEventHandler));
            raiseMethod.GetILBuilder(new Type[] { typeof(string) })
                .This().LoadField(eventBacker).StoreLocal(tmp)
                .Call(typeof(Object).GetMethod("ReferenceEquals"), 
                    new LocalCallParameter(tmp),
                    new LiteralCallParameter(null))
                .If(
                    then: null,
                    @else: t =>
                    {
                        t
                        .LoadLocal(tmp)
                        .This()
                        .New(
                            typeof(PropertyChangedEventArgs).GetConstructor(new Type[] { typeof(string) }),
                            new ArgumentCallParameter() { Index = 1 }
                        )
                        .Call(typeof(PropertyChangedEventHandler).GetMethod("Invoke"));
                    }
                )
                .Return();
            ((PropertyChangedMethodBuilderArgs)args).NotifyPropertyChangedMethod = raiseMethod;
        }
    }
}
