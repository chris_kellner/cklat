﻿using CKLib.Utilities.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using CKLib.Utilities.Emit;
using CKLib.Utilities.Emit.Parameters;

namespace CKLib.Utilities.Interception.Behaviors
{
    public class ProxyInterceptorFactory : AInterceptorFactory
    {
        #region Variables
        private ProxyTypeValidator m_Validator;

        private Binder m_Binder;
        #endregion

        #region Properties
        public Binder Binder
        {
            get
            {
                if (m_Binder == null)
                {
                    m_Binder = new ConversionBinder(this);
                }

                return m_Binder;
            }
            set { m_Binder = value; }
        }


        public ProxyTypeValidator Validator
        {
            get
            {
                if (m_Validator == null)
                {
                    m_Validator = new ProxyTypeValidator();
                }

                return m_Validator;
            }

            set
            {
                m_Validator = value;
            }
        }
        #endregion

        protected override void DefineMethodBody(IMethodBuilderArgs args)
        {
            // Pass through, no type conversion
            var ilB = args.GetILBuilder();
            ilB
                .DelegateTo(
                    new FieldCallParameter(ArgumentCallParameter.This(args.TypeBuilder), args.BuilderStorageMember),
                    Binder,
                    parameterLoadedHandler: (s, e) =>
                    {
                        if (!e.NeededType.IsAssignableFrom(e.Parameter.ParameterType))
                        {
                            var converter = typeof(ProxyInterceptorFactory).GetMethod("GenericUnwrap", BindingFlags.Static|BindingFlags.Public)
                                .MakeGenericMethod(e.Parameter.ParameterType, e.NeededType);
                            ilB.Call(converter, ArgumentCallParameter.This(args.TypeBuilder));
                        }
                    },
                    methodCallLoadedHandler: (s, e) =>
                    {
                        MethodCallParameter call = s as MethodCallParameter;
                        if (!IsNullOrVoid(call.ParameterType) && !args.ReturnType.IsAssignableFrom(call.ParameterType))
                        {
                            var converter = typeof(ProxyInterceptorFactory).GetMethod("GenericUnwrap", BindingFlags.Static | BindingFlags.Public)
                                .MakeGenericMethod(call.ParameterType, args.ReturnType);
                            ilB.Call(converter, ArgumentCallParameter.This(args.TypeBuilder));
                        }
                    })
                .Return();
        }

        protected override bool ValidateCompatibility(Type innerType, Type[] interfaces, bool throwOnError)
        {
            return Validator.EnsureProxyCompatibility(this, innerType, interfaces, throwOnError);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                Binder = null;
            }
        }

        #region Helpers
        private static bool IsNullOrVoid(Type type)
        {
            return type == null || type == typeof(void);
        }

        /// <summary>
        /// This function returns the unwrapped version of the wrapped object (and wraps it again if it needs to),
        /// as a conversion to the targetType.
        /// </summary>
        /// <param name="obj">A Proxy object.</param>
        /// <param name="targetType">The type that should be returned from this function.</param>
        /// <returns>The wrapped object contained in the proxy.</returns>
        public static TOut GenericUnwrap<TIn, TOut>(TIn obj, IInterceptor interceptor)
        {
            if (typeof(TOut).IsAssignableFrom(typeof(TIn)))
            {
                object tmp = obj;
                return (TOut)tmp;
            }

            IInterceptor proxy = obj as IInterceptor;
            if (proxy != null)
            {
                var wrapped = proxy.GetRoot();
                if (wrapped != null)
                {
                    var wrappedType = wrapped.GetType();
                    if (typeof(TOut).IsAssignableFrom(wrappedType))
                    {
                        return (TOut)wrapped;
                    }
                    else if (proxy.CreatorFactory.IsCompatible(wrapped, typeof(TOut)))
                    {
                        return (TOut)proxy.CreatorFactory.CreateInterceptor(wrapped, typeof(TOut), null);
                    }
                }
            }
            else if (interceptor.CreatorFactory.IsCompatible(obj, typeof(TOut)))
            {
                return (TOut)interceptor.CreatorFactory.CreateInterceptor(obj, typeof(TOut), null);
            }

            return default(TOut);
        }

        /// <summary>
        /// This function returns the unwrapped version of the wrapped object (and wraps it again if it needs to),
        /// as a conversion to the targetType.
        /// </summary>
        /// <param name="obj">A Proxy object.</param>
        /// <param name="targetType">The type that should be returned from this function.</param>
        /// <returns>The wrapped object contained in the proxy.</returns>
        public static object Unwrap(object obj, Type targetType)
        {
            if (targetType.IsInstanceOfType(obj))
            {
                return obj;
            }

            IInterceptor proxy = obj as IInterceptor;
            if (proxy != null)
            {
                var wrapped = proxy.GetRoot();
                if (wrapped != null)
                {
                    var wrappedType = wrapped.GetType();
                    if (targetType.IsAssignableFrom(wrappedType))
                    {
                        return wrapped;
                    }
                    else if (proxy.CreatorFactory.IsCompatible(wrapped, targetType))
                    {
                        return proxy.CreatorFactory.CreateInterceptor(wrapped, targetType, null);
                    }
                }
            }

            if (targetType.IsValueType)
            {
                return Activator.CreateInstance(targetType);
            }

            return null;
        }
        #endregion
    }
}
