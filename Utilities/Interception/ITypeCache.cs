﻿using System;
namespace CKLib.Utilities.Interception
{
    public interface ITypeCache
    {
        System.Reflection.ConstructorInfo GetProxyConstructor(Type typeOfObjectToWrap, Type exposedType);
        Type GetTypeFromCache(Type typeOfObjectToWrap, params Type[] interfaces);
        Type GetTypeFromCache(Type typeOfObjectToWrap, Type @interface);
    }
}
