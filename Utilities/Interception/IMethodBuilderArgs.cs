﻿using CKLib.Utilities.Emit;
using System;
using System.Reflection;
using System.Reflection.Emit;
namespace CKLib.Utilities.Interception
{
    public interface IMethodBuilderArgs
    {
        FieldInfo BuilderStorageMember { get; set; }
        FieldInfo CreatorFactoryField { get; set; }
        MethodBuilder CurrentMethodBuilder { get; set; }
        Type InterfaceBeingImplemented { get; }
        MemberInfo InterfaceMemberToBuild { get; set; }
        MethodInfo InterfaceMethodToBuild { get; set; }
        Type[] ParameterTypes { get; set; }
        Type ReturnType { get; set; }
        TypeBuilder TypeBuilder { get; }
        Type TypeOfObjectBeingWrapped { get; }

        ILBuilder GetILBuilder();
    }
}
