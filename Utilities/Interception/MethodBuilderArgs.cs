﻿using CKLib.Utilities.Emit;
using CKLib.Utilities.Proxy.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public class MethodBuilderArgs : EventArgs, IMethodBuilderArgs
    {
        private ILBuilder m_ILBuilder;
        private MethodBuilder m_MethodBuilder;

        public MethodBuilderArgs(TypeBuilder builder, Type @interface, Type innerType)
        {
            TypeBuilder = builder;
            InterfaceBeingImplemented = @interface;
            TypeOfObjectBeingWrapped = innerType;
        }

        public TypeBuilder TypeBuilder
        {
            get;
            private set;
        }

        public Type TypeOfObjectBeingWrapped
        {
            get;
            private set;
        }

        public Type InterfaceBeingImplemented
        {
            get;
            private set;
        }

        public MethodBuilder CurrentMethodBuilder
        {
            get { return m_MethodBuilder; }
            set { 
                m_MethodBuilder = value;
                m_ILBuilder = null;
            }
        }

        public Type[] ParameterTypes
        {
            get;
            set;
        }

        public Type ReturnType
        {
            get;
            set;
        }

        public FieldInfo BuilderStorageMember
        {
            get;
            set;
        }

        public MemberInfo InterfaceMemberToBuild
        {
            get;
            set;
        }

        public MethodInfo InterfaceMethodToBuild
        {
            get;
            set;
        }


        public ILBuilder GetILBuilder()
        {
            if (m_ILBuilder == null)
            {
                m_ILBuilder = CurrentMethodBuilder.GetILBuilder(ParameterTypes);
            }

            return m_ILBuilder;
        }


        public FieldInfo CreatorFactoryField
        {
            get;
            set;
        }
    }
}
