﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace CKLib.Utilities.Interception
{
    public class CallTarget
    {

        public CallTarget(MemberInfo target)
        {
            Target = target;
            IsSelf = target == null;
        }

        public MemberInfo Target { get; private set; }
        public bool IsSelf { get; private set; }

        public void EmitCallVirt(ILGenerator ilGen, MethodInfo method, Action<ILGenerator> loadParametersCallback)
        {
            // This
            ilGen.Emit(OpCodes.Ldarg_0);
            if (!IsSelf)
            {
                if (Target is FieldInfo)
                {
                    ilGen.Emit(OpCodes.Ldfld, (FieldInfo)Target);
                }
                else if (Target is PropertyInfo)
                {
                    ilGen.Emit(OpCodes.Callvirt, ((PropertyInfo)Target).GetGetMethod());
                }
                else
                {
                    throw new InvalidOperationException("Unexpected MemberInfo type: " + Target.GetType());
                }
            }

            loadParametersCallback(ilGen);
            ilGen.Emit(OpCodes.Callvirt, method);
        }
    }
}
