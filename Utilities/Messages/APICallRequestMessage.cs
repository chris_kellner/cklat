#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    #endregion
    /// <summary>
    /// This message is used by the SharedObjectPool to route method call requests between connections.
    /// </summary>
    [DataContract]
    public class APICallRequestMessage : IDataMessage
    {
        #region Variables
        /// <summary>
        /// Holds the parameters which have been deserialized.
        /// </summary>
        private object[] m_DeserializedParameters;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the APICallRequestMessage class.
        /// </summary>
        public APICallRequestMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the APICallRequestMessage class.
        /// </summary>
        /// <param name="assemblyQualifiedTypeName">Type name of the shared object.</param>
        /// <param name="instanceId">Instance id of the shared object.</param>
        /// <param name="functionName">Name of the function to call.</param>
        /// <param name="parameters">Parameters to pass to the function.</param>
        public APICallRequestMessage(string assemblyQualifiedTypeName, Guid instanceId, string functionName, object[] parameters)
        {
            RequestId = Guid.NewGuid();
            AssemblyQualifiedTypeName = assemblyQualifiedTypeName;
            InstanceId = instanceId;
            FunctionName = functionName;
            Parameters = parameters;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the RequestId.
        /// </summary>
        [DataMember(IsRequired = true)]
        public Guid RequestId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the AssembleQualifiedTypeName of the SharedObject.
        /// </summary>
        [DataMember(IsRequired=true)]
        public string AssemblyQualifiedTypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the method to call.
        /// </summary>
        [DataMember(IsRequired=true)]
        public string FunctionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the instance id of the SharedObject.
        /// </summary>
        [DataMember]
        public Guid InstanceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the parameters in XmlSerialized Form.
        /// </summary>
        [DataMember]
        public XmlSerializedObject[] XmlSerializedParameters
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Parameters to pass to the function.
        /// </summary>
        [XmlIgnore]
        public object[] Parameters
        {
            get
            {
                if (m_DeserializedParameters == null)
                {
                    m_DeserializedParameters = XmlSerializedParameters.Select(x => x.Value).ToArray();
                }
                return m_DeserializedParameters;
            }
            private set
            {
                XmlSerializedParameters = value.Select(o => new XmlSerializedObject(o)).ToArray();
            }
        }
        #endregion
    }
}
