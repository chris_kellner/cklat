#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Messages
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    #endregion
    /// <summary>
    /// Class used to register new types with the messenger.
    /// </summary>
    public class TypeDefinitionEventArgs : EventArgs
    {
        #region Variables
        /// <summary>
        /// Used to store the types being registered.
        /// </summary>
        private HashSet<Type> m_Types;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TypeDefinitionEventArgs class.
        /// </summary>
        public TypeDefinitionEventArgs()
        {
            m_Types = new HashSet<Type>();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Adds a new type to the availability list.
        /// </summary>
        /// <param name="t">The type to add.</param>
        public void AddType(Type t)
        {
            m_Types.Add(t);
        }

        /// <summary>
        /// Gets the set of messages that were registered.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> GetTypes()
        {
            return m_Types;
        }
        #endregion
    }

    /// <summary>
    /// This is the base class for all messages.
    /// </summary>
    [DataContract]
    [KnownType("GetKnownTypes")]
    public abstract class IDataMessage
    {
        #region Properties
        /// <summary>
        /// Gets or sets the id of the sender of the message.
        /// </summary>
        [DataMember]
        public Guid SenderId { get; set; }
        #endregion
        #region Methods
        /// <summary>
        /// Retrieves all known types for the server.
        /// </summary>
        /// <remarks>Expand later to add support for extensions.</remarks>
        /// <returns></returns>
        public static IEnumerable<Type> GetKnownTypes()
        {
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            var x = from type in asm.GetTypes()
                    where typeof(IDataMessage).IsAssignableFrom(type)
                    let attrs = type.GetCustomAttributes(typeof(DataContractAttribute), false)
                    where attrs != null && attrs.Length > 0
                    select type;

            TypeDefinitionEventArgs args = new TypeDefinitionEventArgs();
            if (FindMessageTypes != null)
            {
                FindMessageTypes(null, args);
            }

            var returnTypes = x.Concat(args.GetTypes());

            return returnTypes;
        }
        #endregion
        #region Events
        /// <summary>
        /// Called when WCF asks for the available message types.
        /// </summary>
        public static event EventHandler<TypeDefinitionEventArgs> FindMessageTypes;
        #endregion
    }
}
