#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Messages
{
    #region Using List
    using CKLib.Utilities.Serialization;
    using System;
    #endregion

    /// <summary>
    /// A callback which accepts the message pair object.
    /// </summary>
    /// <param name="args">The object containing the message and sender information.</param>
    public delegate void DataMessageCallback(MessageEnvelope args);

    /// <summary>
    /// Container class which provides metadata about the message contained within.
    /// </summary>
    public class MessageEnvelope : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new intance if the DataMessageEventArgs class.
        /// </summary>
        /// <param name="sender">The original sender of the message.</param>
        /// <param name="message">The message of these args.</param>
        public MessageEnvelope(Guid sender, IDataMessage message)
        {
            Sender = sender;
            Message = message;
            UtcSendTime = DateTime.UtcNow;
        }

        private MessageEnvelope(ObjectGraph info)
        {
            Sender = (Guid)info.ReadObject("Sender");
            Message = (IDataMessage)info.ReadObject("Message");
            UtcSendTime = info.ReadDateTime("UtcSendTime");
        }
        #endregion
        #region Properties
        /// <summary>
        /// The unique identifier of the connection that sent the message.
        /// </summary>
        public Guid Sender { get; private set; }

        /// <summary>
        /// Gets the message.
        /// </summary>
        public IDataMessage Message { get; private set; }

        /// <summary>
        /// Gets the time at which the message was sent in UTC.
        /// </summary>
        public DateTime UtcSendTime { get; private set; }
        #endregion
    }
}
