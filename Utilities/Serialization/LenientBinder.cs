﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
    #endregion

    public class TypeResolveEventArgs : EventArgs
    {
        public TypeResolveEventArgs(string typeName)
        {
            TypeName = typeName;
        }

        public string TypeName { get; private set; }
        public Type ResolvedType { get; set; }
    }

    /// <summary>
    /// This class provides a serializer with a means of discovering types with or without their assembly name.
    /// </summary>
    public class LenientBinder : SerializationBinder
    {
        #region Fields
        private Dictionary<string, Type> m_TypeCache = new Dictionary<string, Type>();
        #endregion

        /// <summary>
        /// Finds the type within the loaded assemblies whose name matches the specified
        /// typeName. The most efficient input for this method (besides <c>null</c>) is the
        /// AssemblyQualified type name.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns>The type whose name matches the specified type, or null if one could not be found.</returns>
        public static Type FindType(string typeName)
        {
            if (!string.IsNullOrEmpty(typeName))
            {
                Type ret = Type.GetType(typeName);
                if (ret == null)
                {
                    string[] parts = typeName.Split(',');
                    if (parts.Length > 1)
                    {
                        typeName = parts[0];
                        string asmName = parts[1];
                        if (!string.IsNullOrEmpty(asmName))
                        {
                            var asms = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name == asmName);
                            foreach (var asm in asms)
                            {
                                ret = asm.GetType(typeName, false, false);
                                if (ret != null)
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (ret == null)
                    {
                        var asms = AppDomain.CurrentDomain.GetAssemblies();
                        for (int i = 0; i < asms.Length; i++)
                        {
                            try
                            {
                                var cAsm = asms[i];
                                ret = cAsm.GetType(typeName, false, false);
                                if (ret != null)
                                {
                                    break;
                                }
                            }
                            catch (TypeLoadException)
                            {
                            }
                        }
                    }
                }

                return ret;
            }

            return null;
        }

        // Summary:
        //     When overridden in a derived class, controls the binding of a serialized
        //     object to a type.
        //
        // Parameters:
        //   assemblyName:
        //     Specifies the System.Reflection.Assembly name of the serialized object.
        //
        //   typeName:
        //     Specifies the System.Type name of the serialized object.
        //
        // Returns:
        //     The type of the object the formatter creates a new instance of.
        public override Type BindToType(string assemblyName, string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                throw new ArgumentNullException("typeName");
            }

            Type type = null;
            if (!string.IsNullOrEmpty(assemblyName))
            {
                typeName = typeName + ", " + assemblyName;
            }

            if (!m_TypeCache.TryGetValue(typeName, out type))
            {
                type = FindType(typeName);
                if (type == null)
                {
                    type = OnTypeNotFound(new TypeResolveEventArgs(typeName));
                }

                if (type == null)
                {
                    throw new SerializationException("Could not resolve type: " + typeName);
                }

                m_TypeCache[typeName] = type;
            }

            return type;
        }

        /// <summary>
        /// When a type is requested, but cannot be found this method provides a chance to supply the requested type.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>The type being searched for.</returns>
        protected virtual Type OnTypeNotFound(TypeResolveEventArgs e)
        {
            if (TypeNotFound != null)
            {
                TypeNotFound(this, e);
            }

            return e.ResolvedType;
        }

        /// <summary>
        /// Fired when a type is requested, but unavailable.
        /// </summary>
        public event EventHandler<TypeResolveEventArgs> TypeNotFound;
    }
}
