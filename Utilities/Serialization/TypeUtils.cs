﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Serialization
{
    #region Using List
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Proxy.Builder;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Runtime.Serialization;
    #endregion

    public delegate byte[] PrimitiveWriter(object obj, ISerializer serializer);
    public delegate object PrimitiveReader(byte[] bytes, ISerializer serializer);
    public delegate void Setter(ref object instance, object value);

    public static class TypeUtils
    {
        public static bool IsPrimitive(this Type t)
        {
            return Type.GetTypeCode(t) != TypeCode.Object || t.IsEnum || t == typeof(TimeSpan);
        }

        public static Func<object> CreateDefaultConstructor(this Type t, out bool isRawConstruction)
        {
            isRawConstruction = false;
            if (!t.IsPrimitive())
            {
                var cons = t.GetConstructor(Type.EmptyTypes);
                DynamicMethod meth = null;
                if (cons != null)
                {
                    meth = FactoryHelpers.DefineDynamicMethod(t.Name + "_constructor_" + Guid.NewGuid(), t, Type.EmptyTypes);
                    var ilGen = meth.GetILGenerator();
                    ilGen.Emit(OpCodes.Newobj, cons);
                    if (t.IsValueType)
                    {
                        ilGen.Emit(OpCodes.Box, t);
                    }

                    ilGen.Emit(OpCodes.Ret);
                }
                else
                {
                    isRawConstruction = true;
                    return () => FormatterServices.GetUninitializedObject(t);
                }

                if (meth != null)
                {
                    return (Func<object>)meth.CreateDelegate(typeof(Func<object>));
                }
            }

            return null;
        }

        public static Func<ObjectGraph, object> CreateSerializationConstructor(this Type t)
        {
            if (!t.IsPrimitive())
            {
                var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
                var cons = t.GetConstructor(flags, null, new Type[] { typeof(ObjectGraph) }, null);
                DynamicMethod meth = null;
                if (cons != null)
                {
                    meth = FactoryHelpers.DefineDynamicMethod(t.Name + "_constructor_" + Guid.NewGuid(), t, new Type[] { typeof(ObjectGraph) });
                    var ilGen = meth.GetILGenerator();
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Newobj, cons);
                    if (t.IsValueType)
                    {
                        ilGen.Emit(OpCodes.Box, t);
                    }

                    ilGen.Emit(OpCodes.Ret);
                }

                if (meth != null)
                {
                    return (Func<ObjectGraph, object>)meth.CreateDelegate(typeof(Func<ObjectGraph, object>));
                }
            }

            return null;
        }

        public static Func<IEnumerable, object> CreateEnumerableConstructor(this Type t)
        {
            if (!t.IsPrimitive())
            {
                DynamicMethod meth = null;
                var componentType = t.GetComponentType();
                var iEnumT = typeof(IEnumerable<>).MakeGenericType(componentType);
                var cons = t.GetConstructor(new Type[] { iEnumT });
                if (cons != null)
                {
                    meth = FactoryHelpers.DefineDynamicMethod(t.Name + "_constructor_" + Guid.NewGuid(), t, new Type[] { typeof(IEnumerable) });
                    var ilGen = meth.GetILGenerator();
                    ilGen.Emit(OpCodes.Ldarg_0);
                    var castMethod = typeof(Enumerable).GetMethod("Cast").MakeGenericMethod(componentType);
                    ilGen.Emit(OpCodes.Call, castMethod);
                    ilGen.Emit(OpCodes.Newobj, cons);
                    ilGen.Emit(OpCodes.Ret);
                }

                if (cons == null && typeof(ICollection).IsAssignableFrom(t))
                {
                    cons = t.GetConstructor(new Type[] { typeof(ICollection) });
                    if (cons != null)
                    {
                        meth = FactoryHelpers.DefineDynamicMethod(t.Name + "_constructor_" + Guid.NewGuid(), t, new Type[] { typeof(IEnumerable) });
                        var ilGen = meth.GetILGenerator();
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Castclass, typeof(ICollection));
                        ilGen.Emit(OpCodes.Newobj, cons);
                        ilGen.Emit(OpCodes.Ret);
                    }
                }

                if (meth != null)
                {
                    return (Func<IEnumerable, object>)meth.CreateDelegate(typeof(Func<IEnumerable, object>));
                }
            }

            return null;
        }

        public static Func<object, int> CreateCountAccessor(this Type t)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(t))
            {
                throw new InvalidOperationException(string.Format("Type \"{0}\" is not enumerable", t));
            }

            var meth = FactoryHelpers.DefineDynamicMethod(t.Name + "_counter_" + Guid.NewGuid(), typeof(int), new Type[] { typeof(object) });
            var ilGen = meth.GetILGenerator();

            BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy;

            MethodInfo countMethod = null;
            var countProp = t.GetProperty("Count", flags);
            if (countProp == null)
            {
                countProp = t.GetProperty("Length", flags);
            }

            if (countProp != null)
            {
                countMethod = countProp.GetGetMethod(true);
            }

            if (countMethod == null)
            {
                countMethod = t.GetMethod("Count", flags, null, Type.EmptyTypes, null);
            }

            if (countMethod != null)
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Callvirt, countMethod);
            }
            else
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Castclass, typeof(IEnumerable));
                ilGen.Emit(OpCodes.Call, typeof(System.Linq.Enumerable).GetMethod("Cast").MakeGenericMethod(typeof(object)));
                var linqCount = typeof(System.Linq.Enumerable).GetMethod("Count", new Type[] { typeof(IEnumerable<object>) });
                ilGen.Emit(OpCodes.Call, linqCount);
            }

            ilGen.Emit(OpCodes.Ret);
            return (Func<object, int>)meth.CreateDelegate(typeof(Func<object, int>));
        }

        public static Type GetComponentType(this Type t)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(t))
            {
                throw new InvalidOperationException(string.Format("Type \"{0}\" is not enumerable", t));
            }

            if (t.IsArray)
            {
                return t.GetElementType();
            }

            var iFace = t.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
            if (iFace != null)
            {
                return iFace.GetGenericArguments()[0];
            }

            if (typeof(IDictionary).IsAssignableFrom(t))
            {
                return typeof(DictionaryEntry);
            }

            return typeof(object);
        }

        #region Adder
        public static Action<object, object> CreateAdder(this Type t)
        {
            if (!typeof(IEnumerable).IsAssignableFrom(t))
            {
                throw new InvalidOperationException(string.Format("Type \"{0}\" is not enumerable", t));
            }

            var elemT = t.GetComponentType();
            MethodInfo targetMethod = null;
            var flags = BindingFlags.Static | BindingFlags.NonPublic;
            var iFace = t.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IDictionary<,>));
            var iColFace = t.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(ICollection<>));
            if (iFace != null)
            {
                Type[] types = iFace.GetGenericArguments();
                targetMethod = typeof(TypeUtils).GetMethod("CreateGenericDictionaryAdder", flags);
                targetMethod = targetMethod.MakeGenericMethod(types);
            }
            else if (t.GetInterfaces().Contains(typeof(IDictionary)))
            {
                targetMethod = typeof(TypeUtils).GetMethod("CreateDictionaryAdder", flags);
            }
            else if (iColFace != null)
            {
                Type[] types = iFace.GetGenericArguments();
                targetMethod = typeof(TypeUtils).GetMethod("CreateCollectionAdder", flags);
                targetMethod = targetMethod.MakeGenericMethod(types);
            }
            else if (t.GetInterfaces().Contains(typeof(IList)))
            {
                targetMethod = typeof(TypeUtils).GetMethod("CreateListAdder", flags);
            }

            if (targetMethod != null)
            {
                return (Action<object, object>)targetMethod.Invoke(null, new object[] { t });
            }

            throw new InvalidOperationException("Unrecognized collection type, cannot create adder function.");
        }

        private static Action<object, object> CreateCollectionAdder<TCollection>(this Type t)
        {
            return (o, v) =>
            {
                if (t.IsInstanceOfType(o))
                {
                    ((ICollection<TCollection>)o).Add((TCollection)v);
                }
            };
        }

        private static Action<object, object> CreateListAdder(this Type t)
        {
            return (o, v) =>
            {
                if (t.IsInstanceOfType(o))
                {
                    ((IList)o).Add(v);
                }
            };
        }

        private static Action<object, object> CreateDictionaryAdder(this Type t)
        {
            return (o, kv) =>
            {
                if (t.IsInstanceOfType(o))
                {
                    var kvp = (DictionaryEntry)kv;
                    ((IDictionary)o)[kvp.Key] = kvp.Value;
                }
            };
        }

        private static Action<object, object> CreateGenericDictionaryAdder<TKey, TVal>(this Type t)
        {
            return (o, kv) =>
            {
                if (t.IsInstanceOfType(o))
                {
                    var kvp = (KeyValuePair<TKey, TVal>)kv;
                    ((IDictionary<TKey, TVal>)o)[kvp.Key] = kvp.Value;
                }
            };
        }
        #endregion

        #region Getter/Setter
        /// <summary>
        /// Creates a delegate which can dynamically assign a value using the specified property info.
        /// </summary>
        /// <param name="info">The property info to create the setter for.</param>
        /// <returns>A delegate which can dynamically assign a value using the specified property info.</returns>
        /// <see cref="http://stackoverflow.com/questions/1272454/generate-dynamic-method-to-set-a-field-of-a-struct-instead-of-using-reflection"/>
        public static Setter CreateSetter(this MemberInfo info)
        {
            Type ParamType;
            if (info is PropertyInfo)
            {
                ParamType = ((PropertyInfo)info).PropertyType;
            }
            else if (info is FieldInfo)
            {
                info = FieldInfo.GetFieldFromHandle(((FieldInfo)info).FieldHandle, info.DeclaringType.TypeHandle);
                if (info == null)
                {
                    throw new InvalidOperationException("Unable to create getter for non-runtime field info: " + info);
                }

                ParamType = ((FieldInfo)info).FieldType;
            }
            else
                throw new Exception("Can only create set methods for properties and fields.");

            var meth = FactoryHelpers.DefineDynamicMethod(info.Name + "_setter" + Guid.NewGuid(), typeof(void),
                new Type[] { typeof(object).MakeByRefType(), typeof(object) });
            var ilGen = meth.GetILGenerator();
            var label = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldind_Ref);
            ilGen.Emit(OpCodes.Isinst, info.DeclaringType);
            ilGen.Emit(OpCodes.Brfalse_S, label);

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldind_Ref);
            if (info.DeclaringType.IsValueType)
            {
                ilGen.Emit(OpCodes.Unbox, info.DeclaringType);
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, info.DeclaringType);
            }

            ilGen.Emit(OpCodes.Ldarg_1);
            if (ParamType.IsValueType)
            {
                ilGen.Emit(OpCodes.Unbox_Any, ParamType);
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, ParamType);
            }

            if (info is PropertyInfo)
            {
                ilGen.Emit(OpCodes.Callvirt, ((PropertyInfo)info).GetSetMethod(true));
            }
            else
            {
                ilGen.Emit(OpCodes.Stfld, (FieldInfo)info);
            }

            ilGen.MarkLabel(label);
            ilGen.Emit(OpCodes.Ret);
            return (Setter)meth.CreateDelegate(typeof(Setter));
        }

        /// <summary>
        /// Creates a delegate which can dynamically access a value using the specified property info.
        /// </summary>
        /// <param name="info">The property info to create the getter for.</param>
        /// <returns>A delegate which can dynamically access a value using the specified property info.</returns>
        public static Func<object, object> CreateGetter(this PropertyInfo info)
        {
            var meth = FactoryHelpers.DefineDynamicMethod(info.Name + "_getter" + Guid.NewGuid(), typeof(object),
                new Type[] { typeof(object) });
            var ilGen = meth.GetILGenerator();
            var label = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Isinst, info.DeclaringType);
            ilGen.Emit(OpCodes.Brfalse_S, label);

            ilGen.Emit(OpCodes.Ldarg_0);
            if (info.DeclaringType.IsValueType)
            {
                ilGen.Emit(OpCodes.Unbox_Any, info.DeclaringType);
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, info.DeclaringType);
            }

            ilGen.Emit(OpCodes.Callvirt, info.GetGetMethod(true));
            if (info.PropertyType.IsValueType)
            {
                ilGen.Emit(OpCodes.Box, info.PropertyType);
            }

            ilGen.Emit(OpCodes.Ret);
            ilGen.MarkLabel(label);
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Ret);
            return (Func<object, object>)meth.CreateDelegate(typeof(Func<object, object>), null);
        }
        
        /// <summary>
        /// Creates a delegate which can dynamically access a value using the specified property info.
        /// </summary>
        /// <param name="info">The field info to create the getter for.</param>
        /// <returns>A delegate which can dynamically access a value using the specified property info.</returns>
        public static Func<object, object> CreateGetter(this FieldInfo info)
        {
            info = FieldInfo.GetFieldFromHandle(info.FieldHandle, info.DeclaringType.TypeHandle);
            var meth = FactoryHelpers.DefineDynamicMethod(info.Name + "_getter" + Guid.NewGuid(), typeof(object),
                new Type[] { typeof(object) });
            var ilGen = meth.GetILGenerator();
            var label = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Isinst, info.DeclaringType);
            ilGen.Emit(OpCodes.Brfalse_S, label);

            ilGen.Emit(OpCodes.Ldarg_0);
            if (info.DeclaringType.IsValueType)
            {
                ilGen.Emit(OpCodes.Unbox_Any, info.DeclaringType);
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, info.DeclaringType);
            }

            ilGen.Emit(OpCodes.Ldfld, info);
            if (info.FieldType.IsValueType)
            {
                ilGen.Emit(OpCodes.Box, info.FieldType);
            }

            ilGen.Emit(OpCodes.Ret);
            ilGen.MarkLabel(label);
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Ret);
            return (Func<object, object>)meth.CreateDelegate(typeof(Func<object, object>), null);
        }
        #endregion

        public static Predicate<object> CreateIsDefaultValue(this Type type)
        {
            var meth = FactoryHelpers.DefineDynamicMethod(type.Name + "_IsDefault_" + Guid.NewGuid(), typeof(bool), new Type[] { typeof(object) });
            var ilGen = meth.GetILGenerator();
            var label = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Isinst, type);
            ilGen.Emit(OpCodes.Brfalse_S, label);
            ProxyBuildingPatterns.Default(ilGen, type);

            ilGen.Emit(OpCodes.Ldarg_0);
            if (type.IsValueType)
            {
                bool unbox;
                var eqM = GetCustomEqualityOperator(type, out unbox);
                if (eqM == null || unbox)
                {
                    ilGen.Emit(OpCodes.Unbox_Any, type);
                }
                if (eqM != null)
                {
                    ilGen.Emit(eqM.IsStatic ? OpCodes.Call : OpCodes.Callvirt, eqM);
                }
                else
                {
                    ilGen.Emit(OpCodes.Ceq);
                }
            }
            else
            {
                ilGen.Emit(OpCodes.Castclass, type);
                ilGen.Emit(OpCodes.Ceq);
            }

            ilGen.Emit(OpCodes.Ret);
            ilGen.MarkLabel(label);
            ilGen.Emit(OpCodes.Ldc_I4_0);
            ilGen.Emit(OpCodes.Ret);
            return (Predicate<object>)meth.CreateDelegate(typeof(Predicate<object>), null);
        }

        private static MethodInfo GetCustomEqualityOperator(Type type, out bool unbox)
        {
            unbox = false;
            var meth = type.GetMethod("op_Equality", BindingFlags.Static | BindingFlags.Public, null, new Type[] { type, type }, null);
            if (meth != null)
            {
                unbox = true;
                return meth;
            }

            return meth;
        }
    }
}
