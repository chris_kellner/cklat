﻿using CKLib.Utilities.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKLib.Utilities.Serialization.Json
{
    public class JsonUtils
    {
        public static string JsonEncode(bool value)
        {
            return value.ToString();
        }

        public static string JsonEncode(byte value)
        {
            return value.ToString();
        }

        public static string JsonEncode(short value)
        {
            return value.ToString();
        }

        public static string JsonEncode(ushort value)
        {
            return value.ToString();
        }

        public static string JsonEncode(int value)
        {
            return value.ToString();
        }

        public static string JsonEncode(uint value)
        {
            return value.ToString();
        }

        public static string JsonEncode(long value)
        {
            return value.ToString();
        }

        public static string JsonEncode(ulong value)
        {
            return value.ToString();
        }

        public static string JsonEncode(float value)
        {
            return value.ToString();
        }

        public static string JsonEncode(double value)
        {
            return value.ToString();
        }

        public static string JsonEncode(string value)
        {
            return JsonEscape(value);
        }

        public static string JsonEncode(Enum value)
        {
            return JsonEscape(value.ToString());
        }

        public static string JsonEncode(TimeSpan value)
        {
            return JsonEscape(value.ToString());
        }

        public static string JsonEncode(DateTime value)
        {
            return JsonEscape(value.ToString());
        }

        public static string JsonEscape(string unescaped)
        {
            return StringUtils.ToStringLiteral(unescaped, '"', '/');
        }
    }
}
