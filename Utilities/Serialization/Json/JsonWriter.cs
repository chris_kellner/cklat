﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CKLib.Utilities.Serialization.Json
{
    public class JsonWriter : IDisposable
    {
        private static readonly XmlIgnoreAttribute xmlIgnore = new XmlIgnoreAttribute();
        private TextWriter m_Writer;
        private XmlWriterSettings m_Settings;
        private int m_Indent;

        public JsonWriter(TextWriter writer)
        {
            m_Writer = writer;
            m_Settings = new XmlWriterSettings()
            {
                CloseOutput = true,
                ConformanceLevel = ConformanceLevel.Fragment,
                Indent = true,
                Encoding = Encoding.UTF8,
                IndentChars = "  ",
                NewLineChars = Environment.NewLine,
                DoNotEscapeUriAttributes = false
            };
        }

        public JsonWriter(Stream outputStream)
            : this(new StreamWriter(outputStream))
        {
            if (!outputStream.CanWrite)
            {
                throw new ArgumentException("Unable to write to target stream");
            }
        }

        public XmlWriterSettings Settings
        {
            get
            {
                return m_Settings;
            }
        }

        public void WriteValue(bool value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(byte value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(short value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(ushort value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(int value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(uint value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(long value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(ulong value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(float value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(double value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(decimal value)
        {
            WriteValue((double)value);
        }

        public void WriteValue(TimeSpan value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(DateTime value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(Enum value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(string value)
        {
            m_Writer.Write(JsonUtils.JsonEncode(value));
        }

        public void WriteValue(object value)
        {
            if (null == value)
            {
                m_Writer.Write("null");
                return;
            }

            Type t = value.GetType();
            if (t.IsEnum)
            {
                WriteValue((Enum)value);
            }
            else
            {
                TypeCode code = Type.GetTypeCode(t);
                switch (code)
                {
                    case TypeCode.Boolean:
                        WriteValue((bool)value);
                        break;
                    case TypeCode.Byte:
                        WriteValue((byte)value);
                        break;
                    case TypeCode.Char:
                        WriteValue(value.ToString());
                        break;
                    case TypeCode.DateTime:
                        WriteValue((DateTime)value);
                        break;
                    case TypeCode.Decimal:
                        WriteValue((Decimal)value);
                        break;
                    case TypeCode.Single:
                        WriteValue((float)value);
                        break;
                    case TypeCode.Double:
                        WriteValue((double)value);
                        break;
                    case TypeCode.Int16:
                        WriteValue((short)value);
                        break;
                    case TypeCode.UInt16:
                        WriteValue((ushort)value);
                        break;
                    case TypeCode.Int32:
                        WriteValue((int)value);
                        break;
                    case TypeCode.UInt32:
                        WriteValue((uint)value);
                        break;
                    case TypeCode.Int64:
                        WriteValue((long)value);
                        break;
                    case TypeCode.UInt64:
                        WriteValue((ulong)value);
                        break;
                    case TypeCode.SByte:
                        WriteValue((int)(sbyte)value);
                        break;
                    case TypeCode.String:
                        WriteValue((string)value);
                        break;
                    case TypeCode.Object:
                        if (typeof(TimeSpan) == t)
                        {
                            WriteValue((TimeSpan)value);
                        }
                        else if (IsValidDictionaryType(t))
                        {
                            WriteDictionary((IDictionary)value);
                        }
                        else if (value is IEnumerable)
                        {
                            WriteValue((IEnumerable)value);
                        }
                        else if (value is Type)
                        {
                            WriteValue(value.ToString());
                        }
                        else
                        {
                            WriteObject(value);
                        }
                        break;
                }
            }
        }

        private void WriteDictionary(IDictionary obj)
        {
            if (obj.Count == 0)
            {
                m_Writer.Write("{}");
                return;
            }

            m_Writer.Write(GetLineEnding() + "{" + GetLineEnding());
            ++Indent;
            bool first = true;
            foreach (DictionaryEntry pair in obj)
            {
                WriteMember(pair.Key.ToString(), pair.Value, first);
                first = false;
            }
            --Indent;
            m_Writer.Write(GetLineEnding() + "}" + GetLineEnding());
        }

        private bool IsValidDictionaryType(Type type)
        {
            return typeof(IDictionary).IsAssignableFrom(type);
        }

        private void WriteObject(object obj)
        {
            var props = GetProperties(obj);
            if (props.Count == 0)
            {
                m_Writer.Write("{}");
                return;
            }
            else
            {
                m_Writer.Write(GetLineEnding() + "{" + GetLineEnding());
                ++Indent;
                var xmlIgnore = new XmlIgnoreAttribute();
                bool first = true;
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    if (!typeof(Delegate).IsAssignableFrom(prop.PropertyType))
                    {
                        if (prop.ShouldSerializeValue(obj))
                        {
                            if (!prop.Attributes.Matches(xmlIgnore))
                            {
                                WriteMember(prop.Name, prop.GetValue(obj), first);
                                first = false;
                            }
                        }
                    }
                }
                --Indent;
                m_Writer.Write(GetLineEnding() + "}");
            }
        }

        private PropertyDescriptorCollection GetProperties(object obj)
        {
            var props = TypeDescriptor.GetProperties(obj);
            var wantedProps = props.Cast<PropertyDescriptor>()
                .Where(prop =>
                {
                    if (!typeof(Delegate).IsAssignableFrom(prop.PropertyType))
                    {
                        if (prop.ShouldSerializeValue(obj))
                        {
                            if (!prop.Attributes.Matches(xmlIgnore))
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                });
            return new PropertyDescriptorCollection(wantedProps.ToArray(), true);
        }

        private void WriteMember(string memberName, object value, bool first)
        {
            if (!first)
            {
                m_Writer.Write("," + m_Settings.NewLineChars);
            }

            m_Writer.Write(m_IndentString + JsonUtils.JsonEscape(memberName) + ": ");
            WriteValue(value);
        }

        public void WriteValue(IEnumerable values)
        {
            m_Writer.Write("[");
            bool first = true;
            foreach (var item in values)
            {
                if (!first)
                {
                    m_Writer.Write(", ");
                }

                first = false;
                WriteValue(item);
            }
            m_Writer.Write("]");
        }

        private string GetLineEnding()
        {
            return m_Settings.NewLineChars + m_IndentString;
        }

        private string CalculateIndentString()
        {
            if (m_Settings.Indent)
            {
                return string.Concat(Enumerable.Repeat(m_Settings.IndentChars, m_Indent).ToArray());
            }

            return string.Empty;
        }

        private string m_IndentString = string.Empty;
        private int Indent
        {
            get
            {
                return m_Indent;
            }
            set
            {
                m_Indent = value;
                m_IndentString = CalculateIndentString();
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~JsonWriter()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (m_Settings.CloseOutput)
                {
                    m_Writer.Dispose();
                }
            }
        }
        #endregion
    }
}
