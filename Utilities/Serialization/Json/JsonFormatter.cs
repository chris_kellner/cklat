﻿using CKLib.Utilities.Language.Samples;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CKLib.Utilities.Serialization.Json
{
    public class JsonFormatter : IFormatter, ISerializer
    {
        #region Constants
        /// <summary>
        /// The name of the format that this serializer handles.
        /// </summary>
        public const string FORMAT_NAME = "CK-JSON";
        #endregion

        #region Variables
        private JsonGrammar m_Grammar;
        private ISerializerProvider m_SerializerProvider;
        private Type m_StrictType;
        #endregion

        public JsonFormatter()
        {
            m_Grammar = new JsonGrammar();
            m_Grammar.AotCompile();
        }

        public JsonFormatter(Type objectType)
            : this()
        {
            m_StrictType = objectType;
        }


        public SerializationBinder Binder
        {
            get;
            set;
        }

        public StreamingContext Context
        {
            get;
            set;
        }

        public object Deserialize(Stream serializationStream)
        {
            StreamReader sr = new StreamReader(serializationStream);
            string s = sr.ReadToEnd();
            if (m_StrictType != null)
            {
                return m_Grammar.Evaluate(m_StrictType, s);
            }
            else
            {
                return m_Grammar.Evaluate<object>(s);
            }
        }

        public TResult Deserialize<TResult>(Stream serializationStream)
        {
            StreamReader sr = new StreamReader(serializationStream);
            string s = sr.ReadToEnd();
            return m_Grammar.Evaluate<TResult>(s);
        }

        public void Serialize(Stream serializationStream, object graph)
        {
            JsonWriter jw = new JsonWriter(serializationStream);
            jw.WriteValue(graph);
        }

        public static string Stringify(object graph)
        {
            using (var writer = new StringWriter())
            {
                JsonWriter jw = new JsonWriter(writer);
                jw.WriteValue(graph);
                return writer.ToString();
            }
        }

        public ISurrogateSelector SurrogateSelector
        {
            get;
            set;
        }


        public string FormatName
        {
            get { return FORMAT_NAME; }
        }

        public BitConverter Converter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the object used for retrieving serializers of other formats.
        /// </summary>
        public ISerializerProvider SerializerProvider
        {
            get
            {
                if (m_SerializerProvider == null)
                {
                    m_SerializerProvider = new DefaultSerializerProvider();
                }

                return m_SerializerProvider;
            }
            set
            {
                m_SerializerProvider = value;
            }
        }
    }
}
