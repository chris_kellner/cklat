﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System.IO;
    using System.Runtime.Serialization;
    #endregion

    /// <summary>
    /// This interface defines the functions used for (de)serializing objects to/from a stream.
    /// </summary>
    public interface ISerializer
    {
        // Summary:
        //     Gets or sets the System.Runtime.Serialization.SerializationBinder that performs
        //     type lookups during deserialization.
        //
        // Returns:
        //     The System.Runtime.Serialization.SerializationBinder that performs type lookups
        //     during deserialization.
        SerializationBinder Binder { get; set; }
        //
        // Summary:
        //     Gets or sets the System.Runtime.Serialization.StreamingContext used for serialization
        //     and deserialization.
        //
        // Returns:
        //     The System.Runtime.Serialization.StreamingContext used for serialization
        //     and deserialization.
        StreamingContext Context { get; set; }

        /// <summary>
        /// Gets the name of the format that this serializer reads and writes. 
        /// </summary>
        string FormatName { get; }

        /// <summary>
        /// Gets or sets an object used for converting values to-and-from bytes.
        /// </summary>
        BitConverter Converter { get; set; }

        /// <summary>
        /// Gets or sets the object used for retrieving serializers of other formats.
        /// </summary>
        ISerializerProvider SerializerProvider { get; set; }

        // Summary:
        //     Deserializes the data on the provided stream and reconstitutes the graph
        //     of objects.
        //
        // Parameters:
        //   serializationStream:
        //     The stream that contains the data to deserialize.
        //
        // Returns:
        //     The top object of the deserialized graph.
        object Deserialize(Stream serializationStream);
        
        //
        // Summary:
        //     Serializes an object, or graph of objects with the given root to the provided
        //     stream.
        //
        // Parameters:
        //   serializationStream:
        //     The stream where the formatter puts the serialized data. This stream can
        //     reference a variety of backing stores (such as files, network, memory, and
        //     so on).
        //
        //   graph:
        //     The object, or root of the object graph, to serialize. All child objects
        //     of this root object are automatically serialized.
        void Serialize(Stream serializationStream, object graph);
    }
}
