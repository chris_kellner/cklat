﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Collections.Generic;
    #endregion

    /// <summary>
    /// This class serves as a fall-back deserialization container for unknown types.
    /// </summary>
    public sealed class Unknown : IEnumerable<KeyValuePair<string, object>>, IGraphWriter
    {
        #region Fields
        private Dictionary<string, object> m_Value = new Dictionary<string, object>();
        #endregion

        #region Constructors
        private Unknown(ObjectGraph graph)
        {
            TypeName = graph.TypeName;
            foreach (var field in graph.FieldNames())
            {
                m_Value[field] = graph.ReadObject(field);
            }
        }
        #endregion

        #region Properties
        public object this[string key]
        {
            get
            {
                return m_Value[key];
            }

            set
            {
                m_Value[key] = value;
            }
        }

        public string TypeName { get; private set; }
        #endregion

        #region Methods
        public bool Contains(string key)
        {
            return m_Value.ContainsKey(key);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return m_Value.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object MakeProxy(Type tProxy)
        {
            return null;
        }

        public TProxy MakeProxy<TProxy>() where TProxy : class
        {
            return (TProxy)MakeProxy(typeof(TProxy));
        }

        public void WriteObjectData(ObjectGraph graph)
        {
            foreach (var field in m_Value)
            {
                graph.WriteObject(field.Key, field.Value);
            }
        }
        #endregion
    }
}
