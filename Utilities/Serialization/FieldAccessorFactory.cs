﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Proxy.Builder;
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    #endregion

    /// <summary>
    /// A class used for creating IFieldAccessor instances.
    /// </summary>
    public class FieldAccessorFactory
    {
        #region Inner Classes
        private class FieldAccessor : IFieldAccessor
        {
            private Func<object, object> m_Getter;
            private Setter m_Setter;
            private Predicate<object> m_IsDefaultValue;

            public FieldAccessor(FieldInfo info)
            {
                FieldType = info.FieldType;
                FieldName = info.Name;
                m_IsDefaultValue = FieldType.CreateIsDefaultValue();
                m_Getter = info.CreateGetter();
                m_Setter = info.CreateSetter();

                var attrs = info.GetCustomAttributes(typeof(RequestedFormatAttribute), true);
                if (attrs.Length > 0)
                {
                    var atr = attrs[0] as RequestedFormatAttribute;
                    RequestedFormat = atr.FormatName;
                }
            }

            public string FieldName
            {
                get;
                private set;
            }

            public Type FieldType
            {
                get;
                private set;
            }

            public bool IsReadOnly
            {
                get { return false; }
            }

            public bool ShouldSerializeValue(object value)
            {
                return value != null ? !m_IsDefaultValue(value) : false;
            }

            public object GetValue(object target)
            {
                return m_Getter(target);
            }

            public void SetValue(object target, object value)
            {
                m_Setter(ref target, value);
            }


            public string RequestedFormat
            {
                get;
                private set;
            }
        }

        private class PropertyAccessor : IFieldAccessor
        {
            private Func<object, object> m_Getter;
            private Setter m_Setter;
            private Predicate<object> m_IsDefaultValue;

            public PropertyAccessor(PropertyInfo info)
            {
                FieldType = info.PropertyType;
                IsReadOnly = !info.CanWrite;
                FieldName = info.Name;
                m_IsDefaultValue = FieldType.CreateIsDefaultValue();
                m_Getter = info.CreateGetter();
                if (!IsReadOnly)
                {
                    m_Setter = info.CreateSetter();
                }

                var attrs = info.GetCustomAttributes(typeof(RequestedFormatAttribute), true);
                if (attrs.Length > 0)
                {
                    var atr = attrs[0] as RequestedFormatAttribute;
                    RequestedFormat = atr.FormatName;
                }
            }

            public string FieldName
            {
                get;
                private set;
            }

            public Type FieldType
            {
                get;
                private set;
            }

            public bool IsReadOnly
            {
                get;
                private set;
            }

            public bool ShouldSerializeValue(object value)
            {
                return value != null ? !m_IsDefaultValue(value) : false;
            }

            public object GetValue(object target)
            {
                return m_Getter(target);
            }

            public void SetValue(object target, object value)
            {
                if (m_Setter != null)
                {
                    m_Setter(ref target, value);
                }
                else
                {
                    throw new InvalidOperationException("Property is Read-Only");
                }
            }

            public string RequestedFormat
            {
                get;
                private set;
            }
        }
        #endregion

        #region Public Interface
        /// <summary>
        /// Creates a FieldAccessor for the specified property.
        /// </summary>
        /// <param name="info">The property to create the accessor for.</param>
        /// <returns>A FieldAccessor for the specified property.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception if an accessor cannot be created for the specified property.</exception>
        public static IFieldAccessor CreateAccessor(MemberInfo info)
        {
            IsMemberValid(info, true);
            if (info is PropertyInfo)
            {
                return new PropertyAccessor((PropertyInfo)info);
            }
            else if (info is FieldInfo)
            {
                return new FieldAccessor((FieldInfo)info);
            }
            else
            {
                throw new InvalidOperationException("Received unexpected MemberInfo type");
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not a FieldAccessor can be created for the specified member info.
        /// </summary>
        /// <param name="info">The member to validate.</param>
        /// <param name="throwOnNotValid">True to throw an exception if the member is not valid.</param>
        /// <returns>True if the member is valid, false otherwise.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception on invalid members if <c>throwOnNotValid</c> if true.</exception>
        public static bool IsMemberValid(MemberInfo info, bool throwOnNotValid)
        {
            if (info is PropertyInfo)
            {
                return IsPropertyValid((PropertyInfo)info, throwOnNotValid);
            }
            else if (info is FieldInfo)
            {
                return IsFieldValid((FieldInfo)info, throwOnNotValid);
            }
            else if(throwOnNotValid)
            {
                throw new InvalidOperationException("Unexpected member type");
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether or not a FieldAccessor can be created for the specified property info.
        /// </summary>
        /// <param name="info">The property to validate.</param>
        /// <param name="throwOnNotValid">True to throw an exception if the property is not valid.</param>
        /// <returns>True if the property is valid, false otherwise.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception on invalid properties if <c>throwOnNotValid</c> if true.</exception>
        public static bool IsPropertyValid(PropertyInfo info, bool throwOnNotValid)
        {
            if (!info.CanRead)
            {
                if (throwOnNotValid)
                {
                    throw new InvalidOperationException("Property cannot be read");
                }
                return false;
            }

            if (info.GetIndexParameters().Length > 0)
            {
                if (throwOnNotValid)
                {
                    throw new InvalidOperationException("Cannot create accessor for indexer properties");
                }
                return false;
            }

            if (info.DeclaringType.IsNestedPrivate)
            {
                if (throwOnNotValid)
                {
                    throw new InvalidOperationException("Cannot create accessor for property of private nested class");
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets a value indicating whether or not a FieldAccessor can be created for the specified field info.
        /// </summary>
        /// <param name="info">The field to validate.</param>
        /// <param name="throwOnNotValid">True to throw an exception if the field is not valid.</param>
        /// <returns>True if the field is valid, false otherwise.</returns>
        /// <exception cref="System.InvalidOperationException">Throws an exception on invalid fields if <c>throwOnNotValid</c> if true.</exception>
        public static bool IsFieldValid(FieldInfo info, bool throwOnNotValid)
        {
            if (info.DeclaringType.IsNestedPrivate)
            {
                if (throwOnNotValid)
                {
                    throw new InvalidOperationException("Cannot create accessor for field of private nested class");
                }
                return false;
            }

            if (info.FieldType.IsPointer)
            {
                if (throwOnNotValid)
                {
                    throw new InvalidOperationException("Cannot create accessor for pointer type.");
                }

                return false;
            }

            return true;
        }
        #endregion

        #region HelperMethods
        private static void AssertPropertyIsValid(PropertyInfo info)
        {
            IsPropertyValid(info, true);
        }

        private static void AssertFieldIsValid(FieldInfo info)
        {
            IsFieldValid(info, true);
        }
        #endregion
    }
}
