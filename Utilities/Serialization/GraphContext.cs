﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.Generic;
    #endregion

    /// <summary>
    /// This class is used for maintaining the set of handles discovered during the serialization process.
    /// </summary>
    internal class GraphContext
    {
        public const string ROOT = "<![ROOT]>";
        public const string ITEM = "<![ITEM]>";
        private static readonly List<string> SYSTEM_TYPE_NAMES = new List<string>()
        {
            ROOT,
            ITEM,
            typeof(bool).FullName,
            typeof(byte).FullName,
            typeof(byte[]).FullName,
            typeof(sbyte).FullName,
            typeof(char).FullName,
            typeof(short).FullName,
            typeof(ushort).FullName,
            typeof(int).FullName,
            typeof(uint).FullName,
            typeof(long).FullName,
            typeof(ulong).FullName,
            typeof(float).FullName,
            typeof(double).FullName,
            typeof(TimeSpan).FullName,
            typeof(DateTime).FullName,
            typeof(string).FullName
        };

        private List<object> m_Objects = new List<object>();
        private Dictionary<string, int> m_Strings = new Dictionary<string, int>();
        private List<string> m_StringList = new List<string>(SYSTEM_TYPE_NAMES);
        private int m_StringHandle = 0;
        public GraphContext()
        {
            SYSTEM_TYPE_NAMES.ForEach(s => m_Strings[s] = m_StringHandle++);
        }

        public string ResolveStringHandle(int handle)
        {
            return m_StringList[handle];
        }

        public int TryRegisterString(string obj)
        {
            int idx;
            if (!m_Strings.TryGetValue(obj, out idx))
            {
                m_Strings[obj] = m_StringHandle++;
                m_StringList.Add(obj);
                return -1;
            }

            return idx;
        }

        public object ResolveObjectHandle(int handle)
        {
            return m_Objects[handle];
        }

        public int TryRegisterObject(object obj)
        {
            int idx = m_Objects.IndexOf(obj);
            if (idx < 0)
            {
                m_Objects.Add(obj);
            }

            return idx;
        }
    }
}
