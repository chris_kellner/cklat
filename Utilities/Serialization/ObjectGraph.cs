﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    #endregion

    /// <summary>
    /// This class is used to hold all of the information needed to read/write an object from/to a Stream.
    /// </summary>
    public sealed class ObjectGraph
    {
        #region Constants
        private const int HANDLE = 0;
        private const int OBJECT = 1;
        private const int ARRAY = 2;
        private const int MISSING = 3;
        private const int LENGTH_OFFSET = MISSING;
        #endregion

        #region Fields
        private bool m_IsPrimitive;
        private bool m_IsHandle;
        private bool m_IsCollection;
        private object m_ObjectValue;
        private ObjectGraph m_Surrogate;
        private byte[] m_Value;
        private int m_Length;
        private Type m_Type;
        private ObjectGraph[] m_Elements;
        private Dictionary<string, ObjectGraph> m_Fields = new Dictionary<string,ObjectGraph>();
        private ITypeDefinition m_Definition;
        private ISerializer m_Serializer;
        private BitConverter m_Converter;
        private string m_FieldName;
        #endregion

        #region Constructors
        private ObjectGraph(ISerializer serializer)
        {
            m_Serializer = serializer;
            m_Converter = serializer != null ? serializer.Converter : null;
            Context = new GraphContext();
            SetFieldName(GraphContext.ROOT);
        }

        protected ObjectGraph(ObjectGraph parent, string fieldName)
        {
            if (parent != null)
            {
                Context = parent.Context;
                m_Serializer = parent.m_Serializer;
                m_Converter = m_Serializer.Converter;
            }
            else
            {
                Context = new GraphContext();
            }

            SetFieldName(fieldName);
        }

        public ObjectGraph(ISerializer serializer, object root)
            : this(serializer)
        {
            SetRoot(root);
        }

        private ObjectGraph(ObjectGraph parent, string fieldName, object root)
            : this(parent, fieldName)
        {
            SetRoot(root);
        }

        private ObjectGraph(ObjectGraph parent, string fieldName, Type type, byte[] value)
            : this(parent, fieldName)
        {
            InitializeTypeData(type);
            m_Value = value;
            m_Length = CalulateLength();
        }
        #endregion

        #region Properties
        private GraphContext Context
        {
            get;
            set;
        }

        public bool UsesCustomSerializer
        {
            get
            {
                return m_Serializer.FormatName != BinarySerializer.FORMAT_NAME;
            }
        }

        public ISerializer Serializer
        {
            get
            {
                return m_Serializer;
            }
        }

        public BitConverter Converter
        {
            get
            {
                return m_Converter;
            }
        }

        public string TypeName
        {
            get;
            private set;
        }

        public Type Type
        {
            get
            {
                return m_Type;
            }
        }

        public ITypeDefinition TypeDefinition
        {
            get
            {
                return m_Definition;
            }
        }

        public byte[] Value
        {
            get
            {
                if (m_Value != null)
                {
                    return (byte[])m_Value.Clone();
                }

                return null;
            }
        }

        public string FieldName { get { return m_FieldName; } }

        public IEnumerable<string> FieldNames()
        {
            return m_Fields.Keys;
        }
        #endregion

        #region Initialization
        private void InitializeTypeData(Type type)
        {
            m_Type = type;
            var def = TypeRegistry.Instance.GetTypeDefinition(m_Type, true);
            m_Definition = def;
            TypeName = m_Definition.QualifiedName;
            m_IsPrimitive = def.IsPrimitive;
            m_IsCollection = def.IsCollection;
            if (!m_IsPrimitive)
            {
                if (!m_IsCollection)
                {
                    m_Elements = new ObjectGraph[def.FieldCount];
                }
            }
        }

        private int CalulateLength()
        {
            int len = 0;
            if (m_IsPrimitive || UsesCustomSerializer)
            {
                len += m_Value.Length;
            }
            else if (m_IsCollection)
            {
                //m_CountBytes = m_Converter.SevenBitEncode(m_Elements != null ? m_Elements.Length : 0);
                return ARRAY;
            }
            else
            {
                //m_CountBytes = m_Converter.SevenBitEncode(m_Fields != null ? m_Fields.Count : 0);
                return OBJECT;
            }

            return len;
        }
        #endregion

        #region Write
        public void WriteNull(string fieldName)
        {
            SetField(fieldName, new ObjectGraph(this, fieldName));
        }

        public void WriteValue(string fieldName, bool value)
        {
            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(bool), bytes));
        }

        public void WriteValue(string fieldName, byte value)
        {
            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(byte), bytes));
        }

        public void WriteValue(string fieldName, sbyte value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(sbyte), bytes));
        }

        public void WriteValue(string fieldName, char value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(char), bytes));
        }

        public void WriteValue(string fieldName, short value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(short), bytes));
        }

        public void WriteValue(string fieldName, ushort value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(ushort), bytes));
        }

        public void WriteValue(string fieldName, int value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(int), bytes));
        }

        public void WriteValue(string fieldName, uint value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(uint), bytes));
        }

        public void WriteValue(string fieldName, long value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(long), bytes));
        }

        public void WriteValue(string fieldName, ulong value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(ulong), bytes));
        }

        public void WriteValue(string fieldName, float value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(float), bytes));
        }

        public void WriteValue(string fieldName, double value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(double), bytes));
        }

        public void WriteValue(string fieldName, DateTime value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(DateTime), bytes));
        }

        public void WriteValue(string fieldName, TimeSpan value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(TimeSpan), bytes));
        }

        public void WriteValue(string fieldName, string value)
        {

            byte[] bytes = m_Converter.GetBytes(value, Encoding.UTF8);
            SetField(fieldName, new ObjectGraph(this, fieldName, typeof(string), bytes));
        }

        public void WriteValue(string fieldName, Enum value)
        {

            byte[] bytes = m_Converter.GetBytes(value);
            SetField(fieldName, new ObjectGraph(this, fieldName, value.GetType(), bytes));
        }

        public void WriteObject(string fieldName, object value, string format)
        {
            if (!string.IsNullOrEmpty(format) && format != BinarySerializer.FORMAT_NAME)
            {
                var provider = m_Serializer.SerializerProvider;
                ISerializer serializer = null;
                if (provider != null)
                {
                    serializer = provider.GetSerializer(format, value.GetType());
                }

                if (serializer != null)
                {
                    var handle = Context.TryRegisterObject(value);
                    if (handle == -1)
                    {

                        var graph = new ObjectGraph(this, fieldName);
                        graph.m_Serializer = serializer;
                        graph.InitializeTypeData(value.GetType());
                        using (var stream = new MemoryStream())
                        {
                            serializer.Serialize(stream, value);
                            graph.SetRaw(stream.ToArray());
                        }

                        SetField(fieldName, graph);
                        return;
                    }
                }
            }

            WriteObject(fieldName, value);
        }

        public void WriteObject(string fieldName, object value)
        {

            SetField(fieldName, new ObjectGraph(this, fieldName, value));
        }

        public void WriteCollectionCount(int count)
        {
            if (m_Elements == null)
            {
                m_Elements = new ObjectGraph[count];
            }
            else if (m_Elements.Length != count)
            {
                Array.Resize(ref m_Elements, count);
            }
        }

        public void WriteCollectionItem(int index, object value)
        {
            m_Elements[index] = new ObjectGraph(this, GraphContext.ITEM, value);
        }

        public void SetRaw(byte[] value)
        {
            m_Value = value;
            m_Length = CalulateLength();
        }

        /// <summary>
        /// Sets the root object of this graph.
        /// </summary>
        /// <param name="root"></param>
        public void SetRoot(object root)
        {
            if (m_Definition != null)
            {
                throw new SerializationException("Root has already been set");
            }

            if (root != null)
            {
                var type = root.GetType();
                InitializeTypeData(type);
                if (m_Definition.IsPrimitive)
                {
                    m_Definition.WriteObjectData(root, this);
                }
                else
                {
                    var idx = Context.TryRegisterObject(root);
                    if (idx < 0)
                    {
                        m_Definition.WriteObjectData(root, this);
                        m_Length = CalulateLength();
                    }
                    else
                    {
                        // Write the handle:
                        m_Value = m_Converter.SevenBitEncode(idx);
                        m_IsHandle = true;
                        m_Length = 0;
                    }
                }
            }
            else
            {
                m_Length = MISSING;
            }
        }

        public void WriteRaw(string fieldName, Type type, byte[] value)
        {
            SetField(fieldName, new ObjectGraph(this, fieldName, type, value));
        }
        #endregion

        #region Read
        public ITypeDefinition GetFieldDefinition(string fieldName)
        {
            var fld = GetField(fieldName);
            if (fld != null)
            {
                return fld.TypeDefinition;
            }

            return null;
        }

        public object ReadObject()
        {
            if (m_ObjectValue == null)
            {
                if (m_Length != 0)
                {
                    if (UsesCustomSerializer)
                    {
                        using (var stream = new MemoryStream(m_Value))
                        {
                            m_ObjectValue = m_Serializer.Deserialize(stream);
                        }
                    }
                    else
                    {
                        m_ObjectValue = TypeDefinition.ReadObjectData(this);
                    }
                }
                else if (m_IsHandle)
                {
                    if (m_Surrogate != null)
                    {
                        return m_Surrogate.ReadObject();
                    }

                    throw new InvalidDataException("Handle not defined");
                }
            }

            return m_ObjectValue;
        }

        public object ReadObject(string fieldName)
        {

            var fld = GetField(fieldName);
            if (fld != null)
            {
                return fld.ReadObject();
            }

            return null;
        }

        public int ReadCollectionCount()
        {
            if (m_Elements != null)
            {
                return m_Elements.Length;
            }

            return -1;
        }

        public object ReadCollectionItem(int index)
        {
            var fld = m_Elements[index];
            if (fld != null)
            {
                return fld.ReadObject();
            }

            return null;
        }

        public IEnumerable ReadCollection()
        {
            int count = ReadCollectionCount();
            for (int i = 0; i < count; i++)
            {
                yield return ReadCollectionItem(i);
            }
        }

        public IEnumerable<T> ReadCollection<T>()
        {
            int count = ReadCollectionCount();
            for (int i = 0; i < count; i++)
            {
                yield return (T)ReadCollectionItem(i);
            }
        }

        public byte[] ReadRaw(string fieldName)
        {

            var fld = GetField(fieldName);
            if (fld != null)
            {
                return fld.Value;
            }

            return null;
        }

        public bool ReadBoolean(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToBoolean(fld.Value, 0);
            return value;
        }

        public byte ReadValue(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToByte(fld.Value, 0);
            return value;
        }

        public sbyte ReadSByte(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToSByte(fld.Value, 0);
            return value;
        }

        public char ReadChar(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToChar(fld.Value, 0);
            return value;
        }

        public short ReadInt16(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToInt16(fld.Value, 0);
            return value;
        }

        public ushort ReadUInt16(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToUInt16(fld.Value, 0);
            return value;
        }

        public int ReadInt32(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToInt32(fld.Value, 0);
            return value;
        }

        public uint ReadUInt32(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToUInt32(fld.Value, 0);
            return value;
        }

        public long ReadInt64(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToInt64(fld.Value, 0);
            return value;
        }

        public ulong ReadUInt64(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToUInt64(fld.Value, 0);
            return value;
        }

        public float ReadSingle(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToSingle(fld.Value, 0);
            return value;
        }

        public double ReadDouble(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToDouble(fld.Value, 0);
            return value;
        }

        public DateTime ReadDateTime(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToDateTime(fld.Value, 0);
            return value;
        }

        public TimeSpan ReadTimeSpan(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToTimeSpan(fld.Value, 0);
            return value;
        }

        public string ReadString(string fieldName)
        {

            var fld = GetField(fieldName);
            var value = m_Converter.ToString(fld.Value, 0);
            return value;
        }

        public Enum ReadEnum(string fieldName, Type enumType)
        {

            var fld = GetField(fieldName);
            Enum value = m_Converter.ToEnum(fld.Value, 0, enumType);
            return value;
        }
        #endregion

        #region Helpers
        private ObjectGraph GetField(string fieldName)
        {
            ObjectGraph graph;
            if (m_Fields.TryGetValue(fieldName, out graph))
            {
                return graph;
            }

            return NULL_GRAPH;
        }

        private static readonly ObjectGraph NULL_GRAPH = new ObjectGraph(null);

        private void SetField(string fieldName, ObjectGraph graph)
        {
            if (m_Fields.ContainsKey(fieldName))
            {
                throw new SerializationException(string.Format("Field \"{0}\" already defined", fieldName));
            }

            graph.SetFieldName(fieldName);
            m_Fields[fieldName] = graph;
        }

        private void SetFieldName(string fieldName)
        {
            m_FieldName = fieldName;
        }

        private void WriteLengthEncodedString(Stream stream, string value)
        {
            var idx = Context.TryRegisterString(value);
            if (idx < 0)
            {
                byte[] data = m_Converter.GetBytes(value, Encoding.UTF8);
                m_Converter.SevenBitEncode(stream, data.Length + 1);
                stream.Write(data, 0, data.Length);
            }
            else
            {
                stream.WriteByte((byte)HANDLE);
                m_Converter.SevenBitEncode(stream, idx);
            }
        }

        private string FromLengthEncodedBytes(Stream stream)
        {
            string str = null;
            int length = m_Converter.SevenBitDecodeInt32(stream);
            if (length == HANDLE)
            {
                int handle = m_Converter.SevenBitDecodeInt32(stream);
                str = (string)Context.ResolveStringHandle(handle);
            }
            else
            {
                length -= 1;
                var data = m_Converter.ReadBytes(stream, length);
                str = m_Converter.ToString(data, 0);
                Context.TryRegisterString(str);
            }

            return str;
        }

        private static Dictionary<Type, PrimitiveWriter> s_PrimitiveWritersCache;
        private static Dictionary<Type, PrimitiveReader> s_PrimitiveReadersCache;

        static ObjectGraph()
        {
            s_PrimitiveReadersCache =
            new Dictionary<Type, PrimitiveReader>()
            {
                { typeof(bool), (o,c) => c.Converter.ToBoolean(o, 0) },
                { typeof(byte), (o,c) => c.Converter.ToByte(o, 0) },
                { typeof(sbyte), (o,c) => c.Converter.ToSByte(o, 0) },
                { typeof(char), (o,c) => c.Converter.ToChar(o, 0) },
                { typeof(short), (o,c) => c.Converter.ToInt16(o, 0) },
                { typeof(ushort), (o,c) => c.Converter.ToUInt16(o, 0) },
                { typeof(int), (o,c) => c.Converter.ToInt32(o, 0) },
                { typeof(uint), (o,c) => c.Converter.ToUInt32(o, 0) },
                { typeof(long), (o,c) => c.Converter.ToInt64(o, 0) },
                { typeof(ulong), (o,c) => c.Converter.ToUInt64(o, 0) },
                { typeof(float), (o,c) => c.Converter.ToSingle(o, 0) },
                { typeof(double), (o,c) => c.Converter.ToDouble(o, 0) },
                { typeof(TimeSpan), (o,c) => c.Converter.ToTimeSpan(o, 0) },
                { typeof(DateTime), (o,c) => c.Converter.ToDateTime(o, 0) },
                { typeof(string), (o,c) => c.Converter.ToString(o, 0) },
            };

            s_PrimitiveWritersCache =
            new Dictionary<Type, PrimitiveWriter>()
            {
                { typeof(bool), (o, c) => c.Converter.GetBytes((bool)o) },
                { typeof(byte), (o,c) => c.Converter.GetBytes((byte)o) },
                { typeof(sbyte), (o,c) => c.Converter.GetBytes((sbyte)o) },
                { typeof(char), (o,c) => c.Converter.GetBytes((char)o) },
                { typeof(short), (o,c) => c.Converter.GetBytes((short)o) },
                { typeof(ushort), (o,c) => c.Converter.GetBytes((ushort)o) },
                { typeof(int), (o,c) => c.Converter.GetBytes((int)o) },
                { typeof(uint), (o,c) => c.Converter.GetBytes((uint)o) },
                { typeof(long), (o,c) => c.Converter.GetBytes((long)o) },
                { typeof(ulong), (o,c) => c.Converter.GetBytes((ulong)o) },
                { typeof(float), (o,c) => c.Converter.GetBytes((float)o) },
                { typeof(double), (o,c) => c.Converter.GetBytes((double)o) },
                { typeof(TimeSpan), (o,c) => c.Converter.GetBytes((TimeSpan)o) },
                { typeof(DateTime), (o,c) => c.Converter.GetBytes((DateTime)o) },
                { typeof(string), (o,c) => c.Converter.GetBytes((string)o, Encoding.UTF8) },
            };
        }

        public static PrimitiveWriter GetPrimitiveWriter(Type primitiveType)
        {
            PrimitiveWriter writer;
            s_PrimitiveWritersCache.TryGetValue(primitiveType, out writer);
            return writer;
        }

        public static PrimitiveReader GetPrimitiveReader(Type primitiveType)
        {
            PrimitiveReader reader;
            s_PrimitiveReadersCache.TryGetValue(primitiveType, out reader);
            return reader;
        }
        #endregion

        #region Serialization
        public void WriteToStream(Stream stream)
        {
            if (m_Length == 0)
            {
                if (m_IsHandle)
                {
                    stream.WriteByte(HANDLE);
                    stream.Write(m_Value, 0, m_Value.Length);
                    WriteLengthEncodedString(stream, m_FieldName);
                }
                else
                {
                    stream.WriteByte(MISSING);
                }
            }
            else
            {
                m_Converter.SevenBitEncode(stream, m_Length + LENGTH_OFFSET);
                WriteLengthEncodedString(stream, m_Serializer.FormatName);
                WriteLengthEncodedString(stream, m_Definition.QualifiedName);
                WriteLengthEncodedString(stream, m_FieldName);
                if (m_IsPrimitive)
                {
                    stream.Write(m_Value, 0, m_Value.Length);
                }
                else if (UsesCustomSerializer)
                {
                    stream.Write(m_Value, 0, m_Value.Length);
                }
                else
                {
                    if (m_IsCollection)
                    {
                        m_Converter.SevenBitEncode(stream, m_Elements.Length);
                        foreach (var field in m_Elements)
                        {
                            field.WriteToStream(stream);
                        }
                    }
                    else
                    {
                        m_Converter.SevenBitEncode(stream, m_Fields.Count);
                        foreach (var field in m_Fields.Values)
                        {
                            field.WriteToStream(stream);
                        }
                    }
                }
            }
        }

        private void ReadHandle(Stream stream)
        {
            var conv = m_Converter;
            m_IsHandle = true;
            int handle = conv.SevenBitDecodeInt32(stream);
            m_Surrogate = Context.ResolveObjectHandle(handle) as ObjectGraph;
            m_Definition = m_Surrogate.m_Definition;
            m_Type = m_Definition.Type;
            m_Value = conv.SevenBitEncode(handle);
        }

        private int ReadHeader(Stream stream)
        {
            if (m_Length == ARRAY)
            {
                m_IsCollection = true;
            }

            string format = FromLengthEncodedBytes(stream);
            string typeQName = FromLengthEncodedBytes(stream);
            m_Type = m_Serializer.Binder.BindToType(null, typeQName);
            m_FieldName = FromLengthEncodedBytes(stream);
            if (format != m_Serializer.FormatName)
            {
                var provider = m_Serializer.SerializerProvider;
                if (provider != null)
                {
                    m_Serializer = provider.GetDeserializer(format, m_Type);
                }
            }

            InitializeTypeData(m_Type);
            if (!m_IsPrimitive && !UsesCustomSerializer)
            {
                int count = m_Converter.SevenBitDecodeInt32(stream);
                if (m_IsCollection)
                {
                    m_Elements = new ObjectGraph[(int)count];
                }

                return count;
            }

            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        /// <exception cref="System.IO.EndOfStreamException"></exception>
        public void ReadStream(Stream stream, int infoLength)
        {
            var conv = m_Converter;
            m_Length = infoLength;
            if (m_Length == HANDLE)
            {
                ReadHandle(stream);
            }
            else if (m_Length != MISSING) // not missing
            {
                int fieldCount = ReadHeader(stream);
                if (m_IsPrimitive)
                {
                    m_Length -= LENGTH_OFFSET;
                    m_Value = conv.ReadBytes(stream, (int)m_Length);
                }
                else if (UsesCustomSerializer)
                {
                    m_Length -= LENGTH_OFFSET;
                    m_Value = conv.ReadBytes(stream, (int)m_Length);
                    Context.TryRegisterObject(this);
                }
                else if(fieldCount > 0)
                {
                    Context.TryRegisterObject(this);
                    for (int i = 0; i < fieldCount; i++)
                    {
                        int fLen = conv.SevenBitDecodeInt32(stream);
                        if (fLen == MISSING)
                        {
                            continue;
                        }
                        else
                        {
                            var graph = new ObjectGraph(this, string.Empty);
                            graph.ReadStream(stream, fLen);
                            if (m_IsCollection)
                            {
                                m_Elements[i] = graph;
                            }
                            else
                            {
                                SetField(graph.FieldName, graph);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        /// <exception cref="System.IO.EndOfStreamException"></exception>
        public static ObjectGraph ReadFromStream(ISerializer serializer, Stream stream)
        {
            var info = new ObjectGraph(serializer);
            var conv = serializer.Converter;
            int length = conv.SevenBitDecodeInt32(stream);
            info.ReadStream(stream, length);
            return info;
        }
        #endregion
    }
}
