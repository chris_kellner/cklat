﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    #endregion

    /// <summary>
    /// This class is used for serializing PoCos to/from a byte array.
    /// Support is added for combining serialization techniques form other serializers, and type
    /// information is encoded into the stream.
    /// </summary>
    /// <remarks>
    /// Default serialization is done based on the following rules:
    /// - Type has a default constructor, or defines a .ctr(ObjectGraph) constructor:
    /// -- Serialize based on the <c>public</c> properties
    /// -- Ignore properties marked with [XmlIgnore]
    /// - Type has no valid constructor, but is marked as [Serializable]:
    /// -- Type is serialized using serializable fields
    /// -- Type will be reconstituted without invoking the constructor
    /// - Type has no valid constructor and is not marked as [Serializable]:
    /// -- Type cannot be serialized
    /// 
    /// Primitive data types may be serialized on their own without a parent container.
    /// - Additional primitives may be defined by registering them explicitly with the 
    /// <see cref="CKLib.Utilities.Serialization.TypeRegistry"/>.
    /// </remarks>
    public class BinarySerializer : IFormatter, ISerializer
    {
        #region Constants
        /// <summary>
        /// The name of the format that this serializer handles.
        /// </summary>
        public const string FORMAT_NAME = "CK-BIN";
        #endregion

        #region Variables
        private SerializationBinder m_Binder;
        private BitConverter m_Converter;
        private ISerializerProvider m_SerializerProvider;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the BinarySerializer class.
        /// </summary>
        public BinarySerializer()
            : this(StreamingContextStates.All)
        {
        }

        /// <summary>
        /// Initializes a new instance of the BinarySerializer class.
        /// </summary>
        /// <param name="state">The state for which serialization is being performed.</param>
        public BinarySerializer(StreamingContextStates state)
        {
            Context = new StreamingContext(state);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Not supported, required by the IFormatter interface.
        /// </summary>
        ISurrogateSelector IFormatter.SurrogateSelector
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the format name identifier for this serializer.
        /// </summary>
        public string FormatName
        {
            get
            {
                return FORMAT_NAME;
            }
        }

        /// <summary>
        /// Gets or sets the SerializationBinder implementation to use during the deserialization process.
        /// </summary>
        public SerializationBinder Binder
        {
            get
            {
                if (m_Binder == null)
                {
                    m_Binder = new LenientBinder();
                }

                return m_Binder;
            }

            set
            {
                m_Binder = value;
            }
        }

        /// <summary>
        /// Gets or sets the streaming context.
        /// </summary>
        public StreamingContext Context
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the BitConverter to use during the (de)serialization process.
        /// </summary>
        public BitConverter Converter
        {
            get
            {
                if (m_Converter == null)
                {
                    m_Converter = new BitConverter(false);
                }

                return m_Converter;
            }

            set
            {
                m_Converter = value;
            }
        }

        /// <summary>
        /// Gets or sets the object used for retrieving serializers of other formats.
        /// </summary>
        public ISerializerProvider SerializerProvider
        {
            get
            {
                if (m_SerializerProvider == null)
                {
                    m_SerializerProvider = new DefaultSerializerProvider();
                }

                return m_SerializerProvider;
            }
            set
            {
                m_SerializerProvider = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Deserializes an object from the specified stream.
        /// </summary>
        /// <param name="stream">The stream to deserialize from.</param>
        /// <returns>An object read from the data contained in the specified stream.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.IO.EndOfStreamException"></exception>
        public object Deserialize(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var info = ObjectGraph.ReadFromStream(this, stream);
            return info.ReadObject();
        }

        /// <summary>
        /// Deserializes an object of the specified type from the specified stream.
        /// </summary>
        /// <remarks>If the object in the stream if not of the specified type, it will not be deserialized.</remarks>
        /// <typeparam name="T">The type of object expected to be returned.</typeparam>
        /// <param name="stream">The stream to deserialize the object from.</param>
        /// <returns>An object read from the data contained in the specified stream, or default(T).</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.IO.EndOfStreamException"></exception>
        public T Deserialize<T>(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var info = ObjectGraph.ReadFromStream(this, stream);
            if (typeof(T).IsAssignableFrom(info.Type))
            {
                return (T)info.ReadObject();
            }

            return default(T);
        }

        /// <summary>
        /// Serializes an object to the specified stream.
        /// </summary>
        /// <param name="stream">The stream to write the object to.</param>
        /// <param name="obj">The object to be written to the stream.</param>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        public void Serialize(Stream stream, object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            ObjectGraph info = new ObjectGraph(this, obj);
            info.WriteToStream(stream);
        }
        #endregion
    }
}
