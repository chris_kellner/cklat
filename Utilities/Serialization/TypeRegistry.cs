﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Patterns;
    using System;
    using System.Text;
    #endregion

    /// <summary>
    /// This class serves to maintain the set of type serializers which have been generated.
    /// </summary>
    public class TypeRegistry : ITypeRegistry, IPostConstructionInitializable
    {
        #region Fields
        private SynchronizedDictionary<Type, ITypeDefinition> m_TypeCache = new SynchronizedDictionary<Type, ITypeDefinition>();
        private bool m_SystemTypesConfigured = false;
        #endregion

        #region Constructors
        static TypeRegistry()
        {
            Singleton<ITypeRegistry>.ValueFactory = () => new TypeRegistry(); 
        }

        private TypeRegistry()
        {
        }
        #endregion

        #region Properties
        public static ITypeRegistry Instance
        {
            get
            {
                return Singleton<ITypeRegistry>.Instance;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initializes the base set of types to be serialized.
        /// </summary>
        protected void AddSystemTypes()
        {
            if (m_SystemTypesConfigured)
            {
                return;
            }

            m_SystemTypesConfigured = true;
            Type[] systemTypes = {
                                     typeof(bool), typeof(byte), typeof(sbyte), typeof(char),
                                     typeof(short), typeof(ushort), typeof(int), typeof(uint),
                                     typeof(long), typeof(ulong), typeof(float), typeof(double),
                                     typeof(string), typeof(TimeSpan), typeof(DateTime)
                                 };

            foreach (var type in systemTypes)
            {
                Register(type, null);
            }

            Register(typeof(Uri), t =>
                TypeDefinition.Create(t,
                (o, c) => c.Converter.GetBytes(((Uri)o).AbsoluteUri, Encoding.UTF8),
                (b, c) => new Uri(c.Converter.ToString(b, 0)))
                );

            Register(typeof(Guid), t =>
                TypeDefinition.Create(t,
                (o, c) => ((Guid)o).ToByteArray(),
                (b, c) => new Guid(b))
                );

            Register(typeof(byte[]), t =>
                TypeDefinition.Create(t,
                (o, c) => (byte[])o,
                (o, c) => o)
                );

            Register(typeof(char[]), t =>
                TypeDefinition.Create(t,
                (o, c) => c.Converter.GetBytes(new string((char[])o), Encoding.UTF8),
                (o, c) => c.Converter.ToString(o, 0).ToCharArray())
                );
        }

        /// <summary>
        /// Gets a value indicating whether the specified Type has already been registered.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsRegistered(Type type)
        {
            return m_TypeCache.ContainsKey(type);
        }

        public ITypeDefinition Register(Type type, Func<Type, ITypeDefinition> factory)
        {
            ITypeDefinition def = null;
            if (factory == null)
            {
                def = m_TypeCache.GetOrAdd(type, TypeDefinition.Create);
            }
            else
            {
                def = m_TypeCache.GetOrAdd(type, factory);
            }

            if (def != null && !def.IsComplete)
            {
                def.Complete(type);
            }

            return def;
        }

        public ITypeDefinition GetTypeDefinition(Type type, bool allowRegistration)
        {
            if (allowRegistration)
            {
                return Register(type, null);
            }
            else
            {
                ITypeDefinition def;
                m_TypeCache.TryGetValue(type, out def);
                return def;
            }
        }
        #endregion

        void IPostConstructionInitializable.Initialize()
        {
            AddSystemTypes();
        }

        public bool IsInitialized
        {
            get { return m_SystemTypesConfigured; }
        }
    }
}
