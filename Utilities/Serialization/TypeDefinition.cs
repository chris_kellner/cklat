﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012-2013, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Serialization
{
    #region Using List
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.DataStructures.Linq;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    #endregion

    /// <summary>
    /// This class defines the (de)serializer for a specific Type.
    /// </summary>
    public class TypeDefinition : ITypeDefinition
    {
        #region Inner Classes
        /// <summary>
        /// TypeDefinition used for handling enumerations
        /// </summary>
        private class EnumDefinition : TypeDefinition
        {
            public override void Complete(Type type)
            {
                IsPrimitive = true;
                Reader = (o, c) => c.Converter.ToEnum(o, 0, type);
                Writer = (o, c) => c.Converter.GetBytes((Enum)o);
                base.Complete(type);
            }
        }

        /// <summary>
        /// TypeDefinition implementation used for handling Enumerable types.
        /// </summary>
        public class CollectionDefinition : TypeDefinition
        {
            private Func<object, int> m_CountAccessor;
            public override void Complete(Type type)
            {
                m_CountAccessor = type.CreateCountAccessor();
                if (m_SerializationConstructor == null)
                {
                    m_SerializationConstructor = CreateSerializationConstructor(type);
                }

                IsCollection = true;
                base.Complete(type);
            }

            protected virtual Func<ObjectGraph, object> CreateSerializationConstructor(Type type)
            {
                if (type.IsArray)
                {
                    return info =>
                    {
                        var length = info.ReadCollectionCount();
                        var array = Array.CreateInstance(Type.GetElementType(), length);
                        for (int i = 0; i < length; i++)
                        {
                            array.SetValue(info.ReadCollectionItem(i), i);
                        }

                        return array;
                    };
                }
                else
                {
                    var enumerableConstructor = type.CreateEnumerableConstructor();
                    if (enumerableConstructor != null)
                    {
                        return info =>
                        {
                            var length = info.ReadCollectionCount();
                            object[] items = new object[length];
                            for (int i = 0; i < length; i++)
                            {
                                items[i] = info.ReadCollectionItem(i);
                            }

                            return enumerableConstructor(items);
                        };
                    }

                    var adder = type.CreateAdder();
                    bool isRaw;
                    var constructor = type.CreateDefaultConstructor(out isRaw);
                    if (adder != null && constructor != null && !isRaw)
                    {
                        return info =>
                        {
                            var target = constructor();
                            foreach (var item in info.ReadCollection())
                            {
                                adder(target, item);
                            }

                            return target;
                        };
                    }
                }

                throw new InvalidOperationException("Unable to create serialization constructor for type: " + type);
            }

            protected override bool PropertyFilter(MemberInfo info)
            {
                return false;
            }

            public override int GetCollectionCount(object instance)
            {
                return m_CountAccessor(instance);
            }

            public override void WriteObjectData(object value, ObjectGraph info)
            {
                info.WriteCollectionCount(GetCollectionCount(value));
                var values = value as IEnumerable;
                int idx = 0;
                foreach (var item in values)
                {
                    info.WriteCollectionItem(idx++, item);
                }
            }

            public override object ReadObjectData(ObjectGraph info)
            {
                return m_SerializationConstructor(info);
            }
        }
        #endregion

        #region Fields
        private List<IFieldAccessor> m_FieldAccessors;
        private PrimitiveWriter m_PrimitiveWriter;
        private PrimitiveReader m_PrimitiveReader;
        private Func<object> m_Constructor;
        private bool m_IsComplete;
        private Func<ObjectGraph, object> m_SerializationConstructor;
        #endregion

        #region Constructors
        protected TypeDefinition()
        {
            m_FieldAccessors = new List<IFieldAccessor>();
        }
        #endregion

        #region Properties
        public string QualifiedName { get; private set; }
        public Type Type { get; private set; }
        public int FieldCount { get { return m_FieldAccessors.Count; } }
        public bool IsPrimitive { get; private set; }
        public bool IsCollection { get; private set; }
        public IEnumerable<string> FieldNames { get { return m_FieldAccessors.Select(f => f.FieldName); } }
        public bool IsComplete { get { return m_IsComplete; } }
        public PrimitiveReader Reader { get { return m_PrimitiveReader; } protected set { m_PrimitiveReader = value; } }
        public PrimitiveWriter Writer { get { return m_PrimitiveWriter; } protected set { m_PrimitiveWriter = value; } }
        #endregion

        #region Methods
        public static ITypeDefinition Create(Type type)
        {
            return Create(type, type.CreateSerializationConstructor());
        }

        public static ITypeDefinition Create(Type type, Func<ObjectGraph, object> serializationConstructor)
        {
            if (type.IsInterface || type.IsAbstract)
            {
                return null;
            }

            TypeDefinition def = null;
            if (type.IsEnum)
            {
                def = new EnumDefinition();
            }
            else if (typeof(string) != type && typeof(IEnumerable).IsAssignableFrom(type))
            {
                def = new CollectionDefinition();
            }
            else
            {
                def = new TypeDefinition();
            }

            def.m_SerializationConstructor = serializationConstructor;
            return def;
        }

        public static ITypeDefinition Create(Type type,
            PrimitiveWriter primitiveWriter,
            PrimitiveReader primitiveReader)
        {
            if (primitiveReader != null && primitiveWriter != null)
            {
                TypeDefinition def = new TypeDefinition();
                def.IsPrimitive = true;
                def.m_PrimitiveReader = primitiveReader;
                def.m_PrimitiveWriter = primitiveWriter;
                return def;
            }

            throw new NotSupportedException("Specified primitive does not define reader/writer");
        }

        /// <summary>
        /// Finalizes the TypeDefinition for the specified type.
        /// </summary>
        /// <param name="type">The type being defined.</param>
        public virtual void Complete(Type type)
        {
            Type = type;
            if (type.IsGenericType && type.Assembly != typeof(int).Assembly)
            {
                QualifiedName = type.AssemblyQualifiedName;
            }
            else
            {
                QualifiedName = type.FullName;
            }

            IsPrimitive |= type.IsPrimitive();
            if (IsPrimitive)
            {
                if (m_PrimitiveReader == null || m_PrimitiveWriter == null)
                {
                    m_PrimitiveWriter = ObjectGraph.GetPrimitiveWriter(type);
                    m_PrimitiveReader = ObjectGraph.GetPrimitiveReader(type);
                }
            }
            else
            {
                bool raw;
                m_Constructor = type.CreateDefaultConstructor(out raw);
                GetProperties(type, (raw && m_SerializationConstructor == null) || type.IsSerializable)
                    .Select(p => CreateAccessor(p))
                    .Foreach(f => m_FieldAccessors.Add(f));
            }

            m_IsComplete = true;
        }

        protected virtual IEnumerable<MemberInfo> GetProperties(Type type, bool useFields)
        {
            if (type.GetCustomAttributes(typeof(DataContractAttribute), false).Length > 0)
            {
                return type.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy)
                    .Where(p => p.GetCustomAttributes(typeof(DataMemberAttribute), false).Length > 0)
                    .Where(p => PropertyFilter(p))
                    .OrderBy(p => p.MetadataToken);
            }
            else
            {
                if (useFields)
                {
                    var members = System.Runtime.Serialization.FormatterServices.GetSerializableMembers(type);
                    return members
                        .Where(p => PropertyFilter(p))
                        .OrderBy(p => p.MetadataToken)
                        .Cast<MemberInfo>();
                }
                else
                {
                    return type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy)
                        .Where(p => PropertyFilter(p))
                        .OrderBy(p => p.MetadataToken)
                        .Cast<MemberInfo>();
                }
            }
        }

        protected virtual bool PropertyFilter(MemberInfo info)
        {
            if (FieldAccessorFactory.IsMemberValid(info, false))
            {
                var xmlIgnore = info.GetCustomAttributes(typeof(System.Xml.Serialization.XmlIgnoreAttribute), false);
                if (xmlIgnore.Length > 0)
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        protected virtual IFieldAccessor CreateAccessor(MemberInfo info)
        {
            return FieldAccessorFactory.CreateAccessor(info);
        }

        /// <summary>
        /// Create a new instance of the non-primitive Type.
        /// </summary>
        /// <returns>A new instance of the non-primitive type, or null.</returns>
        public virtual object NewInstance(ObjectGraph info)
        {
            if (m_SerializationConstructor != null)
            {
                return m_SerializationConstructor(info);
            }
            else if (m_Constructor != null)
            {
                return m_Constructor();
            }

            return null;
        }

        /// <summary>
        /// Gets the number of items present in the collection when overridden in a derived class.
        /// </summary>
        /// <returns>Gets the number of items to be serialized.</returns>
        public virtual int GetCollectionCount(object instance)
        {
            throw new InvalidOperationException("Specified type is not a collection");
        }

        public IFieldAccessor this[int index]
        {
            get
            {
                return m_FieldAccessors[index];
            }
        }

        public IFieldAccessor this[string name]
        {
            get
            {
                return m_FieldAccessors.FirstOrDefault(f => f.FieldName == name);
            }
        }
        #endregion

        #region ICustomTypeSerializer
        public virtual void WriteObjectData(object value, ObjectGraph info)
        {
            var serial = value as IGraphWriter;
            if (serial != null)
            {
                serial.WriteObjectData(info);
            }
            else if (IsPrimitive)
            {
                info.SetRaw(m_PrimitiveWriter(value, info.Serializer));
            }
            else
            {
                foreach (var field in m_FieldAccessors)
                {
                    var fVal = field.GetValue(value);
                    if (field.ShouldSerializeValue(fVal))
                    {
                        var def = TypeRegistry.Instance.GetTypeDefinition(fVal.GetType(), true);
                        if (def.IsPrimitive)
                        {
                            info.WriteRaw(field.FieldName, field.FieldType, def.Writer(fVal, info.Serializer));
                        }
                        else
                        {
                            info.WriteObject(field.FieldName, fVal, field.RequestedFormat);
                        }
                    }
                    else
                    {
                        info.WriteNull(field.FieldName);
                    }
                }
            }
        }

        public virtual object ReadObjectData(ObjectGraph info)
        {
            if (IsPrimitive)
            {
                return m_PrimitiveReader(info.Value, info.Serializer);
            }
            else
            {
                var target = NewInstance(info);
                foreach (var field in m_FieldAccessors)
                {
                    var def = info.GetFieldDefinition(field.FieldName);
                    if (def == null) { continue; }
                    object fVal = null;
                    if (def.IsPrimitive)
                    {
                        byte[] data = info.ReadRaw(field.FieldName);
                        if (data == null) { continue; }
                        fVal = def.Reader(data, info.Serializer);
                    }
                    else
                    {
                        fVal = info.ReadObject(field.FieldName);
                    }

                    if (!field.IsReadOnly && field.ShouldSerializeValue(fVal))
                    {
                        field.SetValue(target, fVal);
                    }
                }

                return target;
            }
        }
        #endregion
    }
}
