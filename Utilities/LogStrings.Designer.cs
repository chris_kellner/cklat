﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CKLib.Utilities {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class LogStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LogStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CKLib.Utilities.LogStrings", typeof(LogStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to locate plugin file: {0}.
        /// </summary>
        internal static string ERR_EXTENSION_MGR_CANNOT_LOCATE_FILE {
            get {
                return ResourceManager.GetString("ERR_EXTENSION_MGR_CANNOT_LOCATE_FILE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Caught Exception while attempting to activate extension: {0}.
        /// </summary>
        internal static string ERR_EXTENSION_MGR_FAIL_TO_ACTIVATE {
            get {
                return ResourceManager.GetString("ERR_EXTENSION_MGR_FAIL_TO_ACTIVATE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Caught Exception while attempting to load extensions..
        /// </summary>
        internal static string ERR_EXTENSION_MGR_FAIL_TO_LOAD {
            get {
                return ResourceManager.GetString("ERR_EXTENSION_MGR_FAIL_TO_LOAD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client: {0} has connected..
        /// </summary>
        internal static string STD_SERVER_CLIENT_CONNECTED {
            get {
                return ResourceManager.GetString("STD_SERVER_CLIENT_CONNECTED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client: {0} has disconnected..
        /// </summary>
        internal static string STD_SERVER_CLIENT_DISCONNECTED {
            get {
                return ResourceManager.GetString("STD_SERVER_CLIENT_DISCONNECTED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server: {0} has been started..
        /// </summary>
        internal static string STD_SERVER_STARTED {
            get {
                return ResourceManager.GetString("STD_SERVER_STARTED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server: {0} has been stopped..
        /// </summary>
        internal static string STD_SERVER_STOPPED {
            get {
                return ResourceManager.GetString("STD_SERVER_STOPPED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Creating proxy with passed-parameter type {0}, but expected type was {1}. Caution is urged over use of the method: {2} of the proxy type {3}..
        /// </summary>
        internal static string WARN_PARAM_CAST_CLASS {
            get {
                return ResourceManager.GetString("WARN_PARAM_CAST_CLASS", resourceCulture);
            }
        }
    }
}
