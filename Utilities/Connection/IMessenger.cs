#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection
{
    #region Using List
    using System;
    using System.ServiceModel;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Proxy.ObjectSharing;
    #endregion

    /// <summary>
    /// Interface which generalizes the communication object of a client of server.
    /// </summary>
    public interface IMessenger
    {
        /// <summary>
        /// Gets the local connection status.
        /// </summary>
        CommunicationState ConnectionStatus { get; }

        /// <summary>
        /// The unique identifier for the messenger.
        /// </summary>
        Guid MessengerId { get; }

        /// <summary>
        /// Gets a reference to the shared object pool for the messenger.
        /// </summary>
        ISharedObjectPool SharedObjectPool { get; }

        /// <summary>
        /// Gets a value indicating whether the messenger is a server.
        /// </summary>
        bool IsServer { get; }

        /// <summary>
        /// Gets a value indicating whether the input remote connection id is local or not.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>True if the id belongs to a localhost connection, false otherwise.</returns>
        bool IsConnectionLocal(Guid connectionId);

        /// <summary>
        /// Gets an array of identifiers corresponding to the active connections.
        /// </summary>
        /// <returns>An array of identifiers corresponding to the active connections.</returns>
        Guid[] GetConnectionIdentifiers();

        /// <summary>
        /// Gets information about the specified connection.
        /// </summary>
        /// <param name="connectionId">The id of the connection to retrieve information about.</param>
        /// <returns>An object containing information about the specified connection.</returns>
        IConnectionInformation GetConnectionInformation(Guid connectionId);

        /// <summary>
        /// Sends a message on the local message queue.
        /// </summary>
        /// <param name="message"></param>
        void SendLocalMessage(IDataMessage message);

        /// <summary>
        /// Sends a network message to all connections.
        /// </summary>
        /// <param name="message"></param>
        void SendNetworkMessage(IDataMessage message);

        /// <summary>
        /// Sends a network message to the input client or server id.
        /// </summary>
        /// <remarks>
        /// If this method is called by the client it will send it to the server with that id,
        /// if it is called by the server the message will be routed to the client with that id.</remarks>
        /// <param name="clientId">The client id to tag the message with.</param>
        /// <param name="message">The message to send.</param>
        void SendNetworkMessage(Guid clientId, IDataMessage message);
        void AddCallback(Type messageType, DataMessageCallback callback);
        void AddCallback(Type messageType, DataMessageCallback callback, bool synchronous);
        void RemoveCallback(Type messageType, DataMessageCallback callback);
    }
}
