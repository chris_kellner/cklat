#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Client
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using CKLib.Utilities.Connection.Extensions;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Patterns;
    using CKLib.Utilities.Proxy.ObjectSharing;
    #endregion
    /// <summary>
    /// This class represents the main connection point for a client application.
    /// </summary>
    public class Client : IMessenger, IDisposable
    {
        #region Variables
        /// <summary>
        /// The identifier for this client.
        /// </summary>
        private Guid m_ClientIdentifier;

        /// <summary>
        /// The main message pump for the client.
        /// </summary>
        private ProcessQueue m_ProcessQueue;

        /// <summary>
        /// Queue used to process reconnection requests.
        /// </summary>
        private SynchronizedQueue<ClientConnection> m_ReconnectionQueue;

        /// <summary>
        /// Triggers when the localhost connection is faulted.
        /// </summary>
        public event EventHandler ConnectionFaulted;

        /// <summary>
        /// Triggers when a message is received from any connection.
        /// </summary>
        public event EventHandler MessageReceived;

        /// <summary>
        /// Occurs when a connection has been opened.
        /// </summary>
        public event EventHandler<ClientConnectionEventArgs> ConnectionOpened;

        /// <summary>
        /// Holds the reference to the shared object pool for the client side.
        /// </summary>
        private SharedObjectPool m_SharedObjectPool;

        /// <summary>
        /// The ExtensionManager for the client.
        /// </summary>
        private ExtensionManager m_ExtensionManager;

        /// <summary>
        /// Maintains a list of active connections.
        /// </summary>
        private SynchronizedDictionary<Guid, ClientConnection> m_Connections;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Client class.
        /// </summary>
        private Client()
        {
            m_ClientIdentifier = Guid.NewGuid();
            m_Connections = new SynchronizedDictionary<Guid, ClientConnection>();
            m_ProcessQueue = new ProcessQueue();
            m_SharedObjectPool = new SharedObjectPool();
            m_SharedObjectPool.Initialize(this);
            m_ReconnectionQueue = new SynchronizedQueue<ClientConnection>();
            m_ProcessQueue.AddCallback(typeof(ProcessReconnectionsMessage), ProcessReconnections);
            m_ExtensionManager = new ExtensionManager();
            StartMessagePump();

            if (!Singleton<Client>.IsValueAssigned)
            {
                Singleton<Client>.ValueFactory = () => this;
            }
        }

        /// <summary>
        /// Initializes static members of the Client class.
        /// </summary>
        static Client()
        {
            Singleton<Client>.ValueFactory = () => new Client();
        }
        #endregion
        /// <summary>
        /// Gets the unique instance of the Client class.
        /// </summary>
        public static Client Instance
        {
            get
            {
                return Singleton<Client>.Instance;
            }
        }

        /// <summary>
        /// Gets or sets the way in which the client should handle a connection fault.
        /// </summary>
        public ClientReconnectionMode ReconnectionMode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the SharedObjectPool for the client.
        /// </summary>
        public ISharedObjectPool SharedObjectPool
        {
            get
            {
                return m_SharedObjectPool;
            }
        }

        /// <summary>
        /// Gets a reference to the ExtensionManager for the client.
        /// </summary>
        public ExtensionManager ExtensionManager
        {
            get
            {
                return m_ExtensionManager;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the connection is still active.
        /// </summary>
        public bool ConnectionOk
        {
            get
            {
                return ConnectionStatus == CommunicationState.Opened;
            }
        }

        /// <summary>
        /// Starts processing messages on the client side.
        /// </summary>
        private void StartMessagePump()
        {
            m_ProcessQueue.StartMessagePump();
        }

        /// <summary>
        /// Stops processing messages on the client side.
        /// </summary>
        private void StopMessagePump()
        {
            m_ProcessQueue.StopMessagePump();
        }

        /// <summary>
        /// Creates a new client pointing at the input address and asynchronously begins the connection request.
        /// </summary>
        /// <returns>The new client object which is attempting a connection to the server.</returns>
        public void ConnectToLocalhost()
        {
            string serverName = null;
            int port = 6969;
            var group = ConfigurationManager.Current[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP];
            if (group != null)
            {
                string setting;
                if (group.TryReadValue<string>(Setting.SERVER_NAME_SETTING, out setting))
                {
                    serverName = setting;
                }

                int portSetting;
                if (group.TryReadValue<int>(Setting.SERVER_TCP_PORT, out portSetting))
                {
                    port = portSetting;
                }
            }

            ClientConnection client = new ClientConnection("LocalHost", serverName, port, false);
            ThreadPool.QueueUserWorkItem((o) =>
            {
                var theClient = (ClientConnection)o;
                try
                {
                    if (theClient.Connect(null, null))
                    {
                        m_Connections.Add(theClient.ServerIdentifier, theClient);
                        OnConnectionSucceeded(theClient, EventArgs.Empty);
                    }
                }
                catch
                {
                    m_Connections.Remove(theClient.ServerIdentifier);
                }
            }, client);
            //return client;
        }

        /// <summary>
        /// Called when the connection is successfully established.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnConnectionSucceeded(object sender, EventArgs e)
        {
            ClientConnection client = sender as ClientConnection;
            if (client != null)
            {
                if (m_ExtensionManager != null)
                {
                    m_ExtensionManager.NotifyConnectionOfAvailableExtensions(client.ServerIdentifier);
                }

                SendLocalMessage(new ConnectionOpenedMessage(client.ServerIdentifier));
                if (ConnectionOpened != null)
                {
                    ConnectionOpened(this, new ClientConnectionEventArgs(client));
                }

                var pool = m_SharedObjectPool as SharedObjectPool;
                if (pool != null)
                {
                    pool.SendPendingNotifications();
                }
            }
        }

        /// <summary>
        /// Creates a new client pointing at the input address and asynchronously begins the connection request.
        /// </summary>
        /// <param name="networkAddress">The ip address or friendly name of the target server.</param>
        /// <param name="serverName">The name of the server to connect to.</param>
        /// <param name="forceTcp">A flag which, when true will force the connection to use TCP even if the connection is to the localhost.</param>
        /// <param name="userName">[NOT USED] The UserName to connect with.</param>
        /// <param name="password">[NOT USED] The password to connect with.</param>
        /// <returns>The new client object which is attempting a connection to the server.</returns>
        public ClientConnection ConnectToServer(string networkAddress, string serverName, bool forceTcp, string userName, string password)
        {
            return ConnectToServer(networkAddress, serverName, 6969, false, userName, password);
        }

        /// <summary>
        /// Creates a new client pointing at the input address and asynchronously begins the connection request.
        /// </summary>
        /// <param name="networkAddress">The ip address or friendly name of the target server.</param>
        /// <param name="serverName">The name of the server to connect to.</param>
        /// <param name="port">The port on the remote tcp server to use.</param>
        /// <param name="forceTcp">A flag which, when true will force the connection to use TCP even if the connection is to the localhost.</param>
        /// <param name="userName">[NOT USED] The UserName to connect with.</param>
        /// <param name="password">[NOT USED] The password to connect with.</param>
        /// <returns>The new client object which is attempting a connection to the server.</returns>
        public ClientConnection ConnectToServer(string networkAddress, string serverName, int port, bool forceTcp, string userName, string password)
        {
            ClientConnection client = new ClientConnection(networkAddress, serverName, port, forceTcp);
            ThreadPool.QueueUserWorkItem((o) =>
            {
                var theClient = (ClientConnection)o;
                try
                {
                    if (theClient.Connect(userName, password))
                    {
                        m_Connections[theClient.ServerIdentifier] = theClient;
                        OnConnectionSucceeded(theClient, EventArgs.Empty);
                    }
                }
                catch
                {
                    m_Connections.Remove(theClient.ServerIdentifier);
                }
            }, client);

            return client;
        }

        /// <summary>
        /// Called when a client connection has been faulted.
        /// </summary>
        /// <param name="connection">The connection which has been faulted.</param>
        /// <param name="e">The Event Args.</param>
        internal void OnConnectionFaulted(ClientConnection connection, EventArgs e)
        {
            if (m_ExtensionManager != null)
            {
                m_ExtensionManager.NotifyConnectionOfAvailableExtensions(connection.ServerIdentifier, false);
                m_ExtensionManager.RevokeConnectionFromAllExtensions(connection.ServerIdentifier);
            }

            SendLocalMessage(new ConnectionClosedMessage(connection.ServerIdentifier));
            if (ConnectionFaulted != null)
            {
                ConnectionFaulted(this, e);
            }

            if (ReconnectionMode > ClientReconnectionMode.DoNotReconnect)
            {
                m_ReconnectionQueue.Enqueue(connection);
                SendLocalMessage(new ProcessReconnectionsMessage());
            }
            else
            {
                connection.Dispose();
                m_Connections.Remove(connection.ServerIdentifier);
            }
        }

        /// <summary>
        /// Processes any reconnection attempts on the asynchronous message queue.
        /// </summary>
        /// <param name="pair">The reconnection message to process.</param>
        private void ProcessReconnections(MessageEnvelope pair)
        {
            bool reQ = false;
            int count = m_ReconnectionQueue.Count;
            while (count-- > 0)
            {
                var connection = m_ReconnectionQueue.Dequeue();
                if (connection != null)
                {
                    if (connection.ConnectionOk)
                    {
                        m_ReconnectionQueue.Enqueue(connection);
                        reQ = true;
                    }
                    else
                    {
                        connection.Reconnect();
                    }
                }
            }
            if (reQ)
            {
                SendLocalMessage(new ProcessReconnectionsMessage());
            }
        }

        /// <summary>
        /// Gets the localhost connection of this client.
        /// </summary>
        private ClientConnection LocalConnection
        {
            get
            {
                foreach (var connection in m_Connections)
                {
                    if (connection.Value.IsLocalHostClient)
                    {
                        return connection.Value;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the client connection with the input server id.
        /// </summary>
        /// <param name="serverId">The ClientConnection that is connected to the server with the input id.</param>
        /// <returns>A ClientConnection or null.</returns>
        public ClientConnection GetConnection(Guid serverId)
        {
            ClientConnection conn;
            m_Connections.TryGetValue(serverId, out conn);
            return conn;
        }

        /// <summary>
        /// Gets the client connection whose full address matches the specified address.
        /// </summary>
        /// <param name="fullServerAddress"></param>
        /// <returns>The client connection whose full address matches the specified address, or null.</returns>
        public ClientConnection GetConnectionByAddress(string fullServerAddress)
        {
            return m_Connections.Values.FirstOrDefault(c => c.ServerAddress.Equals(fullServerAddress));
        }

        /// <summary>
        /// Gets the all connections which route to a server with the input name.
        /// </summary>
        /// <param name="serverName">The server name to search for.</param>
        /// <returns>A set of ClientConnections.</returns>
        public IEnumerable<ClientConnection> GetConnections(string serverName)
        {
            foreach (var conn in m_Connections)
            {
                if (conn.Value.ServerName == serverName)
                {
                    yield return conn.Value;
                }
            }
        }

        #region IMessenger Members
        /// <summary>
        /// Gets the current communication state with the server.
        /// </summary>
        public CommunicationState ConnectionStatus
        {
            get
            {
                foreach (var connection in m_Connections)
                {
                    if (connection.Value.Communicator != null)
                    {
                        if (connection.Value.Communicator.State == CommunicationState.Opened)
                        {
                            return connection.Value.Communicator.State;
                        }
                    }
                }

                return CommunicationState.Closed;
            }
        }

        /// <summary>
        /// Gets an array of identifiers corresponding to the active connections.
        /// </summary>
        /// <returns>An array of identifiers corresponding to the active connections.</returns>
        public Guid[] GetConnectionIdentifiers()
        {
            return m_Connections.Keys.ToArray();
        }

        /// <summary>
        /// Sends a network message using the specified client id.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="message"></param>
        public void SendNetworkMessage(Guid clientId, IDataMessage message)
        {
            message.SenderId = MessengerId;
            var conn = GetConnection(clientId);
            if (conn != null && conn.ConnectionOk)
            {
                conn.SendMessage(message);
            }
        }

        /// <summary>
        ///  Sends a message to all server connections.
        /// </summary>
        /// <param name="message"></param>
        public void SendNetworkMessage(IDataMessage message)
        {
            foreach (var client in m_Connections)
            {
                SendNetworkMessage(client.Key, message);
            }
        }

        /// <summary>
        /// Puts a message on the local message queue.
        /// </summary>
        /// <param name="message"></param>
        public void SendLocalMessage(IDataMessage message)
        {
            m_ProcessQueue.SendMessage(new MessageEnvelope(MessengerId, message));
        }

        /// <summary>
        /// Adds an asynchronous callback for the input message type.
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="callback"></param>
        public void AddCallback(Type messageType, DataMessageCallback callback)
        {
            m_ProcessQueue.AddCallback(messageType, callback);
        }

        /// <summary>
        /// Adds an asynchronous callback for the input message type.
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="callback"></param>
        /// <param name="synchronous"></param>
        public void AddCallback(Type messageType, DataMessageCallback callback, bool synchronous)
        {
            m_ProcessQueue.AddCallback(messageType, callback, synchronous);
        }

        /// <summary>
        /// Removes an asynchronous callback for the input message type.
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="callback"></param>
        public void RemoveCallback(Type messageType, DataMessageCallback callback)
        {
            m_ProcessQueue.RemoveCallback(messageType, callback);
        }

        /// <summary>
        /// Gets a value indicating whether the input remote connection id is local or not.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>True if the id belongs to a localhost connection, false otherwise.</returns>
        public bool IsConnectionLocal(Guid connectionId)
        {
            var client = GetConnection(connectionId);
            if (client != null)
            {
                return client.IsLocalHostClient;
            }

            return false;
        }

        /// <summary>
        /// Gets information about the specified connection.
        /// </summary>
        /// <param name="connectionId">The id of the connection to retrieve information about.</param>
        /// <returns>An object containing information about the specified connection.</returns>
        public IConnectionInformation GetConnectionInformation(Guid connectionId)
        {
            ClientConnection connection;
            if (m_Connections.TryGetValue(connectionId, out connection))
            {
                return connection.ConnectionInformation;
            }

            return null;
        }

        /// <summary>
        /// Gets the unique identifier for this client.
        /// </summary>
        public Guid MessengerId
        {
            get
            {
                return m_ClientIdentifier;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this messenger is a server.
        /// </summary>
        public bool IsServer
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region IClientCallback Members
        /// <summary>
        /// Accepts messages from the clients and places them into the message queue for the client.
        /// </summary>
        /// <param name="receiver">The connection which has received the message.</param>
        /// <param name="message">The message that was received.</param>
        internal void ReceiveMessage(ClientConnection receiver, IDataMessage message)
        {
            m_ProcessQueue.SendMessage(new MessageEnvelope(message.SenderId, message));
            if (MessageReceived != null)
            {
                MessageReceived(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IDisposable Members
        /// <summary>
        /// Stops processing messages and closes all client connections.
        /// </summary>
        public void Dispose()
        {

            m_ProcessQueue.Dispose();
            ClientConnection[] closeArray = new ClientConnection[m_Connections.Count];
            m_Connections.Values.CopyTo(closeArray, 0);
            foreach (var conn in closeArray)
            {
                conn.Dispose();
            }
        }

        #endregion
    }
}
