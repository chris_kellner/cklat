#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Client
{
    #region Using List
    using System;
    using System.ServiceModel;
    using CKLib.Utilities.Connection.Server;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Security;
    #endregion

    /// <summary>
    /// This class represents a client's connection to a server.
    /// </summary>
    public class ClientConnection : IClientCallback, IDisposable
    {
        #region Variables
        /// <summary>
        /// Occurs when the connection to the server is faulted.
        /// </summary>
        public event EventHandler ConnectionFaulted;

        /// <summary>
        /// Used to create a connection to the server.
        /// </summary>
        DuplexChannelFactory<IServer> m_ChannelFactory;

        /// <summary>
        /// Reference to the server to communicate with.
        /// </summary>
        private IServer m_Server;

        /// <summary>
        /// The address of the server.
        /// </summary>
        private readonly string m_ServerAddress;

        /// <summary>
        /// A value indicating whether this connection is to the localhost.
        /// </summary>
        private readonly bool m_IsLocalhost;

        /// <summary>
        /// A value indicating the name of the server this connection is communicating with.
        /// </summary>
        private readonly string m_ServerName;

        /// <summary>
        /// The identifier of the server.
        /// </summary>
        private Guid m_ServerId;

        /// <summary>
        /// Indicates whether the server has terminated the connection with the cient.
        /// </summary>
        private bool m_ServerShutdown;

        /// <summary>
        /// Stores information about this connection.
        /// </summary>
        private ConnectionInformation m_ConnectionInformation;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ClientConnection class.
        /// </summary>
        /// <param name="networkAddress">The IP address or friendly name of the server.</param>
        /// <param name="serverName">The name of the server to connect to.</param>
        /// <param name="port">The port to use for a network server.</param>
        /// <param name="forceTcp">True to force a tcp connection even if the target is the localhost.</param>
        internal ClientConnection(string networkAddress, string serverName, int port, bool forceTcp)
        {
            m_ServerName = serverName;
            m_IsLocalhost = ("localhost".Equals(networkAddress, StringComparison.InvariantCultureIgnoreCase) || networkAddress == "127.0.0.1");
            if (m_IsLocalhost && !forceTcp)
            {
                m_ServerAddress = string.Format("net.pipe://{0}/DeveloperServers/{1}", networkAddress, serverName);
            }
            else
            {
                m_ServerAddress = string.Format("net.tcp://{0}:{1}/DeveloperServers/{2}", networkAddress, port, serverName);
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this connection is to the localhost.
        /// </summary>
        public bool IsLocalHostClient
        {
            get
            {
                return m_IsLocalhost;
            }
        }

        /// <summary>
        /// Gets the name of the server that this connection is communicating with.
        /// </summary>
        public string ServerName
        {
            get
            {
                return m_ServerName;
            }
        }

        /// <summary>
        /// Gets the full address of the server that this connection is communicating with.
        /// </summary>
        public string ServerAddress
        {
            get
            {
                return m_ServerAddress;
            }
        }

        /// <summary>
        /// Gets the unique identifier for the server of this connection.
        /// </summary>
        public Guid ServerIdentifier
        {
            get
            {
                return m_ServerId;
            }
        }

        /// <summary>
        /// Gets the server as a communuication object.
        /// </summary>
        public ICommunicationObject Communicator
        {
            get
            {
                return m_Server as ICommunicationObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the connection is still active.
        /// </summary>
        public bool ConnectionOk
        {
            get
            {
                if (Communicator != null)
                {
                    return Communicator.State == CommunicationState.Opened;
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this client can attempt reconnection to the server.
        /// </summary>
        public bool CanReconnect
        {
            get
            {
                return !m_ServerShutdown && !ConnectionOk;
            }
        }

        /// <summary>
        /// Gets information about this connection.
        /// </summary>
        public ConnectionInformation ConnectionInformation
        {
            get
            {
                return m_ConnectionInformation;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets the name of the server given the specified inputs.
        /// </summary>
        /// <param name="ipAddress">The ip address or friendly name of the server.</param>
        /// <param name="serverName">The name of the server running at the specifed ip address.</param>
        /// <param name="port">The tcp port the server is running on.</param>
        /// <param name="forceTcp">A flag indicating whether to use tcp even for local connections.</param>
        /// <returns>The full server name string.</returns>
        public static string GetServerName(string ipAddress, string serverName, int port, bool forceTcp)
        {
            bool local = ("localhost".Equals(ipAddress, StringComparison.InvariantCultureIgnoreCase) || ipAddress == "127.0.0.1");
            if (local && ! forceTcp)
            {
                return string.Format("net.pipe://{0}/DeveloperServers/{1}", ipAddress, serverName);
            }
            else
            {
                return string.Format("net.tcp://{0}:{1}/DeveloperServers/{2}", ipAddress, port, serverName);
            }
        }

        /// <summary>
        /// Attempts to connect to the server address specified during the construction of this client.
        /// </summary>
        /// <param name="userName">User name to use to connect with.</param>
        /// <param name="password">Passwrod to connect with.</param>
        internal bool Connect(string userName, string password)
        {
            if (m_ServerAddress.StartsWith("net.pipe"))
            {
                m_ChannelFactory = new DuplexChannelFactory<IServer>(
                    this, new NetNamedPipeBinding()
                    {
                        CloseTimeout = TimeSpan.FromSeconds(10),
                        ReceiveTimeout = TimeSpan.FromHours(20f),
                        SendTimeout = TimeSpan.FromHours(20f)
                    }, new EndpointAddress(m_ServerAddress));
            }
            else
            {
                var endpoint = new EndpointAddress(m_ServerAddress);
                m_ChannelFactory = new DuplexChannelFactory<IServer>(
                    this, new NetTcpBinding(SecurityMode.None)
                    {
                        CloseTimeout = TimeSpan.FromSeconds(10),
                        ReceiveTimeout = TimeSpan.FromHours(20f),
                        SendTimeout = TimeSpan.FromHours(20f)
                    }, endpoint);
            }

            m_Server = m_ChannelFactory.CreateChannel();

            try
            {
                Communicator.Open();

                m_ServerShutdown = false;
                ConnectionInformation info = ConnectionInformation.GetLocalInfo(Client.Instance.MessengerId);

                m_ServerId = m_Server.RegisterClient(info);
                if (m_ConnectionInformation == null)
                {
                    m_ConnectionInformation = new ConnectionInformation(m_ServerId);
                }

                m_ConnectionInformation.ConnectionAddress = new Uri(m_ServerAddress);
                m_ConnectionInformation.ConnectionStatus = ConnectionStatus.Connected;

                m_Server.ResendExtensionsAndShares(Client.Instance.MessengerId);
                ICommunicationObject commObj = m_Server as ICommunicationObject;
                if (commObj != null)
                {
                    commObj.Faulted += commObj_Faulted;
                    commObj.Closed += commObj_Faulted;
                }

                Client.Instance.AddCallback(typeof(ShutdownMessage), (m) =>
                {
                    if (m.Sender == m_ServerId)
                    {
                        Disconnect();
                    }
                });

                return true;
            }
            catch (Exception e)
            {
                commObj_Faulted(this, new UnhandledExceptionEventArgs(e, false));
            }

            return false;
        }

        /// <summary>
        /// Disconnects from the server.
        /// </summary>
        public void Disconnect()
        {
            m_ServerShutdown = true;
            if (m_ConnectionInformation != null)
            {
                m_ConnectionInformation.ConnectionStatus = ConnectionStatus.NotConnected;
            }

            if (m_Server != null && ConnectionOk)
            {
                try
                {
                    m_Server.UnregisterClient(Client.Instance.MessengerId);
                    ((IDisposable)m_Server).Dispose();
                    ((IDisposable)m_ChannelFactory).Dispose();
                }
                catch (Exception)
                {
                    // Already aborted
                }

                m_ChannelFactory = null;
                m_Server = null;
            }

            Client.Instance.SharedObjectPool.DisposeObjectsForOwner(m_ServerId);
        }

        /// <summary>
        /// Sends a message to the connected server.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public void SendMessage(IDataMessage message)
        {
            if (ConnectionOk)
            {
                if (LocalOnlyAttribute.CheckAccess(message.GetType(), Client.Instance, m_ServerId))
                {
                    message.SenderId = Client.Instance.MessengerId;
                    m_Server.SendMessage(message);
                }
            }
        }

        /// <summary>
        /// Attempts to reconnect to the server.
        /// </summary>
        internal void Reconnect()
        {
            if (CanReconnect)
            {
                Connect(null, null);
            }
        }

        #region IClientCallback Members
        /// <summary>
        /// Method that the server calls when it wants to pass a message to this client.
        /// </summary>
        /// <param name="message"></param>
        void IClientCallback.SendMessage(IDataMessage message)
        {
            message.SenderId = m_ServerId;
            Client.Instance.ReceiveMessage(this, message);
        }

        /// <summary>
        /// Method that the server calls to request that shared objects be re-sent.
        /// </summary>
        void IClientCallback.ResendSharedObjects()
        {
            foreach (var share in Client.Instance.SharedObjectPool.GetObjectsSharedByOwner(Client.Instance.MessengerId))
            {
                SendMessage(new SharedObjectAvailabilityMessage(share, true));
            }

            Client.Instance.ExtensionManager.NotifyConnectionOfAvailableExtensions(m_ServerId);
        }
        #endregion

        /// <summary>
        /// Occurs when the connection is faulted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void commObj_Faulted(object sender, EventArgs e)
        {
            Client.Instance.OnConnectionFaulted(this, e);
            if (ConnectionFaulted != null)
            {
                ConnectionFaulted(this, e);
            }
        }

        #region IDisposable Members
        /// <summary>
        /// Unregisters the client from the server and releases resources.
        /// </summary>
        public void Dispose()
        {
            Disconnect();
        }
        #endregion
        #endregion
    }
}
