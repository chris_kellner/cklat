#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Connection
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;
    using System.Net;
    using System.Runtime.Serialization;
    using System.Diagnostics;
    #endregion
    [DataContract]
    public class ConnectionInformation : INotifyPropertyChanged, IConnectionInformation
    {
        #region Variables
        /// <summary>
        /// The id of this connection.
        /// </summary>
        private Guid m_ConnectionId;

        /// <summary>
        /// The current status for this connection.
        /// </summary>
        private ConnectionStatus m_Status;

        /// <summary>
        /// The process name.
        /// </summary>
        private string m_ProcessName;

        /// <summary>
        /// The process id.
        /// </summary>
        private int m_ProcessId;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ConnectionInformation class.
        /// </summary>
        public ConnectionInformation(Guid connectionId)
        {
            m_ConnectionId = connectionId;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the id of this connection.
        /// </summary>
        [DataMember]
        public Guid ConnectionId
        {
            get
            {
                return m_ConnectionId;
            }

            set
            {
                m_ConnectionId = value;
            }
        }

        /// <summary>
        /// Gets or sets the connection status.
        /// </summary>
        [DataMember]
        public ConnectionStatus ConnectionStatus
        {
            get
            {
                return m_Status;
            }

            set
            {
                m_Status = value;
                OnPropertyChanged("ConnectionStatus");
            }
        }

        /// <summary>
        /// Gets or sets the UserName that owns this connection.
        /// </summary>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Endpoint Address for this connection.
        /// </summary>
        [DataMember]
        public Uri ConnectionAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ProcessId for this connection.
        /// </summary>
        [DataMember]
        public int ProcessId
        {
            get
            {
                return m_ProcessId;
            }

            set
            {
                m_ProcessId = value;
                OnPropertyChanged("ProcessId");
            }
        }

        /// <summary>
        /// Gets or sets the ProcessName for this connection.
        /// </summary>
        [DataMember]
        public string ProcessName
        {
            get
            {
                return m_ProcessName;
            }

            set
            {
                m_ProcessName = value;
                OnPropertyChanged("ProcessName");
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Gets a ConnectionInformation object containing the local data.
        /// </summary>
        /// <param name="connectionId">The local messenger id.</param>
        /// <returns></returns>
        public static ConnectionInformation GetLocalInfo(Guid connectionId)
        {
            var proc = Process.GetCurrentProcess();
            ConnectionInformation info = new ConnectionInformation(connectionId);
            info.ProcessId = proc.Id;
            info.ProcessName = proc.ProcessName;
            info.UserName = Environment.UserName;
            return info;
        }

        /// <summary>
        /// Gets a formatted string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(ProcessName) && !string.IsNullOrEmpty(UserName))
            {
                return string.Format("{0} | {1} [{2}]", ProcessName, ProcessId, UserName);
            }

            return ConnectionId.ToString();
        }

        /// <summary>
        /// Updates this instance with the information from another instance with the same connection id.
        /// </summary>
        /// <param name="info">The IConnectionInformation instance to update with.</param>
        public void Update(IConnectionInformation info)
        {
            if (ConnectionId == info.ConnectionId)
            {
                ConnectionStatus = info.ConnectionStatus;
                ProcessName = info.ProcessName;
                ProcessId = info.ProcessId;
            }
        }

        /// <summary>
        /// Called when a property changes
        /// </summary>
        /// <param name="propertyChanged">The property that changed.</param>
        private void OnPropertyChanged(string propertyChanged)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyChanged));
            }
        }
        #endregion
        #region INotifyPropertyChanged Members
        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
