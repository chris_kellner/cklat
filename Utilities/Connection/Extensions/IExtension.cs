#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Extensions
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using CKLib.Utilities.Proxy;
    #endregion
    public interface IExtension : IDisposable
    {
        /// <summary>
        /// Gets the name of the extension.
        /// </summary>
        string ExtensionName { get; }

        /// <summary>
        /// Initializes the extension.
        /// If this method returns false on the client side then it is considered to be invalid and it will not
        /// be available for use on the server side.
        /// </summary>
        /// <param name="messenger">The client or server used for messaging the other side.</param>
        /// <param name="proxyFactory">A proxy factory which can be used to generate type proxies for communication.</param>
        /// <returns>True if the extension initialized successfully and is available for use, false otherwise.</returns>
        bool Initialize(IMessenger messenger, IProxyFactory proxyFactory);

        /// <summary>
        /// Gets any custom message types which will be marshalled accross the communication channel.
        /// </summary>
        /// <returns>An array of message types which should be added to the set of known types.</returns>
        Type[] GetCustomMessageTypes();

        /// <summary>
        /// Allows the specified connection to communicate with this extension.
        /// </summary>
        /// <param name="connectionId">The connection to permit communication with.</param>
        /// <returns></returns>
        bool EnableConnectionAvailability(Guid connectionId);

        /// <summary>
        /// Revokes the specified connection's communication rights to this extension.
        /// </summary>
        /// <param name="connectionId">The connection to revoke communication with.</param>
        void RevokeConnectionAvailability(Guid connectionId);

        /// <summary>
        /// Gets all of the available connections.
        /// </summary>
        /// <returns>An enumeration of available connections.</returns>
        IEnumerable<Guid> GetAvailableConnections();

        /// <summary>
        /// Gets a value indicating whether the specified connection is available.
        /// </summary>
        /// <param name="connectionId">The connection to check the availability of.</param>
        /// <returns>True if the connection has rights, false otherwise.</returns>
        bool IsConnectionAvailable(Guid connectionId);

        /// <summary>
        /// Gets whatever graphical user interface the extension may use.
        /// The use of this interface will be dependent on the application being extended.
        /// </summary>
        object UserInterface { get; }
    }
}
