#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Extensions
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Security;
    using CKLib.Utilities.Diagnostics;
    #endregion
    /// <summary>
    /// Class used for managing extensions that communicate using the client/server architecture.
    /// </summary>
    public class ExtensionManager
    {
        #region Variables
        /// <summary>
        /// The collection of extensions.
        /// </summary>
        private SynchronizedDictionary<string, IExtension> m_Extensions;

        /// <summary>
        /// The IMessenger that owns this instance.
        /// </summary>
        private IMessenger m_Messenger;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ExtensionManager class.
        /// </summary>
        public ExtensionManager()
        {
            m_Extensions = new SynchronizedDictionary<string, IExtension>();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Initializes the Extension manager and loads the plugins from the fileNames specified.
        /// </summary>
        /// <param name="messenger">The IMessenger that owns this instance.</param>
        /// <param name="pluginAssemblies">An array of fileNames that contain extensions to load.</param>
        /// <returns>True if the messenger is valid, false otherwise.</returns>
        public bool Initialize(IMessenger messenger, string[] pluginAssemblies)
        {
            if (messenger != null)
            {
                m_Messenger = messenger;
                if (pluginAssemblies != null)
                {
                    for (int i = 0; i < pluginAssemblies.Length; i++)
                    {
                        if (File.Exists(pluginAssemblies[i]))
                        {
                            LoadExtensionsFromFile(pluginAssemblies[i], m_Messenger.IsServer);
                        }
                        else
                        {
                            Error.WriteEntry(string.Format(LogStrings.ERR_EXTENSION_MGR_CANNOT_LOCATE_FILE, pluginAssemblies[i]));
                        }
                    }
                }

                m_Messenger.AddCallback(typeof(ExtensionAvailabilityMessage), OnMessage);
                m_Messenger.AddCallback(typeof(ConnectionOpenedMessage), OnMessage);
                m_Messenger.AddCallback(typeof(ConnectionClosedMessage), OnMessage);
                IDataMessage.FindMessageTypes += IDataMessage_FindMessageTypes;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Provides the set of custom message types that the extensions need to communicate.
        /// </summary>
        /// <param name="sender">The sender is null.</param>
        /// <param name="e">An EventArgs object which provides a means to register message types with the client or server.</param>
        private void IDataMessage_FindMessageTypes(object sender, TypeDefinitionEventArgs e)
        {
            foreach (var ext in m_Extensions)
            {
                Type[] types = ext.Value.GetCustomMessageTypes();
                if (types != null)
                {
                    for (int i = 0; i < types.Length; i++)
                    {
                        if (types[i] != null)
                        {
                            e.AddType(types[i]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Releases resources held by the extension manager.
        /// </summary>
        public void Release()
        {
            if (m_Messenger != null)
            {
                m_Messenger.RemoveCallback(typeof(ExtensionAvailabilityMessage), OnMessage);
                m_Messenger.RemoveCallback(typeof(ConnectionOpenedMessage), OnMessage);
                m_Messenger.RemoveCallback(typeof(ConnectionClosedMessage), OnMessage);
                m_Messenger = null;
            }

            foreach (var extension in m_Extensions)
            {
                extension.Value.Dispose();
            }

            m_Extensions.Clear();
        }

        /// <summary>
        /// Gets all of the extensions available for local use.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, IExtension>> GetLocalExtensions()
        {
            return m_Extensions.ToArray();
        }

        /// <summary>
        /// Gets all of the extensions which are available for use by the specified connection.
        /// </summary>
        /// <param name="connectionId">The connection to check the plugins of.</param>
        /// <returns>An enumeration of extensions for the specified connection id.</returns>
        public IEnumerable<IExtension> GetExtensionsForConnection(Guid connectionId)
        {
            List<IExtension> validExtensions = new List<IExtension>();
            foreach (var extension in m_Extensions)
            {
                if (extension.Value.IsConnectionAvailable(connectionId))
                {
                    validExtensions.Add(extension.Value);
                }
            }

            return validExtensions;
        }

        /// <summary>
        /// Gets an IExtension by name.
        /// </summary>
        /// <param name="extensionName">The name of the extension to get.</param>
        /// <returns>The Extension or null.</returns>
        public IExtension GetExtension(string extensionName)
        {
            IExtension ext;
            if (m_Extensions.TryGetValue(extensionName, out ext))
            {
                return ext;
            }

            return null;
        }

        /// <summary>
        /// Loads an extension from the specified file.
        /// </summary>
        /// <param name="fileName">The path to the file containing the extension.</param>
        /// <param name="isServerSide">A flag indicating whether to search for server extensions or client ones.</param>
        public void LoadExtensionsFromFile(string fileName, bool isServerSide)
        {
            if (!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
            {
                try
                {
                    AssemblyName name = AssemblyName.GetAssemblyName(fileName);
                    Assembly tryAsm = Assembly.Load(name);
                    LoadExtensionsFromAssembly(tryAsm, isServerSide);
                }
                catch (Exception e)
                {
                    Error.WriteEntry(LogStrings.ERR_EXTENSION_MGR_FAIL_TO_LOAD, e);
                }
            }
        }

        /// <summary>
        /// Loads extensions from the specified assembly.
        /// </summary>
        /// <param name="asm">The assembly to search for extensions.</param>
        /// <param name="isServerSide">A flag indicating whether to search for server or client extensions.</param>
        public void LoadExtensionsFromAssembly(Assembly asm, bool isServerSide)
        {
            var tryAsm = asm;
            try
            {
                if (tryAsm != null)
                {
                    var typesInAsm = tryAsm.GetTypes();
                    var foundExtensions = from type in typesInAsm
                                          let attr = type.GetCustomAttributes(false)
                                          //let attrs = type.GetCustomAttributes(isServerSide ? typeof(ServerSideExtensionAttribute) : typeof(ClientSideExtensionAttribute), false)
                                          where attr.Length > 0
                                          where attr.FirstOrDefault(a => a.GetType().Name == (isServerSide ? "ServerSideExtensionAttribute" : "ClientSideExtensionAttribute")) != null
                                          let interfaces = type.FindInterfaces((t, notUsed) => t.Name == "IExtension", null)
                                          where interfaces != null && interfaces.Length > 0
                                          select type;
                    try
                    {
                        foreach (var extension in foundExtensions)
                        {
                            IExtension extProxy = null;
                            var extObj = Activator.CreateInstance(extension);

                            if (extObj is IExtension)
                            {
                                extProxy = (IExtension)extObj;
                            }
                            else
                            {
                                extProxy = ProxyFactory.Instance.CreateProxy<IExtension>(extObj, false);
                            }

                            if (extProxy != null)
                            {
                                if (extProxy.Initialize(m_Messenger, ProxyFactory.Instance))
                                {
                                    m_Extensions[extProxy.ExtensionName] = extProxy;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string location = null;
                        Proxy.FactoryHelpers.ExportCommunicationModule(ref location);

                        Error.WriteEntry(LogStrings.ERR_EXTENSION_MGR_FAIL_TO_LOAD, ex);
                    }
                }
            }
            catch (Exception e)
            {
                Error.WriteEntry(LogStrings.ERR_EXTENSION_MGR_FAIL_TO_LOAD, e);
            }
        }

        /// <summary>
        /// Notifies the connection of the extensions that are loaded by this messenger.
        /// </summary>
        /// <param name="connectionId">The connection to send the notifications to.</param>
        /// <param name="isAvailable">A flag indicating whether the extensions are available or not.</param>
        public void NotifyConnectionOfAvailableExtensions(Guid connectionId, bool isAvailable = true)
        {
            if (m_Messenger != null && m_Extensions != null)
            {
                foreach (var ext in m_Extensions)
                {
                    if (LocalOnlyAttribute.CheckAccess(ext.Value.GetType(), m_Messenger, connectionId))
                    {
                        m_Messenger.SendNetworkMessage(connectionId, new ExtensionAvailabilityMessage()
                        {
                            ExtensionName = ext.Key,
                            IsAvailable = isAvailable
                        });
                    }

                    if (IsExtensionStandalone(ext.Value))
                    {
                        ext.Value.EnableConnectionAvailability(connectionId);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the specified extension is a standalone extension.
        /// That is, it does not require a companion to function.
        /// </summary>
        /// <param name="ext">The extension to check.</param>
        /// <returns>True it the extension is a standalone extension, false otherwise.</returns>
        public bool IsExtensionStandalone(IExtension ext)
        {
            if (ext != null)
            {
                var attributes = ext.GetType().GetCustomAttributes(typeof(ExtensionAttribute), true);
                if (attributes != null && attributes.Length > 0)
                {
                    return attributes.Cast<ExtensionAttribute>().Any(e => e.IsStandalone);
                }
            }

            return false;
        }

        /// <summary>
        /// Receives messages relating to extensions.
        /// </summary>
        /// <param name="pair">An object describing the message coming in.</param>
        private void OnMessage(MessageEnvelope pair)
        {
            if (pair.Message is ExtensionAvailabilityMessage)
            {
                ExtensionAvailabilityMessage msg = (ExtensionAvailabilityMessage)pair.Message;
                IExtension ext;
                if (!string.IsNullOrEmpty(msg.ExtensionName))
                {
                    if (m_Extensions.TryGetValue(msg.ExtensionName, out ext))
                    {
                        if (msg.IsAvailable)
                        {
                            if (LocalOnlyAttribute.CheckAccess(ext.GetType(), m_Messenger, msg.SenderId))
                            {
                                ext.EnableConnectionAvailability(msg.SenderId);
                            }
                        }
                        else
                        {
                            ext.RevokeConnectionAvailability(msg.SenderId);
                        }
                    }
                }
            }
            else if (pair.Message is ConnectionOpenedMessage)
            {
                ConnectionOpenedMessage msg = (ConnectionOpenedMessage)pair.Message;
                foreach (var ext in m_Extensions.Where(ext => IsExtensionStandalone(ext.Value)))
                {
                    if (LocalOnlyAttribute.CheckAccess(ext.GetType(), m_Messenger, msg.ConnectionId))
                    {
                        ext.Value.EnableConnectionAvailability(msg.ConnectionId);
                    }
                }
            }
            else if (pair.Message is ConnectionClosedMessage)
            {
                ConnectionClosedMessage msg = (ConnectionClosedMessage)pair.Message;
                foreach (var ext in m_Extensions.Where(ext => IsExtensionStandalone(ext.Value)))
                {
                    if (LocalOnlyAttribute.CheckAccess(ext.GetType(), m_Messenger, msg.ConnectionId))
                    {
                        ext.Value.RevokeConnectionAvailability(msg.ConnectionId);
                    }
                }
            }
        }

        /// <summary>
        /// Revokes a connection's access to all extensions held by this messenger.
        /// </summary>
        /// <param name="connectionId">The id to revoke the access of.</param>
        public void RevokeConnectionFromAllExtensions(Guid connectionId)
        {
            foreach (var ext in m_Extensions)
            {
                ext.Value.RevokeConnectionAvailability(connectionId);
            }
        }
        #endregion
    }
}
