#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion


namespace CKLib.Utilities.Connection.Server
{
    #region Using List
    using System;
    using System.ServiceModel;
    using CKLib.Utilities.Connection.Client;
    using CKLib.Utilities.Messages;
    #endregion
    [ServiceContract(
    Name = "DeveloperServer",
    CallbackContract = typeof(IClientCallback),
    SessionMode = SessionMode.Required)]
    public interface IServer {
        /// <summary>
        /// Registers a client with the server.
        /// </summary>
        /// <param name="clientId">The client to register with the server.</param>
        /// <returns>The connection identifier of the server.</returns>
        [OperationContract(IsInitiating=true)]
        Guid RegisterClient(ConnectionInformation clientId);

        /// <summary>
        /// Unregisters a client from the server.
        /// </summary>
        /// <param name="clientId">The cilent to unregister from the server.</param>
        [OperationContract(IsOneWay = true, IsTerminating=true)]
        void UnregisterClient(Guid clientId);

        /// <summary>
        /// Sends a message to the server.
        /// </summary>
        /// <param name="message">The message to send to the server.</param>
        [OperationContract(IsOneWay = true, IsInitiating=false)]
        void SendMessage(IDataMessage message);

        /// <summary>
        /// Requests that the server re-send its notifications for extensions and shared objects.
        /// </summary>
        /// <param name="clientId">The connection id of the client to send the notifications to.</param>
        [OperationContract(IsOneWay = true, IsInitiating = false)]
        void ResendExtensionsAndShares(Guid clientId);
    }
}
