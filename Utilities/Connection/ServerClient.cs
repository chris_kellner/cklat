using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WCFTest.Patterns;
using System.ServiceModel;
using WCFTest.Messages;

namespace WCFTest.Connection
{
    public class ServerClient : IDisposable
    {
        private string m_NetworkAddress;
        private string m_ServerName;

        private Client m_Client;
        private AsyncAutoWaitLoop<DataClientMessageCallback> m_ProcessLoop;
        private CallbackMap<DataClientMessageCallback> m_CallbackMap;
        DuplexChannelFactory<IServer> m_ChannelFactory;

        public ServerClient()
            : this("net.pipe://localhost/", "DeveloperServer")
        {
        }

        public ServerClient(string networkAddress, string serverName)
        {
            m_NetworkAddress = networkAddress;
            m_ServerName = serverName;
            m_Client = new Client();
            m_Client.MessageReceived += new EventHandler(m_Client_MessageReceived);
            m_CallbackMap = new CallbackMap<DataClientMessageCallback>();
            m_ProcessLoop = new AsyncAutoWaitLoop<DataClientMessageCallback>(m_Client.ProcessMessages, ReceiveMessage);
        }

        void m_Client_MessageReceived(object sender, EventArgs e)
        {
            if (m_ProcessLoop != null)
            {
                m_ProcessLoop.SignalIteration();
            }
        }

        public void StartClient()
        {
            m_ChannelFactory = new DuplexChannelFactory<IServer>(
                m_Client, new NetNamedPipeBinding(), new EndpointAddress(m_NetworkAddress + m_ServerName));

            IServer server = m_ChannelFactory.CreateChannel();
            Singleton<IServer>.ValueFactory = () => server;

            Singleton<IServer>.Instance.RegisterClient(m_Client.ClientId);
            m_ProcessLoop.RunLoop();
            ConnectionActive = true;
        }

        public bool ConnectionActive
        {
            get;
            set;
        }

        public IServer Server
        {
            get
            {
                if (Singleton<IServer>.IsValueAssigned)
                {
                    return Singleton<IServer>.Instance;
                }
                return null;
            }
        }

        public void SendMessage(IDataMessage message)
        {
            if (ConnectionActive)
            {
                Server.SendMessage(m_Client.ClientId, message);
            }
        }

        protected virtual void ReceiveMessage(DataMessageEventArgs message)
        {
            if (message.Message is ShutdownMessage)
            {
                ConnectionActive = false;
                if (m_ProcessLoop != null)
                {
                    m_ProcessLoop.Exit();
                }
            }
            Type t = message.Message.GetType();
            if (m_CallbackMap.HasCallbacks(t))
            {
                // Cache the result so as to avoid recursive locking.
                var callbacks = m_CallbackMap.GetCallbacks(t).ToArray();
                foreach (var callback in callbacks)
                {
                    callback(message);
                }
            }
        }

        public void AddCallback(Type messageType, DataClientMessageCallback callback)
        {
            m_CallbackMap.AddCallback(messageType, callback);
        }

        public void RemoveCallback(Type messageType, DataClientMessageCallback callback)
        {
            m_CallbackMap.RemoveCallback(messageType, callback);
        }

        #region IDisposable Members

        public void Dispose()
        {
            m_ProcessLoop.Dispose();
            try
            {
                Server.UnregisterClient(m_Client.ClientId);
            }
            catch (Exception)
            {
                ConnectionActive = false;
            }
            m_CallbackMap.Clear();
        }

        #endregion
    }
}
