﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Diagnostics
{
    #region Using List
    using System;
    using System.Threading;
    using CKLib.Utilities.DataStructures;
    #endregion

    /// <summary>
    /// This class is used to output messages to the various streams within the system, while providing notification
    /// as it happens.
    /// </summary>
    public class OutputChannel : IDisposable, ILockable
    {
        #region Variables
        /// <summary>
        /// Delegate called when output is produced.
        /// </summary>
        private event EventHandler<OutputEventArgs> m_OutputWritten;

        /// <summary>
        /// Indicates what the default output level is for this channel.
        /// </summary>
        private readonly OutputLevel m_DefaultLevel;

        /// <summary>
        /// Object used for synchronizing this instance.
        /// </summary>
        private readonly object m_Locker = new object();
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the OutputChannel class.
        /// </summary>
        /// <param name="defaultChannelLevel">The default level that this channel will produce output for.</param>
        public OutputChannel(OutputLevel defaultChannelLevel)
        {
            m_DefaultLevel = defaultChannelLevel;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        public void WriteEntry(string message)
        {
            WriteEntry(m_DefaultLevel, message, null, null);
        }

        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        /// <param name="exception">An exception to output along with the message.</param>
        public void WriteEntry(string message, Exception exception)
        {
            WriteEntry(m_DefaultLevel, message, exception, null);
        }

        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public void WriteEntry(string message, object[] data)
        {
            WriteEntry(m_DefaultLevel, message, null, data);
        }

        /// <summary>
        /// Emits a message to this channel using the specified parameters.
        /// </summary>
        /// <param name="level">The level at which the message should be output.</param>
        /// <param name="message">The message to output.</param>
        /// <param name="exception">An exception to output along with the message.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public virtual void WriteEntry(OutputLevel level, string message, Exception exception, object[] data)
        {
            OnOutputInternal(new OutputEventArgs(level, message, exception, data));
        }

        /// <summary>
        /// Called when output is produced on this channel.
        /// </summary>
        /// <param name="e">The OutputEventArgs describing what is being written.</param>
        protected void OnOutputInternal(OutputEventArgs e)
        {
            if (e != null && e.Message != null)
            {
                //object obj;
                //Monitor.Enter(obj = m_Locker); try
                //{
                    OnOutput(e);
                    if (m_OutputWritten != null)
                    {
                        m_OutputWritten(this, e);
                    }
                //}
                //finally
                //{
                //    Monitor.Exit(obj);
                //}
            }
        }

        /// <summary>
        /// Called when output is produced on this channel.
        /// </summary>
        /// <param name="e">The OutputEventArgs describing what is being written.</param>
        protected virtual void OnOutput(OutputEventArgs e)
        {
        }

        #region IDisposable Members
        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged resources held by this instance, and optionally releases managed ones as well.
        /// </summary>
        /// <param name="disposing">True to release managed resoruces as well.</param>
        protected virtual void Dispose(bool disposing)
        {
            m_OutputWritten = null;
        }
        #endregion
        #endregion
        #region Events
        /// <summary>
        /// This event is triggered when output is written to this channel.
        /// </summary>
        public event EventHandler<OutputEventArgs> OutputWritten
        {
            add
            {
                object obj;
                Monitor.Enter(obj = m_Locker); try
                {
                    m_OutputWritten += value;
                }
                finally
                {
                    Monitor.Exit(obj);
                }
            }

            remove
            {
                object obj;
                Monitor.Enter(obj = m_Locker); try
                {
                    m_OutputWritten -= value;
                }
                finally
                {
                    Monitor.Exit(obj);
                }
            }
        }
        #endregion
        #region ILockable Members
        /// <summary>
        /// Locks this instance.
        /// </summary>
        public void Lock()
        {
            Monitor.Enter(m_Locker);
        }

        /// <summary>
        /// Unlocks this instance.
        /// </summary>
        public void Unlock()
        {
            Monitor.Exit(m_Locker);
        }

        /// <summary>
        /// Returns true.
        /// </summary>
        public bool IsSynchronized
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the default output level for this channel.
        /// </summary>
        public OutputLevel ChannelLevel
        {
            get
            {
                return m_DefaultLevel;
            }
        }

        /// <summary>
        /// Gets or sets the LockId of the object.
        /// </summary>
        /// <remarks>This property is reserved for future use and should only be implemented as a default property.</remarks>
        int ILockable.LockId
        {
            get;
            set;
        }
        #endregion
    }
}
