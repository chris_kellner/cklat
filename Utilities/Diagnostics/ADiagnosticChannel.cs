﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Diagnostics
{
    #region Using List
    using System;
    using CKLib.Utilities.Patterns;
using System.IO;
    using CKLib.Utilities.DataStructures.Configuration;
    #endregion
    /// <summary>
    /// This class is used as a central API for logging debug statements.
    /// </summary>
    public abstract class ADiagnosticChannel<TChannel> where TChannel : ADiagnosticChannel<TChannel>
    {
        #region Internal Classes
        /// <summary>
        /// This channel is used to output its data to a file.
        /// </summary>
        protected class FileOutputChannel : OutputChannel
        {
            #region Variables
            /// <summary>
            /// Path to the file to write entries to.
            /// </summary>
            private string m_FilePath;

            /// <summary>
            /// StreamWriter used to append to the file.
            /// </summary>
            private StreamWriter m_StreamWriter;

            /// <summary>
            /// Minimum level to output to the file.
            /// </summary>
            private OutputLevel m_MinimumWriteLevel;
            #endregion
            #region Constructors
            /// <summary>
            /// Intialized a new instance of the FileOutputChannel class.
            /// </summary>
            public FileOutputChannel()
                : base(OutputLevel.Verbose)
            {
            }
            #endregion
            #region Methods
            /// <summary>
            /// Intializes the file logger.
            /// </summary>
            /// <param name="filePath">Path to the file to write entries to</param>
            /// <param name="minimumWriteLevel"></param>
            public void Initialize(string filePath, OutputLevel minimumWriteLevel)
            {
                m_MinimumWriteLevel = minimumWriteLevel;
                if (m_StreamWriter != null)
                {
                    m_StreamWriter.Dispose();
                    m_StreamWriter = null;
                }

                if (!string.IsNullOrEmpty(filePath))
                {
                    m_FilePath = filePath;
                    var directory = Path.GetDirectoryName(m_FilePath);
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    CreateWriter();
                }
            }

            /// <summary>
            /// Creates the file stream writer.
            /// </summary>
            private void CreateWriter()
            {
                FileStream stream = new FileStream(m_FilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                m_StreamWriter = new StreamWriter(FileStream.Synchronized(stream));
                m_StreamWriter.AutoFlush = true;
            }

            /// <summary>
            /// Releases of resources held by this instance.
            /// </summary>
            /// <param name="disposing"></param>
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                if (m_StreamWriter != null)
                {
                    m_StreamWriter.Dispose();
                    m_StreamWriter = null;
                }
            }

            /// <summary>
            /// Called when output is written to this channel.
            /// </summary>
            /// <param name="e"></param>
            protected override void OnOutput(OutputEventArgs e)
            {
                base.OnOutput(e);
                if (m_StreamWriter != null)
                {
                    if (e.Level >= m_MinimumWriteLevel)
                    {
                        RollLogIfNeeded();
                        m_StreamWriter.WriteLine(e.ToString());
                    }
                }
            }

            /// <summary>
            /// Checks to see if the file has gotten too big, if so the file is recreated.
            /// </summary>
            private void RollLogIfNeeded()
            {
                if (m_StreamWriter != null)
                {
                    const long TenMegaBytes = 0x100000L;
                    if (m_StreamWriter.BaseStream.Position > TenMegaBytes)
                    {
                        m_StreamWriter.Dispose();
                        File.Delete(m_FilePath);
                        CreateWriter();
                    }
                }
            }
            #endregion
        }
        #endregion
        #region Variables
        /// <summary>
        /// Output channel used for submitting the Debugging statements.
        /// </summary>
        private OutputChannel m_OutputChannel;
        #endregion
        #region Constructors
        /// <summary>
        /// Prevents external initialization of the ADiagnosticChannel class.
        /// </summary>
        protected ADiagnosticChannel()
        {
            m_OutputChannel = new OutputChannel(OutputLevel);
            m_OutputChannel.OutputWritten += m_OutputChannel_OutputWritten;
        }

        /// <summary>
        /// Prevents external initialization of the ADiagnosticChannel class,
        /// but allows subclasses to specify the output channel.
        /// </summary>
        /// <param name="outputChannel">The output channel to use.</param>
        protected ADiagnosticChannel(OutputChannel outputChannel)
        {
            m_OutputChannel = outputChannel;
            m_OutputChannel.OutputWritten += m_OutputChannel_OutputWritten;
        }
        #endregion
        #region Methods
        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        public static void WriteEntry(string message)
        {
            WriteEntry(Instance.OutputLevel, message, null, null);
        }

        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        public static void WriteEntry(object message)
        {
            if (message != null)
            {
                WriteEntry(Instance.OutputLevel, message.ToString(), null, null);
            }
        }

        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        /// <param name="exception">An exception to output along with the message.</param>
        public static void WriteEntry(string message, Exception exception)
        {
            WriteEntry(Instance.OutputLevel, message, exception, null);
        }

        /// <summary>
        /// Emits a message to this channel at the default output level.
        /// </summary>
        /// <param name="message">The message to output.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public static void WriteEntry(string message, object[] data)
        {
            WriteEntry(Instance.OutputLevel, message, null, data);
        }

        /// <summary>
        /// Emits a message to this channel using the specified parameters.
        /// </summary>
        /// <param name="level">The level at which the message should be output.</param>
        /// <param name="message">The message to output.</param>
        /// <param name="exception">An exception to output along with the message.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public static void WriteEntry(OutputLevel level, string message, Exception exception, object[] data)
        {
            Instance.m_OutputChannel.WriteEntry(level, message, exception, data);
            if (Instance.AllowFileLog)
            {
                FileChannel.WriteEntry(level, message, exception, data);
            }
        }

        /// <summary>
        /// Called when output is written to this channel.
        /// </summary>
        /// <param name="e">The arguments describing what is being written.</param>
        protected abstract void OutputWritten(OutputEventArgs e);

        /// <summary>
        /// Called when entries are written.
        /// </summary>
        /// <param name="sender">The output channel.</param>
        /// <param name="e">The event args.</param>
        private void m_OutputChannel_OutputWritten(object sender, OutputEventArgs e)
        {
            OutputWritten(e);
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        private static TChannel Instance
        {
            get
            {
                return Singleton<TChannel>.Instance;
            }
        }

        /// <summary>
        /// Gets the default level at which this channel outputs.
        /// </summary>
        protected abstract OutputLevel OutputLevel { get; }

        /// <summary>
        /// Gets the output channel used to produce entries.
        /// </summary>
        protected OutputChannel InnerChannel
        {
            get
            {
                return m_OutputChannel;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this channel can be written to the file log.
        /// </summary>
        protected virtual bool AllowFileLog
        {
            get
            {
                return true;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Called when an debugging statement has been written.
        /// </summary>
        public static event EventHandler<OutputEventArgs> OnEntry
        {
            add
            {
                Instance.m_OutputChannel.OutputWritten += value;
            }

            remove
            {
                Instance.m_OutputChannel.OutputWritten -= value;
            }
        }
        #endregion
    }

    internal class FileChannel : ADiagnosticChannel<FileChannel>, IDisposable
    {
        #region Constructors
        /// <summary>
        /// Prevents external initialization of the FileChannel class.
        /// </summary>
        protected FileChannel()
            : base(new FileOutputChannel())
        {
        }
        #endregion
        #region properties
        /// <summary>
        /// Gets the default output level of this channel.
        /// </summary>
        protected override OutputLevel OutputLevel
        {
            get { return OutputLevel.Off; }
        }

        /// <summary>
        /// Overridden to prevent this channel from calling itself.
        /// </summary>
        protected override bool AllowFileLog
        {
            get
            {
                return false;
            }
        }
        #endregion
        #region Methods
        /// <summary>
        /// Retrieves the log level from the configuration settings.
        /// </summary>
        /// <returns></returns>
        private static OutputLevel GetLogLevel()
        {
            var group = ConfigurationManager.Current[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP, true];
            OutputLevel level;
            if (group.TryReadValue<OutputLevel>(Setting.LOG_LEVEL, out level))
            {
                return level;
            }

            return OutputLevel.Off;
        }

        /// <summary>
        /// Initializes the FileChannel based on configuration settings.
        /// </summary>
        public static void Initialize()
        {
            var chan = Singleton<FileChannel>.Instance.InnerChannel as FileOutputChannel;
            if (chan != null)
            {
                RemoveOldLogs();
                var level = GetLogLevel();
                if (level != OutputLevel.Off)
                {
                    chan.Initialize(ConfigurationManager.LogFilePath, level);
                }
            }
        }

        /// <summary>
        /// Cleans up the log directory by removing outdated logs.
        /// </summary>
        private static void RemoveOldLogs()
        {
            var path = ConfigurationManager.AppDataFolder;
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path, "Log_Pid*");
                if (files != null)
                {
                    DateTime currentTime = DateTime.UtcNow;
                    for (int i = 0; i < files.Length; i++)
                    {
                        DateTime lastWriteTime = File.GetLastWriteTimeUtc(files[i]);
                        if (currentTime - lastWriteTime > TimeSpan.FromDays(1))
                        {
                            try
                            {
                                File.Delete(files[i]);
                            }
                            catch (IOException)
                            {
                                Error.WriteEntry("A Lock is still held on the file: " + files[i]);
                            }
                            catch (UnauthorizedAccessException)
                            {
                                Error.WriteEntry("Insufficient rights to delete file: " + files[i]);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when output is written to this channel.
        /// </summary>
        /// <param name="e"></param>
        protected override void OutputWritten(OutputEventArgs e)
        {
        }

        /// <summary>
        /// Releases resources held by this instance.
        /// </summary>
        public void Dispose()
        {
            var chan = InnerChannel;
            if (chan != null)
            {
                chan.Dispose();
            }
        }
        #endregion
    }
}
