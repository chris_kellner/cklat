﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CKLib.Utilities.Diagnostics
{
    public class TraceSource
    {
        public TraceSource() { }

        public TraceSource(Type sourceType, MethodBase sourceMethod)
        {
            SourceType = sourceType;
            SourceMethod = sourceMethod;
        }

        public Type SourceType { get; set; }
        public MethodBase SourceMethod { get; set; }
    }

    public enum TraceEvent
    {
        Enter,
        Exit,
        Error,
        Other
    }

    public class TraceEventArgs : EventArgs
    {
        public TraceEventArgs()
        {
        }

        public TraceEventArgs(TraceSource src, TraceEvent evt, string message)
        {
            Source = src;
            Event = evt;
            Message = message;
        }

        public TraceSource Source { get; set; }
        public TraceEvent Event { get; set; }
        public string Message { get; set; }
        public TimeSpan Elapsed { get; set; }

        public object Data { get; set; }

        public IEnumerable<KeyValuePair<string, object>> GetParameters()
        {
            if (Event == TraceEvent.Enter)
            {
                object[] args = Data as object[];
                var parameters = Source.SourceMethod.GetParameters();
                if (args.Length == parameters.Length)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        yield return new KeyValuePair<string, object>(parameters[i].Name, args[i]);
                    }
                }
            }
        }
    }

    public interface ITraceable
    {
        event EventHandler<TraceEventArgs> TraceEvent;
    }
}
