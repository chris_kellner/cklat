﻿#region Usage Rights
/******************************************************************************
Copyright (c) 2012, Christopher Kellner
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
#endregion

namespace CKLib.Utilities.Diagnostics
{
    #region Using List
    using System;
    #endregion

    /// <summary>
    /// This class is used as the containing structure for the OutputWritten event.
    /// </summary>
    public class OutputEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the OutputEventArgs class.
        /// </summary>
        /// <param name="message">The message being output.</param>
        public OutputEventArgs(string message)
            : this(OutputLevel.Debug, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the OutputEventArgs class.
        /// </summary>
        /// <param name="level">The level to tag this message as.</param>
        /// <param name="message">The message being output.</param>
        public OutputEventArgs(OutputLevel level, string message)
            : this(level, message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the OutputEventArgs class.
        /// </summary>
        /// <param name="level">The level to tag this message as.</param>
        /// <param name="message">The message being output.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public OutputEventArgs(OutputLevel level, string message, object[] data)
            : this(level, message, null, data)
        {
        }

        /// <summary>
        /// Initializes a new instance of the OutputEventArgs class.
        /// </summary>
        /// <param name="message">The message being output.</param>
        /// <param name="exception">An exception that was generated with the message.</param>
        public OutputEventArgs(string message, Exception exception)
            : this(OutputLevel.Error, message, exception, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the OutputEventArgs class.
        /// </summary>
        /// <param name="level">The level to tag this message as.</param>
        /// <param name="message">The message being output.</param>
        /// <param name="exception">An exception that was generated with the message.</param>
        /// <param name="data">Additional data to send with the message.</param>
        public OutputEventArgs(OutputLevel level, string message, Exception exception, object[] data)
        {
            if (level == OutputLevel.All)
            {
                level = OutputLevel.Debug;
            }

            Message = message;
            Level = level;
            Data = data;
            Exception = exception;
            TimeStampUtc = DateTime.UtcNow;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the message being output.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the exception if one was included.
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Gets any additional data that was sent with the message.
        /// </summary>
        public object[] Data { get; private set; }

        /// <summary>
        /// Gets the level at which this message is being output.
        /// </summary>
        public OutputLevel Level { get; private set; }

        /// <summary>
        /// Gets the UTC time at which this message was generated.
        /// </summary>
        public DateTime TimeStampUtc { get; private set; }
        #endregion
        #region Methods
        /// <summary>
        /// Gets the string representation of this output event message.
        /// </summary>
        /// <returns>The string representation of this output event message.</returns>
        public override string ToString()
        {
            if (Exception == null)
            {
                return string.Format("[{0}] - {1} : {2}", Level, TimeStampUtc.ToString("u"), Message);
            }
            else
            {
                return string.Format(
                    "[{0}] - {1} : {2}{3}Exception:{3}{4}",
                    Level,
                    TimeStampUtc.ToString("u"),
                    Message,
                    Environment.NewLine,
                    Exception);
            }
        }
        #endregion
    }
}
