﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Connection;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace TestFormClient
{


    public class ShareTest : /*IShareTest,*/ INotifyPropertyChanged
    {
        private int m_int = 0;

        public ShareTest()
        {
        }

        public void DoIt()
        {
            MessageBox.Show("Did it");
        }

        public int CountIt()
        {
            return ++MyInt;
        }

        public int AddIt(int num)
        {
            return MyInt += num;
        }

        public int MyInt
        {
            get
            {
                return m_int;
            }
            set
            {
                m_int = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MyInt"));
                }
            }
        }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle(MyInt, MyInt, MyInt, MyInt);
            }
        }

        public ShareTest MyTest
        {
            get
            {
                return this;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
