﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;
using CKLib.Utilities.Connection.Client;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Patterns;
using CKLib.Utilities.Proxy;
using System.Drawing;
using CKLib.Utilities.Proxy.ObjectSharing;

namespace TestFormClient
{
    public partial class Form1 : Form
    {
        //Client m_client;
        ShareTest m_shareTest;
        IShareTest m_ServerShare;

        public Form1()
        {
            InitializeComponent();
            m_shareTest = new ShareTest();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Client.Instance.AddCallback(typeof(StringMessage), OnMessage);
            Client.Instance.ConnectionFaulted += new EventHandler(m_client_ConnectionFaulted);
            Client.Instance.SharedObjectPool.SharedObjectReceived += new EventHandler<SharedObjectAvailabilityEventArgs>(SharedObjectPool_SharedObjectReceived);
            m_shareTest.PropertyChanged += new PropertyChangedEventHandler(m_shareTest_PropertyChanged);
        }

        void SharedObjectPool_SharedObjectReceived(object sender, SharedObjectAvailabilityEventArgs e)
        {
            if (e.SharedObject.ObjectType == typeof(IShareTest))
            {
                if (e.IsAvailable)
                {
                    m_ServerShare = e.SharedObject as IShareTest;
                }
            }
        }

        void m_shareTest_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (InvokeRequired)
            //{
            //    Invoke(new PropertyChangedEventHandler(m_shareTest_PropertyChanged), sender, e);
            //}
            //else
            //{
                textBox3.Text = m_shareTest.MyInt.ToString();
            //}
        }

        void m_client_ConnectionFaulted(object sender, EventArgs e)
        {
            AppendText("Connection has been faulted.");
            if (e is UnhandledExceptionEventArgs)
            {
                var args = (UnhandledExceptionEventArgs)e;
                Exception ex = args.ExceptionObject as Exception;
                AppendText(ex.Message);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Client.Instance.Dispose();
            Singleton.ReleaseAllSingletons();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Client.Instance.SendNetworkMessage(new StringMessage(textBox2.Text));
            textBox2.Text = "";
        }

        private void AppendText(string text)
        {
            if (!InvokeRequired)
            {
                textBox1.AppendText(text + Environment.NewLine);
            }
            else
            {
                BeginInvoke(new Action<string>(AppendText), text);
            }
        }

        private void OnMessage(MessagePair e)
        {
            if (e.MessageInformation.Message is StringMessage)
            {
                AppendText(((StringMessage)e.MessageInformation.Message).Message);
            }
        }


        ISharedObject holder;
        private void btn_share_Click(object sender, EventArgs e)
        {
            try
            {
                holder = Client.Instance.SharedObjectPool.ShareObject<IShareTest>(m_shareTest);
            }
            catch
            {
                FactoryHelpers.ExportCommunicationModule();
            }
        }

        private void btn_unshare_Click(object sender, EventArgs e)
        {
            if (holder != null)
            {
                Client.Instance.SharedObjectPool.UnshareObject(holder);
            }
        }

        #region TESTING AREA
        private void button2_Click(object sender, EventArgs e)
        {
            FactoryHelpers.ExportCommunicationModule();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Rectangle aRectangle = new Rectangle(1, 0, 0, 0);
            IRectangle iRect = ProxyFactory.Instance.CreateProxy<IRectangle>(aRectangle, true);

            ATestClass aTest = new ATestClass();
            ITestClass iTest = ProxyFactory.Instance.CreateProxy<ITestClass>(aTest, true);
            var rect = iTest.MyRectangle;

            PRectangle test = new PRectangle(aRectangle);
        }

        public class PRectangle : IRectangle
        {
            private Rectangle m_WrappedObject;
            public PRectangle(Rectangle test)
            {
                m_WrappedObject = test;
            }
            #region ITestClass Members

            

            #endregion

            #region IRectangle Members

            public int X
            {
                get { return m_WrappedObject.X; }
                set { m_WrappedObject.X = value; }
            }

            public int Y
            {
                get { return m_WrappedObject.Y; }
            }

            public int Width
            {
                get { return m_WrappedObject.Width; }
            }

            public int Height
            {
                get { return m_WrappedObject.Height; }
            }

            #endregion
        }

        public interface ITestClass
        {
            Rectangle MyRectangle { get; }
            Form MyForm { get; }
        }

        public class ATestClass
        {
            public ATestClass()
            {
                MyRectangle = new Rectangle(1, 0, 0, 0);
                MyForm = new Form1();
            }

            public Rectangle MyRectangle { get; set; }
            public Form MyForm { get; set; }
        }
        #endregion

        private void btn_count_Click(object sender, EventArgs e)
        {
            m_ServerShare.CountIt();
        }

        private void btn_do_Click(object sender, EventArgs e)
        {
            m_ServerShare.DoIt();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            m_ServerShare.AddIt(5);
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            Client.Instance.ConnectToServer(tb_serverAddress.Text, tb_serverName.Text, cb_forceTcp.Checked, tb_user.Text, tb_pass.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var conns = Client.Instance.GetConnections(tb_serverName.Text).ToArray();
            foreach (var server in conns)
            {
                server.Disconnect();
            }
        }

    }
}
