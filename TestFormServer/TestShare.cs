﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TestFormServer
{
    public class TestShare: INotifyPropertyChanged
    {
        private int m_int = 1;

        public TestShare()
        {
        }

        public void DoIt()
        {
            MessageBox.Show("I Did it");
        }

        public int CountIt()
        {
            return MyInt += 2;
        }

        public int AddIt(int num)
        {
            return MyInt += num;
        }

        public int MyInt
        {
            get
            {
                return m_int;
            }
            set
            {
                m_int = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MyInt"));
                }
            }
        }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle(MyInt, MyInt, MyInt, MyInt);
            }
        }

        public TestShare MyTest
        {
            get
            {
                return this;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
