﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using CKLib.Utilities.Connection.Server;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Patterns;
using CKLib.Utilities.Proxy;
using CKLib.Utilities.Proxy.ObjectSharing;
using TestFormServer.Properties;

namespace TestFormServer
{
    public partial class Form1 : Form
    {
        TestShare m_Share = new TestShare();

        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Server.Instance[Resources.MAIN_SERVER_NAME].AddCallback(typeof(StringMessage), MessageCallback, true);
            Server.Instance[Resources.MAIN_SERVER_NAME].AddCallback(typeof(IDataMessage), MessageCallback, true);
            Server.Instance[Resources.MAIN_SERVER_NAME].ServerStarted += new EventHandler(Instance_ServerStarted);
            Server.Instance[Resources.MAIN_SERVER_NAME].ServerStopped += new EventHandler(Instance_ServerStopped);
            Server.Instance[Resources.MAIN_SERVER_NAME].ClientConnected += new EventHandler<ClientEventArgs>(Instance_ClientConnected);
            Server.Instance[Resources.MAIN_SERVER_NAME].ClientDisconnected += new EventHandler<ClientEventArgs>(Instance_ClientDisconnected);
            Server.Instance[Resources.MAIN_SERVER_NAME].SharedObjectPool.SharedObjectReceived += new EventHandler<SharedObjectAvailabilityEventArgs>(pool_SharedObjectReceived);
            m_Share.PropertyChanged += new PropertyChangedEventHandler(m_Share_PropertyChanged);
        }

        void Instance_ServerStopped(object sender, EventArgs e)
        {
            AppendText("Server is stopped.");
        }

        void Instance_ServerStarted(object sender, EventArgs e)
        {
            AppendText("Server is available.");
        }

        void m_Share_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                PropertyChangedEventHandler cb = new PropertyChangedEventHandler(m_Share_PropertyChanged);
                Invoke(cb, sender, e);
            }
            else
            {
                tb_share.Text = m_Share.MyInt.ToString();
            }
        }

        void pool_SharedObjectReceived(object sender, SharedObjectAvailabilityEventArgs e)
        {
            if (e.SharedObject.ObjectType == typeof(IShareTest))
            {
                m_SharedTest = e.SharedObject as IShareTest;
            }
        }

        void Instance_ClientDisconnected(object sender, ClientEventArgs e)
        {
            AppendText(e.Client.Identifier + " has disconnected.");
        }

        void Instance_ClientConnected(object sender, ClientEventArgs e)
        {
            AppendText(e.Client.Identifier + " has connected.");
        }

        private void AppendText(string text)
        {
            if (!InvokeRequired)
            {
                textBox1.AppendText(text + Environment.NewLine);
            }
            else
            {
                BeginInvoke(new Action<string>(AppendText), text);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Server.Instance[Resources.MAIN_SERVER_NAME].RemoveCallback(typeof(StringMessage), MessageCallback);
            Server.Instance[Resources.MAIN_SERVER_NAME].StopServer();
            Singleton.ReleaseAllSingletons();
        }

        private void MessageCallback(MessagePair pair)
        {
            Guid sender = pair.Sender;
            DataMessageEventArgs e = pair.MessageInformation;
            if (e.Message is StringMessage)
            {
                AppendText(((StringMessage)e.Message).Message);
            }
            else
            {
                AppendText(e.Message.ToString());
            }
             lastSender = sender;
        }

        Guid lastSender;
        bool spam = false;

        private void button1_Click(object sender, EventArgs e)
        {
            Server.Instance[Resources.MAIN_SERVER_NAME].SendNetworkMessage(new StringMessage(textBox2.Text));
            textBox2.Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (lastSender != null)
            {
                Server.Instance[Resources.MAIN_SERVER_NAME].SendNetworkMessage(lastSender, new StringMessage("SPAM SPAM SPAM SPAM"));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lastSender != null)
            {
                spam = !spam;
                if (spam)
                {
                    timer1.Start();
                }
                else
                {
                    timer1.Stop();
                }
            }
        }

        #region Shared Test
        IShareTest m_SharedTest;
        private void button3_Click(object sender, EventArgs e)
        {
            if (m_SharedTest != null)
            {
                m_SharedTest.DoIt();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (m_SharedTest != null)
            {
                int i = m_SharedTest.CountIt();
                MessageBox.Show("" + i);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (m_SharedTest != null)
            {
                m_SharedTest.AddIt(3);
            }
        }
        #endregion

        ISharedObject m_sharedObj = null;
        private void btn_unshare_Click(object sender, EventArgs e)
        {
            if (m_sharedObj != null)
            {
                Server.Instance[Resources.MAIN_SERVER_NAME].SharedObjectPool.UnshareObject(m_sharedObj);
                m_sharedObj = null;
            }
        }

        private void btn_share_Click(object sender, EventArgs e)
        {
            m_sharedObj = Server.Instance[Resources.MAIN_SERVER_NAME].SharedObjectPool.ShareObject<IShareTest>(m_Share);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Server.Instance[Resources.MAIN_SERVER_NAME].KickClient(lastSender);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            if (Enum.IsDefined(typeof(ServerAccessType), cmb_Type.Text))
            {
                if (!Server.Instance[Resources.MAIN_SERVER_NAME].IsRunning)
                {
                    ServerAccessType access = (ServerAccessType)Enum.Parse(typeof(ServerAccessType), cmb_Type.Text, true);
                    ServerManager.RunServerAsync(Resources.MAIN_SERVER_NAME, access, tb_serverName.Text);
                }
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            if (Server.Instance[Resources.MAIN_SERVER_NAME].IsRunning)
            {
                Server.Instance[Resources.MAIN_SERVER_NAME].StopServer();
            }
        }
    }
}
