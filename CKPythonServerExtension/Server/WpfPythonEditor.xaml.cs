﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CKPythonExtension.Server
{
    /// <summary>
    /// Interaction logic for WpfPythonEditor.xaml
    /// </summary>
    public partial class WpfPythonEditor : UserControl
    {
        #region Dependency Properties
        // Using a DependencyProperty as the backing store for Script.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScriptProperty =
            DependencyProperty.Register("Script", typeof(RemoteScript), typeof(WpfPythonEditor), new UIPropertyMetadata(null, OnScriptChanged));

        public static void OnScriptChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            WpfPythonEditor editor = source as WpfPythonEditor;
            if (editor != null)
            {
                editor.OnScriptChanged(e.NewValue as RemoteScript);
            }
        }

        // Using a DependencyProperty as the backing store for SelectedClient.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedClientProperty =
            DependencyProperty.Register("SelectedClient", typeof(Guid), typeof(WpfPythonEditor), new UIPropertyMetadata(Guid.Empty));

        #endregion

        public WpfPythonEditor()
        {
            //DataContext = PythonSettings.Instance;
            InitializeComponent();
            tb_In.SyntaxLexer = new CKPythonExtension.IronPython.IronPythonTokenParser();
            tb_In.SyntaxRulesChanged += tb_In_SyntaxRulesChanged;
        }

        void tb_In_SyntaxRulesChanged(object sender, RoutedEventArgs e)
        {
            tb_In.InvalidateVisual();
        }

        #region Properties

        public Guid SelectedClient
        {
            get { return (Guid)GetValue(SelectedClientProperty); }
            set { SetValue(SelectedClientProperty, value); }
        }

        public RemoteScript Script
        {
            get { return (RemoteScript)GetValue(ScriptProperty); }
            set { SetValue(ScriptProperty, value); }
        }

        public TextBox OutputTextBox
        {
            get
            {
                return tb_Out;
            }
        }

        public TextBox InputTextBox
        {
            get
            {
                return tb_In;
            }
        }

        public Button CloseButton
        {
            get
            {
                return btn_Close;
            }
        }
        #endregion
        #region Methods
        protected virtual void OnScriptChanged(RemoteScript newScript)
        {
            newScript.Intialize();
        }
        #endregion
        #region Event Handlers
        private void btn_Run_Click(object sender, RoutedEventArgs e)
        {
            OutputTextBox.Clear();
            if (Script != null)
            {
                Script.Run(SelectedClient);
            }
        }

        private void tb_Out_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tb_Out.Text))
            {
                tb_Out.Visibility = Visibility.Collapsed;
            }
            else
            {
                tb_Out.Visibility = Visibility.Visible;
            }
        }
        #endregion

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void grid_main_Initialized(object sender, EventArgs e)
        {
            grid_main.DataContext = PythonSettings.Instance;
        }
    }
}
