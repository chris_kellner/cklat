﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using CKPythonExtension.Messages;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Diagnostics;

namespace CKPythonExtension.Server
{
    public class RemoteScript : INotifyPropertyChanged
    {
        #region Variables
        /// <summary>
        /// Reference to the python plugin.
        /// </summary>
        private CKServerPythonExtension m_Plugin;

        /// <summary>
        /// The name of the script file.
        /// </summary>
        private string m_FileName;

        /// <summary>
        /// A Flag indicating whether the script has been modified in memory.
        /// </summary>
        private bool m_IsDirty;

        /// <summary>
        /// The text value of the script.
        /// </summary>
        private string m_ScriptText;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the LocalScript class.
        /// </summary>
        public RemoteScript(CKServerPythonExtension plugin)
        {
            m_Plugin = plugin;
        }
        #endregion
        #region Properties
        public string ScriptFileName
        {
            get
            {
                return m_FileName;
            }

            set
            {
                m_FileName = value;
                OnPropertyChanged("ScriptFileName");
                OnPropertyChanged("FormattedScriptName");
            }
        }

        public bool IsDirty
        {
            get
            {
                return m_IsDirty;
            }

            set
            {
                m_IsDirty = value;
                OnPropertyChanged("IsDirty");
                OnPropertyChanged("FormattedScriptName");
            }
        }

        public string ScriptText
        {
            get
            {
                return m_ScriptText;
            }

            set
            {
                m_ScriptText = value;
                OnPropertyChanged("ScriptText");
                IsDirty = true;
            }
        }

        public string FormattedScriptName
        {
            get
            {
                return (Path.GetFileName(ScriptFileName) ?? "[unnamed]") + (IsDirty ? "*" : "");
            }
        }

        public Guid AwaitingResponse
        {
            get;
            set;
        }
        #endregion
        #region Methods
        public void Intialize()
        {
            if (m_Plugin != null)
            {
                var messenger = m_Plugin.Messenger;
                if (messenger != null)
                {
                    messenger.AddCallback(typeof(RunScriptResponseMessage), OnMessageReceived);
                }
            }
        }

        private void OnMessageReceived(MessagePair pair)
        {
            if (pair.MessageInformation.Message is RunScriptResponseMessage)
            {
                var msg = pair.MessageInformation.Message as RunScriptResponseMessage;
                if (msg.RequestId == AwaitingResponse)
                {
                    AwaitingResponse = Guid.Empty;
                }
            }
        }

        public bool Run(Guid runAtClient)
        {
            if (runAtClient != Guid.Empty  &&
                m_Plugin.IsConnectionAvailable(runAtClient))
            {
                if (AwaitingResponse == Guid.Empty)
                {
                    AwaitingResponse = Guid.NewGuid();
                    m_Plugin.Messenger.SendNetworkMessage(runAtClient,
                        new RunScriptRequestMessage()
                        {
                            RequestId = AwaitingResponse,
                            ScriptString = ScriptText
                        });
                    return true;
                }
                else
                {
                    Error.WriteEntry("Another script is still running.");
                }
            }
            else
            {
                Error.WriteEntry("Unable to run script, no client selected.");
            }

            return false;
        }

        public void Release()
        {
            if (m_Plugin != null)
            {
                var messenger = m_Plugin.Messenger;
                if (messenger != null)
                {
                    messenger.RemoveCallback(typeof(RunScriptResponseMessage), OnMessageReceived);
                }
            }
        }

        public void SaveAs()
        {
            Microsoft.Win32.SaveFileDialog d = new Microsoft.Win32.SaveFileDialog();
            {
                d.Filter = "Python Files *.py|*.py|All Files *.*|*.*";
                if (d.ShowDialog() == true)
                {
                    try
                    {
                        using (FileStream fs = new FileStream(d.FileName, FileMode.Create, FileAccess.Write))
                        {
                            using (StreamWriter writer = new StreamWriter(fs))
                            {
                                writer.Write(ScriptText);
                                ScriptFileName = d.FileName;
                                IsDirty = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Error.WriteEntry("Error while saving script", ex);
                    }
                }
            }
        }

        public void Save()
        {
            if (!string.IsNullOrEmpty(ScriptFileName) && File.Exists(ScriptFileName))
            {
                try
                {
                    using (FileStream fs = new FileStream(ScriptFileName, FileMode.Create, FileAccess.Write))
                    {
                        using (StreamWriter writer = new StreamWriter(fs))
                        {
                            writer.Write(ScriptText);
                            IsDirty = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Error.WriteEntry("Caught exception while saving script.", ex);
                }
            }
            else
            {
                SaveAs();
            }
        }

        /// <summary>
        /// Called when a property changes.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Called when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
