﻿

namespace CKPythonExtension.Server
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CKLib.Utilities.Connection;
    using CKLib.Utilities.Connection.Extensions;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Proxy;
    using CKLib.Utilities.Security;
    using CKPythonExtension.Messages;
    using System.Windows;
    #endregion
    [LocalOnly]
    [ServerSideExtension(ExtensionName="Remote Python Scripter")]
    public class CKServerPythonExtension : IExtension
    {
        #region Variables
        /// <summary>
        /// Collection of connections which are allowed to use this extension.
        /// </summary>
        private SynchronizedObservableDictionary<Guid, PythonClient> m_AllowedConnections;

        /// <summary>
        /// Reference to the main python GUI.
        /// </summary>
        private PythonMainGui m_MainGui;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the CKServerPythonExtension class.
        /// </summary>
        public CKServerPythonExtension()
        {
            m_MainGui = new PythonMainGui();
            m_AllowedConnections = new SynchronizedObservableDictionary<Guid, PythonClient>(m_MainGui.Dispatcher);
        }
        #endregion

        #region Methods
        private void OnMessageReceived(MessagePair pair)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the IMessenger used by this instance.
        /// </summary>
        public IMessenger Messenger
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the IProxyFactory used by this instance.
        /// </summary>
        public IProxyFactory ProxyFactory
        {
            get;
            private set;
        }

        public ICollection<PythonClient> PythonClients
        {
            get
            {
                return m_AllowedConnections.GetValueNotifier();
            }
        }
        #endregion

        #region IExtension Members
        public string ExtensionName
        {
            get { return "Remote Python Scripter"; }
        }

        public bool Initialize(IMessenger messenger, IProxyFactory proxyFactory)
        {
            Messenger = messenger;
            ProxyFactory = proxyFactory;
            m_MainGui.ServerPlugin = this;

            if (PythonSettings.Instance.PreserveOpenTabs)
            {
                var tabs = PythonSettings.Instance.OpenTabs;
                if (tabs != null)
                {
                    for (int i = 0; i < tabs.Length; i++)
                    {
                        m_MainGui.OpenScript(tabs[i]);
                    }
                }
            }

            return true;
        }

        public Type[] GetCustomMessageTypes()
        {
            return new Type[] {
                typeof(PythonOutputMessage),
                typeof(RunScriptRequestMessage),
                typeof(RunScriptResponseMessage)
            };
        }

        public bool EnableConnectionAvailability(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(CKServerPythonExtension), Messenger, connectionId))
            {
                if (!m_AllowedConnections.ContainsKey(connectionId))
                {
                    m_AllowedConnections.TryAddValue(connectionId, new PythonClient(connectionId, this));
                }

                return true;
            }

            return false;
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            m_AllowedConnections.Remove(connectionId);
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_AllowedConnections.Keys.ToArray();
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            if (LocalOnlyAttribute.CheckAccess(typeof(CKServerPythonExtension), Messenger, connectionId))
            {
                return m_AllowedConnections.ContainsKey(connectionId);
            }

            return false;
        }

        public object UserInterface
        {
            get { return m_MainGui; }
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (m_MainGui != null)
            {
                var scripts = m_MainGui.OpenScripts;
                for (int i = 0; i < scripts.Length; i++)
                {
                    if (scripts[i].IsDirty)
                    {
                        scripts[i].Save();
                    }
                }

                if (PythonSettings.Instance.PreserveOpenTabs)
                {
                    PythonSettings.Instance.OpenTabs = scripts
                        .Where(s => !string.IsNullOrEmpty(s.ScriptFileName))
                        .Select(s => s.ScriptFileName)
                        .ToArray();
                }

                PythonSettings.Instance.Save();
            }
        }
        #endregion
    }
}
