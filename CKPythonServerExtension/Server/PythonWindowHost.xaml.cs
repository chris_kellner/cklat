﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CKPythonExtension.Server
{
    /// <summary>
    /// Interaction logic for PythonWindowHost.xaml
    /// </summary>
    public partial class PythonWindowHost : Window
    {
        public object GridContent
        {
            get { return (UIElementCollection)GetValue(GridContentProperty); }
            set { SetValue(GridContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridContentProperty =
            DependencyProperty.Register("GridContent", typeof(object), typeof(PythonWindowHost), new PropertyMetadata(null, GridContentChanged));

        private static void GridContentChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            PythonWindowHost view = source as PythonWindowHost;
            if (view != null)
            {
                if (e.NewValue != null)
                {
                    view.my_Content.Content = e.NewValue;
                }
            }
        }

        public PythonWindowHost()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            GridContent = null;
        }
    }
}
