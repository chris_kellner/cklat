﻿

namespace CKPythonExtension.Server
{
    #region using List
    using System;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using CKLib.Utilities.Diagnostics;
    using CKLib.Utilities.Messages;
    using CKPythonExtension.Messages;
    using Microsoft.Win32;
    using System.Collections.Generic;
    using CKLib.Controls.Configuration;
    #endregion
    /// <summary>
    /// Interaction logic for PythonMainGui.xaml
    /// </summary>
    public partial class PythonMainGui : UserControl
    {
        #region Dependency Properties

        /// <summary>
        /// Gets or sets the id of the selected client.
        /// </summary>
        public Guid SelectedClient
        {
            get { return (Guid)GetValue(SelectedClientProperty); }
            set { SetValue(SelectedClientProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedClient.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedClientProperty =
            DependencyProperty.Register("SelectedClient", typeof(Guid), typeof(PythonMainGui), new UIPropertyMetadata(Guid.Empty));

        public WpfPythonEditor SelectedEditor
        {
            get { return (WpfPythonEditor)GetValue(SelectedEditorProperty); }
            set { SetValue(SelectedEditorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedEditor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedEditorProperty =
            DependencyProperty.Register("SelectedEditor", typeof(WpfPythonEditor), typeof(PythonMainGui), new UIPropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ServerPlugin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ServerPluginProperty =
            DependencyProperty.Register("ServerPlugin", typeof(CKServerPythonExtension), typeof(PythonMainGui), new UIPropertyMetadata(null, ServerPluginChanged));

        private static void ServerPluginChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            PythonMainGui view = source as PythonMainGui;
            if (view != null)
            {
                if (e.NewValue != null)
                {
                    var oldPlugin = ((CKServerPythonExtension)e.OldValue);
                    if (oldPlugin != null)
                    {
                        oldPlugin.Messenger.RemoveCallback(typeof(PythonOutputMessage), view.OnMessageReceived);
                        oldPlugin.Messenger.RemoveCallback(typeof(RunScriptResponseMessage), view.OnMessageReceived);
                    }

                    var newPlugin = (CKServerPythonExtension)e.NewValue;

                    if (newPlugin != null)
                    {
                        view.list_Clients.ItemsSource = ((CKServerPythonExtension)e.NewValue).PythonClients;
                        newPlugin.Messenger.AddCallback(typeof(PythonOutputMessage), view.OnMessageReceived);
                        newPlugin.Messenger.AddCallback(typeof(RunScriptResponseMessage), view.OnMessageReceived);
                        if (view.tab_Scripts.Items.Count < 1)
                        {
                            view.AddScript(null);
                        }
                    }
                }
            }
        }
        #endregion


        List<RemoteScript> m_OpenScripts = new List<RemoteScript>();


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PythonMainGUI class.
        /// </summary>
        public PythonMainGui()
        {
            DataContext = this;
            InitializeComponent();

            KeyGesture ctrlAltS = new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Alt);
            ApplicationCommands.SaveAs.InputGestures.Add(ctrlAltS);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the server plugin.
        /// </summary>
        public CKServerPythonExtension ServerPlugin
        {
            get
            {
                return (CKServerPythonExtension)GetValue(ServerPluginProperty);
            }
            set
            {
                SetValue(ServerPluginProperty, value);
            }
        }

        private TextBox tb_out
        {
            get
            {
                return SelectedEditor.OutputTextBox;
            }
        }

        public RemoteScript[] OpenScripts
        {
            get
            {
                return m_OpenScripts.ToArray();
            }
        }
        #endregion      

        #region Methods

        private void CommandHandler(object o, ExecutedRoutedEventArgs e)
        {
            RoutedCommand cmd = e.Command as RoutedCommand;
            if (cmd != null)
            {
                switch (cmd.Name)
                {
                    case "New":
                        AddScript(null);
                        break;
                    case "Open":
                        OpenScript();
                        break;
                    case "Save":
                        Save();
                        break;
                    case "SaveAs":
                        SaveAs();
                        break;
                }
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
        }

        private void OnMessageReceived(MessagePair pair)
        {
            if (pair.MessageInformation.Message is PythonOutputMessage)
            {
                var msg = pair.MessageInformation.Message as PythonOutputMessage;
                if (msg.IsError)
                {
                    Instance_PythonError(msg.PythonMessage);
                }
                else
                {
                    Instance_PythonOutput(msg.PythonMessage);
                }
            }
            else if (pair.MessageInformation.Message is RunScriptResponseMessage)
            {
                var msg = pair.MessageInformation.Message as RunScriptResponseMessage;
                Instance_PythonOutput(msg.ScriptResult);
            }
        }

        void Instance_PythonError(string error)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new Action(() => tb_out.AppendText(error)));
            }
            else
            {
                tb_out.AppendText(error);
            }
        }

        void Instance_PythonOutput(string output)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new Action(() => tb_out.AppendText(output)));
            }
            else
            {
                tb_out.AppendText(output);
            }
        }

        /// <summary>
        /// Adds a new script editor to the gui.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private WpfPythonEditor AddScript(string name)
        {
            TabItem page = new TabItem();
            tab_Scripts.Items.Add(page);
            Grid grid = new Grid();
            page.Content = grid;
            var editor = new WpfPythonEditor();
            editor.Script = new RemoteScript(ServerPlugin)
            {
                ScriptFileName = name
            };

            m_OpenScripts.Add(editor.Script);
            editor.SetBinding(WpfPythonEditor.SelectedClientProperty, "SelectedClient");
            grid.Children.Add(editor);
            page.Tag = editor;
            page.Header = editor.Script.FormattedScriptName;
            Binding b = new Binding("FormattedScriptName")
            {
                Source = editor.Script,
                Mode = BindingMode.OneWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };

            page.SetBinding(TabItem.HeaderProperty, b);
            page.IsSelected = true;

            editor.CloseButton.Click += (s, e) =>
            {
                tab_Scripts.Items.Remove(page);
                m_OpenScripts.Remove(editor.Script);
            };

            return editor;
        }

        private void OpenScript()
        {
            OpenFileDialog d = new OpenFileDialog();
            {
                d.Filter = "Python Files *.py|*.py|All Files *.*|*.*";
                if (d.ShowDialog() == true)
                {
                    OpenScript(d.FileName);
                }
            }
        }

        public void OpenScript(string fileName)
        {
            if (File.Exists(fileName))
            {
                var editor = AddScript(fileName);
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(fs))
                    {
                        editor.InputTextBox.AppendText(reader.ReadToEnd());
                        editor.Script.ScriptFileName = fileName;
                        editor.Script.IsDirty = false;
                    }
                }
            }
        }

        private void openScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenScript();
        }

        private void SaveAs()
        {
            SelectedEditor.Script.SaveAs();
        }

        private void Save()
        {
            SelectedEditor.Script.Save();
        }
        #endregion

        private void tab_Scripts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items.Count == 1)
            {
                TabItem selectedItem = items[0] as TabItem;
                if (selectedItem != null)
                {
                    var host = selectedItem.Tag as WpfPythonEditor;
                    if (host != null)
                    {
                        SelectedEditor = host;
                    }
                }
            }
        }

        private void list_Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items.Count == 1)
            {
                var item = (PythonClient)items[0];
                SelectedClient = item.ClientInfo.ConnectionId;
            }
        }

        private void menu_new_Click(object sender, RoutedEventArgs e)
        {
            AddScript(null);
        }

        private void menu_open_Click(object sender, RoutedEventArgs e)
        {
            OpenScript();
        }

        private void menu_save_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void menu_saveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void menu_prefernces_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationWindow host = new ConfigurationWindow();
            host.ConfigurationItems.Add(PythonSettings.Instance);
            host.Title = "Editor Settings";
            host.Show();
        }
    }
}
