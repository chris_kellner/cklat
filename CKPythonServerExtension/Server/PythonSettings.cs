﻿

namespace CKPythonExtension.Server
{
    #region Using List
    using CKLib.Controls.Configuration;
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.DataStructures.Configuration.View;
    using CKLib.Utilities.Patterns;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Media;
    #endregion

    public class PythonSettings : ComponentConfiguration
    {
        #region Variables
        /// <summary>
        /// This converter is used for storing and reading the colors for the python editor.
        /// </summary>
        private BrushConverter m_BrushConverter = new BrushConverter();

        /// <summary>
        /// A dictionary which maintains the references to the property change notifications.
        /// </summary>
        private Dictionary<string, IDisposable> m_PropertyChanges = new Dictionary<string, IDisposable>();
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PythonSettings class.
        /// </summary>
        private PythonSettings()
            : base("CKPythonExtension")
        {
        }

        /// <summary>
        /// Initializes static members of the PythonSettings class.
        /// </summary>
        static PythonSettings()
        {
            LoadConfiguration("CKPythonExtension");
            Singleton<PythonSettings>.ValueFactory = () => new PythonSettings();
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of the PythonSettings class.
        /// </summary>
        [Browsable(false)]
        public static PythonSettings Instance
        {
            get
            {
                return Singleton<PythonSettings>.Instance;
            }
        }

        #region Syntax Highlight
        /// <summary>
        /// The internal collection of syntax rules.
        /// </summary>
        private SyntaxHighlight.SyntaxRuleCollection m_Rules;

        /// <summary>
        /// Gets the rule collection used by the code editor text box.
        /// </summary>
        [Browsable(false)]
        public SyntaxHighlight.SyntaxRuleCollection RuleCollection
        {
            get
            {
                if (m_Rules == null)
                {
                    m_Rules = new SyntaxHighlight.SyntaxRuleCollection();
                    m_Rules.Add(GetRule(SyntaxHighlight.CodeTokenType.Comment));
                    m_Rules.Add(GetRule(SyntaxHighlight.CodeTokenType.Number));
                    m_Rules.Add(GetRule(SyntaxHighlight.CodeTokenType.String));
                    m_Rules.Add(GetRule(SyntaxHighlight.CodeTokenType.Keyword));
                    m_Rules.Add(GetRule(SyntaxHighlight.CodeTokenType.Operator));
                }

                // Make a copy of the rules so that the refence changes and the data binding is satisfied that there
                //  is a difference.
                var tmp = new SyntaxHighlight.SyntaxRuleCollection();
                tmp.AddRange(m_Rules);
                return tmp;
            }
        }

        /// <summary>
        /// Gets the syntax rule for the specified coke token type and establishes a data bind for it.
        /// </summary>
        /// <param name="tokenType">The type of token to get the rule for.</param>
        /// <returns>The syntax rule item for the specified code token type.</returns>
        private SyntaxHighlight.SyntaxRuleItem GetRule(SyntaxHighlight.CodeTokenType tokenType)
        {
            var rule = new SyntaxHighlight.SyntaxRuleItem()
            {
                RuleType = tokenType
            };

            string propertyName = "";
            Func<Brush> getter = null;
            switch (tokenType)
            {
                case SyntaxHighlight.CodeTokenType.Comment:
                    propertyName = "EditorCommentColor";
                    getter = () => EditorCommentColor;
                    break;
                case SyntaxHighlight.CodeTokenType.Keyword:
                    propertyName = "EditorKeywordColor";
                    getter = () => EditorKeywordColor;
                    break;
                case SyntaxHighlight.CodeTokenType.Number:
                    propertyName = "EditorNumberColor";
                    getter = () => EditorNumberColor;
                    break;
                case SyntaxHighlight.CodeTokenType.String:
                    propertyName = "EditorStringColor";
                    getter = () => EditorStringColor;
                    break;
                case SyntaxHighlight.CodeTokenType.Operator:
                    propertyName = "EditorOperatorColor";
                    getter = () => EditorOperatorColor;
                    break;
            }

            if (getter != null && !string.IsNullOrEmpty(propertyName))
            {
                rule.Foreground = getter();

                var disposer = GetWeakNotifier(propertyName, (s, e) =>
                {
                    rule.Foreground = getter();
                    OnPropertyChanged("RuleCollection");
                });

                RegisterDisposable(propertyName, disposer);
                return rule;
            }

            return null;
        }

        /// <summary>
        /// Registers a disposable callback for the specified property key.
        /// </summary>
        /// <param name="key">The name of the property that the callback is for.</param>
        /// <param name="disposer">The callback.</param>
        private void RegisterDisposable(string key, IDisposable disposer)
        {
            if (!m_PropertyChanges.ContainsKey(key))
            {
                m_PropertyChanges.Add(key, disposer);
            }
        }
        #endregion

        /// <summary>
        /// Gets or sets the background color for the editor.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Background Color")]
        [Description("The background color to use for the editor window.")]
        public SolidColorBrush EditorBackgroundColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorBackgroundColor", m_BrushConverter, Brushes.White);
            }

            set
            {
                SetValue("EditorBackgroundColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets the default foreground color to use for the editor.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Default Foreground Color")]
        [Description("The foreground when the symbol does not match any specific definition.")]
        public SolidColorBrush EditorForegroundColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorForegroundColor", m_BrushConverter, Brushes.Black);
            }

            set
            {
                SetValue("EditorForegroundColor", value, m_BrushConverter);
            }
        }

        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Comment Color")]
        [Description("The foreground color to use for Comments.")]
        public SolidColorBrush EditorCommentColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorCommentColor", m_BrushConverter, Brushes.LightGreen);
            }

            set
            {
                SetValue("EditorCommentColor", value, m_BrushConverter);
            }
        }

        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor String Color")]
        [Description("The foreground color to use for Strings.")]
        public SolidColorBrush EditorStringColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorStringColor", m_BrushConverter, Brushes.DarkRed);
            }

            set
            {
                SetValue("EditorStringColor", value, m_BrushConverter);
            }
        }

        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Number Color")]
        [Description("The foreground color to use for Numbers.")]
        public SolidColorBrush EditorNumberColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorNumberColor", m_BrushConverter, Brushes.Cyan);
            }

            set
            {
                SetValue("EditorNumberColor", value, m_BrushConverter);
            }
        }

        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Keyword Color")]
        [Description("The foreground color to use for Keywords.")]
        public SolidColorBrush EditorKeywordColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorKeywordColor", m_BrushConverter, Brushes.Blue);
            }

            set
            {
                SetValue("EditorKeywordColor", value, m_BrushConverter);
            }
        }

        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [DisplayName("Text Editor Operator Color")]
        [Description("The foreground color to use for Operators.")]
        public SolidColorBrush EditorOperatorColor
        {
            get
            {
                return GetValue<SolidColorBrush>("EditorOperatorColor", m_BrushConverter, Brushes.Violet);
            }

            set
            {
                SetValue("EditorOperatorColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to preserve the open scripts for next time upon exit.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [DisplayName("Preserve Open Scripts On Close")]
        [Description("Determines whether or not to restore the currently open scripts on startup.")]
        public bool PreserveOpenTabs
        {
            get
            {
                return GetValue<bool>("PreserveOpenTabs", true);
            }

            set
            {
                SetValue("PreserveOpenTabs", value);
            }
        }

        /// <summary>
        /// If <c>PreserveOpenTabs</c> is true, this indicates the last open tabs upon close/initialization.
        /// </summary>
        [Browsable(false)]
        [ConfigurationEditorBrowsable(false)]
        public string[] OpenTabs
        {
            get
            {
                return GetValues<string>("OpenTabs", new string[0]);
            }

            set
            {
                SetValue("OpenTabs", value);
            }
        }
        #endregion
    }
}
