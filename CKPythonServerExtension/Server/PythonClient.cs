﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CKLib.Utilities.Connection;

namespace CKPythonExtension.Server
{
    public class PythonClient
    {
        private Guid m_Identifier;
        private CKServerPythonExtension m_Plugin;
        private IConnectionInformation m_ClientInfo;
        public PythonClient(Guid identifier, CKServerPythonExtension plugin)
        {
            m_Identifier = identifier;
            m_Plugin = plugin;
            m_ClientInfo = m_Plugin.Messenger.GetConnectionInformation(identifier);
        }

        public IConnectionInformation ClientInfo
        {
            get
            {
                return m_ClientInfo;
            }

            set
            {
                m_ClientInfo = value;
            }
        }

        public override string ToString()
        {
            if (ClientInfo != null)
            {
                return ClientInfo.ToString();
            }

            return base.ToString();
        }
    }
}
