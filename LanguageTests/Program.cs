﻿using CKLib.Utilities.Language;
using CKLib.Utilities.Language.Evaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageTests
{
    class Program
    {
        public static void TestForLoop(Grammar grammar)
        {
            string input = @"
{
    for(int i=0;i<5;i=i + 1){
        Console.WriteLine(i);
    }
    for(int i=0;i<5;i++){
        Console.WriteLine(i);
    }
    for(int i=5;i>= 0;--i){
        Console.WriteLine(i);
    }
    // Skip this one until we can break
    int i = 0;
    for(;;){
        Console.WriteLine(i++);
        if(i > 3)
        {
            Console.WriteLine(""breaking"");
            break;
        }
    }

    int j = 10;
    // prints 9 ... 1
    while(--j > 0){
        Console.WriteLine(j);
    }

    // prints 0 ... 10
    do{
        Console.WriteLine(j);
    }while(j++ < 10);
}
";
            ScriptContext ctx = new ScriptContext();
            ctx.SetGlobal("x", 100);
            ctx.References.ImportNamespace(typeof(int).Assembly, "System");
            grammar.Evaluate<object>(ctx, input);
            Console.WriteLine(input);
            foreach (var var in ctx.Globals)
            {
                Console.WriteLine(var.Key + ": " + var.Value);
            }
        }
 
        static void Main(string[] args)
        {
            MathGrammar grammar = new MathGrammar();
            grammar.AotCompile();
            REPL repl = new REPL(grammar);
            repl.Context.References.ImportNamespace("System");
            repl.Run(Console.In, Console.Out);
            TestForLoop(grammar);
        }
    }
}
