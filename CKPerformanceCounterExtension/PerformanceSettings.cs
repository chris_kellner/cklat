﻿
namespace CKPerformanceCounterExtension
{
    #region Using List
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.DataStructures.Configuration.View;
    using CKLib.Utilities.Patterns;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    #endregion

    public class PerformanceSettings : ComponentConfiguration
    {
        #region Constructors
        private PerformanceSettings()
            : base("PerformanceCounterSettings")
        {
            RawSettings.ComponentName = "CKPerformanceMonitor";
        }

        static PerformanceSettings()
        {
            LoadConfiguration("CKPerformanceMonitor");
            Singleton<PerformanceSettings>.ValueFactory = () => new PerformanceSettings();
        }
        #endregion

        #region Properties
        public static PerformanceSettings Instance
        {
            get
            {
                return Singleton<PerformanceSettings>.Instance;
            }
        }

        [Browsable(false)]
        public CounterId[] SavedCounters
        {
            get
            {
                return GetValues<CounterId>("SavedCounters",
                    new CounterId[]{
                        new CounterId()
                        {
                           CategoryName = "Process",
                           CounterName = "Working Set",
                           CounterScale = 1E6
                        },

                        new CounterId()
                        {
                            CategoryName = "Process",
                            CounterName = "% Processor Time",
                            CounterScale = 1
                        }
                    });
            }

            set
            {
                SetValue("SavedCounters", value);
            }
        }

        [ConfigurationEditorBrowsable(true)]
        [DisplayName("Time to keep samples (minutes)")]
        [Description("Maximum amount of time to keep a sample before 'aging' it out.")]
        public int MaximumCounterAge
        {
            get
            {
                return GetValue("MaximumCounterAge", 5);
            }

            set
            {
                if (value > 10)
                {
                    value = 10;
                }

                if (value < 1)
                {
                    value = 1;
                }

                SetValue("MaximumCounterAge", value);
            }
        }
        #endregion
    }
}
