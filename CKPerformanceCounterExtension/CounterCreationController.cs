﻿using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace CKPerformanceCounterExtension
{
    public class CounterCreationController : INotifyPropertyChanged
    {
        public CounterCreationController()
        {
            Scale = 1.0;
        }

        public CounterCreationController(string[] instanceNames) : this()
        {
            m_InstanceNames = instanceNames;
        }

        #region Variables
        private PerformanceCounterCategory m_CategoryName;

        private PerformanceCounter m_CounterName;

        /// <summary>
        /// The set of valid instance names to seek in the counter categories.
        /// </summary>
        private string[] m_InstanceNames;

        private double m_Scale;

        private PerformanceCounter[] m_AvailableCounters;
        #endregion

        #region Properties
        public double Scale
        {
            get
            {
                return m_Scale;
            }
            set
            {
                m_Scale = value;
                OnPropertyChanged("Scale");
            }
        }

        public string[] InstanceNames
        {
            get
            {
                return m_InstanceNames;
            }

            set
            {
                m_InstanceNames = value;
                OnPropertyChanged("InstanceNames");
                OnPropertyChanged("Categories");
            }
        }

        public PerformanceCounter CounterName
        {
            get
            {
                return m_CounterName;
            }

            set
            {
                m_CounterName = value;
                OnPropertyChanged("CounterName");
            }
        }

        public PerformanceCounterCategory[] Categories
        {
            get
            {
                var categories = from cat in PerformanceCounterCategory.GetCategories()
                                 where m_InstanceNames.Any(name => cat.InstanceExists(name))
                                 select cat;
                return categories.ToArray();
            }
        }

        public PerformanceCounter[] AvailableCounters
        {
            get
            {
                return m_AvailableCounters;
            }
            set
            {
                if (m_AvailableCounters != value)
                {
                    m_AvailableCounters = value;
                    OnPropertyChanged("AvailableCounters");
                }
            }
        }


        public PerformanceCounterCategory CategoryName
        {
            get
            {
                return m_CategoryName;
            }

            set
            {
                if (m_CategoryName != value)
                {
                    m_CategoryName = value;
                    OnPropertyChanged("CategoryName");
                    AvailableCounters = GetCounters(m_CategoryName.CategoryName, m_InstanceNames);
                }
            }
        }
        #endregion

        #region Methods
        public CounterId CollectCounter()
        {
            return new CounterId()
            {
                CategoryName = CategoryName.CategoryName,
                CounterName = CounterName.CounterName,
                CounterScale = Scale
            };
        }

        public static PerformanceCounter[] GetCounters(string categoryName, string[] instanceNames)
        {
            if (PerformanceCounterCategory.Exists(categoryName))
            {
                var category = new PerformanceCounterCategory(categoryName);
                if (category.CategoryType == PerformanceCounterCategoryType.MultiInstance)
                {
                    var counters = (from name in instanceNames
                                    where category.InstanceExists(name)
                                    from counter in category.GetCounters(name)
                                    select counter).Distinct();
                    return counters.ToArray();
                }
                else
                {
                    return category.GetCounters();
                }
            }

            return new PerformanceCounter[0];
        }
        #endregion

        #region INotifyPropertyChanged
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
