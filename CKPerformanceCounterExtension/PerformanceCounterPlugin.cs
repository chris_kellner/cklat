﻿using CKLib.Utilities.Connection;
using CKLib.Utilities.Connection.Extensions;
using CKLib.Utilities.DataStructures;
using CKLib.Utilities.Security;
using CKPerformanceCounterExtension.Gui;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Threading;

namespace CKPerformanceCounterExtension
{
    [ServerSideExtension(ExtensionName = "Performance Monitor", IsStandalone=true)]
    [LocalOnly]
    public class PerformanceCounterPlugin : IExtension
    {
        #region Variables
        private SynchronizedObservableDictionary<Guid, CounterModel> m_ConnectedClients;
        private SynchronizedObservableCollection<CounterId> m_ActiveCounters;
        private DispatcherTimer m_Timer;
        private PerformanceCounterGui m_MainGui;
        private GuiViewModel m_ViewModel;
        private ICollection<CounterModel> m_ValueEnumerator;
        private IEnumerator<System.Drawing.Color> m_ColorEnumerator;
        #endregion

        #region Constructors
        public PerformanceCounterPlugin()
        {
            m_MainGui = new PerformanceCounterGui();
            m_ViewModel = new GuiViewModel();
            m_MainGui.Plugin = this;
            m_MainGui.DataContext = m_ViewModel;
            m_ActiveCounters = new SynchronizedObservableCollection<CounterId>(m_MainGui.Dispatcher);
            m_ConnectedClients = new SynchronizedObservableDictionary<Guid, CounterModel>(m_MainGui.Dispatcher);
            m_ValueEnumerator = m_ConnectedClients.GetValueNotifier();
            m_MainGui.ClientSource = Models;
            m_MainGui.CounterSource = m_ActiveCounters;
            m_Timer = new DispatcherTimer(DispatcherPriority.Normal, m_MainGui.Dispatcher);
            m_Timer.Interval = TimeSpan.FromSeconds(1);
            m_Timer.Tick += m_Timer_Tick;
        }

        private void m_Timer_Tick(object sender, EventArgs e)
        {
            var span = TimeSpan.FromMinutes(PerformanceSettings.Instance.MaximumCounterAge);
            var minTime = DateTime.Now.Subtract(span);
            double minValue = minTime.ToOADate();

            foreach (var model in m_ConnectedClients.ToArray())
            {
                model.Value.Pulse();

                // If there are counters, age them
                if (model.Value.Counters.Any())
                {
                    // If the model has "aged out", get rid of it.
                    if (!model.Value.Age(minValue) &&
                        // Make sure it has actually disconnected.
                        Messenger.GetConnectionInformation(model.Key) == null)
                    {
                        model.Value.Dispose();
                        m_ConnectedClients.Remove(model.Key);
                    }
                }
            }

            m_ViewModel.Update(this);
        }
        #endregion

        #region Properties
        public IMessenger Messenger
        {
            get;
            set;
        }

        public ICollection<CounterModel> Models
        {
            get
            {
                return m_ValueEnumerator;
            }
        }

        public ICollection<CounterId> Counters
        {
            get
            {
                return m_ActiveCounters;
            }
        }
        #endregion

        #region Methods
        public void AddCounter(CounterId counter)
        {
            if (!m_ActiveCounters.Contains(counter))
            {
                CreateChartArea(counter);
                m_ActiveCounters.Add(counter);
                foreach (var model in m_ConnectedClients)
                {
                    model.Value.AddCounter(counter);
                }
            }
        }

        public void RemoveCounter(CounterId counter)
        {
            if (m_ActiveCounters.Contains(counter))
            {             
                foreach (var model in m_ConnectedClients)
                {
                    model.Value.RemoveCounter(counter);
                }

                m_ActiveCounters.Remove(counter);
                DeleteChartArea(counter);
            }
        }

        public void ResetCounters()
        {
            foreach (var model in m_ConnectedClients)
            {
                model.Value.ResetCounters();
            }
        }

        public Series CreateSeries(CounterModel model, CounterId counter)
        {
            Series series = new Series(model.GetSeriesName(counter));
            series.ChartType = SeriesChartType.FastLine;
            series.Color = model.GdiColor;
            series.ChartArea = counter.GetChartAreaName();
            m_MainGui.AddSeries(series);
            return series;
        }

        public void RemoveSeries(Series series)
        {
            m_MainGui.RemoveSeries(series);
        }

        public ChartArea CreateChartArea(CounterId counter)
        {
            ChartArea area = new ChartArea(counter.GetChartAreaName());
            area.AxisX.Enabled = AxisEnabled.False;
            area.AxisY.Title = counter.CounterName;
            m_MainGui.AddChartArea(area);
            return area;
        }

        public void DeleteChartArea(CounterId counter)
        {
            m_MainGui.RemoveChartArea(counter.GetChartAreaName());
        }

        private void ConfigureModel(CounterModel model)
        {
            foreach (var counter in m_ActiveCounters)
            {
                model.AddCounter(counter);
            }
        }

        public System.Drawing.Color GetNextColor()
        {
            if (m_ColorEnumerator == null)
            {
                m_ColorEnumerator = GetColors().GetEnumerator();
            }

            m_ColorEnumerator.MoveNext();
            return m_ColorEnumerator.Current;
        }

        private IEnumerable<System.Drawing.Color> GetColors()
        {
            System.Drawing.Color[] colors = {
                                                System.Drawing.Color.Red,
                                                System.Drawing.Color.Blue,
                                                System.Drawing.Color.Green,
                                                System.Drawing.Color.Purple,
                                                System.Drawing.Color.Yellow,
                                                System.Drawing.Color.Brown,
                                                System.Drawing.Color.Turquoise,
                                                System.Drawing.Color.Orange
                                            };
            int i = 0;
            while (true)
            {
                yield return colors[i++ % colors.Length];
            }
        }
        #endregion

        #region IExtension Members
        public string ExtensionName
        {
            get { return "Performance Monitor"; }
        }

        public bool Initialize(CKLib.Utilities.Connection.IMessenger messenger, CKLib.Utilities.Proxy.IProxyFactory proxyFactory)
        {
            Messenger = messenger;
            foreach (var counter in PerformanceSettings.Instance.SavedCounters)
            {
                AddCounter(counter);
            }

            m_MainGui.IsVisibleChanged += m_MainGui_IsVisibleChanged;
            return true;
        }

        private void m_MainGui_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            m_Timer.IsEnabled = m_MainGui != null && m_MainGui.IsVisible;
        }

        public Type[] GetCustomMessageTypes()
        {
            return Type.EmptyTypes;
        }

        public bool EnableConnectionAvailability(Guid connectionId)
        {
            EnsureInstanceNames();
            if (LocalOnlyAttribute.CheckAccess(typeof(PerformanceCounterPlugin), Messenger, connectionId))
            {
                CounterModel model;
                if (!m_ConnectedClients.TryGetValue(connectionId, out model))
                {
                    model = new CounterModel(connectionId, this);
                    m_ConnectedClients.Add(connectionId, model);
                    ConfigureModel(model);
                }

                model.Enabled = true;
                return true;
            }

            return false;
        }

        private void EnsureInstanceNames()
        {
            foreach (var model in m_ConnectedClients.Values)
            {
                model.EnsureInstanceName();
            }
        }

        public void RevokeConnectionAvailability(Guid connectionId)
        {
            CounterModel model;
            if (m_ConnectedClients.TryGetValue(connectionId, out model))
            {
                model.Enabled = false;
            }

            EnsureInstanceNames();
        }

        public IEnumerable<Guid> GetAvailableConnections()
        {
            return m_ConnectedClients.Keys;
        }

        public bool IsConnectionAvailable(Guid connectionId)
        {
            return m_ConnectedClients.ContainsKey(connectionId);
        }

        public object UserInterface
        {
            get { return m_MainGui; }
        }

        public void Dispose()
        {
            PerformanceSettings.Instance.SavedCounters = m_ActiveCounters.ToArray();
            PerformanceSettings.Instance.Save();
            foreach (var model in m_ConnectedClients)
            {
                model.Value.Dispose();
            }

            m_ConnectedClients.Clear();
            m_MainGui = null;
        }
        #endregion
    }
}
