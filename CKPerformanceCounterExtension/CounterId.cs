﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKPerformanceCounterExtension
{
    public class CounterId
    {
        public string CategoryName { get; set; }
        public string CounterName { get; set; }
        public double CounterScale { get; set; }


        public override bool Equals(object obj)
        {
            if (obj is CounterId)
            {
                return Equals((CounterId)obj);
            }

            return false;
        }

        protected virtual bool Equals(CounterId other)
        {
            if (null != other)
            {
                return CategoryName == other.CategoryName &&
                    CounterName == other.CounterName;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (CategoryName ?? string.Empty).GetHashCode() ^
                (CounterName ?? string.Empty).GetHashCode();
        }

        public string GetChartAreaName()
        {
            return string.Format("{0}: {1}", CategoryName, CounterName);
        }

        public override string ToString()
        {
            return GetChartAreaName();
        }
    }
}
