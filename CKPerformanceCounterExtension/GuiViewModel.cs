﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CKPerformanceCounterExtension
{
    public class GuiViewModel : INotifyPropertyChanged
    {
        #region Variables
        private string m_MinDataAge;
        private string m_MaxDataAge;
        #endregion

        #region Properties
        public string MaxDataAge
        {
            get { return m_MaxDataAge; }
            set
            {
                if (m_MaxDataAge != value)
                {
                    m_MaxDataAge = value;
                    OnPropertyChanged("MaxDataAge");
                }
            }
        }

        public string MinDataAge
        {
            get { return m_MinDataAge; }

            set
            {
                m_MinDataAge = value;
                OnPropertyChanged("MinDataAge");
            }
        }
        #endregion

        #region Methods
        public void Update(PerformanceCounterPlugin plugin)
        {
            var data = from model in plugin.Models
                       from counter in model.Counters
                       from dataPoint in counter.Series.Points
                       select dataPoint;
            if (data.Any())
            {
                var minValue = data.Min(dp => dp.XValue);
                var maxValue = data.Max(dp => dp.XValue);
                var minDt = DateTime.FromOADate(minValue);
                var maxDt = DateTime.FromOADate(maxValue);
                var now = DateTime.Now;
                MinDataAge = (minDt - now).ToString(@"'-'mm\:ss");
                MaxDataAge = (maxDt - now).ToString(@"'-'mm\:ss");
            }
            else
            {
                MinDataAge = "[no data]";
                MaxDataAge = "[no data]";
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
