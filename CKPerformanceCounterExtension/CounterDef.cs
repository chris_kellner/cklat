﻿

namespace CKPerformanceCounterExtension
{
    #region Using List
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms.DataVisualization.Charting;
    #endregion

    public class CounterDef : IDisposable
    {
        public PerformanceCounter Counter { get; set; }
        public Series Series { get; set; }
        public double Scale { get; set; }

        public bool Validate()
        {
            return Counter != null && Series != null && Scale != 0.0;
        }

        public bool Pulse()
        {
            // Try to avoid an exception
            if (PerformanceCounterCategory.InstanceExists(Counter.InstanceName, Counter.CategoryName))
            {
                try
                {
                    Series.Points.AddXY(DateTime.Now, Counter.NextValue() / Scale);
                    return true;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Ages out all points older than the specified min value.
        /// </summary>
        /// <param name="min">The minimum value to keep.</param>
        /// <returns>True if there are still points, false otherwise.</returns>
        public bool Age(double minValue)
        {
            while (Series.Points.Count > 0 &&
                Series.Points[0].XValue < minValue)
            {
                Series.Points.RemoveAt(0);
            }

            return Series.Points.Count > 0;
        }

        public void Reset()
        {
            Series.Points.Clear();
        }

        public void Dispose()
        {
            if (Counter != null)
            {
                Counter.Dispose();
            }

            if (Series != null)
            {
                Series.Enabled = false;
            }

            if (Disposed != null)
            {
                Disposed(this, EventArgs.Empty);
                Disposed = null;
            }
        }

        public event EventHandler Disposed;
    }
}
