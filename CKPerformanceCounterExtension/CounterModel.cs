﻿using CKLib.Utilities.Connection;
using CKLib.Utilities.DataStructures;
using CKLib.Utilities.Diagnostics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace CKPerformanceCounterExtension
{
    public class CounterModel : INotifyPropertyChanged, IDisposable
    {
        #region Variables
        private PerformanceCounterPlugin m_Plugin;

        private bool m_IsVisible;

        private BindingList<CounterDef> m_Counters;
        #endregion

        #region Constructors
        public CounterModel(Guid id, PerformanceCounterPlugin plugin)
        {
            if (plugin == null)
            {
                throw new ArgumentNullException("plugin");
            }

            m_Counters = new BindingList<CounterDef>();
            m_Plugin = plugin;
            GdiColor = m_Plugin.GetNextColor();
            Information = plugin.Messenger.GetConnectionInformation(id);
            m_IsVisible = true;
            m_Counters.ListChanged += m_Counters_ListChanged;
        }
        #endregion

        #region Properties
        public bool Enabled
        {
            get;
            set;
        }

        public IConnectionInformation Information
        {
            get;
            private set;
        }

        public IEnumerable<CounterDef> Counters
        {
            get
            {
                return m_Counters;
            }
        }

        public System.Drawing.Color GdiColor
        {
            get;
            set;
        }

        public Brush MediaColor
        {
            get
            {
                return new SolidColorBrush(Color.FromArgb(GdiColor.A, GdiColor.R, GdiColor.G, GdiColor.B));
            }
        }

        public bool IsVisible
        {
            get
            {
                return m_IsVisible;
            }

            set
            {
                if (m_IsVisible != value)
                {
                    m_IsVisible = value;
                    foreach (var counter in m_Counters)
                    {
                        counter.Series.Enabled = value;
                    }

                    OnPropertyChanged("IsVisible");
                }
            }
        }
        
        #endregion

        #region Methods
        public void Pulse()
        {
            if (Enabled)
            {
                foreach (var counter in m_Counters.ToArray())
                {
                    if (!counter.Pulse())
                    {
                        Error.WriteEntry(string.Format("Counter [0] failed to read value, removing counter.", counter));
                        m_Counters.Remove(counter);
                        counter.Dispose();
                    }
                }
            }
        }

        public void EnsureInstanceName()
        {
            var name = GetInstanceName();
            foreach (var counter in m_Counters)
            {
                if (counter.Counter.InstanceName != name)
                {
                    counter.Counter.InstanceName = name;
                }
            }
        }

        public bool Age(double minValue)
        {
            foreach (var counter in m_Counters.ToArray())
            {
                if (!counter.Age(minValue))
                {
                    counter.Dispose();
                    m_Counters.Remove(counter);
                }
            }

            return m_Counters.Count > 0;
        }

        public void AddCounter(CounterId counter)
        {
            var name = GetInstanceName();
            if (name != string.Empty)
            {
                var series = m_Plugin.CreateSeries(this, counter);
                var c = new PerformanceCounter(counter.CategoryName, counter.CounterName, name);
                var def = new CounterDef()
                {
                    Counter = c,
                    Series = series,
                    Scale = counter.CounterScale
                };

                def.Disposed += def_Disposed;
                m_Counters.Add(def);
            }
        }

        void def_Disposed(object sender, EventArgs e)
        {
            CounterDef def = sender as CounterDef;
            if (def != null)
            {
                m_Plugin.RemoveSeries(def.Series);
            }
        }

        public void RemoveCounter(CounterId counter)
        {
            var def = m_Counters.FirstOrDefault(c =>
            {
                return c.Counter.CategoryName == counter.CategoryName && c.Counter.CounterName == counter.CounterName;
            });

            m_Counters.Remove(def);
            def.Dispose();
        }

        public void ResetCounters()
        {
            foreach (var counter in m_Counters)
            {
                counter.Reset();
            }
        }

        public string GetInstanceName()
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");
            var processId = Information.ProcessId;
            foreach (var name in cat.GetInstanceNames())
            {
                using (var counter = new PerformanceCounter("Process", "ID Process", name))
                {
                    try
                    {
                        if (counter.RawValue == processId)
                        {
                            return name;
                        }
                    }
                    catch
                    {
                        CKLib.Utilities.Diagnostics.Debug.WriteEntry("ID Process Counter: [" + name + "] exited before counter could be read.");
                    }
                }
            }

            return string.Empty;
        }

        public string GetSeriesName(CounterId id)
        {
            return string.Format("[{0}] {1}: {2}", Information.ToString(), id.CategoryName, id.CounterName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Dispose()
        {
            Enabled = false;
            foreach (var counter in m_Counters)
            {
                counter.Dispose();
            }

            m_Counters.Clear();
            m_Plugin = null;
        }

        private void m_Counters_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    OnAdded(m_Counters[e.NewIndex]);
                    break;
            }
        }

        private void OnAdded(CounterDef def)
        {
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
