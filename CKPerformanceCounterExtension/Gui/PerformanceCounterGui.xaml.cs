﻿using CKLib.Controls.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CKPerformanceCounterExtension.Gui
{
    /// <summary>
    /// Interaction logic for PerformanceCounterGui.xaml
    /// </summary>
    public partial class PerformanceCounterGui : UserControl
    {
        public PerformanceCounterGui()
        {
            InitializeComponent();
            UpdateChartColor();
            IsVisibleChanged += PerformanceCounterGui_IsVisibleChanged;
        }

        #region Properties
        public PerformanceCounterPlugin Plugin { get; set; }
        #endregion

        #region Methods
        void PerformanceCounterGui_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
            {
                UpdateChartColor();
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == Control.BackgroundProperty ||
                e.Property == Control.ForegroundProperty ||
                e.Property == Control.StyleProperty)
            {
                UpdateChartColor();
            }
        }

        private void UpdateChartColor()
        {
            SolidColorBrush backBrush = Background as SolidColorBrush;
            if (backBrush != null)
            {
                var backColor = backBrush.Color;
                chart_Main.BackColor = System.Drawing.Color.FromArgb(backColor.A, backColor.R, backColor.G, backColor.B);
            }

            SolidColorBrush foreBrush = Foreground as SolidColorBrush;
            if (foreBrush != null)
            {
                var backColor = foreBrush.Color;
                chart_Main.ForeColor = System.Drawing.Color.FromArgb(backColor.A, backColor.R, backColor.G, backColor.B);
            }
        }

        public IEnumerable ClientSource
        {
            get
            {
                return items_Area.ItemsSource;
            }

            set
            {
                items_Area.ItemsSource = value;
            }
        }

        public IEnumerable CounterSource
        {
            get
            {
                return items_Counters.ItemsSource;
            }

            set
            {
                items_Counters.ItemsSource = value;
            }
        }

        public void AddChartArea(ChartArea area)
        {
            if (chart_Main.InvokeRequired)
            {
                Dispatcher.Invoke(new Action<ChartArea>(AddChartArea), area);
            }
            else
            {
                chart_Main.ChartAreas.Add(area);
            }
        }

        public void RemoveChartArea(string name)
        {
            if (chart_Main.InvokeRequired)
            {
                Dispatcher.Invoke(new Action<string>(RemoveChartArea), name);
            }
            else
            {
                using (var area = chart_Main.ChartAreas[name])
                {
                    if (area != null)
                    {
                        chart_Main.ChartAreas.Remove(area);
                    }
                }
            }
        }

        public void AddSeries(Series series)
        {
            if (chart_Main.InvokeRequired)
            {
                Dispatcher.Invoke(new Action<Series>(AddSeries), series);
            }
            else
            {
                chart_Main.Series.Add(series);
            }
        }

        public void RemoveSeries(Series series)
        {
            if (chart_Main.InvokeRequired)
            {
                Dispatcher.Invoke(new Action<Series>(RemoveSeries), series);
            }
            else
            {
                chart_Main.Series.Remove(series);
            }
        }

        private void btn_Del_Counter_Click(object sender, RoutedEventArgs e)
        {
            var source = sender as Button;
            if (source != null)
            {
                var id = source.CommandParameter as CounterId;
                if (id != null)
                {
                    string message = string.Format(
                        "This will delete all data currently collected for the Performance Counter [{0}]. Are you sure you want to continue?",
                        id);
                    if (MessageBox.Show(message, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        Plugin.RemoveCounter(id);
                    }
                }
            }
        }

        private void menu_AddCounter_Click(object sender, RoutedEventArgs e)
        {
            Window w = new Window();
            w.SizeToContent = SizeToContent.WidthAndHeight;
            AddCounter ac = new AddCounter();
            var names = Plugin.Models.Select(m=>m.GetInstanceName()).ToArray();
            ac.CounterCreator = new CounterCreationController(names);
            w.Content = ac;
            ac.ResultReady += (s, a) =>
            {
                w.DialogResult = ac.DialogResult;
                w.Close();
            };

            if (w.ShowDialog() == true)
            {
                var counter = ac.CounterCreator.CollectCounter();
                if (counter != null)
                {
                    Plugin.AddCounter(counter);
                }
            }
        }

        private void menu_Preferences_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationWindow w = new ConfigurationWindow();
            w.Title = "Performance Counter Settings";
            w.ConfigurationItems.Add(PerformanceSettings.Instance);
            w.Show();
        }

        private void menu_ResetCounters_Click(object sender, RoutedEventArgs e)
        {
            Plugin.ResetCounters();
        }

        private void btn_HideShow_Click(object sender, RoutedEventArgs e)
        {
            bool shown = border_Clients.Visibility == System.Windows.Visibility.Visible;
            btn_HideShow.Content = shown ? "Show Panels" : "Hide Panels";
            border_Clients.Visibility = !shown ? Visibility.Visible : System.Windows.Visibility.Collapsed;
            border_Counters.Visibility = !shown ? Visibility.Visible : System.Windows.Visibility.Collapsed;
        }
        #endregion
    }
}
