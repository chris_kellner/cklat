﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CKPerformanceCounterExtension.Gui
{
    /// <summary>
    /// Interaction logic for AddCounter.xaml
    /// </summary>
    public partial class AddCounter : UserControl
    {


        public CounterCreationController CounterCreator
        {
            get { return (CounterCreationController)GetValue(CounterCreatorProperty); }
            set { SetValue(CounterCreatorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CounterCreator.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CounterCreatorProperty =
            DependencyProperty.Register("CounterCreator", typeof(CounterCreationController), typeof(AddCounter), new PropertyMetadata(null));    

        public AddCounter()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event EventHandler ResultReady = delegate { };

        public bool? DialogResult { get; set; }

        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            ResultReady(this, e);
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            ResultReady(this, e);
        }
    }
}
