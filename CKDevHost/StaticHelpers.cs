﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using CKDevHost.Native;

namespace CKDevHost
{
    public class StaticHelpers
    {
        public static bool AmI64Bit
        {
            get
            {
                return IntPtr.Size == 8;
            }
        }

        public static bool Is64BitOS
        {
            get
            {
                if (AmI64Bit)
                {
                    return true;
                }
                bool is64Bit;
                return ModuleContainsFunction("kernel32.dll", "IsWow64Process") &&
                    Kernel32.IsWow64Process(
                    Kernel32.GetCurrentProcess(), out is64Bit)
                    && is64Bit;
            }
        }

        public static bool ModuleContainsFunction(string module, string function)
        {
            IntPtr mod = Kernel32.GetModuleHandle(module);
            if (mod != IntPtr.Zero)
            {
                return Kernel32.GetProcAddress(mod, function) != UIntPtr.Zero;
            }
            return false;
        }

        public static bool Is64BitProcess(IntPtr procHandle)
        {
            bool is64bit = true;
            if (Is64BitOS)
            {
                Kernel32.IsWow64Process(procHandle, out is64bit);
            }

            return !is64bit;
        }

        private enum MachineType : ushort
        {
            UNKNOWN = 0x0,
            AMD64 = 0x8664,
            IA64 = 0x0200
        }

        public static bool IsUmanagedDll64Bit(string dllPath)
        {
            // see http://www.microsoft.com/whdc/system/platform/firmware/PECOFF.mspx
            // offset to PE header is always at 0x3C
            // PE header starts with "PE\0\0" = 0x50 0x45 0x00 0x00
            // Followed by 2-byte machine type field
            if (File.Exists(dllPath))
            {
                using (FileStream fs = new FileStream(dllPath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        fs.Seek(0x3c, SeekOrigin.Begin);
                        Int32 offset = br.ReadInt32();
                        fs.Seek(offset, SeekOrigin.Begin);
                        UInt32 pHead = br.ReadUInt32();
                        if (pHead != 0x00004550)    // "PE\0\0" little-endian
                        {
                            throw new Exception("File is not a valid portable-executable.");
                        }
                        ushort machineType = br.ReadUInt16();
                        if (machineType != (ushort)MachineType.UNKNOWN)
                        {
                            if (Enum.IsDefined(typeof(MachineType), machineType))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
