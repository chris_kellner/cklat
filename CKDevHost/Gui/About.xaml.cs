﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace CKDevHost
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Gets the product name.
        /// </summary>
        public string ProductName
        {
            get
            {
                return "CK Live Analysis Toolkit";
            }
        }

        /// <summary>
        /// Gets the copyright information.
        /// </summary>
        public string Copyright
        {
            get
            {
                return "Copyright © Christopher Kellner 2012";
            }
        }

        /// <summary>
        /// Gets the disclaimer.
        /// </summary>
        public string Disclaimer
        {
            get
            {
                return "This product is intended for diagnostic purposes only, and should be used only with products owned by you or those you work for.";
            }
        }

        /// <summary>
        /// Gets the about text.
        /// </summary>
        public string AboutText
        {
            get
            {
                return string.Format(
                    "{1}{0}{2}{0}{3}",
                    Environment.NewLine,
                    ProductName,
                    Copyright,
                    Disclaimer);
            }
        }

        /// <summary>
        /// Gets the website address for the product.
        /// </summary>
        public string Website
        {
            get
            {
                return "http://scriptogr.am/lightbringer";
            }
        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Website);
        }
    }
}
