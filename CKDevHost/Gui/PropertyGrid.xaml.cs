﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CKLib.Utilities.DataStructures;

namespace CKDevHost.Gui
{
    /// <summary>
    /// Interaction logic for PropertyGrid.xaml
    /// </summary>
    public partial class PropertyGrid : UserControl
    {

        #region Dependency Properties
        protected static readonly DependencyPropertyKey DisplayedPropertiesPropertyKey =
            DependencyProperty.RegisterReadOnly("DisplayedProperties", typeof(PropertyEnumerator), typeof(PropertyGrid), new UIPropertyMetadata(null));

        public static readonly DependencyProperty DisplayedPropertiesProperty =
            DisplayedPropertiesPropertyKey.DependencyProperty;

        // Using a DependencyProperty as the backing store for DisplayedObject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayedObjectProperty =
            DependencyProperty.Register("DisplayedObject", typeof(object), typeof(PropertyGrid), new UIPropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ShowGridLines.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowGridLinesProperty =
            DependencyProperty.Register("ShowGridLines", typeof(bool), typeof(PropertyGrid), new UIPropertyMetadata(true));

        // Using a DependencyProperty as the backing store for ShowHeaders.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowHeadersProperty =
            DependencyProperty.Register("ShowHeaders", typeof(bool), typeof(PropertyGrid), new UIPropertyMetadata(true));

        
        #endregion

        #region Variables
        #endregion
        public PropertyGrid()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == DisplayedObjectProperty)
            {
                if (Properties != null)
                {
                    Properties.Dispose();
                }

                Properties = new PropertyEnumerator(e.NewValue);
            }
            else if (e.Property == DisplayedPropertiesProperty)
            {
                items_Main.ItemsSource = Properties;
            }
            else if (e.Property == ShowGridLinesProperty)
            {
                bool showLines = (bool)e.NewValue;
                items_Main.GridLinesVisibility = showLines ? DataGridGridLinesVisibility.All : DataGridGridLinesVisibility.None;
            }
            else if (e.Property == ShowHeadersProperty)
            {
                items_Main.HeadersVisibility = (bool)e.NewValue ? DataGridHeadersVisibility.Column : DataGridHeadersVisibility.None;
            }
        }

        public object DisplayedObject
        {
            get { return (object)GetValue(DisplayedObjectProperty); }
            set { SetValue(DisplayedObjectProperty, value); }
        }

        public PropertyEnumerator Properties
        {
            get
            {
                return (PropertyEnumerator)GetValue(DisplayedPropertiesProperty);
            }

            set
            {
                SetValue(DisplayedPropertiesPropertyKey, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid lines should be displayed.
        /// </summary>
        public bool ShowGridLines
        {
            get { return (bool)GetValue(ShowGridLinesProperty); }
            set { SetValue(ShowGridLinesProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the headers will be displayed.
        /// </summary>
        public bool ShowHeaders
        {
            get { return (bool)GetValue(ShowHeadersProperty); }
            set { SetValue(ShowHeadersProperty, value); }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
