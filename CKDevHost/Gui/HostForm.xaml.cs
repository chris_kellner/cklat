﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Linq;
using CKDevClient.Interface;
using CKLib.Utilities.Connection.Server;
using CKLib.Utilities.Messages;
using System.Threading;
using CKDevHost.Properties;
using CKLib.Utilities.Patterns;
using System.Windows.Controls;
using System.Net;
using System.ComponentModel;
using System.Collections.Generic;
using CKDevHost.Gui;
using CKLib.Utilities.Diagnostics;
using CKLib.Controls.Configuration;

namespace CKDevHost
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        #region constants
        /// <summary>
        /// The properties to ignore in the object viewer
        /// </summary>
        string[] IGNORE_PROPERTIES = {
                                             "ExitCode",
                                             "ExitTime",
                                             "StandardInput",
                                             "StandardOutput",
                                             "StandardError",
                                         };

        #endregion
        #region Variables
        /// <summary>
        /// The datacontext for this window.
        /// </summary>
        HostViewModel m_ViewModel;
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the HostForm class.
        /// </summary>
        public Window1()
        {
            InitializeComponent();

            m_ViewModel = App.ViewModel;
            DataContext = m_ViewModel;
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extensionName"></param>
        public void BringExtensionIntoView(string extensionName)
        {
            if (!string.IsNullOrEmpty(extensionName))
            {
                var tabs = tab_MainContent.Items;
                if (tabs != null)
                {
                    foreach (TabItem tab in tabs)
                    {
                        if (extensionName.Equals(tab.Header))
                        {
                            tab.IsSelected = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Occurs when the window loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            m_ViewModel.Logged += new EventHandler<LoggedEventArgs>(m_ViewModel_Logged);
            var extensions = m_ViewModel.DeveloperServer.ExtensionManager.GetLocalExtensions().Select(p => p.Value).ToList();
            foreach (var extension in extensions)
            {
                try
                {
                    TabItem item = new TabItem();
                    item.Content = extension.UserInterface;
                    item.Header = extension.ExtensionName;
                    tab_MainContent.Items.Add(item);
                }
                catch (Exception ex)
                {
                    Error.WriteEntry("Caught exception while attempting to load extension: " + extension.GetType().Name, ex);
                }
            }

            m_ViewModel.ProcessBacklog();
            m_ViewModel.ConfigureResources();
        }

        /// <summary>
        /// Occurs when the window closes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_ViewModel.Release();
        }

        /// <summary>
        /// Appends the input text to the console textbox.
        /// </summary>
        /// <param name="text"></param>
        private void WriteToConsole(string text)
        {
            if (!Dispatcher.CheckAccess())
            {
                Action<string> cb = new Action<string>(WriteToConsole);
                Dispatcher.Invoke(cb, text);
            }
            else
            {
                tb_Console.AppendText(text + Environment.NewLine);
            }
        }

        #endregion
        #region Event Handlers

        #region TreeView
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            ClientProxy client = SelectedClient;
            if (client != null && client.ConnectionOk)
            {
                client.ClientCallback.ResendSharedObjects();
            }
        }

        private void list_Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                KeyValuePair<Guid, ClientProxy> client = (KeyValuePair<Guid, ClientProxy>)e.AddedItems[0];
                m_ViewModel.SelectedClient = client.Value;
            }
        }
        #endregion

        /// <summary>
        /// Occurs when something is logged via the view model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_ViewModel_Logged(object sender, LoggedEventArgs e)
        {
            WriteToConsole(e.LogMessage);
        }

        /// <summary>
        /// Called when the object viewer is adding child nodes.
        /// </summary>
        /// <param name="sender">The object viewer.</param>
        /// <param name="e">The event args.</param>
        private void ObjectViewer_FilterProperty(object sender, CKLib.Controls.ObjectViewer.PropertyFilterEventArgs e)
        {
            var name = e.Property.Name;
            e.RejectProperty = (Array.IndexOf(IGNORE_PROPERTIES, name) >= 0);
        }

        /// <summary>
        /// Requests that the selected client connect to an additional server on the network.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_clientConnect_Click(object sender, RoutedEventArgs e)
        {
            ClientProxy client = SelectedClient;
            if (client != null && client.ConnectionOk)
            {
                var properties = client.GetSharedObject<IClientSettings>();
                if (properties != null)
                {
                    properties.ConnectToServer(tb_serverAdress.Text, tb_serverName.Text);
                }
            }
        }

        /// <summary>
        /// Requests that the client export the dynamic type module. For debugging.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Dump_Click(object sender, RoutedEventArgs e)
        {
            ClientProxy client = SelectedClient;
            if (client != null && client.ConnectionOk)
            {
                var properties = client.GetSharedObject<IClientSettings>();
                if (properties != null)
                {
                    properties.DumpDynamicTypeModule();
                }
            }
        }

        /// <summary>
        /// Scrolls the console when the text changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_Console_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            tb_Console.ScrollToEnd();
        }

        /// <summary>
        /// Kicks the selected client from the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_KickClient_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedClient != null)
            {
                m_ViewModel.DeveloperServer.KickClient(SelectedClient.Identifier);
            }
        }

        /// <summary>
        /// Called when the Show EULA menu item is clicked.
        /// </summary>
        /// <param name="sender">The show EULA menu item.</param>
        /// <param name="e"></param>
        private void EULA_menu_Click(object sender, RoutedEventArgs e)
        {
            EULA eula = new EULA();
            eula.Owner = this;
            eula.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            if (eula.ShowDialog() == false)
            {
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Called when the "--->" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ShowProcessChooserTab_Click(object sender, RoutedEventArgs e)
        {
            ControlHost host = new ControlHost(new ConnectToProcess(), "Running .Net Processes:", new Size(621, 365));
            host.Owner = this;
            host.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            host.Show();
        }

        #region Menu Handlers
        private void menu_StartLocal_Click(object sender, RoutedEventArgs e)
        {
            ServerManager.RunServerAsync(StringTable.MAIN_SERVER_NAME, ServerAccessType.LocalOnly);
        }

        private void menu_StartBoth_Click(object sender, RoutedEventArgs e)
        {
            ServerManager.RunServerAsync(StringTable.MAIN_SERVER_NAME, ServerAccessType.LocalAndNetwork);
        }

        private void menu_StopServer_Click(object sender, RoutedEventArgs e)
        {
            ServerManager.StopServer(StringTable.MAIN_SERVER_NAME);
        }

        private void btn_StartServer_Click(object sender, RoutedEventArgs e)
        {
            ServerManager.RunServerAsync(StringTable.MAIN_SERVER_NAME, 
                cb_NetworkToo.IsChecked == true ? ServerAccessType.LocalAndNetwork : ServerAccessType.LocalOnly);
        }

        private void btn_SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            CKLib.Utilities.DataStructures.Configuration.ConfigurationManager.Save("CKDevHost");
        }

        /// <summary>
        /// Called when the exit menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Called when the preferences menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_Properties_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationWindow window = new ConfigurationWindow();
            window.ConfigurationItems.Add(CKDevHost.Configuration.HostSettings.Instance);
            window.Owner = this;
            window.ShowDialog();
            //Preferences pref = new Preferences();
            //pref.Owner = this;
            //pref.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            //pref.ShowDialog();
        }

        /// <summary>
        /// Called when about is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutItem_Click(object sender, RoutedEventArgs e)
        {
            About abt = new About();
            abt.Owner = this;
            abt.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            abt.ShowDialog();
        }
        #endregion
        #endregion

        #region Properties
        /// <summary>
        /// Gets the currently selected client in the tree view.
        /// </summary>
        public ClientProxy SelectedClient
        {
            get
            {
                return m_ViewModel.SelectedClient;
            }
        }
        #endregion

    }
}
