﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CKDevHost.Gui
{
    /// <summary>
    /// Interaction logic for ConnectToProcess.xaml
    /// </summary>
    public partial class ConnectToProcess : UserControl
    {
        private HostViewModel m_ViewModel;
        private Injector m_Injector;

        public ConnectToProcess()
        {
            m_ViewModel = App.ViewModel;
            m_Injector = new Injector(m_ViewModel);
            DataContext = m_ViewModel;

            InitializeComponent();
        }

        /// <summary>
        /// Scans for new processes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Scan_Click(object sender, RoutedEventArgs e)
        {
            btn_Scan.IsEnabled = false;
            m_ViewModel.StartTask(
                "Scanning for processes...",
                m_ViewModel.DetectedProcesses.ScanForProcesses,
                () => btn_Scan.IsEnabled = true,
                Dispatcher);
        }

        /// <summary>
        /// Connects to the selected process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DetectedProcess dp = listBox1.SelectedItem as DetectedProcess;
            var host = ControlHost.GetMyHost(this);
            if (host != null)
            {
                var oldAuto = m_ViewModel.Settings.AutoConnectProcesses;
                if (!oldAuto.Any(a => a.Equals(dp.ProcessName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    if (DevDialog.Show(
                        host,
                        "Auto Connection Policy:",
                        "Would you like to always automatically connect to this process each time it starts up?",
                        DialogButtons.YesNoCancel) == true)
                    {
                        string[] newAuto = new string[oldAuto.Length + 1];
                        oldAuto.CopyTo(newAuto, 0);
                        newAuto[newAuto.Length - 1] = dp.ProcessName;
                        m_ViewModel.Settings.AutoConnectProcesses = newAuto;
                        m_ViewModel.Settings.Save();
                    }
                }

                m_Injector.DoInjectAsync(dp);
                host.Close();
            }
        }

        string[] IGNORE_PROPERTIES = {
                                             "ExitCode",
                                             "ExitTime",
                                             "StandardInput",
                                             "StandardOutput",
                                             "StandardError",
                                         };

        private void obj_Viewer_FilterProperty(object sender, CKLib.Controls.ObjectViewer.PropertyFilterEventArgs e)
        {
            var name = e.Property.Name;
            e.RejectProperty = (Array.IndexOf(IGNORE_PROPERTIES, name) >= 0);
        }
    }
}
