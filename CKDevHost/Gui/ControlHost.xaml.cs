﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CKDevHost.Gui
{
    /// <summary>
    /// Interaction logic for ControlHost.xaml
    /// </summary>
    public partial class ControlHost : Window
    {

        #region Dependency Properties
        public object HostedContent
        {
            get { return (object)GetValue(HostedContentProperty); }
            set { SetValue(HostedContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HostedContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HostedContentProperty =
            DependencyProperty.Register("HostedContent", typeof(object), typeof(ControlHost), new UIPropertyMetadata(null));



        public static ControlHost GetMyHost(DependencyObject obj)
        {
            return (ControlHost)obj.GetValue(MyHostProperty);
        }

        public static void SetMyHost(DependencyObject obj, ControlHost value)
        {
            obj.SetValue(MyHostProperty, value);
        }

        // Using a DependencyProperty as the backing store for MyHost.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyHostProperty =
            DependencyProperty.RegisterAttached("MyHost", typeof(ControlHost), typeof(ControlHost), new UIPropertyMetadata(null));

        
        #endregion

        public ControlHost(object content, string titleText, Size displaySize)
        {
            DataContext = this;
            InitializeComponent();
            Height = displaySize.Height;
            Width = displaySize.Width;
            Title = titleText;
            HostedContent = content;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == HostedContentProperty)
            {
                var oldObj = e.OldValue as DependencyObject;
                if (oldObj != null)
                {
                    if (GetMyHost(oldObj) == this)
                    {
                        SetMyHost(oldObj, null);
                    }
                }

                var newObj = e.NewValue as DependencyObject;
                if (newObj != null)
                {
                    SetMyHost(newObj, this);
                }
            }
        }
    }
}
