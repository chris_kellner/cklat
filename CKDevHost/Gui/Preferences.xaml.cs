﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CKDevHost.Configuration;
using CKLib.Utilities.DataStructures.Configuration;

namespace CKDevHost
{
    /// <summary>
    /// Interaction logic for Preferences.xaml
    /// </summary>
    public partial class Preferences : Window
    {
        public Preferences()
        {
            InitializeComponent();
            DataContext = HostSettings.Instance;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationManager.Save("CKDevHost");
            Close();
        }

        private void primaryColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HostSettings.Instance.PrimaryColor = ((KeyValuePair<string, SolidColorBrush>)e.AddedItems[0]).Value;
        }

        private void cmb_SecondaryColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HostSettings.Instance.SecondaryColor = ((KeyValuePair<string, SolidColorBrush>)e.AddedItems[0]).Value;
        }
    }
}
