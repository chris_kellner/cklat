﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.IO;
using CKDevHost.Configuration;
using CKLib.Utilities.DataStructures.Configuration;

namespace CKDevHost.Gui
{
    /// <summary>
    /// Interaction logic for EULA.xaml
    /// </summary>
    public partial class EULA : Window
    {
        static readonly string m_EulaText;

        /// <summary>
        /// Initializes a new instance of the EULA window.
        /// </summary>
        public EULA()
        {
            DataContext = this;
            InitializeComponent();
        }

        static EULA()
        {
            using (Stream fs = typeof(EULA).Assembly.GetManifestResourceStream("CKDevHost.CK Live Analysis Toolkit EULA.txt"))
            {
                if (fs != null)
                {
                    using (StreamReader reader = new StreamReader(fs, Encoding.UTF8))
                    {
                        m_EulaText = reader.ReadToEnd();
                    }
                }
            }
        }

        public string EULAText
        {
            get
            {
                return m_EulaText;
            }
        }

        public bool IsValid
        {
            get
            {
                string guid = "{D08FCA18-949F-46c5-A2B5-5F31CB32B97B}";
                byte[] b = Encoding.ASCII.GetBytes(guid);

                using(System.Security.Cryptography.HMACSHA1 sha = new System.Security.Cryptography.HMACSHA1(b)){
                    byte[] a = Encoding.UTF8.GetBytes(m_EulaText);
                    if (a.Length == 1878)
                    {
                        byte[] hash = sha.ComputeHash(a);
                        byte[] expected = { 7, 181, 214, 177, 26, 174, 26, 119, 93, 108, 178, 207, 235, 80, 158, 106, 201, 105, 62, 168 };
                        if (hash.Length == expected.Length)
                        {
                            bool equal = true;
                            for (int i = 0; i < hash.Length; i++)
                            {
                                if (hash[i] != expected[i])
                                {
                                    equal = false;
                                }
                            }

                            return equal;
                        }
                    }
                }

                return false;
            }
        }

        private void btn_Reject_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            HostSettings.Instance.AcceptedEULA = false;
            ConfigurationManager.Save("CKDevHost");
            Close();
        }

        private void btn_Accept_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            HostSettings.Instance.AcceptedEULA = true;
            ConfigurationManager.Save("CKDevHost");
            Close();
        }
    }
}
