﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CKDevHost.Gui
{
    [Flags]
    public enum DialogButtons
    {
        Invalid = 0,
        Cancel = 1,
        OK = 2,
        OKCancel = 3,
        Yes = 4,
        No = 8,
        YesNo = 12,
        YesNoCancel = 13
    }

    /// <summary>
    /// Interaction logic for DevDialog.xaml
    /// </summary>
    public partial class DevDialog : Window
    {

        #region Dependency Properties
        // Using a DependencyProperty as the backing store for CaptionText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaptionTextProperty =
            DependencyProperty.Register("CaptionText", typeof(string), typeof(DevDialog), new UIPropertyMetadata(string.Empty));


        // Using a DependencyProperty as the backing store for DialogButtons.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DialogButtonsProperty =
            DependencyProperty.Register("DialogButtons", typeof(DialogButtons), typeof(DevDialog), new UIPropertyMetadata(DialogButtons.OKCancel));
        #endregion

        /// <summary>
        /// Initializes a new instance of the DevDialog class.
        /// </summary>
        public DevDialog()
        {
            InitializeComponent();
            DialogButtons = Gui.DialogButtons.OKCancel;
        }

        /// <summary>
        /// Initializes a new instance of the DevDialog class.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="caption"></param>
        /// <param name="buttons"></param>
        public DevDialog(string title, string caption, DialogButtons buttons)
            : this()
        {
            Title = title;
            CaptionText = caption;
            DialogButtons = buttons;
        }

        #region Properties
        public DialogButtons DialogButtons
        {
            get { return (DialogButtons)GetValue(DialogButtonsProperty); }
            set { SetValue(DialogButtonsProperty, value); }
        }

        public string CaptionText
        {
            get { return (string)GetValue(CaptionTextProperty); }
            set { SetValue(CaptionTextProperty, value); }
        }
        #endregion

        public static bool? Show(Window owner, string title, string caption, DialogButtons buttons)
        {
            DevDialog dialog = new DevDialog(title, caption, buttons);
            if (owner != null)
            {
                dialog.Owner = owner;
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }

            return dialog.ShowDialog();
        }

        public static bool? Show(string title, string caption, DialogButtons buttons)
        {
            return Show(null, title, caption, buttons);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == DialogButtonsProperty)
            {
                DialogButtons btns = (DialogButtons)e.NewValue;
                btn_CANCEL.Visibility = btns.HasFlag(DialogButtons.Cancel) ? Visibility.Visible : Visibility.Collapsed;
                btn_OK.Visibility = btns.HasFlag(DialogButtons.OK) ? Visibility.Visible : Visibility.Collapsed;
                btn_YES.Visibility = btns.HasFlag(DialogButtons.Yes) ? Visibility.Visible : Visibility.Collapsed;
                btn_NO.Visibility = btns.HasFlag(DialogButtons.No) ? Visibility.Visible : Visibility.Collapsed;
            }
            else if (e.Property == CaptionTextProperty)
            {
                lbl_Caption.Text = (string)e.NewValue;
            }
        }

        private void btn_NO_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btn_YES_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btn_CANCEL_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = null;
            Close();
        }
    }
}
