﻿Copyright © 2012 Christopher Kellner
License and Liability

LICENSE

This License does not provide you with title to or ownership of the software program "CK Runtime Analysis Toolkit" (the "Software") but only a right of limited use of the Software, and ownership of the media on which a copy of the Software is reproduced.  The Software, including its source code, is, and shall remain, the property of Christopher Kellner, (the "Author").  You are free to copy the software and documentation as many times as you would like, provided that you reproduce all proprietary notices (e.g., copyright and trademark) in the same form as in the original. The term "copy" as used in this License means any reproduction of the Software, in whole or in part, in any form whatsoever, including without limitation, print-outs on any legible material, duplication in memory devices of any type, and handwritten or oral duplication or reproduction. All rights not specifically granted in this Agreement are reserved by the Author.
 
You shall not, in any way, modify or enhance the  Software.  User-created  extensions may be distributed by you, free of charge, or may be sold, licensed, or included as part of any package or product that is sold or licensed, without the prior written consent of the Author.

LIMITATION OF LIABILITY
THE AUTHOR MAKES NO WARRANTY OR REPRESENTATION, EXPRESS, IMPLIED, OR ANY WARRANTY ARISING FROM A COURSE OF DEALING, TRADE USAGE, OR TRADE PRACTICE WITH RESPECT TO THE SOFTWARE OR RELATED MATERIALS, THEIR QUALITY, PERFORMANCE, MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE.  AS A RESULT, THE SOFTWARE AND RELATED MATERIALS ARE LICENSED "AS IS."  IN NO EVENT WILL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES RESULTING FROM POSSESSION, USE, OR MALFUNCTION OF THE SOFTWARE AND RELATED MATERIALS.