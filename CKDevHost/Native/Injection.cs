using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using System.Text;
using System.Collections.Generic;
using CKDevHost.Native;
using System.Windows;

namespace CKDevHost.Native
{
    public class UnsafeNativeMethods {

        public const uint PROCESS_SYNCHRONIZE = 0x00100000;
        public const uint PROCESS_STANDARD_RIGHTS_REQUIRED = 0x000F0000;
        public const uint PROCESS_ALL_ACCESS = (PROCESS_STANDARD_RIGHTS_REQUIRED | PROCESS_SYNCHRONIZE | 0xFFFF);
        public const uint LIST_MODULES_ALL = 0x03;

        const uint THREAD_CREATE_RUNNING = 0x00000000;
        const uint THREAD_CREATE_SUSPENDED = 0x00000004;

        const int TOKEN_QUERY = (0x0008);
        const int TOKEN_ADJUST_PRIVILEGES = (0x0020);
        const int SE_PRIVILEGE_ENABLED = (0x00000002);
        const string SE_DEBUG_NAME = ("SeDebugPrivilege");

        private enum WaitFlags
        {
            /// <summary>
            /// The state of the specified object is signaled.
            /// </summary>
            WAIT_OBJECT_0 = 0x00000000,

            /// <summary>
            /// The specified object is a mutex object that was not released by the thread that owned the mutex object before the owning thread terminated.
            /// Ownership of the mutex object is granted to the calling thread and the mutex state is set to nonsignaled.
            /// If the mutex was protecting persistent state information, you should check it for consistency.
            /// </summary>
            WAIT_ABANDONED = 0x00000080,

            /// <summary>
            /// The time-out interval elapsed, and the object's state is nonsignaled.
            /// </summary>
            WAIT_TIMEOUT = 0x00000102,

            /// <summary>
            /// The function has failed. To get extended error information, call GetLastError.
            /// </summary>
            WAIT_FAILED = -1    //0xFFFFFFFF
        }


        public static bool EnableDebugPriv(){
            IntPtr hToken = IntPtr.Zero;
	        long sedebugnameValue = 0;
            Advapi32.TokPriv1Luid tkp;

            if (Advapi32.OpenProcessToken(Kernel32.GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref hToken))
                if (Advapi32.LookupPrivilegeValue(null, SE_DEBUG_NAME, ref sedebugnameValue)) 
		        {
                    tkp.Count = 1;
                    tkp.Luid = sedebugnameValue;
                    tkp.Attr = SE_PRIVILEGE_ENABLED;

                    if (Advapi32.AdjustTokenPrivileges(hToken, false, ref tkp, 0, IntPtr.Zero, IntPtr.Zero)) 
			        {
				        Kernel32.CloseHandle(hToken);
				        return true;
 			        }
		        }

            Kernel32.CloseHandle(hToken);

	        return false; 
        }

        public static Dictionary<string, IntPtr> GetWOW64ProcessModules(IntPtr hProcess)
        {
            Dictionary<string, IntPtr> mapping = new Dictionary<string, IntPtr>();
            bool iswow64;
            if (hProcess != IntPtr.Zero)
            {
                Kernel32.IsWow64Process(hProcess, out iswow64);
                if (iswow64)
                {
                    IntPtr[] hModules = new IntPtr[0];
                    int lpcbNeeded = 0;
                    try
                    {
                        Psapi.EnumProcessModulesEx(hProcess, hModules, 0, out lpcbNeeded, LIST_MODULES_ALL);
                        hModules = new IntPtr[lpcbNeeded / IntPtr.Size];
                        Psapi.EnumProcessModulesEx(hProcess, hModules, lpcbNeeded, out lpcbNeeded, LIST_MODULES_ALL);
                    }
                    catch (EntryPointNotFoundException e)
                    {
                        Debug.WriteLine(e.Message);
                        return mapping;
                    }

                    foreach (var module in hModules)
                    {
                        StringBuilder modName = new StringBuilder(256);
                        if (Psapi.GetModuleFileNameEx(hProcess, module, modName, modName.Capacity) != 0)
                        {
                            mapping.Add(modName.ToString(), module);
                        }
                    }
                }
            }
            return mapping;
        }

        public static bool InvokeRemoteMethod(DetectedProcess process, string dllName, string functionName, string parameter)
        {
            if (process.IsLoaded)
            {
                if (process.ProcessContainsModule(dllName))
                {
                    var pId = Kernel32.OpenProcess(UnsafeNativeMethods.PROCESS_ALL_ACCESS, 0, process.Id);
                    UIntPtr function = process.GetRemoteProcAddress(dllName, functionName);

                    // Check to make sure the pointers are valid
                    if (pId == IntPtr.Zero || function == UIntPtr.Zero)
                    {
                        return false;
                    }

                    IntPtr bytesout;
                    IntPtr parameterPointer = IntPtr.Zero;
                    if (!string.IsNullOrEmpty(parameter))
                    {
                        // Length of string containing the DLL file name +1 byte padding
                        Int32 LenWrite = parameter.Length + 1;
                        // Allocate memory within the virtual address space of the target process
                        // Needs PROCESS_VM_OPERATION access right
                        parameterPointer = (IntPtr)Kernel32.VirtualAllocEx(pId, (IntPtr)null, (uint)LenWrite, 0x1000, 0x40); //allocation for WriteProcessMemory
                        if (parameterPointer == IntPtr.Zero)
                        {
                            //Memory allocation failed
                            MessageBox.Show(" Memory Allocation [ 0 ] Error! \n " + Kernel32.GetError());
                            return false;
                        }

                        // Write DLL file name to allocated memory in target process
                        if (!Kernel32.WriteProcessMemory(pId, parameterPointer, parameter, (UIntPtr)LenWrite, out bytesout))
                        {
                            Kernel32.VirtualFreeEx(pId, parameterPointer, (UIntPtr)0, 0x8000);
                            return false;
                        }
                    }

                    // Create thread in target process, and store handle in hThread
                    IntPtr hThread = (IntPtr)Kernel32.CreateRemoteThread(pId, (IntPtr)null, 0, function, parameterPointer, THREAD_CREATE_RUNNING, out bytesout);

                    // Make sure thread handle is valid
                    if (hThread == IntPtr.Zero)
                    {
                        //incorrect thread handle ... return failed
                        MessageBox.Show(" hThread [ 1 ] Error! \n " + Kernel32.GetError());
                        return false;
                    }

                    // Set Time-out to 10 seconds
                    WaitFlags result = (WaitFlags)Kernel32.WaitForSingleObject(hThread, 10 * 1000);

                    // Check whether the thread timed out
                    if (result != WaitFlags.WAIT_OBJECT_0)
                    {
                        // Make sure thread handle is valid before closing
                        if (hThread != IntPtr.Zero)
                        {
                            //Close thread in target process
                            Kernel32.CloseHandle(hThread);
                            hThread = IntPtr.Zero;
                        }

                        return false;
                    }

                    // Sleep for 1 second to allow the other process to finish.
                    Thread.Sleep(1000);

                    if (parameterPointer != IntPtr.Zero)
                    {
                        // Clean up memory
                        Kernel32.VirtualFreeEx(pId, parameterPointer, (UIntPtr)0, 0x8000);
                    }

                    // Make sure thread handle is valid before closing
                    if (hThread != IntPtr.Zero)
                    {
                        //Close thread in target process
                        Kernel32.CloseHandle(hThread);
                    }

                    return true;
                }
            }

            return false;
        }
    }
}
