﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace CKDevHost.Native
{
    public class PEHelper
    {
        const uint IMAGE_DIRECTORY_ENTRY_EXPORT = 0;   // Export Directory
        const uint IMAGE_SIZEOF_FILE_HEADER = 20;
        const uint IMAGE_NT_SIGNATURE = 0x00004550;  // PE00
        const int IMAGE_NUMBEROF_DIRECTORY_ENTRIES = 16;

        const ushort IMAGE_NT_OPTIONAL_HDR32_MAGIC = 0x10b;
        const ushort IMAGE_NT_OPTIONAL_HDR64_MAGIC = 0x20b;
        const ushort IMAGE_ROM_OPTIONAL_HDR_MAGIC = 0x107;
        const int SIZEOF_IMAGEDATA_DIRECTORY = 8;

        static uint IMAGE_FIRST_SECTION(IMAGE_NT_HEADERS ntheader)
        {
            return (uint)(0x3c + 4 + 20 + ntheader.FileHeader.SizeOfOptionalHeader);
        }

        const int IMAGE_SIZEOF_SHORT_NAME = 8;
   
        [StructLayout(LayoutKind.Explicit)]
        struct IMAGE_SECTION_HEADER
        {
            [FieldOffset(0)]
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = IMAGE_SIZEOF_SHORT_NAME)]
            public char[] Name;
            //union {
            [FieldOffset(8)]
            public uint VirtualSize;
            //} Misc;
            [FieldOffset(12)]
            public uint VirtualAddress;
            [FieldOffset(16)]
            public uint SizeOfRawData;
            [FieldOffset(20)]
            public uint PointerToRawData;
            [FieldOffset(24)]
            public uint PointerToRelocations;
            [FieldOffset(28)]
            public uint PointerToLinenumbers;
            [FieldOffset(32)]
            public ushort NumberOfRelocations;
            [FieldOffset(34)]
            public ushort NumberOfLinenumbers;
            [FieldOffset(36)]
            public uint Characteristics;
        }

        const int IMAGE_SIZEOF_SECTION_HEADER = 40;

    //    #define IMAGE_FIRST_SECTION( ntheader ) ((PIMAGE_SECTION_HEADER)        \
    //((ULONG_PTR)(ntheader) +                                            \
    // FIELD_OFFSET( IMAGE_NT_HEADERS, OptionalHeader ) +                 \
    // ((ntheader))->FileHeader.SizeOfOptionalHeader   \
    //))

        [StructLayout(LayoutKind.Sequential)]
        struct IMAGE_DOS_HEADER
        {      // DOS .EXE header
            public ushort e_magic;                     // Magic number
            public ushort e_cblp;                      // Bytes on last page of file
            public ushort e_cp;                        // Pages in file
            public ushort e_crlc;                      // Relocations
            public ushort e_cparhdr;                   // Size of header in paragraphs
            public ushort e_minalloc;                  // Minimum extra paragraphs needed
            public ushort e_maxalloc;                  // Maximum extra paragraphs needed
            public ushort e_ss;                        // Initial (relative) SS value
            public ushort e_sp;                        // Initial SP value
            public ushort e_csum;                      // Checksum
            public ushort e_ip;                        // Initial IP value
            public ushort e_cs;                        // Initial (relative) CS value
            public ushort e_lfarlc;                    // File address of relocation table
            public ushort e_ovno;                      // Overlay number
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=4)]
            public ushort[] e_res;                    // Reserved words
            public ushort e_oemid;                     // OEM identifier (for e_oeminfo)
            public ushort e_oeminfo;                   // OEM information; e_oemid specific
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public ushort[] e_res2;                  // Reserved words
            public long e_lfanew;                    // File address of new exe header
        }

        /// <summary>
        /// Pointer to IMAGE_NT_HEADER is located at 0x3c
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_NT_HEADERS64
        {
            /// <summary>
            /// 0x3c in the file
            /// </summary>
            public uint Signature;
            /// <summary>
            /// 0x3c + 4
            /// </summary>
            public IMAGE_FILE_HEADER FileHeader;
            /// <summary>
            /// 0x3c + 4 + 20
            /// </summary>
            public IMAGE_OPTIONAL_HEADER64 OptionalHeader;
        }

        /// <summary>
        /// Pointer to IMAGE_NT_HEADER is located at 0x3c
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_NT_HEADERS
        {
            /// <summary>
            /// 0x3c in the file
            /// </summary>
            public uint Signature;
            /// <summary>
            /// 0x3c + 4
            /// </summary>
            public IMAGE_FILE_HEADER FileHeader;
            /// <summary>
            /// 0x3c + 4 + 20
            /// </summary>
            public IMAGE_OPTIONAL_HEADER OptionalHeader;
        } 

        /// <summary>
        /// Pointer to IMAGE_NT_HEADER is located at 0x3c
        /// address = 0x3c
        ///
        /// address  : IMAGE_NT_SIGNATURE
        /// address+4: Machine
        /// address+6: NumberOfSections
        /// etc...
        /// address+22: Characteristics
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_FILE_HEADER
        {
            public static IMAGE_FILE_HEADER FromStream(BinaryReader reader)
            {
                return new IMAGE_FILE_HEADER()
                {
                    Machine = reader.ReadUInt16(),
                    NumberOfSections = reader.ReadUInt16(),
                    TimeDateStamp = reader.ReadUInt32(),
                    PointerToSymbolTable = reader.ReadUInt32(),
                    NumberOfSymbols = reader.ReadUInt32(),
                    SizeOfOptionalHeader = reader.ReadUInt16(),
                    Characteristics = reader.ReadUInt16(),
                };
            }
            public ushort Machine;
            public ushort NumberOfSections;
            public uint TimeDateStamp;
            public uint PointerToSymbolTable;
            public uint NumberOfSymbols;
            /// <summary>
            /// Size of the IMAGE_OPTIONAL_HEADER/64
            /// </summary>
            public ushort SizeOfOptionalHeader;
            public ushort Characteristics;
        }

        /// <summary>
        /// Export Format
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct IMAGE_EXPORT_DIRECTORY
        {
            public static IMAGE_EXPORT_DIRECTORY FromStream(BinaryReader reader)
            {
                return new IMAGE_EXPORT_DIRECTORY()
                {
                    Characteristics =reader.ReadUInt32(),
                    TimeDateStamp = reader.ReadUInt32(),
                    MajorVersion = reader.ReadUInt16(),
                    MinorVersion = reader.ReadUInt16(),
                    Name = reader.ReadUInt32(),
                    Base = reader.ReadUInt32(),
                    NumberOfFunctions = reader.ReadUInt32(),
                    NumberOfNames = reader.ReadUInt32(),
                    AddressOfFunctions = reader.ReadUInt32(),     // RVA from base of image
                    AddressOfNames = reader.ReadUInt32(),         // RVA from base of image
                    AddressOfNameOrdinals = reader.ReadUInt32(),  // RVA from base of image
                };
            }
            public uint Characteristics;
            public uint TimeDateStamp;
            public ushort MajorVersion;
            public ushort MinorVersion;
            public uint Name;
            public uint Base;
            public uint NumberOfFunctions;
            public uint NumberOfNames;
            public uint AddressOfFunctions;     // RVA from base of image
            public uint AddressOfNames;         // RVA from base of image
            public uint AddressOfNameOrdinals;  // RVA from base of image
        }

        //
        // Optional header format.
        //
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_OPTIONAL_HEADER
        {
            //
            // Standard fields.
            //

            public ushort Magic;
            public byte MajorLinkerVersion;
            public byte MinorLinkerVersion;
            public uint SizeOfCode;
            public uint SizeOfInitializedData;
            public uint SizeOfUninitializedData;
            public uint AddressOfEntryPoint;
            public uint BaseOfCode;
            public uint BaseOfData;

            //
            // NT additional fields.
            //

            public uint ImageBase;
            public uint SectionAlignment;
            public uint FileAlignment;
            public ushort MajorOperatingSystemVersion;
            public ushort MinorOperatingSystemVersion;
            public ushort MajorImageVersion;
            public ushort MinorImageVersion;
            public ushort MajorSubsystemVersion;
            public ushort MinorSubsystemVersion;
            public uint Win32VersionValue;
            public uint SizeOfImage;
            public uint SizeOfHeaders;
            public uint CheckSum;
            public ushort Subsystem;
            public ushort DllCharacteristics;
            public uint SizeOfStackReserve;
            public uint SizeOfStackCommit;
            public uint SizeOfHeapReserve;
            public uint SizeOfHeapCommit;
            public uint LoaderFlags;
            public uint NumberOfRvaAndSizes;
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = IMAGE_NUMBEROF_DIRECTORY_ENTRIES)]
            public IMAGE_DATA_DIRECTORY[] DataDirectory;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_OPTIONAL_HEADER64
        {
            public ushort Magic;
            public byte MajorLinkerVersion;
            public byte MinorLinkerVersion;
            public uint SizeOfCode;
            public uint SizeOfInitializedData;
            public uint SizeOfUninitializedData;
            public uint AddressOfEntryPoint;
            public uint BaseOfCode;
            public ulong ImageBase;
            public uint SectionAlignment;
            public uint FileAlignment;
            public ushort MajorOperatingSystemVersion;
            public ushort MinorOperatingSystemVersion;
            public ushort MajorImageVersion;
            public ushort MinorImageVersion;
            public ushort MajorSubsystemVersion;
            public ushort MinorSubsystemVersion;
            public uint Win32VersionValue;
            public uint SizeOfImage;
            public uint SizeOfHeaders;
            public uint CheckSum;
            public ushort Subsystem;
            public ushort DllCharacteristics;
            public ulong SizeOfStackReserve;
            public ulong SizeOfStackCommit;
            public ulong SizeOfHeapReserve;
            public ulong SizeOfHeapCommit;
            public uint LoaderFlags;
            public uint NumberOfRvaAndSizes;
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst=IMAGE_NUMBEROF_DIRECTORY_ENTRIES)]
            public IMAGE_DATA_DIRECTORY[] DataDirectory;
        }

        //
        // Directory format.
        //
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_DATA_DIRECTORY
        {
            public uint VirtualAddress;
            public uint Size;
        }

        private enum MachineType : ushort
        {
            UNKNOWN = 0x0,
            AMD64 = 0x8664,
            IA64 = 0200
        }

        
        public static T ReadStruct<T>(BinaryReader reader)
        {
            byte[] b = reader.ReadBytes(Marshal.SizeOf(typeof(T)));
            var handle = GCHandle.Alloc(b, GCHandleType.Pinned);
            try
            {
                T structure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
                return structure;
            }
            finally
            {
                handle.Free();
            }
        }


        public static Dictionary<string, UIntPtr> GetFunctionOffsetAddresses(string dllPath)
        {
            Dictionary<string, UIntPtr> functions = new Dictionary<string, UIntPtr>();
            // see http://www.microsoft.com/whdc/system/platform/firmware/PECOFF.mspx
            // offset to PE header is always at 0x3C
            // PE header starts with "PE\0\0" = 0x50 0x45 0x00 0x00
            // Followed by 2-byte machine type field
            if (File.Exists(dllPath))
            {
                using (FileStream fs = new FileStream(dllPath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs, Encoding.ASCII))
                    {
                        //IMAGE_DOS_HEADER dosHeader = ReadStruct<IMAGE_DOS_HEADER>(br);
                        fs.Seek(0x3c, SeekOrigin.Begin);
                        Int32 offset = br.ReadInt32();
                        fs.Seek(offset, SeekOrigin.Begin);
                        UInt32 pHead = br.ReadUInt32();
                        
                        if (pHead != IMAGE_NT_SIGNATURE)    // "PE\0\0" little-endian
                        {
                            throw new Exception("File is not a valid portable-executable.");
                        }
                        //IMAGE_FILE_HEADER fileHeader = IMAGE_FILE_HEADER.FromStream(br);
                        IMAGE_FILE_HEADER fileHeader = ReadStruct<IMAGE_FILE_HEADER>(br);

                        ushort machineType = fileHeader.Machine;
                        bool is64Bit = false;
                        if (machineType != (ushort)MachineType.UNKNOWN)
                        {
                            if (Enum.IsDefined(typeof(MachineType), machineType))
                            {
                                is64Bit = true;
                            }
                        }
                        else
                        {
                            return functions;
                        }
                        IMAGE_DATA_DIRECTORY exportDirectory = default(IMAGE_DATA_DIRECTORY);
                        if (!is64Bit)
                        {
                            //IMAGE_OPTIONAL_HEADER optionalHeader = IMAGE_OPTIONAL_HEADER.FromStream(br);
                            IMAGE_OPTIONAL_HEADER optionalHeader = ReadStruct<IMAGE_OPTIONAL_HEADER>(br);
                            exportDirectory = optionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
                        }
                        else
                        {
                            IMAGE_OPTIONAL_HEADER64 optionalHeader = ReadStruct<IMAGE_OPTIONAL_HEADER64>(br);
                            exportDirectory = optionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
                        }
                        //UInt32 exportPtr = optionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
                        //fs.Seek(exportPtr, SeekOrigin.Begin);
                        // should now be at the .export section!
                        int nSec = fileHeader.NumberOfSections;
                        IMAGE_SECTION_HEADER[] sec = new IMAGE_SECTION_HEADER[nSec];
                        for (int i = 0; i < nSec; i++)
                        {
                            sec[i] = ReadStruct<IMAGE_SECTION_HEADER>(br);
                        }

                        // = optionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
                        uint exportOffset = RVAToOffset(exportDirectory.VirtualAddress, sec);
                        if (exportOffset > 0)
                        {
                            fs.Seek(exportOffset, SeekOrigin.Begin);
                            IMAGE_EXPORT_DIRECTORY exports = ReadStruct<IMAGE_EXPORT_DIRECTORY>(br);

                            uint nameOffset = RVAToOffset(exports.AddressOfNames, sec);
                            uint funcOffset = RVAToOffset(exports.AddressOfFunctions, sec);
                            uint ordinalOffset = RVAToOffset(exports.AddressOfNameOrdinals, sec);

                            uint length = Math.Min(exports.NumberOfFunctions, exports.NumberOfNames);

                            string[] names = new string[length];
                            uint[] addresses = new uint[length];
                            uint[] ordinals = new uint[length];

                            // Read in all named-function names:
                            fs.Seek(nameOffset, SeekOrigin.Begin);
                            for (int i = 0; i < length; i++)
                            {
                                uint stringLocation = br.ReadUInt32();
                                uint theStringStart = RVAToOffset(stringLocation, sec);
                                fs.Seek(theStringStart, SeekOrigin.Begin);
                                names[i] = ReadString(br);
                                fs.Seek(nameOffset + (i + 1) * 4, SeekOrigin.Begin);
                            }

                            // Read in all named-function offsets:
                            fs.Seek(funcOffset, SeekOrigin.Begin);
                            for (int i = 0; i < length; i++)
                            {
                                addresses[i] = br.ReadUInt32();
                            }

                            // Read in all named-function ordinals
                            fs.Seek(ordinalOffset, SeekOrigin.Begin);
                            for (int i = 0; i < length; i++)
                            {
                                ordinals[i] = (uint)(br.ReadUInt16() + (exports.Base));

                                int ord = (int)ordinals[i] - (int)exports.Base;
                                if (ord >= 0 && ord < addresses.Length)
                                {
                                    functions[names[i]] = (UIntPtr)addresses[ord];
                                }
                            }
                        }
                    }
                }
            }
            return functions;
        }

        private static string ReadString(BinaryReader reader)
        {
            StringBuilder builder = new StringBuilder();
            char theChar;
            while (reader.PeekChar() == 0)
            {
                reader.ReadChar();
            }
            while((theChar = reader.ReadChar()) != '\0'){
                builder.Append(theChar);
            }
            return builder.ToString();
        }

        private static uint RVAToOffset(uint rva, IMAGE_SECTION_HEADER[] sections)
        {
            // search all sections for the RVA
            for (int i = 0; i < sections.Length; i++)
            {
                if (rva >= sections[i].VirtualAddress && rva < sections[i].VirtualAddress + sections[i].VirtualSize)
                {
                    return (uint)(rva - sections[i].VirtualAddress + sections[i].PointerToRawData);
                }
            }

            return 0;
        }
    }
}
