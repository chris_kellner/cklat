﻿

namespace CKDevHost.Configuration
{
    #region Using List
    using CKLib.Controls.Configuration;
    using CKLib.Utilities.DataStructures.Configuration;
    using CKLib.Utilities.DataStructures.Configuration.View;
    using CKLib.Utilities.Patterns;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Media;
    #endregion

    [DisplayName("")]
    public class HostSettings : ComponentConfiguration
    {
        #region Variables
        /// <summary>
        /// Stores the reference to the brush converter.
        /// </summary>
        private BrushConverter m_BrushConverter;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes static members of the HostSettings class.
        /// </summary>
        static HostSettings()
        {
            Singleton<HostSettings>.ValueFactory = () => new HostSettings();
        }

        /// <summary>
        /// Prevents extenal instantiation of the HostSettings class.
        /// </summary>
        private HostSettings()
            : base("CKDevHost")
        {
            m_BrushConverter = new BrushConverter();
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the singleton instance of the HostSettings class.
        /// </summary>
        [Browsable(false)]
        [ConfigurationEditorBrowsable(false)]
        public static HostSettings Instance
        {
            get
            {
                return Singleton<HostSettings>.Instance;
            }
        }

        /// <summary>
        /// Gets or sets the primary brush for the app.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [Description("The most popular color.")]
        [DisplayName("Primary Color")]
        public SolidColorBrush PrimaryColor
        {
            get
            {
                return GetValue<SolidColorBrush>("PrimaryColor", m_BrushConverter, Brushes.Black);
            }

            set
            {
                SetValue("PrimaryColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets the secondary brush for the app.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [Description("The second most popular color.")]
        [DisplayName("Secondary Color")]
        public SolidColorBrush SecondaryColor
        {
            get
            {
                return GetValue<SolidColorBrush>("SecondaryColor", m_BrushConverter, Brushes.White);
            }

            set
            {
                SetValue("SecondaryColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets the selection brush for the app.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [Description("The color to use for elements that are selected or highlighted.")]
        [DisplayName("Selection/Highlight Color")]
        public SolidColorBrush SelectionColor
        {
            get
            {
                return GetValue<SolidColorBrush>("SelectionColor", m_BrushConverter, Brushes.DodgerBlue);
            }

            set
            {
                SetValue("SelectionColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets the disabled brush for the app.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [TypeConverter(typeof(ExtendedBrushConverter))]
        [Description("The color to use for elements that are in the Disabled state.")]
        [DisplayName("Disabled Color")]
        public SolidColorBrush DisabledColor
        {
            get
            {
                return GetValue<SolidColorBrush>("DisabledColor", m_BrushConverter, Brushes.Gray);
            }

            set
            {
                SetValue("DisabledColor", value, m_BrushConverter);
            }
        }

        /// <summary>
        /// Gets or sets the default remote server ip.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [Description("The default server address to attempt to share a client with.")]
        [DisplayName("Default Remote Server Address")]
        public string DefaultRemoteServerIp
        {
            get
            {
                return GetValue<string>("DefaultRemoteServerIp", "localhost");
            }

            set
            {
                SetValue("DefaultRemoteServerIp", value);
            }
        }

        /// <summary>
        /// Gets or sets the default remote server name.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [Description("The default server name to attempt to share a client with.")]
        [DisplayName("Default Remote Server Name")]
        public string DefaultRemoteServerName
        {
            get
            {
                return GetValue<string>("DefaultRemoteServerName", "remote");
            }

            set
            {
                SetValue("DefaultRemoteServerName", value);
            }
        }

        /// <summary>
        /// Gets or sets the default remote server port.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [Description("The default port to use when attempting to share a client connection with another server.")]
        [DisplayName("Default Remote Server Port")]
        public int DefaultRemoteServerPort
        {
            get
            {
                return GetValue<int>("DefaultRemoteServerPort", 6969);
            }

            set
            {
                SetValue("DefaultRemoteServerPort", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not native connections should be detected.
        /// </summary>
        [ConfigurationEditorBrowsable(true)]
        [DisplayName("Allow Native Connections")]
        [Description("Controls whether or not to display connections that are not deemed to be managed processes.")]
        [Browsable(true)]
        public bool AllowNativeConnections
        {
            get
            {
                return GetValue<bool>("AllowNativeConnections", false);
            }

            set
            {
                SetValue("AllowNativeConnections", value);
            }
        }

        /// <summary>
        /// Gets or sets the default extensions to load.
        /// </summary>
        [Browsable(false)]
        [ConfigurationEditorBrowsable(false)]
        public string[] Extensions
        {
            get
            {
                return GetValues<string>("Extensions", new string[0]);
            }

            set
            {
                SetValue<string>("Extensions", value);
            }
        }

        /// <summary>
        /// Gets or sets the default processes to automatically connect to.
        /// </summary>
        [Browsable(false)]
        [ConfigurationEditorBrowsable(false)]
        public string[] AutoConnectProcesses
        {
            get
            {
                return GetValues<string>("AutoConnectProcesses", new string[0]);
            }

            set
            {
                SetValue<string>("AutoConnectProcesses", value);
            }
        }

        /// <summary>
        /// Gets or sets the connected processes as a single block of text.
        /// </summary>
        [Browsable(true)]
        [ConfigurationEditorBrowsable(true)]
        [DisplayName("Auto Connect Processes")]
        [Description("Defines the names of processes which should automatically be connected to. Place each entry on its own line.")]
        public string BindableConnectedProcesses
        {
            get
            {
                return string.Join(Environment.NewLine, AutoConnectProcesses);
            }

            set
            {
                AutoConnectProcesses = value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the user accepted the end user liscence agreement.
        /// </summary>
        [Browsable(false)]
        [ConfigurationEditorBrowsable(false)]
        public bool AcceptedEULA
        {
            get
            {
                return GetValue<bool>("AcceptedEULA", false);
            }

            set
            {
                SetValue("AcceptedEULA", value);
            }
        }
        #endregion
    }
}
