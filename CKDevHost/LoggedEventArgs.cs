﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKDevHost
{
    public class LoggedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the LoggedEventArgs class.
        /// </summary>
        /// <param name="logMessage">The message that was logged.</param>
        public LoggedEventArgs(string logMessage)
        {
            LogMessage = logMessage;
            LogTime = DateTime.Now;
        }

        /// <summary>
        /// Gets the message that was logged.
        /// </summary>
        public string LogMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the time at which the message was logged.
        /// </summary>
        public DateTime LogTime
        {
            get;
            private set;
        }
    }
}
