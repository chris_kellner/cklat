﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using CKLib.Utilities.Diagnostics;

namespace CKDevHost
{
    public class Injector
    {
        #region Constants
        /// <summary>
        /// Name of the 32-bit bootstrapper dll.
        /// </summary>
        private const string BOOTSTRAPPER_X86 = "DevClientBootstrapper_x86.dll";

        /// <summary>
        /// Name of the 64-bit bootstrapper dll.
        /// </summary>
        private const string BOOTSTRAPPER_X64 = "DevClientBootstrapper_x64.dll";
        #endregion

        #region Variables
        /// <summary>
        /// Reference to the host view model.
        /// </summary>
        HostViewModel m_ViewModel;
        #endregion
        /// <summary>
        /// Initializes a new instance of the Injector class.
        /// </summary>
        /// <param name="view">The host view model.</param>
        public Injector(HostViewModel view)
        {
            m_ViewModel = view;
        }

        /// <summary>
        /// Performs the injection asynchronously.
        /// </summary>
        /// <param name="dp"></param>
        public void DoInjectAsync(DetectedProcess dp)
        {
            m_ViewModel.StartTask(
                "Connecting to process...",
                () => DoInject(dp),
                null,
                null);
        }

        /// <summary>
        /// Gets the name of the bootstrapper to use for the input process.
        /// </summary>
        /// <param name="dp">The process to check.</param>
        /// <returns></returns>
        public static string GetBootstrapperName(DetectedProcess dp)
        {
            return dp.Is64BitProcess ? BOOTSTRAPPER_X64 : BOOTSTRAPPER_X86;
        }

        /// <summary>
        /// Injects the bootstrapper and calls Bootstrap2()
        /// </summary>
        /// <param name="dp"></param>
        private void DoInject(DetectedProcess dp)
        {
            if (dp != null)
            {
                if (Native.UnsafeNativeMethods.EnableDebugPriv())
                {
                    string bootstrapper = GetBootstrapperName(dp);
                    string filePath = Path.Combine(m_ViewModel.FolderPath, bootstrapper);

                    if (File.Exists(filePath))
                    {
                        if (!dp.ProcessContainsModule(bootstrapper))
                        {
                            Native.UnsafeNativeMethods.InvokeRemoteMethod(dp, "kernel32.dll", "LoadLibraryA", filePath);
                        }

                        if (!dp.ProcessContainsModule(bootstrapper))
                        {
                            string error = Marshal.GetLastWin32Error().ToString();
                            if (!string.IsNullOrEmpty(error))
                            {
                                Debug.WriteEntry(string.Format("Encountered Win32 Error {0} while attempting to load the bootstrapper.", error));
                            }
                        }
                        else
                        {
                            Standard.WriteEntry(string.Format("Bootstrapper loaded into process: {0}", dp.ToString()));

                            Native.UnsafeNativeMethods.InvokeRemoteMethod(dp, bootstrapper, "Bootstrap2", null);
                        }
                    }
                    else
                    {
                        Error.WriteEntry("Bootstrapper not found at: " + filePath);
                    }
                }
            }
        }
    }
}
