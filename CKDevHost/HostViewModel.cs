﻿

namespace CKDevHost
{
    #region Using List
    using CKDevHost.Configuration;
    using CKDevHost.Properties;
    using CKLib.Utilities.Connection.Server;
    using CKLib.Utilities.DataStructures;
    using CKLib.Utilities.Diagnostics;
    using CKLib.Utilities.Messages;
    using CKLib.Utilities.Patterns;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    #endregion

    public class HostViewModel : INotifyPropertyChanged
    {
        #region Variables
        /// <summary>
        /// Reference to the main server.
        /// </summary>
        private Server m_DeveloperServer;

        /// <summary>
        /// Folder path where this assembly resides.
        /// </summary>
        private string m_Path;

        /// <summary>
        /// The name of the currently executing task.
        /// </summary>
        private string m_CurrentTaskName = "Waiting";

        /// <summary>
        /// The dispather for this instance.
        /// </summary>
        private Dispatcher m_Dispatcher;

        /// <summary>
        /// The set of processes that have been detected this pass.
        /// </summary>
        private DetectedProcessCollection m_Detections;

        /// <summary>
        /// The set of clients currently connected to the server.
        /// </summary>
        private SynchronizedObservableDictionary<Guid, ClientProxy> m_ConnectedClients;

        /// <summary>
        /// The currently selected client.
        /// </summary>
        private ClientProxy m_SelectedClient;

        /// <summary>
        /// Queue used to hold onto log messages until they can be unloaded.
        /// </summary>
        SynchronizedQueue<LoggedEventArgs> m_Backlog = new SynchronizedQueue<LoggedEventArgs>();

        /// <summary>
        /// The currently selected detected process.
        /// </summary>
        private DetectedProcess m_SelectedDetectedProcess;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the HostViewModel class.
        /// </summary>
        /// <param name="dispatcher">The dispatcher for this instance.</param>
        public HostViewModel(Dispatcher dispatcher)
        {
            m_DeveloperServer = Server.Instance[StringTable.MAIN_SERVER_NAME];
            m_Dispatcher = dispatcher;
            m_Detections = new DetectedProcessCollection(this, m_Dispatcher);
            m_ConnectedClients = new SynchronizedObservableDictionary<Guid, ClientProxy>(dispatcher);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Initializes resources needed by the view model.
        /// </summary>
        public void Initialize()
        {
            Standard.OnEntry += Standard_OnEntry;
            Error.OnEntry += Standard_OnEntry;
            string currentPath = Assembly.GetExecutingAssembly().Location;
            m_Path = Path.GetDirectoryName(currentPath);
            m_DeveloperServer = Server.Instance[StringTable.MAIN_SERVER_NAME];
            m_DeveloperServer.ClientConnected += new EventHandler<ClientEventArgs>(Instance_ClientConnected);
            m_DeveloperServer.ClientDisconnected += new EventHandler<ClientEventArgs>(Instance_ClientDisconnected);
            m_DeveloperServer.ServerStarted += new EventHandler(m_DeveloperServer_ServerStarted);
            m_DeveloperServer.ServerStopped += new EventHandler(m_DeveloperServer_ServerStopped);
            m_DeveloperServer.AddCallback(typeof(StringMessage), MessageReceived, true);
            ConfigureExtensions();

            m_Detections.StartScanner();
            ConfigureResources();
            HostSettings.Instance.PropertyChanged += new PropertyChangedEventHandler(Instance_PropertyChanged);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            ConfigureForDebug();
        }

        [System.Diagnostics.Conditional("DEBUG")]
        private void ConfigureForDebug()
        {
            Debug.OnEntry += Standard_OnEntry;
            AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
        }

        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            Error.WriteEntry("First chance exception: ", e.Exception);
        }

        /// <summary>
        /// Called when an unhandled exception occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Fatal.WriteEntry("An Unhandled Exception has Occurred.", e.ExceptionObject as Exception);
        }

        /// <summary>
        /// Called when settings have changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PrimaryColor":
                case "SecondaryColor":
                case "SelectionColor":
                case "DisabledColor":
                    ConfigureResources();
                    break;
            }
        }

        /// <summary>
        /// Loads the extensions defined in the CKLibSettings file.
        /// </summary>
        private void ConfigureExtensions()
        {
            var currentPath = m_Path;
            currentPath = Path.Combine(currentPath, @"Plugins\");
            string[] pluginNames = HostSettings.Instance.Extensions;
            if (pluginNames != null)
            {
                var plugins = pluginNames.Cast<string>().Select((s) => Path.Combine(currentPath, s)).ToArray();
                m_DeveloperServer.ExtensionManager.Initialize(
                    m_DeveloperServer,
                    plugins);
            }
        }

        /// <summary>
        /// Reads any configured resources out of the app.config
        /// </summary>
        public void ConfigureResources()
        {
            var primary = HostSettings.Instance.PrimaryColor;
            var secondary = HostSettings.Instance.SecondaryColor;
            var selection = HostSettings.Instance.SelectionColor;
            var disabled = HostSettings.Instance.DisabledColor;
            if (primary != null)
            {
                App.Current.Resources["primaryColor"] = primary;
            }

            if (secondary != null)
            {
                App.Current.Resources["secondaryColor"] = secondary;
            }

            if (selection != null) 
            {
                App.Current.Resources["selectionColor"] = selection;
            }

            if (disabled != null)
            {
                App.Current.Resources["disabledColor"] = disabled;
            }
        }

        /// <summary>
        /// Releases all resources held by the view model.
        /// </summary>
        public void Release()
        {
            m_Detections.Dispose();
            m_DeveloperServer.StopServer();
            m_DeveloperServer.RemoveCallback(typeof(StringMessage), MessageReceived);
            m_DeveloperServer.ClientConnected -= new EventHandler<ClientEventArgs>(Instance_ClientConnected);
            m_DeveloperServer.ClientDisconnected -= new EventHandler<ClientEventArgs>(Instance_ClientDisconnected);
            Singleton.ReleaseAllSingletons();
        }

        /// <summary>
        /// Logs a message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        public void LogMessage(string message)
        {
            OnLogged(new LoggedEventArgs(message));
        }

        /// <summary>
        /// Called when a message is received on the server.
        /// </summary>
        /// <param name="pair"></param>
        private void MessageReceived(MessageEnvelope pair)
        {
            var theMessage = pair.Message;
            if (theMessage is StringMessage)
            {
                var info = m_DeveloperServer.GetConnectionInformation(theMessage.SenderId);
                if (info != null)
                {
                    Standard.WriteEntry(string.Format("{0} -> {1}", info, ((StringMessage)theMessage).Message));
                }
            }
        }

        /// <summary>
        /// Called when a property changed.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// occurs when something is written to the standard stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Standard_OnEntry(object sender, OutputEventArgs e)
        {
            LogMessage(e.ToString());
        }

        /// <summary>
        /// Occurs when something gets logged.
        /// </summary>
        /// <param name="e"></param>
        private void OnLogged(LoggedEventArgs e)
        {
            if (Logged != null)
            {
                Logged(this, e);
            }
            else
            {
                m_Backlog.Enqueue(e);
            }
        }

        /// <summary>
        /// Attempts to process all messages in the log queue.
        /// </summary>
        public void ProcessBacklog()
        {
            while (m_Backlog.Count > 0)
            {
                OnLogged(m_Backlog.Dequeue());
            }
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the settings object.
        /// </summary>
        public HostSettings Settings
        {
            get
            {
                return HostSettings.Instance;
            }
        }

        public PropertyEnumerator Status
        {
            get
            {
                return new PropertyEnumerator(this);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the server is running or not.
        /// </summary>
        public bool ServerRunning
        {
            get
            {
                return m_DeveloperServer.IsRunning;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the server is running or not.
        /// </summary>
        public bool ServerStopped
        {
            get
            {
                return !m_DeveloperServer.IsRunning;
            }
        }

        /// <summary>
        /// Gets the formatted title of the main window.
        /// </summary>
        public string FormattedTitleBar
        {
            get
            {
                string serverStatus = "Not Started";
                if (ServerRunning)
                {
                    string host = Dns.GetHostName();
                    IPHostEntry ip = Dns.GetHostEntry(host);
                    string ipAddress = "<unknown>";
                    if (ip.AddressList != null && ip.AddressList.Length > 0)
                    {
                        var address = ip.AddressList.FirstOrDefault(i => i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                        if (address != null)
                        {
                            ipAddress = address.ToString();
                        }
                    }
                    serverStatus = string.Format("Running on {0} [{1}]", host, ipAddress);
                }

                return string.Format("CK Live Analysis Toolkit - Server Status: {0}", serverStatus);
            }
        }

        /// <summary>
        /// Gets the dictionary of connected clients.
        /// </summary>
        public IDictionary<Guid, ClientProxy> ConnectedClients
        {
            get
            {
                return m_ConnectedClients;
            }
        }

        /// <summary>
        /// Gets the developer server.
        /// </summary>
        public Server DeveloperServer
        {
            get
            {
                return m_DeveloperServer;
            }
        }

        /// <summary>
        /// Gets the collection of processes for the last scan.
        /// </summary>
        public DetectedProcessCollection DetectedProcesses
        {
            get
            {
                return m_Detections;
            }
        }

        /// <summary>
        /// Gets the path to the folder containing the running assembly.
        /// </summary>
        public string FolderPath
        {
            get
            {
                return m_Path;
            }
        }

        /// <summary>
        /// Gets or sets the currently SelectedClient.
        /// </summary>
        public ClientProxy SelectedClient
        {
            get
            {
                return m_SelectedClient;
            }

            set
            {
                m_SelectedClient = value;
                OnPropertyChanged("SelectedClient");
                OnPropertyChanged("HasSelection");
                OnPropertyChanged("SelectedConnectedProcess");
            }
        }

        /// <summary>
        /// Gets the currently selected (connected) process.
        /// </summary>
        public DetectedProcess SelectedConnectedProcess
        {
            get
            {
                if (SelectedClient != null)
                {
                    var info = SelectedClient.Information;
                    if (info != null && info.ConnectionAddress.IsLoopback)
                    {
                        return new DetectedProcess(info.ProcessId);
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the currently selected (detected) process.
        /// </summary>
        public DetectedProcess SelectedDetectedProcess
        {
            get
            {
                return m_SelectedDetectedProcess;
            }

            set
            {
                m_SelectedDetectedProcess = value;
                OnPropertyChanged("SelectedDetectedProcess");
            }
        }

        /// <summary>
        /// Gets a value indicating whether a client has been selected.
        /// </summary>
        public bool HasSelection
        {
            get
            {
                return SelectedClient != null;
            }
        }

        /// <summary>
        /// Gets or sets the display name for the currently executing task.
        /// </summary>
        public string CurrentTaskName
        {
            get
            {
                return m_CurrentTaskName;
            }

            set
            {
                if (!m_Dispatcher.CheckAccess())
                {
                    m_Dispatcher.Invoke(new Action(() => CurrentTaskName = value));
                }
                else
                {
                    m_CurrentTaskName = value;
                    OnPropertyChanged("CurrentTaskName");
                }
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Starts a background task with the input name.
        /// </summary>
        /// <param name="taskName">The display name for the task.</param>
        /// <param name="task">The task to perform.</param>
        /// <param name="onComplete">A method to call when the task is complete.</param>
        /// <param name="dispatch">If specified, the onContinue method will be invoked on the specified dispatcher.</param>
        public void StartTask(string taskName, Action task, Action onComplete, Dispatcher dispatch)
        {
            CurrentTaskName = taskName;
            Task t = Task.Factory.StartNew(task);
            t.ContinueWith(new Action<Task>((k) =>
            {
                if (onComplete != null)
                {
                    if (dispatch != null)
                    {
                        dispatch.Invoke(onComplete);
                    }
                    else
                    {
                        onComplete();
                    }
                }
                CurrentTaskName = "Waiting";
            }));
        }
        #endregion
        #region Events
        /// <summary>
        /// Occurs when a property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs when an event is logged.
        /// </summary>
        public event EventHandler<LoggedEventArgs> Logged;
        #endregion
        #region Event Handlers
        /// <summary>
        /// Occurs when the server is stopped.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_DeveloperServer_ServerStopped(object sender, EventArgs e)
        {
            m_ConnectedClients.Clear();
            OnPropertyChanged("ServerRunning");
            OnPropertyChanged("ServerStopped");
            OnPropertyChanged("FormattedTitleBar");
        }

        /// <summary>
        /// Occurs when the server is started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_DeveloperServer_ServerStarted(object sender, EventArgs e)
        {
            OnPropertyChanged("ServerRunning");
            OnPropertyChanged("ServerStopped");
            OnPropertyChanged("FormattedTitleBar");
        }

        /// <summary>
        /// Occurs when a client disconnects from the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_ClientDisconnected(object sender, ClientEventArgs e)
        {
            m_ConnectedClients.Remove(e.Client.Identifier);
            //LogMessage(string.Format("Client {0} has Disconnected.", e.Client.Information));
        }

        /// <summary>
        /// Occurs when a client connects to the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_ClientConnected(object sender, ClientEventArgs e)
        {
            m_ConnectedClients.Add(e.Client.Identifier, e.Client);
            //LogMessage(string.Format("Client {0} has Connected.", e.Client.Information));
        }
        #endregion
    }
}
