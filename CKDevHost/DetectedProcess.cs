﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using CKDevHost.Native;

namespace CKDevHost
{
    public class DetectedProcess
    {
        private static HashSet<string> S_DoNotPokeList = new HashSet<string>();
        private static HashSet<int> S_Known_Managed_Processes = new HashSet<int>();

        private string m_ProcessName;

        private bool m_IsManaged;

        private bool? m_Is64Bit;

        public DetectedProcess(int id)
        {
            Id = id;
        }

        [Browsable(true)]
        public Process Process
        {
            get
            {
                try
                {
                    return Process.GetProcessById(Id);
                }
                catch
                {
                    return null;
                }
            }
        }

        public string ProcessName
        {
            get
            {
                if (string.IsNullOrEmpty(m_ProcessName))
                {
                    var proc = Process;
                    if (proc != null)
                    {
                        m_ProcessName = proc.ProcessName;
                    }
                    else
                    {
                        m_ProcessName = "[Dead Process]";
                    }
                }

                return m_ProcessName;
            }
        }

        [Browsable(false)]
        public IEnumerable<string> ProcessTypes
        {
            get
            {
                if (Process != null && IsLoaded)
                {
                    var pId = Kernel32.OpenProcess(UnsafeNativeMethods.PROCESS_ALL_ACCESS, 0, Process.Id);
                    try
                    {
                        if (StaticHelpers.Is64BitProcess(pId))
                        {
                            yield return "x64";
                        }
                        else
                        {
                            yield return "x86";
                        }
                    }
                    finally
                    {
                        Kernel32.CloseHandle(pId);
                    }


                    foreach (var module in Modules)
                    {
                        var subModName = Path.GetFileName(module.Key).ToLowerInvariant();
                        switch (subModName)
                        {
                            case "mscorwks.dll":
                                m_IsManaged = true;
                                yield return ".Net 2";// + module.FileVersionInfo.ProductMajorPart;
                                break;
                            case "clr.dll":
                                m_IsManaged = true;
                                yield return ".Net 4";//+ module.FileVersionInfo.ProductMajorPart;
                                break;
                            case "jvm.dll":
                                yield return "Java";// + module.FileVersionInfo.ProductMajorPart;
                                break;
                        }
                    }
                }
            }
        }

        public bool IsDotNet2_0
        {
            get
            {
                return m_IsManaged || ProcessTypes.Contains(".Net 2");
            }
        }

        public bool IsDotNet4_0
        {
            get
            {
                return m_IsManaged || ProcessTypes.Contains(".Net 4");
            }
        }

        public bool IsManagedProcess
        {
            get
            {
                return IsDotNet2_0 || IsDotNet4_0;
            }
        }

        public int Id
        {
            get;
            set;
        }

        public bool Is64BitProcess
        {
            get
            {
                if (m_Is64Bit == null)
                {
                    var proc = Process;
                    if (proc != null)
                    {
                        m_Is64Bit = StaticHelpers.Is64BitProcess(proc.Handle);
                    }
                }

                if (m_Is64Bit.HasValue)
                {
                    return m_Is64Bit.Value;
                }

                return false;
            }
        }

        public string MainTitle
        {
            get
            {
                var process = Process;
                if (process != null)
                {
                    return process.MainWindowTitle;
                }

                return string.Empty;
            }
        }

        public string ExeFilePath
        {
            get
            {
                var proc = Process;
                if (proc != null)
                {
                    return Process.MainModule.FileName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Used to determine if the Process has been fully loaded yet.
        /// </summary>
        [Browsable(false)]
        public bool IsLoaded
        {
            get
            {
                if (Process.GetProcesses().FirstOrDefault(o => o.Id == Id) != null && !S_DoNotPokeList.Contains(ProcessName))
                {
                    try
                    {
                        IntPtr procHandle = Kernel32.OpenProcess(UnsafeNativeMethods.PROCESS_ALL_ACCESS, 0, Id);
                        try
                        {
                            // If we can get the name of the start module then we have access to the process
                                // otherwise this will avoid the AccessDeniedException context switch.
                            StringBuilder builder = new StringBuilder(2000);
                            uint retVal = Psapi.GetProcessImageFileName(procHandle, builder, 2000);
                            if (retVal < 1)
                            {
                                S_DoNotPokeList.Add(ProcessName);
                                return false;
                            }

                            Process p = Process;
                            if (!p.HasExited && p.StartTime < DateTime.Now - TimeSpan.FromSeconds(2))
                            {
                                return true;
                            }
                        }
                        finally
                        {
                            if (procHandle != IntPtr.Zero)
                            {
                                Kernel32.CloseHandle(procHandle);
                            }
                        }
                        
                    }
                    catch (Win32Exception)
                    {
                        S_DoNotPokeList.Add(ProcessName);
                    }
                }
                return false;
            }
        }

        [Browsable(false)]
        public DateTime StartTime
        {
            get
            {
                var proc = Process;
                if (proc != null)
                {
                    return proc.StartTime;
                }

                return DateTime.MinValue;
            }
        }

        public string WorkingDirectory
        {
            get
            {
                var proc = Process;
                if (proc != null)
                {
                    return proc.StartInfo.WorkingDirectory;
                }

                return null;
            }
        }

        public string CommandLineArguments
        {
            get
            {
                var proc = Process;
                if (proc != null)
                {
                    return proc.StartInfo.Arguments;
                }

                return null;
            }
        }

        [Browsable(false)]
        public ProcessStartInfo StartInfo
        {
            get
            {
                var proc = Process;
                if (proc != null)
                {
                    return proc.StartInfo;
                }

                return null;
            }
        }

        public IntPtr GetModuleBaseAddress(string dll)
        {
            if (IsLoaded)
            {
                var mods = Modules;
                foreach (var module in mods)
                {
                    var subModName = Path.GetFileName(module.Key);
                    if (subModName.Equals(dll, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return module.Value;
                    }
                }
            }
            return IntPtr.Zero;
        }

        /// <summary>
        /// Obtains a pointer to a function in the remote process. This will
        /// be the absolute address, which is the Module.BaseAddress + FunctionOffset!
        /// </summary>
        /// <param name="moduleName"></param>
        /// <param name="funcName"></param>
        /// <returns></returns>
        public UIntPtr GetRemoteProcAddress(string moduleName, string funcName)
        {
            if (IsLoaded)
            {
                foreach (var module in Modules)
                {
                    var subModName = Path.GetFileName(module.Key);
                    if (subModName.Equals(moduleName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        try
                        {
                            var funcs = PEHelper.GetFunctionOffsetAddresses(module.Key);
                            if (funcs != null)
                            {
                                UIntPtr offset;
                                if (funcs.TryGetValue(funcName, out offset))
                                {
                                    if (Is64BitProcess)
                                    {
                                        return new UIntPtr((ulong)module.Value.ToInt64() + (ulong)offset.ToUInt32());
                                    }
                                    else
                                    {
                                        return new UIntPtr((uint)module.Value.ToInt32() + offset.ToUInt32());
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                        break;
                    }
                }
            }
            return UIntPtr.Zero;
        }

        public bool ProcessContainsModule(string dll)
        {
            if (IsLoaded)
            {
                foreach (var module in Modules)
                {
                    var moduleName = Path.GetFileName(module.Key);
                    if (moduleName.Equals(dll, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Dictionary<string, IntPtr> Modules
        {
            get
            {
                IntPtr hProcess = Kernel32.OpenProcess(UnsafeNativeMethods.PROCESS_ALL_ACCESS, 0, Id);
                Dictionary<string, IntPtr> foundFunctions = null;
                try
                {
                    if (hProcess != IntPtr.Zero)
                    {
                        bool iswow64;
                        Kernel32.IsWow64Process(hProcess, out iswow64);
                        if (iswow64)
                        {
                            foundFunctions = UnsafeNativeMethods.GetWOW64ProcessModules(hProcess);
                        }
                    }
                }
                finally
                {
                    if (hProcess != IntPtr.Zero)
                    {
                        Kernel32.CloseHandle(hProcess);
                    }
                }

                if (foundFunctions == null)
                {
                    foundFunctions = (from mod in Process.Modules.Cast<ProcessModule>()
                                      select new KeyValuePair<string, IntPtr>(mod.FileName, mod.BaseAddress))
                            .ToDictionary(k => k.Key, v => v.Value);
                }

                return foundFunctions;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is DetectedProcess)
            {
                if (Id.Equals(((DetectedProcess)obj).Id))
                {
                    bool bLoaded = IsLoaded;
                    if (bLoaded == ((DetectedProcess)obj).IsLoaded)
                    {
                        if (bLoaded)
                        {
                            return Process.MainModule.BaseAddress == ((DetectedProcess)obj).Process.MainModule.BaseAddress;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            return base.Equals(obj);
        }

        [Browsable(false)]
        public IntPtr BaseAddress
        {
            get
            {
                bool bLoaded = IsLoaded;
                if (bLoaded)
                {
                    return Process.MainModule.BaseAddress;
                }

                return IntPtr.Zero;
            }
        }

        public override int GetHashCode()
        {
            int hc = Id.GetHashCode();
            return hc;
        }

        public override string ToString()
        {
            try
            {
                return string.Format("ID {0} | {1} | {2}", Id, ProcessName, Is64BitProcess ? "x64" : "x86");
            }
            catch
            {
                return "[Dead Process]";
            }
        }
    }
}
