﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using CKLib.Utilities.Connection.Server;
using CKLib.Utilities.Patterns;
using CKDevHost.Properties;
using CKLib.Utilities.DataStructures.Configuration;
using CKLib.Utilities.Messages;
using CKLib.Utilities.Connection.Client;
using CKDevHost.Configuration;
using CKDevHost.Gui;

namespace CKDevHost
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Server m_CommandLineServer;
        private static HostViewModel m_ViewModel;
        private string[] m_CommandLineArgs;
        private Window1 m_MainWindow;

        private const string COMMAND_SHARE = "/share";
        private const string COMMAND_EXTENSION = "/extension";
        private const string COMMAND_START_LOCAL = "/runlocal";
        private const string COMMAND_START_NETWORK = "/runnetwork";

        public static HostViewModel ViewModel
        {
            get
            {
                return m_ViewModel;
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            m_CommandLineServer.StopServer();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ConfigurationManager.ProductName = "CKDevHost";
            ConfigurationManager.ProductVersion = new Version(0, 3, 0, 0);
            ConfigurationManager.Reload(false);
            Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnMainWindowClose;

            IDataMessage.FindMessageTypes += IDataMessage_FindMessageTypes;
            // Convert the args to lower-case
            string[] commandLine = e.Args;
            commandLine = commandLine.Select(s => s.ToLower()).ToArray();
            m_CommandLineArgs = commandLine;

            if (SetUpCommandLineServer())
            {
                m_ViewModel = new HostViewModel(Dispatcher);
                m_ViewModel.Initialize();

                if (!HostSettings.Instance.AcceptedEULA)
                {
                    EULA eula = new EULA();
                    if (eula.ShowDialog() != true)
                    {
                        Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnLastWindowClose;
                        Environment.Exit(0);
                    }
                }

                m_MainWindow = new Window1();
                MainWindow = m_MainWindow;
                m_MainWindow.Show();

                ProcessCommandLineMessage(new CommandLineMessage()
                {
                    Args = m_CommandLineArgs
                });
            }
        }

        private bool SetUpCommandLineServer()
        {
            var serverGroup = ConfigurationManager.Current[SettingGroup.LOCAL_SERVER_SETTINGS_GROUP];
            string serverName = "Local";
            string overrideName;
            if (serverGroup.TryReadValue(Setting.SERVER_NAME_SETTING, out overrideName))
            {
                serverName = overrideName;
            }

            m_CommandLineServer = ServerManager.Instance[StringTable.COMMAND_LINE_SERVER];
            if (m_CommandLineServer != null)
            {
                string cmdLineServerName = string.Format("CKDevHost_{0}_CommandLine", serverName);
                m_CommandLineServer.RunServer(ServerAccessType.LocalOnly, cmdLineServerName);
                if (!m_CommandLineServer.IsRunning)
                {
                    // If we are sharing then there will be no gui, just pass the args on to the running gui
                    if (m_CommandLineArgs.Contains(COMMAND_SHARE))
                    {
                        Client.Instance.ConnectionOpened += Instance_ConnectionOpened;
                        var connection = Client.Instance.ConnectToServer("localhost", cmdLineServerName, 0, false, null, null);
                        return false;
                    }
                }
                else
                {
                    m_CommandLineServer.AddCallback(typeof(CommandLineMessage), OnCommandLineMessage);
                }
            }

            return true;
        }

        private void OnCommandLineMessage(MessageEnvelope pair)
        {
            var message = pair.Message as CommandLineMessage;
            ProcessCommandLineMessage(message);
        }

        private void ProcessCommandLineMessage(CommandLineMessage message)
        {
            if (message != null)
            {
                ProcessCommandLineArgs(message.Args);
                m_ViewModel.DeveloperServer.SendLocalMessage(message);
            }
        }

        void Instance_ConnectionOpened(object sender, ClientConnectionEventArgs e)
        {
            Client.Instance.ConnectionOpened -= Instance_ConnectionOpened;
            Client.Instance.SendNetworkMessage(new CommandLineMessage()
            {
                Args = m_CommandLineArgs
            });

            e.Connection.Disconnect();
            Singleton.ReleaseAllSingletons();
            Dispatcher.InvokeShutdown();
        }

        void IDataMessage_FindMessageTypes(object sender, TypeDefinitionEventArgs e)
        {
            e.AddType(typeof(CommandLineMessage));
        }

        private void ProcessCommandLineArgs(string[] args)
        {
            if (args != null)
            {
                List<string> list = args.ToList();
                int idx = -1;
                while ((idx = list.IndexOf(COMMAND_EXTENSION, idx+1)) > -1)
                {
                    int next = idx + 1;
                    if (next < list.Count)
                    {
                        string extensionName = list[next];
                        if (!string.IsNullOrEmpty(extensionName))
                        {
                            var extension = m_ViewModel.DeveloperServer.ExtensionManager.GetLocalExtensions().
                                FirstOrDefault(p => extensionName.Equals(p.Key, StringComparison.InvariantCultureIgnoreCase)).Value;
                            if (extension != null)
                            {
                                if (m_MainWindow != null)
                                {
                                    m_MainWindow.Dispatcher.Invoke(
                                        new Action<string>(m_MainWindow.BringExtensionIntoView), extension.ExtensionName);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
