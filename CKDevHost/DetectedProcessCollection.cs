﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using CKLib.Utilities.DataStructures;
using System.Collections.Specialized;
using System.Windows.Threading;
using CKDevHost.Configuration;

namespace CKDevHost
{
    public class DetectedProcessCollection : ICollection<DetectedProcess>, IEnumerable<DetectedProcess>, IEnumerable, IDisposable, ILockable, INotifyCollectionChanged
    {
        private volatile HashSet<DetectedProcess> m_InternalStore;

        private HashSet<DetectedProcess> m_BannedProcesses;
        private readonly object m_Locker;
        private System.Timers.Timer m_Timer;

        /// <summary>
        /// The view model for this instance.
        /// </summary>
        private HostViewModel m_ViewModel;

        /// <summary>
        /// Class used to perform the dll injection.
        /// </summary>
        private Injector m_Injector;

        /// <summary>
        /// Disposable notifier used to be alerted when the native connection flag changes.
        /// </summary>
        private IDisposable m_PropertyNotify;

        public DetectedProcessCollection(HostViewModel view, Dispatcher wpfDispatcher)
        {
            m_Locker = new object();
            m_ViewModel = view;
            m_Injector = new Injector(m_ViewModel);
            m_InternalStore = new HashSet<DetectedProcess>();
            m_BannedProcesses = new HashSet<DetectedProcess>();
            m_Timer = new System.Timers.Timer(3000);
            m_Timer.Elapsed += m_Timer_Elapsed;
            m_PropertyNotify = HostSettings.Instance.GetWeakNotifier("AllowNativeConnections", (s, e) =>
            {
                m_InternalStore.Clear();
                m_BannedProcesses.Clear();
            });
            Dispatcher = wpfDispatcher;
        }

        public void StartScanner()
        {
            m_Timer.AutoReset = false;
            m_Timer.Disposed += new EventHandler(m_Timer_Disposed);
            m_Timer.Start();
        }

        void m_Timer_Disposed(object sender, EventArgs e)
        {
            m_TimerDisposed = true;
        }

        IEnumerable<DetectedProcess> SanitizeList(Process[] processes)
        {
            var dps = (from proc in processes
                       let dp = new DetectedProcess(proc.Id)
                       where !m_BannedProcesses.Contains(dp)
                       where dp.IsLoaded
                       select dp);
            return dps;
        }

        private void ProcessAutos()
        {
            if (m_ViewModel.ServerRunning)
            {
                string[] autoProcesses = HostSettings.Instance.AutoConnectProcesses;
                if (autoProcesses != null)
                {
                    for (int i = 0; i < autoProcesses.Length; i++)
                    {
                        var procs = Process.GetProcessesByName(autoProcesses[i]);
                        foreach (var dp in SanitizeList(procs))
                        {
                            if (!dp.ProcessContainsModule(Injector.GetBootstrapperName(dp)))
                            {
                                m_Injector.DoInjectAsync(dp);
                            }
                        }
                    }
                }
            }
        }

        public void ScanForProcesses()
        {
            HashSet<DetectedProcess> newSet = new HashSet<DetectedProcess>();

            Process[] processes = Process.GetProcesses();
            var dps = SanitizeList(processes);

            foreach (var dp in dps)
            {
                if (m_InternalStore.Contains(dp) || 
                    dp.IsManagedProcess ||
                    HostSettings.Instance.AllowNativeConnections)
                {
                    if (dp.IsLoaded)
                    {
                        newSet.Add(dp);
                    }
                }
                else
                {
                    m_BannedProcesses.Add(dp);
                }
            }

            bool newAdded = false;
            bool oldRemoved = false;
            Lock(); try
            {
                int count = m_InternalStore.Count;
                //m_InternalStore.IntersectWith(newSet);
                m_InternalStore.RemoveWhere(p => !newSet.Contains(p));

                if (count != m_InternalStore.Count)
                {
                    oldRemoved = true;
                    count = m_InternalStore.Count;
                }

                m_InternalStore.UnionWith(newSet);
                if (count != m_InternalStore.Count)
                {
                    newAdded = true;
                }
            }
            finally
            {
                Unlock();
            }

            if (newAdded || oldRemoved)
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        void m_Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                // Try searching for the auto processes first:
                ProcessAutos();
            }
            catch
            {

            }

            if (!m_TimerDisposed)
            {
                try
                {
                    m_Timer.Start();
                }
                catch (ObjectDisposedException)
                {
                }
            }
        }

        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Action<NotifyCollectionChangedEventArgs> cb = new Action<NotifyCollectionChangedEventArgs>(OnCollectionChanged);
                Dispatcher.Invoke(cb, e);
            }
            else
            {
                if (CollectionChanged != null)
                {
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }
            }
        }

        #region ICollection<DetectedProcess> Members

        void ICollection<DetectedProcess>.Add(DetectedProcess item)
        {
            throw new NotSupportedException();
        }

        void ICollection<DetectedProcess>.Clear()
        {
            throw new NotSupportedException();
        }

        public bool Contains(DetectedProcess item)
        {
            Lock(); try
            {
                return m_InternalStore.Contains(item);
            }
            finally
            {
                Unlock();
            }
        }

        public void CopyTo(DetectedProcess[] array, int arrayIndex)
        {
            Lock(); try
            {
                m_InternalStore.CopyTo(array, arrayIndex);
            }
            finally
            {
                Unlock();
            }
        }

        public int Count
        {
            get { return m_InternalStore.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        bool ICollection<DetectedProcess>.Remove(DetectedProcess item)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region IEnumerable<DetectedProcess> Members

        public IEnumerator<DetectedProcess> GetEnumerator()
        {
            return Processes.GetEnumerator();
        }

        public IEnumerable<DetectedProcess> Processes
        {
            get
            {
                DetectedProcess[] procs = new DetectedProcess[Count];
                CopyTo(procs, 0);
                for (int i = 0; i < procs.Length; i++)
                {
                    yield return procs[i];
                }
            }
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IDisposable Members
        private volatile bool m_TimerDisposed;

        public void Dispose()
        {
            if (m_Timer != null)
            {
                m_Timer.Dispose();
            }

            CollectionChanged = null;
            m_InternalStore.Clear();
        }

        #endregion

        #region ILockable Members

        public void Lock()
        {
            Monitor.Enter(m_Locker);
        }

        public void Unlock()
        {
            Monitor.Exit(m_Locker);
        }

        public bool IsSynchronized
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the LockId of the object.
        /// </summary>
        /// <remarks>This property is reserved for future use and should only be implemented as a default property.</remarks>
        public int LockId { get; set; }
        #endregion

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public Dispatcher Dispatcher
        {
            get;
            private set;
        }
    }
}
